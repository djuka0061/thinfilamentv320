import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { getStyle, hexToRgba } from '@coreui/coreui/dist/js/coreui-utilities';
import { CustomTooltips } from '@coreui/coreui-plugin-chartjs-custom-tooltips';
import { FormGroup,  FormBuilder,  Validators, FormControl} from '@angular/forms';
import { ProbabilisticService } from '../../service/probabilistic.service'
import { MatPaginator, MatTableDataSource } from '@angular/material';

export interface PeriodicElement {
  col1: string;
  col2: string;
  col3: string;
  col4: string;
  col5: string;
  col6: string;
  col7: string;
  col8: string;
}

@Component({
  templateUrl: 'atphase.component.html',
  styleUrls: ['./atphase.component.scss'],
})
export class ATPhaseComponent implements OnInit {
  angForm: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    kb_plus: new FormControl(''),
    kb_minus: new FormControl(''),
    kt_plus: new FormControl(''),
    kt_minus: new FormControl(''),
    k1_plus: new FormControl(''),
    k1_minus: new FormControl(''),
    k2_plus: new FormControl(''),
    k2_minus: new FormControl(''),
    molar_concentration_of_actin: new FormControl(''),
    molar_concentration_of_myosin: new FormControl(''),
    type_of_simulation: new FormControl('')
  });

  //displayedColumns: string[] = ['col1', 'col2', 'col3', 'col4', 'col5', 'col6', 'col7', 'col8'];
   constructor(private fb: FormBuilder, private ProbabilisticService: ProbabilisticService) {  }


  public porbabilistic: any;
  public dataSource;
  

  // mainChart
  public mainChartElements = 27;
  public mainChartData1: Array<number> = [];
  public mainChartData2: Array<number> = [];
  public mainChartData3: Array<number> = [];
  public mainChartData4: Array<number> = [];

  public mainChartData: Array<any> = [
    {
      data: this.mainChartData1,
      label: 'Current'
    },
    {
      data: this.mainChartData2,
      label: 'Previous'
    },
    {
      data: this.mainChartData3,
      label: 'BEP'
    },
    {
      data: this.mainChartData4,
      label: 'BEP'
    }

  ];
  /* tslint:disable:max-line-length */
  public mainChartLabels: Array<any> = [];
  /* tslint:enable:max-line-length */
  public mainChartOptions: any = {
    tooltips: {
      enabled: false,
      custom: CustomTooltips,
      intersect: true,
      mode: 'index',
      position: 'nearest',
      callbacks: {
        labelColor: function(tooltipItem, chart) {
          return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
        }
      }
    },
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [{
        gridLines: {
          drawOnChartArea: false,
        },
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 10,
          stepSize: 0.1,
          max: 1
        }
      }],
      yAxes: [{
        ticks: {
          beginAtZero: true,
          maxTicksLimit: 10,
          stepSize: 0.1,
          max: 1
        }
      }]
    },
    elements: {
      line: {
        borderWidth: 2
      },
      point: {
        radius: 0,
        hitRadius: 10,
        hoverRadius: 4,
        hoverBorderWidth: 3,
      }
    },
    legend: {
      display: false
    }
  };
  public mainChartColours: Array<any> = [
    { // brandInfo
      backgroundColor: hexToRgba(getStyle('--info'), 10),
      borderColor: getStyle('--info'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandSuccess
      backgroundColor: 'transparent',
      borderColor: getStyle('--success'),
      pointHoverBackgroundColor: '#fff'
    },
    { // brandDanger
      backgroundColor: 'transparent',
      borderColor: getStyle('--danger'),
      pointHoverBackgroundColor: '#fff',
      borderWidth: 1,
      borderDash: [8, 5]
    }
  ];
  public mainChartLegend = false;
  public mainChartType = 'line';

  public random(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  ngOnInit() {

  }
  onSubmit() {
    console.log(this.profileForm.value)
    const body = 'userID=test21&s='+ this.profileForm.value.s +'&kb_plus='+ this.profileForm.value.kb_plus +'&kb_minus='+ this.profileForm.value.kb_minus +'&kt_plus='+ this.profileForm.value.kt_plus +'&kt_minus='+ this.profileForm.value.kt_minus + '&k1_plus='+ this.profileForm.value.k1_plus +'&k1_minus='+ this.profileForm.value.k1_minus +'&k2_plus='+ this.profileForm.value.k2_plus +'&k2_minus='+ this.profileForm.value.k2_minus +'&molar_concentration_of_actin='+ this.profileForm.value.molar_concentration_of_actin +'&molar_concentration_of_myosin='+ this.profileForm.value.molar_concentration_of_myosin +'&type_of_simulation=test';
    this.ProbabilisticService.getData(body).subscribe(data => {
      this.porbabilistic = data;
      console.log(data);
      const time = [];
      const fractional_fluorescence = [];
      const fraction_saturation = [];
      const fraction_of_unbound_actin = [];
      const free_myosin_concentration = [];
      this.porbabilistic.simulation.forEach(element => {
        time.push(element.time);
        fractional_fluorescence.push(element.fractional_fluorescence);
        fraction_saturation.push(element.fraction_saturation);
        fraction_of_unbound_actin.push(element.fraction_of_unbound_actin);
        free_myosin_concentration.push(element.free_myosin_concentration);
      });

      //this.lineChart1Data = [
      //  {data: fractional_fluorescence, label: 'Series A'},
      //  {data: fraction_saturation, label: 'Series b'},
      //  {data: fraction_of_unbound_actin, label: 'Series c'},
      //  {data: free_myosin_concentration, label: 'Series d'}
      //];
      //console.log(this.lineChart1Data)
      //this.lineChart1Labels = time;

        this.mainChartData1 = fractional_fluorescence;
        this.mainChartData2 = fraction_saturation;
        this.mainChartLabels = time;
        this.mainChartData3 = fraction_of_unbound_actin;
        this.mainChartData4 = free_myosin_concentration

        
        this.dataSource = new MatTableDataSource<PeriodicElement>(this.porbabilistic.histogram);
    })
  }
}
