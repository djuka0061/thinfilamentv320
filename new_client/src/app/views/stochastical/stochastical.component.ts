import { Component, OnInit, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { StochasticalService } from '../../service/stochastical.service'
import { environment } from '../../../environments/environment.prod';
import { Chart } from 'chart.js'

const host = environment.server.host;

export interface PeriodicElement {
  col1: string;
  col2: string;
  col3: string;
  col4: string;
  col5: string;
  col6: string;
  col7: string;
  col8: string;
}

export interface Graph {
  datasets: Array<any>;
  options: any;
  chartType: string;
  colors: Array<any>;
  legend: boolean;
  labels: Array<any>;
  visibility: Boolean;
}

@Component({
  selector: 'app-stochastical',
  templateUrl: './stochastical.component.html',
  styleUrls: ['./stochastical.component.scss']
})
export class StochasticalComponent implements OnInit, AfterViewInit {

  angForm: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    kb_plus: new FormControl(''),
    kb_minus: new FormControl(''),
    kt_plus: new FormControl(''),
    kt_minus: new FormControl(''),
    k1_plus: new FormControl(''),
    k1_minus: new FormControl(''),
    k2_plus: new FormControl(''),
    k2_minus: new FormControl(''),
    molar_concentration_of_actin: new FormControl(''),
    molar_concentration_of_myosin: new FormControl(''),
    final_myosin: new FormControl(''),
    type_of_simulation: new FormControl(''),
    number_of_filaments: new FormControl(''),
    number_of_tm_tn_units: new FormControl(''),
    cooperativity_factor: new FormControl(''),
    negative_cooperativity_factor: new FormControl('')
  });

  constructor(private fb: FormBuilder, private StochasticalService: StochasticalService) { }

  public stochastical: any;
  public dataSource;
  public inputData = this.profileForm;
  public histogramData: PeriodicElement[];
  public displayedColumns: string[];
  public typeOfSimulation: Number = 1;
  public run_button = "RUN";
  public isVisible = false;
  public dugmeRun: Boolean = false;

  public graphData

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.StochasticalService.getInputData('test21').subscribe(data => {
      if (data) {
        this.inputData = data;
      }
    });
    this.graphData = new Chart('graphData', {})
  }

  onSubmit() {
    const body = this.profileForm.value;
    this.isVisible = false;
    this.run_button = "RUNNING";
    this.dugmeRun = true;
    this.graphData.destroy()
    this.StochasticalService.getData(body).subscribe(data => {
      this.stochastical = data;
      this.histogramData = data.histogram;

      const datasets = [
        {
          data: data.fractional_fluorescence,
          label: 'Fractional Fluorescence',
          fill: false,
          lineTension: 0.2,
          borderColor: 'blue',
          borderWidth: 1,
        },
        {
          data: data.fraction_saturation,
          label: 'Fraction Saturation',
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        },
        {
          data: data.fraction_of_unbound_actin,
          label: 'Fraction of Unbound Actin',
          fill: false,
          lineTension: 0.2,
          borderColor: 'green',
          borderWidth: 1,
        },
        {
          data: data.free_myosin_concentration,
          label: 'Free Myosin Concentration',
          fill: false,
          lineTension: 0.2,
          borderColor: 'orange',
          borderWidth: 1,
        }
      ];

      this.parameterForGraph(datasets)

      this.isVisible = true;
      this.dugmeRun = false;
      this.run_button = "RUN";
    });
  }

  download() {
    window.open(host + 'stochasticals/users/test21/download');
  }
  saveSimulation() {
    const body = this.profileForm.value;
    this.StochasticalService.saveSimulation(body).subscribe(data => {
    })
  }

  openSaveSimulation(file) {
    this.graphData.destroy()
    const fileData = file.target.files
    var reader = new FileReader();
    reader.readAsText(fileData[0]);
    reader.onload = (_event) => {
      const data = JSON.parse(reader.result.toString());
      this.stochastical = data;
      this.histogramData = data.histogram;
      if (data.fractional_fluorescence && data.fraction_saturation && data.fraction_of_unbound_actin && data.free_myosin_concentration) {
        const datasets = [
          {
            data: data.fractional_fluorescence,
            label: 'Fractional Fluorescence',
            fill: false,
            lineTension: 0.2,
            borderColor: 'blue',
            borderWidth: 1,
          },
          {
            data: data.fraction_saturation,
            label: 'Fraction Saturation',
            fill: false,
            lineTension: 0.2,
            borderColor: 'red',
            borderWidth: 1,
          },
          {
            data: data.fraction_of_unbound_actin,
            label: 'Fraction of Unbound Actin',
            fill: false,
            lineTension: 0.2,
            borderColor: 'green',
            borderWidth: 1,
          },
          {
            data: data.free_myosin_concentration,
            label: 'Free Myosin Concentration',
            fill: false,
            lineTension: 0.2,
            borderColor: 'orange',
            borderWidth: 1,
          }
        ];

        this.parameterForGraph(datasets)
      }
    };
  }

  onCheckboxChange(box1: string, box2: string) {
    if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
      if (this.inputData[box1])
        this.inputData[box2] = false;
    }
  }

  parameterForGraph(datasets: any) {
    this.graphData = new Chart('graphData', {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Iteration'
            }
          }], yAxes: [{
            ticks: {
              callback: function (value) {
                return value.toExponential(2)
              }
            }
          }]
        }
      }
    })
  }
}
