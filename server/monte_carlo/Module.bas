Attribute VB_Name = "Module"
Option Explicit

' First Method
' Shell to another app and wait for it to finish using Windows Scripting Host
'
' Declared constant explanation:
'
' 0 - Hides the window and activates another window.
' 1 - Activates and displays a window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when displaying the window for the first time.
' 2 - Activates the window and displays it as a minimized window.
' 3 - Activates the window and displays it as a maximized window.
' 4 - Displays a window in its most recent size and position. The active window remains active.
' 5 - Activates the window and displays it in its current size and position.
' 6 - Minimizes the specified window and activates the next top-level window in the Z order.
' 7 - Displays the window as a minimized window. The active window remains active.
' 8 - Displays the window in its current state. The active window remains active.
' 9 - Activates and displays the window. If the window is minimized or maximized, the system restores it to its original size and position. An application should specify this flag when restoring a minimized window.
' 10 - Sets the show state based on the state of the program that started the application.
' blnWait - True, if you want to wait; False, if you don't want to wait

Const intWindowStyle_ShowOther = 0
Const intWindowStyle_ShowNormal = 1
Const intWindowStyle_ShowMinActive = 2
Const intWindowStyle_ShowMaxActive = 3
Const intWindowStyle_ShowBkgrnd = 4
Const intWindowStyle_ShowFront = 5
Const intWindowStyle_ShowMinZ = 6
Const intWindowStyle_ShowMin = 7
Const intWindowStyle_ShowCurrent = 8
Const intWindowStyle_ShowOrigActive = 9
Const intWindowStyle_ShowSame = 10

' Second Method
' The following function provides a replacement for shell() function, the advantage is that it does not return until the command has finished its execution.
Private Type STARTUPINFO
    cb As Long
    lpReserved As String
    lpDesktop As String
    lpTitle As String
    dwX As Long
    dwY As Long
    dwXSize As Long
    dwYSize As Long
    dwXCountChars As Long
    dwYCountChars As Long
    dwFillAttribute As Long
    dwFlags As Long
    wShowWindow As Integer
    cbReserved2 As Integer
    lpReserved2 As Long
    hStdInput As Long
    hStdOutput As Long
    hStdError As Long
End Type

Private Type PROCESS_INFORMATION
    hProcess As Long
    hThread As Long
    dwProcessID As Long
    dwThreadID As Long
End Type

Private Declare Function WaitForSingleObject Lib _
"kernel32" (ByVal hHandle As Long, ByVal dwMilliseconds _
As Long) As Long

Declare Function CreateProcessA Lib "kernel32" _
(ByVal lpApplicationName As Long, ByVal lpCommandLine As _
String, ByVal lpProcessAttributes As Long, ByVal _
lpThreadAttributes As Long, ByVal bInheritHandles As Long, _
ByVal dwCreationFlags As Long, ByVal lpEnvironment As Long, _
ByVal lpCurrentDirectory As Long, lpStartupInfo As _
STARTUPINFO, lpProcessInformation As PROCESS_INFORMATION) _
As Long

Declare Function CloseHandle Lib "kernel32" (ByVal hObject _
As Long) As Long

Private Const NORMAL_PRIORITY_CLASS = &H20&
Private Const INFINITE = -1&

' First Method
Sub xShell(strAppPath As String, intWindowStyle As Integer, blnWait As Boolean)
Dim oShell As IWshRuntimeLibrary.IWshShell_Class

Set oShell = New IWshRuntimeLibrary.IWshShell_Class
'if can't set reference to wshom.ocx use this syntax
'Dim oShell
'Set oShell = CreateObject("Wscript.Shell")
Call oShell.Run(strAppPath, intWindowStyle, blnWait)
Set oShell = Nothing
End Sub

' Second Method
Public Sub ExecCmd(cmdline$)
Dim proc As PROCESS_INFORMATION
Dim start As STARTUPINFO
Dim ret As Long

' Initialize the STARTUPINFO structure:
start.cb = Len(start)

' Start the shelled application:
ret = CreateProcessA(0&, cmdline$, 0&, 0&, 1&, _
NORMAL_PRIORITY_CLASS, 0&, 0&, start, proc)

' Wait for the shelled application to finish:
ret = WaitForSingleObject(proc.hProcess, INFINITE)
ret = CloseHandle(proc.hProcess)
End Sub


