VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form MGMC_Form 
   Caption         =   "MGMC Thin Filament Regulation"
   ClientHeight    =   9390
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9390
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Scheme_Command 
      Caption         =   "Scheme"
      Height          =   435
      Left            =   9765
      TabIndex        =   45
      Top             =   1155
      Width           =   1380
   End
   Begin VB.CommandButton About_Command 
      Caption         =   "About"
      Height          =   435
      Left            =   9765
      TabIndex        =   44
      Top             =   420
      Width           =   1380
   End
   Begin VB.CommandButton Time_Step_Command 
      Caption         =   "Suggest Times"
      Height          =   435
      Left            =   6510
      TabIndex        =   21
      Top             =   7875
      Width           =   1485
   End
   Begin VB.TextBox Text10 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   10
      Text            =   "10"
      Top             =   4620
      Width           =   1485
   End
   Begin VB.TextBox Text12 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   12
      Text            =   "5"
      Top             =   5460
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type of Simulation"
      Height          =   645
      Left            =   3150
      TabIndex        =   38
      Top             =   6195
      Width           =   3270
      Begin VB.OptionButton Titration_Option 
         Caption         =   "Titration"
         Height          =   225
         Left            =   1995
         TabIndex        =   15
         Top             =   315
         Width           =   960
      End
      Begin VB.OptionButton Time_Option 
         Caption         =   "Time Course"
         Height          =   225
         Left            =   525
         TabIndex        =   14
         Top             =   315
         Value           =   -1  'True
         Width           =   1275
      End
   End
   Begin VB.CommandButton Graphs_Button 
      Caption         =   "Graphs"
      Height          =   435
      Left            =   8925
      TabIndex        =   20
      Top             =   7875
      Width           =   1485
   End
   Begin VB.TextBox Text17 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   19
      Text            =   "0.182700"
      Top             =   7350
      Width           =   1485
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run"
      Enabled         =   0   'False
      Height          =   435
      Left            =   6510
      TabIndex        =   22
      Top             =   8505
      Width           =   1485
   End
   Begin VB.CommandButton Output_File_Command 
      Caption         =   "Output File"
      Height          =   435
      Left            =   6510
      TabIndex        =   0
      Top             =   315
      Width           =   1485
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin MSComctlLib.StatusBar StatusBar1 
      Align           =   2  'Align Bottom
      Height          =   330
      Left            =   0
      TabIndex        =   36
      Top             =   9060
      Width           =   11235
      _ExtentX        =   19817
      _ExtentY        =   582
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   1
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
         EndProperty
      EndProperty
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   300
      Left            =   9660
      TabIndex        =   18
      Text            =   "1"
      Top             =   6930
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.TextBox Text15 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   8085
      TabIndex        =   17
      Text            =   "0.25"
      Top             =   6930
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   300
      Left            =   6510
      TabIndex        =   16
      Text            =   "0.25"
      Top             =   6930
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   13
      Text            =   "0.05"
      Top             =   5880
      Width           =   1485
   End
   Begin VB.TextBox Text11 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   11
      Text            =   "1000"
      Top             =   5040
      Width           =   1485
   End
   Begin VB.TextBox Text9 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   9
      Text            =   "2e6"
      Top             =   4200
      Width           =   1485
   End
   Begin VB.TextBox Text8 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   8
      Text            =   "200"
      Top             =   3780
      Width           =   1485
   End
   Begin VB.TextBox Text7 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   7
      Text            =   "0.2"
      Top             =   3360
      Width           =   1485
   End
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   6
      Text            =   "300"
      Top             =   2940
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   5
      Text            =   "0.3"
      Top             =   2520
      Width           =   1485
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   4
      Text            =   "20"
      Top             =   2100
      Width           =   1485
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   3
      Text            =   "1"
      Top             =   1680
      Width           =   1485
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   2
      Text            =   "100"
      Top             =   1260
      Width           =   1485
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   1
      Text            =   "100"
      Top             =   840
      Width           =   1485
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant of Myosin Weak Binding (k1- [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   43
      Top             =   4725
      Width           =   5595
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Isomerization Rate Constant (k2- [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   42
      Top             =   5565
      Width           =   5595
   End
   Begin VB.Label Label17 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File for the Graph:"
      Height          =   285
      Left            =   8190
      TabIndex        =   41
      Top             =   6510
      Visible         =   0   'False
      Width           =   3000
   End
   Begin VB.Label Label16 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File for the Graph:"
      Height          =   285
      Left            =   8190
      TabIndex        =   40
      Top             =   6090
      Visible         =   0   'False
      Width           =   3000
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "Input File for the Graph:"
      Height          =   285
      Left            =   8190
      TabIndex        =   39
      Top             =   5670
      Visible         =   0   'False
      Width           =   3000
   End
   Begin VB.Label Label14 
      Alignment       =   1  'Right Justify
      Caption         =   "Random Seed:"
      Height          =   285
      Left            =   840
      TabIndex        =   37
      Top             =   7455
      Width           =   5595
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Myosin [uM]:"
      Height          =   285
      Left            =   0
      TabIndex        =   35
      Top             =   7035
      Width           =   6435
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Actin [uM]:"
      Height          =   285
      Left            =   840
      TabIndex        =   34
      Top             =   5985
      Width           =   5595
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Isomerization Rate Constant (k2+ [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   33
      Top             =   5145
      Width           =   5595
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Rate Constant of Myosin Weak Binding (k1+ [1/(M*s)]):"
      Height          =   285
      Left            =   840
      TabIndex        =   32
      Top             =   4305
      Width           =   5595
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant Beetwen the Closed and Open States (kt-):"
      Height          =   285
      Left            =   840
      TabIndex        =   31
      Top             =   3885
      Width           =   5595
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Equlibrium Constant Beetwen the Closed and Open States (Kt):"
      Height          =   285
      Left            =   840
      TabIndex        =   30
      Top             =   3465
      Width           =   5595
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant Beetwen the Blocked and Closed States (kb-):"
      Height          =   285
      Left            =   840
      TabIndex        =   29
      Top             =   3045
      Width           =   5595
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Equilibrium Constant Beetwen the Blocked and Closed States (Kb):"
      Height          =   285
      Left            =   840
      TabIndex        =   28
      Top             =   2625
      Width           =   5595
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   840
      TabIndex        =   27
      Top             =   2205
      Width           =   5595
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Cooperativity Factor (1-No Coop):"
      Height          =   285
      Left            =   840
      TabIndex        =   26
      Top             =   1785
      Width           =   5595
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "The Number of Filaments [#]:"
      Height          =   285
      Left            =   840
      TabIndex        =   25
      Top             =   1365
      Width           =   5595
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "The Number of Actin7.TmTn Units [#]:"
      Height          =   285
      Left            =   840
      TabIndex        =   24
      Top             =   945
      Width           =   5595
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   285
      Left            =   840
      TabIndex        =   23
      Top             =   525
      Width           =   5595
   End
End
Attribute VB_Name = "MGMC_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim OutputFileName As String
Dim OutputFileTitle As String
Dim File_ID As Variant
Dim Input_File_for_Graph As String
Dim Input_File_for_Graph_Title As String
Dim Output_File_for_Graph_Base As String
Dim Output_File_for_Graph As String
Dim Output_File_for_Histogram As String

Private Sub About_Command_Click()
    About_Form.Show
End Sub

Private Sub Graphs_Button_Click()
    Dim FileNumber As Integer
    Dim Trash_Text As String
    Dim Good_Input As String
    Dim Good_Input1 As String
    Dim Good_Input2 As String
    Dim InputFileLength As Integer
    Dim InputFileTitleLength As Integer
    Dim InputFilePath As String
    Dim File_Name_Length As Integer

    Graphs_Button.Enabled = False

    ' Set CancelError is True
    CommonDialog1.CancelError = True
    On Error GoTo ErrHandler
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    CommonDialog1.Filter = "Data File (Input*.dat)|Input*.dat"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Dialog Caption
    CommonDialog1.DialogTitle = "Select Input File for the Graph"
    ' Suggest the file name
    CommonDialog1.FileName = ""
    ' Display the Save dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Input_File_for_Graph = CommonDialog1.FileName
    ' Display name of selected file (without the path)
    Input_File_for_Graph_Title = CommonDialog1.FileTitle

    If Input_File_for_Graph <> "" Then
        Label15.Caption = Input_File_for_Graph
        'Import data from a file
        FileNumber = FreeFile
        Open Input_File_for_Graph For Input As #FileNumber
        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Label1.Caption = Good_Input             ' Output File Name

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text1.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text2.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text3.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text4.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text5.Text = Good_Input
        Input #FileNumber, Good_Input
        Text6.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text7.Text = Good_Input
        Input #FileNumber, Good_Input
        Text8.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text9.Text = Good_Input
        Input #FileNumber, Good_Input
        Text10.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text11.Text = Good_Input
        Input #FileNumber, Good_Input
        Text12.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text13.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text14.Text = Good_Input
        Input #FileNumber, Good_Input
        Text15.Text = Good_Input
        Input #FileNumber, Good_Input
        Text16.Text = Good_Input

        Line Input #FileNumber, Trash_Text
        Input #FileNumber, Good_Input
        Text17.Text = Good_Input
        'Close the file
        Close #FileNumber

        InputFileLength = Len(Input_File_for_Graph)
        InputFileTitleLength = Len(Input_File_for_Graph_Title)
        InputFilePath = Left(Input_File_for_Graph, InputFileLength - InputFileTitleLength)

        Output_File_for_Graph = InputFilePath & Mid(Input_File_for_Graph_Title, 11)
        Output_File_for_Graph_Base = Left(Output_File_for_Graph, Len(Output_File_for_Graph) - 4)
        If (Text14.Text = Text15.Text And Text16.Text = "1") Then
            Output_File_for_Graph = Output_File_for_Graph_Base & "-timecourse.dat"
            Output_File_for_Histogram = Output_File_for_Graph_Base & "-histogram.dat"
            Time_Option.Value = True
        Else
            Output_File_for_Graph = Output_File_for_Graph_Base & "-titration.dat"
            Titration_Option.Value = True
        End If
        Label16.Caption = Output_File_for_Graph
        Label17.Caption = Output_File_for_Histogram
    End If

    If Output_File_for_Graph <> "" Then
        Graphs_Form.Show
    End If

    Exit Sub

ErrHandler:
    'User pressed the Cancel button

End Sub

Private Sub Output_File_Command_Click()
    ' Set CancelError is True
    CommonDialog1.CancelError = True
    On Error GoTo ErrHandler
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    CommonDialog1.Filter = "No Extension (*)|*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    CommonDialog1.FileName = "Output"
    ' Display the Save dialog box
    CommonDialog1.ShowSave
    ' Display name of selected file (with the path)
    OutputFileName = CommonDialog1.FileName
    ' Display name of selected file (without the path)
    OutputFileTitle = CommonDialog1.FileTitle
    If OutputFileName <> "" Then
        Label1.Caption = OutputFileName
        Run_Button.Enabled = True
        StatusBar1.Panels.Item(1).Text = "Ready"
    End If

    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub

Private Sub Run_Button_Click()
    Dim OutputFileLength As Integer, OutputFileTitleLength As Integer
    Dim OutputFilePath As String
    Dim FileNumber As Integer
    Dim Input_File_Name As String

    On Error GoTo ErrHandler

    If OutputFileName <> "" Then

        If Time_Option.Value = True Then
            Text14.Text = Text15.Text
            Text14.Enabled = False
            Text14.Visible = False
            Text16.Text = 1
            Text16.Enabled = False
            Text16.Visible = False
            Label13.Caption = "Molar Concentration of Myosin [uM]:"
        End If

        OutputFileLength = Len(OutputFileName)
        OutputFileTitleLength = Len(OutputFileTitle)
        OutputFilePath = Left(OutputFileName, OutputFileLength - OutputFileTitleLength)

        Input_File_Name = OutputFilePath & "Input_for_" & OutputFileTitle & ".dat"
        
        'Export data to a file
        FileNumber = FreeFile
        Open Input_File_Name For Output As #FileNumber
        Print #FileNumber, "#Output File Name:"
        Print #FileNumber, OutputFileName
        Print #FileNumber, "#The Number of Actin7.TmTn Units [#]:"
        Print #FileNumber, Text1.Text
        Print #FileNumber, "#The Number of Filements [#]:"
        Print #FileNumber, Text2.Text
        Print #FileNumber, "#Cooperativity Factor:"
        Print #FileNumber, Text3.Text
        Print #FileNumber, "#Total Time [s]:"
        Print #FileNumber, Text4.Text
        Print #FileNumber, "#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):"
        Print #FileNumber, Text5.Text
        Print #FileNumber, Text6.Text
        Print #FileNumber, "#The Rate Constants Beetwen the Closed and Open States (Kt / kt-):"
        Print #FileNumber, Text7.Text
        Print #FileNumber, Text8.Text
        Print #FileNumber, "#The Rate Constants of Myosin Weak Binding (k1+ / k1-):"
        Print #FileNumber, Text9.Text
        Print #FileNumber, Text10.Text
        Print #FileNumber, "#Isomerization Rate Constants (k2+ / k2-):"
        Print #FileNumber, Text11.Text
        Print #FileNumber, Text12.Text
        Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text13.Text
        Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text15.Text
        'Print #FileNumber, Text16.Text
        'Print #FileNumber, "#Random Seed:"
        'Print #FileNumber, Text17.Text
        'Close the file
        Close #FileNumber

        Thin_Filament_Regulation_Form.MousePointer = vbHourglass
        Output_File_Command.Enabled = False
        Run_Button.Enabled = False
        Graphs_Button.Enabled = False
        StatusBar1.Panels.Item(1).Text = "Running"

        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        'Call ExecCmd(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34))
        Call ExecCmd(App.Path & "\num_rigid.exe " & Chr(34) & Input_File_Name & Chr(34))
        
        Thin_Filament_Regulation_Form.MousePointer = vbArrow
        Output_File_Command.Enabled = True
        Run_Button.Enabled = True
        Graphs_Button.Enabled = True
        StatusBar1.Panels.Item(1).Text = "Finished"
    End If

    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub

Private Sub Scheme_Command_Click()
    Scheme_Form.Show
End Sub

Private Sub Time_Option_Click()
    If Time_Option.Value = True Then
        Text14.Text = Text15.Text
        Text14.Enabled = False
        Text14.Visible = False
        Text16.Text = 1
        Text16.Enabled = False
        Text16.Visible = False
        Label13.Caption = "Molar Concentration of Myosin [uM]:"
    End If
End Sub

Private Sub Time_Step_Command_Click()
    Dim k_max As Double, k_min As Double

    k_max = Val(Text6.Text) / Val(Text5.Text)
    If k_max < Val(Text6.Text) Then k_max = Val(Text6.Text)
    If k_max < Val(Text8.Text) / Val(Text7.Text) Then k_max = Val(Text8.Text) / Val(Text7.Text)
    If k_max < Val(Text8.Text) Then k_max = Val(Text8.Text)
    If k_max < Val(Text9.Text) * Val(Text15.Text) * 0.000001 Then k_max = Val(Text9.Text) * Val(Text15.Text) * 0.000001
    If k_max < Val(Text10.Text) Then k_max = Val(Text10.Text)
    If k_max < Val(Text11.Text) Then k_max = Val(Text11.Text)
    If k_max < Val(Text12.Text) Then k_max = Val(Text12.Text)

    Text3.Text = Format(0.1 / k_max, "0.#####0")

    k_min = Val(Text6.Text) / Val(Text5.Text)
    If k_min > Val(Text6.Text) Then k_min = Val(Text6.Text)
    If k_min > Val(Text8.Text) / Val(Text7.Text) Then k_min = Val(Text8.Text) / Val(Text7.Text)
    If k_min > Val(Text8.Text) Then k_min = Val(Text8.Text)
    If k_min > Val(Text9.Text) * Val(Text15.Text) * 0.000001 Then k_min = Val(Text9.Text) * Val(Text15.Text) * 0.000001
    If k_min > Val(Text10.Text) Then k_min = Val(Text10.Text)
    If k_min > Val(Text11.Text) Then k_min = Val(Text11.Text)
    If k_min > Val(Text12.Text) Then k_min = Val(Text12.Text)

    Text4.Text = Format((1 / k_min) * 10, "0.#####0")
End Sub

Private Sub Titration_Option_Click()
    If Titration_Option.Value = True Then
        Text14.Text = 0
        Text14.Enabled = True
        Text14.Visible = True
        Text16.Text = 500
        Text16.Enabled = True
        Text16.Visible = True
        Label13.Caption = "(For Titration) Molar Concentration of Myosin Initial / Final / # of Steps [uM / uM / #]:"
    End If
End Sub
