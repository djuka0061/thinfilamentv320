'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const zippingFile = require('../../middleware/zipping');
const returnOutputData = require('../../middleware/return_data');
const winston = require('winston')

//Local include
module.exports = function (david) {

  //function for run simulation
  david.runSimulation = (id, parameters) => {

    const exeSimulation = (id, runSimulationId, parameters) => {
      //setup data for start simulation
      const fileName = 'ParaEstimDavid.exe';
      const params = [];
      const path = './david/'
      return exec(fileName, params, { cwd: path }, (err, data) => {
        if (err) {
          winston.info(err)
          const response = {
            statusCode: 400,
            message: 'Simulation failed'
          }
          return response
        } else {
          winston.info('Simulation david successfully finish')
          //checked estimation params
          const cekiranaPolja = [Boolean(parameters.tni_fiting), Boolean(parameters.gama_fiting)];

          returnOutputData.removeData('david')
          //call function for create output data
          return returnOutputData.returnData('david', cekiranaPolja, parameters.number_of_iteration)
            .then(outputData => {
              zippingFile.zipFile('david', cekiranaPolja, parameters, outputData);

              const options = {
                userID: id,
                input_id: runSimulationId,
                ...outputData
              }

              return loopback.getModel('david_output').create(options)
                .then(datas => {
                  return datas;
                })
            })
        }
      })
    }

    //create input file
    let writeStream = fs.createWriteStream('./david/tmtn-ca.dat');
    writeStream.write(parameters.phm1 + '\n');
    writeStream.write(parameters.phm2 + '\n');
    writeStream.write(parameters.cpl + '\n');
    writeStream.write(parameters.kkm1 + '\n');
    writeStream.write(parameters.kkm2 + '\n');
    writeStream.write(parameters.tni + '\n');
    writeStream.write(parameters.km1p + '\n');
    writeStream.write(parameters.id1 + '\n');
    writeStream.write(parameters.id2 + '\n');
    writeStream.write(parameters.id3 + '\n');
    writeStream.write(parameters.alpha + '\n');
    writeStream.write(parameters.dm1 + '\n');
    writeStream.write(parameters.gama + '\n');
    writeStream.write(parameters.dt + '\n');
    writeStream.write(parameters.s + '\n');
    writeStream.end(parameters.no_of_repeats + '\n');

    let writeInputWarp = fs.createWriteStream('./david/input_wrap');
    writeInputWarp.write((parameters.tni_fiting ? parameters.tni_guess_of_params1 : parameters.tni) + '\n');
    writeInputWarp.write((parameters.tni_fiting ? parameters.tni_guess_of_params2 : parameters.tni) + '\n');
    writeInputWarp.write((parameters.tni_fiting ? parameters.tni_lower_bound : parameters.tni) + '\n');
    writeInputWarp.write((parameters.tni_fiting ? parameters.tni_upper_bound : parameters.tni) + '\n');
    writeInputWarp.write((parameters.gama_fiting ? parameters.gama_guess_of_params1 : parameters.gama) + '\n');
    writeInputWarp.write((parameters.gama_fiting ? parameters.gama_guess_of_params2 : parameters.gama) + '\n');
    writeInputWarp.write((parameters.gama_fiting ? parameters.gama_lower_bound : parameters.gama) + '\n');
    writeInputWarp.write((parameters.gama_fiting ? parameters.gama_upper_bound : parameters.gama) + '\n');
    writeInputWarp.write(parameters.epsilon + '\n');
    writeInputWarp.write(parameters.number_of_iteration + '\n');
    writeInputWarp.write('./david_out/a_t.txt' + '\n');
    writeInputWarp.write('./david_out/kkk.dat' + '\n');
    writeInputWarp.write('./david_out/eee.dat' + '\n');
    writeInputWarp.write('./david_out/rrr.dat' + '\n');
    writeInputWarp.end('DavidModel\n');

    const options = {
      userID: id,
      ...parameters
    }
    return loopback.getModel('david').create(options)
      .then(data => {
        exeSimulation(id, data._id, parameters)
        winston.info('Simulation david successfully start')
        const response = {
          statusCode: 200,
          message: "Successfull run simulation",
          simulationId: data._id
        }

        return response
      });
  }

  david.saveData = (id, parameters) => {
    //checked estimation params
    const cekiranaPolja = [Boolean(parameters.tni_fiting), Boolean(parameters.gama_fiting)];

    //call function for create sensitivity matrix
    pravljenjeSensitivityMatrice.createMatrix('./david/new_eee.dat', './david/new_kkk.dat', './david/new_rrr.dat', parameters.number_of_iteration, cekiranaPolja);

    return returnOutputData.returnData('david', cekiranaPolja, parameters.number_of_iteration)
      .then(outputData => {
        if(!outputData && outputData !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for david simulation'
          }
          winston.info('Not found output data for david simulation')
          return response
        }
        const davidParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel(david).create(davidParams)
          .then(david => {
            const options = {
              userID: id,
              input_id: david._id,
              ...outputData
            }
            return loopback.getModel('david_output').create(options)
              .then(data => {
                winston.info('Simulation david successfully save data')
                return data;
              })
          })
      })
  }

  david.returnSaveData = (id) => {
    let response;
    return loopback.getModel('david_output').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found david simulation'
          }
          winston.info('Not found return data for david simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id
        return loopback.getModel('david').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found david simulation'
              }
              winston.info('Not found input data for david simulation')
              return response
            }
            response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response
          })
      })
  }

  david.returnData = (simulationId) => {
    let response;
    return loopback.getModel('atphase').findOne({ where: { _id: simulationId } })
      .then(data => {
        if (!data) {
          response = {
            statusCode: 404,
            message: `Not found david simulation with simulation id: ${simulationId}`
          }
          winston.info(`Not found input data for david simulation with simulation id: ${simulationId}`)
          return response
        }
        return loopback.getModel('atphase_eee').findOne({ where: { input_id: simulationId } })
          .then(estimatedParams => {
            if (!estimatedParams) {
              response = {
                statusCode: 404,
                message: `Not found david simulation with simulation id: ${simulationId}`
              }
              winston.info(`Not found estimated data for david simulation with simulation id: ${simulationId}`)
              return response
            }
            response = {
              statusCode: 200,
              message: 'Successfull find simulation',
              parameters: data,
              estimatedParams: estimatedParams
            }
            return response
          })
      })
  }



  //Save input data for simulation
  david.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    };
    return loopback.getModel('david').create(options)
      .then(data => {
        winston.info('Simulation david successfully save input data')
        return data;
      });
  };

  david.returnInputData = (id) => {
    return loopback.getModel('david').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found david input data`
          }
          winston.info('Not found david input data')
          return response
        }
        return data[data.length - 1];
      })
  }

  david.saveDefaulValue = (id, parameters) => {
    const options = {
      userID: id,
      default_values: true,
      ...parameters
    }
    return loopback.getModel('david').create(options)
      .then(data => {
        winston.info('Simulation david successfully save default values')
        return data;
      })
  }


  david.returnSaveDefaulValue = (id) => {
    return loopback.getModel('atphase').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found david default values`
          }
          winston.info('Not found david default values')
          return response
        }
        return data[data.length - 1];
      })
  }

  david.download = (id, res) => {
    winston.info('Simulation david successfully download data')
    res.download('./david/output_files.zip');
  };

  david.upload = (id, req, res) => {
    var storage = multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './david');
      },
      filename: function (req, file, callback) {
        callback(null, 'a_t.txt');
      }
    });

    let upload = multer({ storage: storage }).single('file');

    upload(req, res, function (err) {
      if (err) {
        winston.info(err)
        return res.end("Error uploading file.");
      }
      winston.info('Simulation david successfully update a_t')
      res.end("File is uploaded");
    });
  }

  /** API for run simulation */
  david.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  david.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  david.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  david.remoteMethod('returnData', {
    http: { verb: 'GET', path: '/users/:simulationId/returnData' },
    accepts: [
      { arg: 'simulationId', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });


  /** API for return input data for simulation */
  david.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  david.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  david.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  david.remoteMethod('upload',
    {
      http: { verb: 'POST', path: '/users/:id/upload' },
      accepts: [
        { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
        { arg: 'req', type: 'object', http: { source: 'req' } },
        { arg: 'res', type: 'object', http: { source: 'res' } }
      ],
      returns: { arg: 'data', type: 'string', root: true }
    }
  );

  david.remoteMethod('saveDefaulValue', {
    http: { verb: 'POST', path: '/users/:id/saveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  david.remoteMethod('returnSaveDefaulValue', {
    http: { verb: 'GET', path: '/users/:id/returnSaveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  david.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download', 'upload', 'returnSaveDefaulValue', 'saveDefaulValue', 'returnData'], method.name)) {
      david.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}