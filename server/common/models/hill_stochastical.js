'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const returnOutputData = require('../../middleware/return_data');
const zip = require('../../middleware/zipping');
const winston = require('winston')

//Local include

module.exports = function (hill_stochastical) {

  //function for run simulation
  hill_stochastical.runSimulation = (id, parameters) => {

    //create input file
    let writeStream = fs.createWriteStream('./hill_stochastical/pain.dat');
    writeStream.write("#Output File Name:\n");
    writeStream.write('./hill_mcout\n');
    writeStream.write("#Number of Filaments:\n");
    writeStream.write(parameters.number_of_filaments + '\n');
    writeStream.write("#Number of TmTn units:\n");
    writeStream.write(parameters.number_of_tm_tn_units + '\n');
    writeStream.write('#Number of Actin per unit:\n');
    writeStream.write(parameters.number_of_actin_per_unit + '\n');
    writeStream.write('#Total Time [s]:\n');
    writeStream.write(parameters.s + '\n');
    writeStream.write('#Molar Concentration of Actin [uM]:\n');
    writeStream.write(parameters.actin + '\n');
    writeStream.write('#Molar Concentration of Myosin Initial / Final [uM / uM]:\n');
    writeStream.write(parameters.myosin + '\n');
    writeStream.write(parameters.final_const_of_myosin ? parameters.final_const_of_myosin + '\n' : parameters.myosin + '\n');
    writeStream.write('#The Rate Constants Beetwen ON and OFF States (Alpha0 / Beta0):\n');
    writeStream.write(parameters.alpha_zero + '\n');
    writeStream.write(parameters.beta_zero + '\n');
    writeStream.write('#Constants Y1 and Y2:\n');
    writeStream.write(parameters.constant_y1 + '\n');
    writeStream.write(parameters.constant_y2 + '\n');
    writeStream.write('#The Foward/Backward Rate Constants of OFF State (K1 / K1\'):\n');
    writeStream.write(parameters.constant_k1 + '\n');
    writeStream.write(parameters.constant_k1_prim + '\n');
    writeStream.write('#The Foward/Backward Rate Constants of ON State (K2/ K2\'):\n')
    writeStream.write(parameters.constant_k2 + '\n');
    writeStream.write(parameters.constant_k2_prim + '\n');
    writeStream.write('#Parameter Gama:\n');
    writeStream.write(parameters.parameter_gama + '\n');
    writeStream.write('#Cooperativity Paramater Delta:\n')
    writeStream.write(parameters.parameter_delta + '\n');
    writeStream.write('#Cooperativity Switch (1 - ON /0 - OFF):\n')
    writeStream.end(parameters.cooperativity_on + '\n');

    const options = {
      userID: id,
      ...parameters
    };
    //Simu_output-histogram.DAT
    //setup data for start simulation
    const fileName = 'wraper.exe';
    const params = [];
    const path = './hill_stochastical/'
    let promise = new Promise((resolve, reject) => {
      exec(fileName, params, { cwd: path }, (err, data) => {
        winston.info('Simulation hill stochastical successfully start')
        if (err) {
          winston.info(err)
          const response = {
            statusCode: 400,
            message: 'Simulation failed'
          }
          return response
        }
        else {
          winston.info('Simulation hill stochastical successfully finish')
          return loopback.getModel('hill_stochastical').create(options)
            .then(data => {
              return loopback.getModel('hill_stochastical').create(options)
                .then(data => {
                  return returnOutputData.returnDataForFirstFourSimulation('hill_stochastical', 'hill_mcout.DAT', 'hill_mcout-histogram.DAT')
                    .then(data => {
                      zip.zippingFile('hill_stochastical', 'hill_mcout', 'hill_mcout-histogram', data);
                      return resolve(data);
                    });
                })
            })
        }
      });
    });
    return promise;
  }

  hill_stochastical.saveData = (id, parameters) => {
    return returnOutputData.returnDataForFirstFourSimulation('hill_stochastical', 'hill_mcout.DAT', 'hill_mcout-histogram.DAT')
      .then(data => {
        if (!data && data !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for hill stochastical simulation'
          }
          winston.info('Not found output data for hill stochastical simulation')
          return response
        }
        const inputParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel('hill_stochastical').create(inputParams)
          .then(inputData => {
            const options = {
              userID: id,
              input_id: inputData._id,
              ...data
            };
            return loopback.getModel('hill_stochastical_out').create(options)
              .then(saveData => {
                winston.info('Simulation hill stochastical successfully save data')
                return saveData;
              })
          })
      });
  }

  hill_stochastical.returnSaveData = (id) => {
    return loopback.getModel('hill_stochastical_out').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found hill stochastical simulation'
          }
          winston.info('Not found return data for hill stochastical simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id;
        return loopback.getModel('hill_stochastical').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found hill stochastical simulation'
              }
              winston.info('Not found input data for hill stochastical simulation')
              return response
            }
            const response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response;
          });
      });
  }

  //Save input data for simulation
  hill_stochastical.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    };
    return loopback.getModel('hill_stochastical').create(options)
      .then(data => {
        winston.info('Simulation hill stochastical successfully save input data')
        return data;
      });
  };

  hill_stochastical.returnInputData = (id) => {
    return loopback.getModel('hill_stochastical').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found hill stochastical input data`
          }
          winston.info('Not found hill stochastical input data')
          return response
        }
        return data[data.length - 1];
      })
  }


  hill_stochastical.download = (id, res) => {
    winston.info('Simulation hill stochastical successfully download data')
    res.download('./hill_stochastical/output_files.zip');
  };


  /** API for run simulation */
  hill_stochastical.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }

    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_stochastical.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_stochastical.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  /** API for return input data for simulation */
  hill_stochastical.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });


  hill_stochastical.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });


  hill_stochastical.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });


  hill_stochastical.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download'], method.name)) {
      hill_stochastical.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}