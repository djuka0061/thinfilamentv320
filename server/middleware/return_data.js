'use strict';
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const pravljenjeSensitivityMatrice = require('./sensirivity_matrix');

module.exports.returnData = (simulation_path, checked_fitings, number_of_iteration) => {
  const params = [];
  const path = './' + simulation_path;
  let formaterKKK, formaterRRR;
  if (simulation_path === 'mg_probabilistic' || simulation_path === 'hill_monte_carlo') {
    formaterKKK = './estim_kkk_eight.exe';
    formaterRRR = './estim_rrr_eight.exe';
  } else if (simulation_path === 'monte_carlo') {
    formaterKKK = './estim_kkk_ten.exe';
    formaterRRR = './estim_rrr_ten.exe';
  } else {
    formaterKKK = './estim_formater.exe';
    formaterRRR = './estim_rrr_formater.exe';
  }

  let promise = new Promise((resolve, reject) => {
    if (!fs.existsSync(path + '/new_eee.dat')) {
      fs.writeFileSync(path + '/new_eee.dat');
    }
    return exec('estim_eee_formater.exe', [], { cwd: path }, (err, data) => {
      const eeePath = fs.readFileSync(path + '/new_eee.dat').toString().split('\n');
      let error = [];
      let count = 1;
      eeePath.forEach(data => {
        const splitData = data.split(',')
        splitData.forEach(iteration => {
          if (iteration !== undefined && iteration !== null && iteration !== '' && number_of_iteration >= count) {
            error.push({
              x: count,
              y: iteration
            });
            count++;
          }
        });
      });
      if (!fs.existsSync(path + '/new_kkk.dat')) {
        fs.writeFileSync(path + '/new_kkk.dat');
      }
      const ka = [], ka_minus = [], kpi = [], kpi_minus = [], kt = [], kt_minus = [],
        kt_star = [], kt_star_minus = [], kt_star_2 = [], kt_star_minus_2 = [], kh = [], kh_minus = [], kah = [], kah_minus = [],
        kb = [], kb_minus = [], k1 = [], k1_minus = [], k2 = [], k2_minus = [], tni = [], gama = [], aplha = [], beta = [],
        y1 = [], y2 = [], k1_prim = [], k2_prim = [];
      return exec(formaterKKK, [], { cwd: path }, (err, data) => {
        const kkkPath = fs.readFileSync(path + '/new_kkk.dat').toString().split('\n');
        if (simulation_path === 'atphase') {
          count = 1;
          kkkPath.forEach(data => {
            const splitData = data.split(',');
            if (number_of_iteration >= count) {
              ka.push({ x: count, y: parseFloat(splitData[0]) })
              ka_minus.push({ x: count, y: parseFloat(splitData[1]) })
              kpi.push({ x: count, y: parseFloat(splitData[2]) })
              kpi_minus.push({ x: count, y: parseFloat(splitData[3]) })
              kt.push({ x: count, y: parseFloat(splitData[4]) })
              kt_minus.push({ x: count, y: parseFloat(splitData[5]) })
              kt_star.push({ x: count, y: parseFloat(splitData[6]) })
              kt_star_minus.push({ x: count, y: parseFloat(splitData[7]) })
              kt_star_2.push({ x: count, y: parseFloat(splitData[8]) })
              kt_star_minus_2.push({ x: count, y: parseFloat(splitData[9]) })
              kh.push({ x: count, y: parseFloat(splitData[10]) })
              kh_minus.push({ x: count, y: parseFloat(splitData[11]) })
              kah.push({ x: count, y: parseFloat(splitData[12]) })
              kah_minus.push({ x: count, y: parseFloat(splitData[13]) })
              count++;
            }
          });
        } else if (simulation_path === 'mg_probabilistic' || simulation_path === 'monte_carlo') {
          count = 1;
          kkkPath.forEach(data => {
            const splitData = data.split(',');
            if (number_of_iteration >= count) {
              kb.push({ x: count, y: parseFloat(splitData[0]) })
              kb_minus.push({ x: count, y: parseFloat(splitData[1]) })
              kt.push({ x: count, y: parseFloat(splitData[2]) })
              kt_minus.push({ x: count, y: parseFloat(splitData[3]) })
              k1.push({ x: count, y: parseFloat(splitData[4]) })
              k1_minus.push({ x: count, y: parseFloat(splitData[5]) })
              k2.push({ x: count, y: parseFloat(splitData[6]) })
              k2_minus.push({ x: count, y: parseFloat(splitData[7]) })
              count++;
            }
          });
        } else if (simulation_path === 'hill_monte_carlo') {
          count = 1;
          kkkPath.forEach(data => {
            const splitData = data.split(',');
            if (number_of_iteration >= count) {
              aplha.push({ x: count, y: parseFloat(splitData[0]) })
              beta.push({ x: count, y: parseFloat(splitData[1]) })
              y1.push({ x: count, y: parseFloat(splitData[2]) })
              y2.push({ x: count, y: parseFloat(splitData[3]) })
              k1.push({ x: count, y: parseFloat(splitData[4]) })
              k1_prim.push({ x: count, y: parseFloat(splitData[5]) })
              k2.push({ x: count, y: parseFloat(splitData[6]) })
              k2_prim.push({ x: count, y: parseFloat(splitData[7]) })
              count++;
            }
          });
        } else if (simulation_path === 'david') {
          count = 1;
          kkkPath.forEach(data => {
            const splitData = data.split(',');
            if (number_of_iteration >= count) {
              tni.push({ x: count, y: parseFloat(splitData[0]) })
              gama.push({ x: count, y: parseFloat(splitData[1]) })
              count++;
            }
          });
        }
        if (!fs.existsSync(path + '/new_rrr.dat')) {
          fs.writeFileSync(path + '/new_rrr.dat');
        }
        return exec(formaterRRR, [], { cwd: path }, (err, data) => {
          if (!fs.existsSync(path + '/new_a_t.txt')) {
            fs.writeFileSync(path + '/new_a_t.txt');
          }
          return exec('formater_a_t.exe', [], { cwd: path }, (err, data) => {
            const a_tPath = fs.readFileSync(path + '/new_a_t.txt').toString().split('\n');
            let a_t = [];
            a_tPath.forEach(dat => {
              const splitData = dat.split(',');
              if (splitData[0] !== undefined && splitData[1] !== undefined) {
                a_t.push({
                  x: parseFloat(splitData[0]),
                  y: parseFloat(splitData[1])
                })
              }
            })
            if (!fs.existsSync(path + '/NEW_MODEL0001.dat')) {
              fs.writeFileSync(path + '/NEW_MODEL0001.dat');
            }
            pravljenjeSensitivityMatrice.createMatrix(path + '/new_eee.dat', path + '/new_kkk.dat', path + '/new_rrr.dat', number_of_iteration, checked_fitings, simulation_path);
            return exec('formater_model.exe', [], { cwd: path }, (err, data) => {
              const modelPath = fs.readFileSync(path + '/NEW_MODEL0001.dat').toString().split('\n');
              let model = [];
              modelPath.forEach(dat => {
                const splitData = dat.split(',');
                if (splitData[0] !== undefined && splitData[1] !== undefined) {
                  model.push({
                    x: parseFloat(splitData[0]),
                    y: parseFloat(splitData[1])
                  })
                }
              });
              let fitings = 0;
              const parameters_name = [];
              const parameters_name_sensitivity = [];
              parameters_name.push('#');
              parameters_name_sensitivity.push('#')
              if (simulation_path === 'atphase') {
                if (checked_fitings[0]) {
                  parameters_name.push('KA');
                  parameters_name_sensitivity.push('KA');
                  fitings++;
                }

                if (checked_fitings[1]) {
                  parameters_name.push('KA-');
                  parameters_name_sensitivity.push('KA-');
                  fitings++;
                }

                if (checked_fitings[2]) {
                  parameters_name.push('KPI');
                  parameters_name_sensitivity.push('KPI');
                  fitings++;
                }

                if (checked_fitings[3]) {
                  parameters_name.push('KPI-');
                  parameters_name_sensitivity.push('KPI-');
                  fitings++;
                }

                if (checked_fitings[4]) {
                  parameters_name.push('KT');
                  parameters_name_sensitivity.push('KT');
                  fitings++;
                }

                if (checked_fitings[5]) {
                  parameters_name.push('KT-');
                  parameters_name_sensitivity.push('KT-');
                  fitings++;
                }

                if (checked_fitings[6]) {
                  parameters_name.push('KT*');
                  parameters_name_sensitivity.push('KT*');
                  fitings++;
                }

                if (checked_fitings[7]) {
                  parameters_name.push('KT*-');
                  parameters_name_sensitivity.push('KT*-');
                  fitings++;
                }

                if (checked_fitings[8]) {
                  parameters_name.push('KT**');
                  parameters_name_sensitivity.push('KT**');
                  fitings++;
                }

                if (checked_fitings[9]) {
                  parameters_name.push('Kt**-');
                  parameters_name_sensitivity.push('Kt**-');
                  fitings++;
                }

                if (checked_fitings[10]) {
                  parameters_name.push('KH');
                  parameters_name_sensitivity.push('KH');
                  fitings++;
                }

                if (checked_fitings[11]) {
                  parameters_name.push('KH-');
                  parameters_name_sensitivity.push('KH-');
                  fitings++;
                }

                if (checked_fitings[12]) {
                  parameters_name.push('KAH');
                  parameters_name_sensitivity.push('KAH');
                  fitings++;
                }

                if (checked_fitings[13]) {
                  parameters_name.push('KAH-');
                  parameters_name_sensitivity.push('KAH-');
                  fitings++;
                }
              } else if (simulation_path === 'mg_probabilistic' || simulation_path === 'monte_carlo') {
                if (checked_fitings[0]) {
                  parameters_name.push('KB');
                  parameters_name_sensitivity.push('KB');
                  fitings++;
                }

                if (checked_fitings[1]) {
                  parameters_name.push('KB-');
                  parameters_name_sensitivity.push('KB-');
                  fitings++;
                }

                if (checked_fitings[2]) {
                  parameters_name.push('KT');
                  parameters_name_sensitivity.push('KT');
                  fitings++;
                }

                if (checked_fitings[3]) {
                  parameters_name.push('KT-');
                  parameters_name_sensitivity.push('KT-');
                  fitings++;
                }

                if (checked_fitings[4]) {
                  parameters_name.push('K1');
                  parameters_name_sensitivity.push('K1');
                  fitings++;
                }

                if (checked_fitings[5]) {
                  parameters_name.push('K1-');
                  parameters_name_sensitivity.push('K1-');
                  fitings++;
                }

                if (checked_fitings[6]) {
                  parameters_name.push('K2');
                  parameters_name_sensitivity.push('K2');
                  fitings++;
                }

                if (checked_fitings[7]) {
                  parameters_name.push('K2-');
                  parameters_name_sensitivity.push('K2-');
                  fitings++;
                }
              } else if (path === 'hill_monte_carlo') {
                if (checked_fitings[0]) {
                  parameters_name.push('Alpha0');
                  parameters_name_sensitivity.push('Alpha0');
                  fitings++;
                }

                if (checked_fitings[1]) {
                  parameters_name.push('Beta0');
                  parameters_name_sensitivity.push('Beta0');
                  fitings++;
                }

                if (checked_fitings[2]) {
                  parameters_name.push('Y1');
                  parameters_name_sensitivity.push('Y1');
                  fitings++;
                }

                if (checked_fitings[3]) {
                  parameters_name.push('Y2');
                  parameters_name_sensitivity.push('Y2');
                  fitings++;
                }

                if (checked_fitings[4]) {
                  parameters_name.push('K1');
                  parameters_name_sensitivity.push('K1');
                  fitings++;
                }

                if (checked_fitings[5]) {
                  parameters_name.push('K1\'');
                  parameters_name_sensitivity.push('K1\'');
                  fitings++;
                }

                if (checked_fitings[6]) {
                  parameters_name.push('K2');
                  parameters_name_sensitivity.push('K2');
                  fitings++;
                }

                if (checked_fitings[7]) {
                  parameters_name.push('K2\'');
                  parameters_name_sensitivity.push('K2\'');
                  fitings++;
                }
              } else if (simulation_path === 'david') {
                if (checked_fitings[0]) {
                  parameters_name.push('Tni');
                  parameters_name_sensitivity.push('Tni');
                  fitings++;
                }

                if (checked_fitings[1]) {
                  parameters_name.push('Gama');
                  parameters_name_sensitivity.push('Gama');
                  fitings++;
                }
              }
              parameters_name.push('Error');
              const estMat = fs.readFileSync(path + '/new_param_est.dat').toString().split('\n');
              const estimated_values = [];
              const sensitivity = [];
              estimated_values.push([
                '#',
                'Last',
                'Average',
                'Standard Deviation'
              ]);

              let i = 1;
              estMat.forEach(dat => {
                const splitData = dat.split(',');
                if (splitData[0] !== undefined && splitData[1] !== undefined) {
                  estimated_values.push([
                    parameters_name[i],
                    parseFloat(splitData[0]),
                    parseFloat(splitData[1]),
                    parseFloat(splitData[2])
                  ]);
                  i++;
                }
              });
              let iteration = [];
              sensitivity.push(parameters_name_sensitivity);
              i = 1;
              const sensMat = fs.readFileSync(path + '/new_sensitivity.dat').toString().split('\n');
              sensMat.forEach(dat => {
                const splitData = dat.split(',');
                iteration = [];

                if (splitData[0] !== undefined && splitData[0] != "") {
                  iteration.push(parameters_name_sensitivity[i])
                  iteration.push(splitData[0]);
                }

                if (splitData[1] !== undefined && splitData[1] !== "")
                  iteration.push(splitData[1]);

                if (splitData[2] !== undefined && splitData[2] !== "")
                  iteration.push(splitData[2]);

                if (splitData[3] !== undefined && splitData[3] !== "")
                  iteration.push(splitData[3]);

                if (splitData[4] !== undefined && splitData[4] !== "")
                  iteration.push(splitData[4]);

                if (splitData[5] !== undefined && splitData[5] !== "")
                  iteration.push(splitData[5]);

                if (splitData[6] !== undefined && splitData[6] !== "")
                  iteration.push(splitData[6]);

                sensitivity.push(iteration)
                i++;
              })

              return resolve({
                a_t, error, ka, ka_minus, kpi, kpi_minus, kt, kt_minus, kt_star, kt_star_minus, kt_star_2, kt_star_minus_2, kh, kh_minus, kah, kah_minus,
                kb, kb_minus, k1, k1_minus, k2, k2_minus, tni, gama, aplha, beta, y1, y2, k1_prim, k2_prim, model, estimated_values, sensitivity
              });
            });
          });
        });
      });
    });
  });
  return promise;
};

module.exports.returnDataForFirstFourSimulation = (path, outputName, histogramName) => {
  let promise = new Promise((resolve, reject) => {
    const simulationPath = fs.readFileSync('./' + path + '/' + outputName).toString().split('\n');
    let fractional_fluorescence = [], fraction_saturation = [], fraction_of_unbound_actin = [], free_myosin_concentration = [];
    simulationPath.forEach(dat => {
      let splitData;
      if (path === 'hill_stochastical' || path === 'hill_probabilistic') {
        splitData = dat.split('     ');
      } else {
        splitData = dat.split('\t');
      }

      if (splitData[1] !== undefined && splitData[2] !== undefined && splitData[3] !== undefined && splitData[4] !== undefined && splitData[5] !== undefined) {
        fractional_fluorescence.push({
          x: parseFloat(splitData[1].replace(/\s/g, '')),
          y: parseFloat(splitData[2].replace(/\s/g, ''))
        })

        fraction_saturation.push({
          x: parseFloat(splitData[1].replace(/\s/g, '')),
          y: parseFloat(splitData[3].replace(/\s/g, ''))
        })

        fraction_of_unbound_actin.push({
          x: parseFloat(splitData[1].replace(/\s/g, '')),
          y: parseFloat(splitData[4].replace(/\s/g, ''))
        })

        free_myosin_concentration.push({
          x: parseFloat(splitData[1].replace(/\s/g, '')),
          y: parseFloat(splitData[5].replace(/\s/g, ''))
        })
      }
    });
    const histogramPath = fs.readFileSync('./' + path + '/' + histogramName).toString().split('\n');
    let histogram = [];
    histogramPath.forEach(dat => {
      const splitData = dat.split('     ');
      if (splitData[1] !== undefined) {
        histogram.push({
          col1: splitData[1].replace(/\s/g, ''),
          col2: splitData[2].replace(/\s/g, ''),
          col3: splitData[3].replace(/\s/g, ''),
          col4: splitData[4].replace(/\s/g, ''),
          col5: splitData[5].replace(/\s/g, ''),
          col6: splitData[6].replace(/\s/g, ''),
          col7: splitData[7].replace(/\s/g, ''),
          col8: splitData[8].replace(/\s/g, ''),
        });
      }
    });
    return resolve({ fractional_fluorescence, fraction_saturation, fraction_of_unbound_actin, free_myosin_concentration, histogram });
  });
  return promise;
};

module.exports.removeData = (path) => {
  if (fs.existsSync('./' + path + '/new_eee.dat')) {
    fs.writeFileSync('./' + path + '/new_eee.dat', '');
    fs.writeFileSync('./' + path + '/new_kkk.dat', '');
    fs.writeFileSync('./' + path + '/new_rrr.dat', '');
    fs.writeFileSync('./' + path + '/new_a_t.txt', '');
    fs.writeFileSync('./' + path + '/NEW_MODEL0001.dat', '');
    fs.writeFileSync('./' + path + '/new_param_est.dat', '');
    fs.writeFileSync('./' + path + '/new_sensitivity.dat', '');
  }
}

module.exports.returnSimann = (checked_fitings) => {
  const path = './simann/';
  let promise = new Promise((resolve, reject) => {
    if (fs.existsSync('./new_param_est.dat')) {
      fs.createWriteStream('./new_param_est.dat')
    }
    fs.writeFileSync(path + '/new_param_est.dat', '');

    if (fs.existsSync('./new_a_t.txt')) {
      fs.createWriteStream('./new_a_t.txt');
    }
    fs.writeFileSync(path + 'new_a_t.txt', '');

    return exec('formater_a_t.exe', [], { cwd: path }, (err, data) => {
      const a_tPath = fs.readFileSync(path + 'new_a_t.txt').toString().split('\n');
      let a_t = [];
      a_tPath.forEach(dat => {
        const splitData = dat.split(',');
        if (splitData[0] !== undefined && splitData[1] !== undefined) {
          a_t.push({
            x: parseFloat(splitData[0]),
            y: parseFloat(splitData[1])
          })
        }
      })
      return exec('siman_formater.exe', [], { cwd: path }, (err, data) => {
        const parameters_name = ['KB', 'KB-', 'KT', 'KT-', 'K1', 'K1-', 'K2', 'K2-', 'Error'];

        const estimated_values = [];
        estimated_values.push([
          '#',
          'Parameters Estimation'
        ]);

        const simann = fs.readFileSync('./simann/new_param_est.dat').toString().split('\n');
        simann.forEach(data => {
          const splitData = data.split(',')
          let i = 0;
          splitData.forEach(iteration => {
            if (checked_fitings[i]) {
              estimated_values.push([
                parameters_name[i],
                parseFloat(splitData[i]),
              ]);
            }
            i++;
          })
          return resolve({ a_t, estimated_values })
        })
      })
    })
  })
  return promise;
}

module.exports.returnQuasi = (checked_fitings) => {
  const path = './quasi_newton/'
  let promise = new Promise((resolve, reject) => {
    if (!fs.existsSync(path + 'new_param_est.dat')) {
      fs.createWriteStream(path + 'new_param_est.dat')
    }
    fs.writeFileSync(path + 'new_param_est.dat');

    if (!fs.existsSync(path + 'new_a_t.txt')) {
      fs.createWriteStream(path + 'new_a_t.txt');
    }
    fs.writeFileSync(path + 'new_a_t.txt', '');

    return exec('formater_a_t.exe', [], { cwd: path }, (err, data) => {
      const a_tPath = fs.readFileSync(path + 'new_a_t.txt').toString().split('\n');
      let a_t = [];
      a_tPath.forEach(dat => {
        const splitData = dat.split(',');
        if (splitData[0] !== undefined && splitData[1] !== undefined) {
          a_t.push({
            x: parseFloat(splitData[0]),
            y: parseFloat(splitData[1])
          })
        }
      })
      return exec('quasi_formater.exe', [], { cwd: path }, (err, data) => {
        const parameters_name = ['KB', 'KB-', 'KT', 'KT-', 'K1', 'K1-', 'K2', 'K2-', 'Error'];
        const estimated_values = [];
        estimated_values.push([
          '#',
          'Parameters Estimation'
        ]);

        const simann = fs.readFileSync('./quasi_newton/new_param_est.dat').toString().split('\n');
        simann.forEach(data => {
          const splitData = data.split(',')
          let i = 0;
          splitData.forEach(iteration => {
            if (checked_fitings[i]) {
              estimated_values.push([
                parameters_name[i],
                parseFloat(splitData[i]),
              ]);
            }
            i++;
          })

        })
        return resolve({ a_t, estimated_values })
      })
    })
  })
  return promise;
}