'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const multer = require('multer');
const zippingFile = require('../../middleware/zipping');
const returnOutputData = require('../../middleware/return_data');
const winston = require('winston')

//Local include

module.exports = function (quasi_newton) {

  //function for run simulation
  quasi_newton.runSimulation = (id, parameters) => {
    //create input file
    let writeStream = fs.createWriteStream('./quasi_newton/Input_params.dat');
    writeStream.write('#Experimental and Output File Name:\n')
    writeStream.write('opt_output\n')
    writeStream.write('a_t.txt\n')
    writeStream.write('#Number of parameters:\n');
    writeStream.write('8\n');
    writeStream.write('#Total Time [s]:\n');
    writeStream.write(parameters.s + '\n');
    writeStream.write('#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):\n')
    writeStream.write(parameters.kb_plus + '\n');
    writeStream.write(parameters.kb_minus + '\n');
    writeStream.write(parameters.kt_plus + '\n');
    writeStream.write(parameters.kt_minus + '\n');
    writeStream.write(parameters.k1_plus + '\n');
    writeStream.write(parameters.k1_minus + '\n');
    writeStream.write(parameters.k2_plus + '\n');
    writeStream.write(parameters.k2_minus + '\n');
    writeStream.write('#Molar Concentration of Actin [uM]:\n');
    writeStream.write(parameters.actin + '\n');
    writeStream.write('#Molar Concentration of Myosin Initial / Final [uM / uM]:\n');
    writeStream.write(parameters.myosin + '\n');
    writeStream.write((parameters.final_myosin ? parameters.final_myosin : parameters.myosin) + '\n');
    writeStream.write('#Parameters for guess, low and upper bound:\n');
    writeStream.write((parameters.kb_fiting ? parameters.kb_guess : parameters.kb_plus) + '\n');
    writeStream.write((parameters.kb_fiting ? parameters.kb_lower_bound : parameters.kb_plus) + '\n');
    writeStream.write((parameters.kb_fiting ? parameters.kb_upper_bound : parameters.kb_plus) + '\n');
    writeStream.write((parameters.kb_minus_fiting ? parameters.kb_minus_guess : parameters.kb_minus) + '\n');
    writeStream.write((parameters.kb_minus_fiting ? parameters.kb_minus_lower_bound : parameters.kb_minus) + '\n');
    writeStream.write((parameters.kb_minus_fiting ? parameters.kb_minus_upper_bound : parameters.kb_minus) + '\n');
    writeStream.write((parameters.kt_fiting ? parameters.kt_guess : parameters.kt_plus) + '\n');
    writeStream.write((parameters.kt_fiting ? parameters.kt_lower_bound : parameters.kt_plus) + '\n');
    writeStream.write((parameters.kt_fiting ? parameters.kt_upper_bound : parameters.kt_plus) + '\n');
    writeStream.write((parameters.kt_minus_fiting ? parameters.kt_minus_guess : parameters.kt_minus) + '\n');
    writeStream.write((parameters.kt_minus_fiting ? parameters.kt_minus_lower_bound : parameters.kt_minus) + '\n');
    writeStream.write((parameters.kt_minus_fiting ? parameters.kt_minus_upper_bound : parameters.kt_minus) + '\n');
    writeStream.write((parameters.k1_fiting ? parameters.k1_guess : parameters.k1_plus) + '\n');
    writeStream.write((parameters.k1_fiting ? parameters.k1_lower_bound : parameters.k1_plus) + '\n');
    writeStream.write((parameters.k1_fiting ? parameters.k1_upper_bound : parameters.k1_plus) + '\n');
    writeStream.write((parameters.k1_minus_fiting ? parameters.k1_minus_guess : parameters.k1_minus) + '\n');
    writeStream.write((parameters.k1_minus_fiting ? parameters.k1_minus_lower_bound : parameters.k1_minus) + '\n');
    writeStream.write((parameters.k1_minus_fiting ? parameters.k1_minus_upper_bound : parameters.k1_minus) + '\n');
    writeStream.write((parameters.k2_fiting ? parameters.k2_guess : parameters.k2_plus) + '\n');
    writeStream.write((parameters.k2_fiting ? parameters.k2_lower_bound : parameters.k2_plus) + '\n');
    writeStream.write((parameters.k2_fiting ? parameters.k2_upper_bound : parameters.k2_plus) + '\n');
    writeStream.write((parameters.k2_minus_fiting ? parameters.k2_minus_guess : parameters.k2_minus) + '\n');
    writeStream.write((parameters.k2_minus_fiting ? parameters.k2_minus_lower_bound : parameters.k2_minus) + '\n');
    writeStream.end((parameters.k2_minus_fiting ? parameters.k2_minus_upper_bound : parameters.k2_minus) + '\n');

    const options = {
      userID: id,
      ...parameters
    };

    //setup data for start simulation
    const fileName = 'opt_var.exe';
    const params = ['Input_params.dat'];
    const path = './quasi_newton/'
    let promise = new Promise((resolve, reject) => {
      return exec(fileName, params, { cwd: path }, (err, data) => {
        winston.info('Simulation quasi newton successfully start')
        if (err) {
          winston.info(err)
          const response = {
            statusCode: 400,
            message: 'Simulation failed'
          }
          return response
        }
        else {
          winston.info('Simulation quasi newton successfully finish')
          //checked estimation params
          const cekiranaPolja = [Boolean(parameters.kb_fiting), Boolean(parameters.kb_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting), Boolean(parameters.k1_fiting), Boolean(parameters.k1_minus_fiting),
          Boolean(parameters.k2_fiting), Boolean(parameters.k2_minus_fiting), true];
          //save input data
          return loopback.getModel('quasi_newton').create(options)
            .then(data => {
              //call function for create output data
              return returnOutputData.returnQuasi(cekiranaPolja)
                .then(outputData => {
                  //call function for zip output data
                  zippingFile.zipFileSimanQuasi('quasi_newton', cekiranaPolja, parameters, outputData);

                  //return output data
                  return resolve(outputData);
                })
            });
        }
      });
    });
    return promise
  }

  quasi_newton.saveData = (id, parameters) => {
    //checked estimation params
    const cekiranaPolja = [Boolean(parameters.kb_fiting), Boolean(parameters.kb_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting), Boolean(parameters.k1_fiting), Boolean(parameters.k1_minus_fiting),
    Boolean(parameters.k2_fiting), Boolean(parameters.k2_minus_fiting), true];

    return returnOutputData.returnQuasi(cekiranaPolja)
      .then(outputData => {
        if (!outputData && outputData !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for quasi newton simulation'
          }
          winston.info('Not found output data for quasi newton simulation')
          return response
        }
        const quasi_newtonParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel(quasi_newton).create(quasi_newtonParams)
          .then(atp => {
            const options = {
              userID: id,
              input_id: atp._id,
              ...outputData
            }
            return loopback.getModel('quasi_newton_output').create(options)
              .then(data => {
                winston.info('Simulation quasi newton successfully save data')
                return data;
              })
          })
      })
  }

  quasi_newton.returnSaveData = (id) => {
    return loopback.getModel('quasi_newton_output').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found quasi newton simulation'
          }
          winston.info('Not found return data for quasi newton simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id
        return loopback.getModel('quasi_newton').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found quasi newton simulation'
              }
              winston.info('Not found input data for quasi newton simulation')
              return response
            }
            const response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response
          })
      })
  }

  quasi_newton.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    }
    return loopback.getModel('quasi_newton').create(options)
      .then(data => {
        winston.info('Simulation quasi newton successfully save input data')
        return data;
      });
  }

  quasi_newton.returnInputData = (id) => {
    return loopback.getModel('quasi_newton').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found quasi newton input data`
          }
          winston.info('Not found quasi newton input data')
          return response
        }
        return data[data.length - 1];
      })
  }

  quasi_newton.saveDefaulValue = (id, parameters) => {
    const options = {
      userID: id,
      default_values: true,
      ...parameters
    }
    return loopback.getModel('quasi_newton').create(options)
      .then(data => {
        winston.info('Simulation quasi newton successfully save default values')
        return data;
      })
  }

  quasi_newton.returnSaveDefaulValue = (id) => {
    return loopback.getModel('quasi_newton').find({ where: { userID: id } })
      .then(data => {
        if(!data) {
          const response = {
            statusCode: 404,
            message: `Not found quasi newton default values`
          } 
          winston.info('Not found quasi newton default values')
          return response
        }
        return data[data.length - 1];
      })
  }

  quasi_newton.download = (id, res) => {
    winston.info('Simulation quasi newton successfully download data')
    res.download('./quasi_newton/output_files.zip');
  };

  quasi_newton.upload = (id, req, res) => {
    var storage = multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './quasi_newton');
      },
      filename: function (req, file, callback) {
        callback(null, 'a_t.txt');
      }
    });

    let upload = multer({ storage: storage }).single('file');

    upload(req, res, function (err) {
      if (err) {
        winston.info(err)
        return res.end({ message: "Error uploading file." });
      }
      winston.info('Simulation quasi newton successfully update a_t')
      res.json({ message: "File is uploaded" });
    });
  }

  /** API for run simulation */
  quasi_newton.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  quasi_newton.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  quasi_newton.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  /** API for return input data for simulation */
  quasi_newton.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  quasi_newton.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  quasi_newton.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  quasi_newton.remoteMethod('upload',
    {
      http: { verb: 'POST', path: '/users/:id/upload' },
      accepts: [
        { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
        { arg: 'req', type: 'object', http: { source: 'req' } },
        { arg: 'res', type: 'object', http: { source: 'res' } }
      ],
      returns: { arg: 'data', type: 'string', root: true }
    }
  );

  quasi_newton.remoteMethod('saveDefaulValue', {
    http: { verb: 'POST', path: '/users/:id/saveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  quasi_newton.remoteMethod('returnSaveDefaulValue', {
    http: { verb: 'GET', path: '/users/:id/returnSaveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  quasi_newton.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download', 'upload', 'returnSaveDefaulValue', 'saveDefaulValue'], method.name)) {
      quasi_newton.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}