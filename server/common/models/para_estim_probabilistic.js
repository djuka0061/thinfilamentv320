'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');

//Local include

module.exports = function (para_estim_probabilistic) {

    //function for run simulation
    para_estim_probabilistic.runSimulation = (
        userID, s, kb_plus, kb_minus, kt_plus, kt_minus, k1_plus, k1_minus, k2_plus, k2_minus, actin, myosin, type_of_simulation, epsilon, number_of_iteration, final_myosin,
        kb_fiting, kb_guess_of_params1, kb_guess_of_params2, kb_lower_bound, kb_upper_bound,
        kb_minus_fiting, kb_minus_guess_of_params1, kb_minus_guess_of_params2, kb_minus_lower_bound, kb_minus_upper_bound,
        kt_fiting, kt_guess_of_params1, kt_guess_of_params2, kt_lower_bound, kt_upper_bound,
        kt_minus_fiting, kt_minus_guess_of_params1, kt_minus_guess_of_params2, kt_minus_lower_bound, kt_minus_upper_bound,
        k1_fiting, k1_guess_of_params1, k1_guess_of_params2, k1_lower_bound, k1_upper_bound,
        k1_minus_fiting, k1_minus_guess_of_params1, k1_minus_guess_of_params2, k1_minus_lower_bound, k1_minus_upper_bound,
        k2_fiting, k2_guess_of_params1, k2_guess_of_params2, k2_lower_bound, k2_upper_bound,
        k2_minus_fiting, k2_minus_guess_of_params1, k2_minus_guess_of_params2, k2_minus_lower_bound, k2_minus_upper_bound
        ) => {
        
        //create input file
        let writeStream = fs.createWriteStream('./para_estim_probabilistic/tmtn-ca.dat');
        writeStream.write('../para_estim_probabilistic_out/Simu_output\n');
        writeStream.write(s + '\n');
        writeStream.write(kb_plus + '\n');
        writeStream.write(kb_minus + '\n');
        writeStream.write(kt_plus + '\n');
        writeStream.write(kt_minus + '\n');
        writeStream.write(k1_plus + '\n');
        writeStream.write(k1_minus + '\n');
        writeStream.write(k2_plus + '\n');
        writeStream.write(k2_minus + '\n');
        writeStream.write(actin + '\n');
        writeStream.write(myosin + '\n');
        writeStream.end((parameters.final_myosin ? parameters.final_myosin : parameters.myosin) + '\n');

        let writeInputWrap = fs.createWriteStream('./para_estim_probabilistic/input_wrap')
        writeInputWrap.write('8\n');
        writeInputWrap.write((kb_fiting ? kb_guess_of_params1 : kb_plus) + '\n');
        writeInputWrap.write((kb_fiting ? kb_guess_of_params2 : kb_plus) + '\n');
        writeInputWrap.write((kb_fiting ? kb_lower_bound : kb_plus) + '\n');
        writeInputWrap.write((kb_fiting ? kb_upper_bound : kb_plus) + '\n');
        writeInputWrap.write((kb_minus_fiting ? kb_minus_guess_of_params1 : kb_minus) + '\n');
        writeInputWrap.write((kb_minus_fiting ? kb_minus_guess_of_params2 : kb_minus) + '\n');
        writeInputWrap.write((kb_minus_fiting ? kb_minus_lower_bound : kb_minus) + '\n');
        writeInputWrap.write((kb_minus_fiting ? kb_minus_upper_bound : kb_minus) + '\n');
        writeInputWrap.write((kt_fiting ? kt_guess_of_params1 : kt_plus) + '\n');
        writeInputWrap.write((kt_fiting ? kt_guess_of_params2 : kt_plus) + '\n');
        writeInputWrap.write((kt_fiting ? kt_lower_bound : kt_plus) + '\n');
        writeInputWrap.write((kt_fiting ? kt_upper_bound : kt_plus) + '\n');
        writeInputWrap.write((kt_minus_fiting ? kt_minus_guess_of_params1 : kt_minus) + '\n');
        writeInputWrap.write((kt_minus_fiting ? kt_minus_guess_of_params2 : kt_minus) + '\n');
        writeInputWrap.write((kt_minus_fiting ? kt_minus_lower_bound : kt_minus) + '\n');
        writeInputWrap.write((kt_minus_fiting ? kt_minus_upper_bound : kt_minus) + '\n');
        writeInputWrap.write((k1_fiting ? k1_guess_of_params1 : k1_plus) + '\n');
        writeInputWrap.write((k1_fiting ? k1_guess_of_params2 : k1_plus) + '\n');
        writeInputWrap.write((k1_fiting ? k1_lower_bound : k1_plus) + '\n');
        writeInputWrap.write((k1_fiting ? k1_upper_bound : k1_plus) + '\n');
        writeInputWrap.write((k1_minus_fiting ? k1_minus_guess_of_params1 : k1_minus) + '\n');
        writeInputWrap.write((k1_minus_fiting ? k1_minus_guess_of_params2 : k1_minus) + '\n');
        writeInputWrap.write((k1_minus_fiting ? k1_minus_lower_bound : k1_minus) + '\n');
        writeInputWrap.write((k1_minus_fiting ? k1_minus_upper_bound : k1_minus) + '\n');
        writeInputWrap.write((k2_fiting ? k2_guess_of_params1 : k2_plus) + '\n');
        writeInputWrap.write((k2_fiting ? k2_guess_of_params2 : k2_plus) + '\n');
        writeInputWrap.write((k2_fiting ? k2_lower_bound : k2_plus) + '\n');
        writeInputWrap.write((k2_fiting ? k2_upper_bound : k2_plus) + '\n');
        writeInputWrap.write((k2_minus_fiting ? k2_minus_guess_of_params1 : k2_minus) + '\n');
        writeInputWrap.write((k2_minus_fiting ? k2_minus_guess_of_params2 : k2_minus) + '\n');
        writeInputWrap.write((k2_minus_fiting ? k2_minus_lower_bound : k2_minus) + '\n');
        writeInputWrap.write((k2_minus_fiting ? k2_minus_upper_bound : k2_minus) + '\n');
        writeInputWrap.write(epsilon +'\n');
        writeInputWrap.write(number_of_iteration +'\n');
        writeInputWrap.write('./para_estim_probabilistic/a_t.txt' +'\n');
        writeInputWrap.write('./para_estim_probabilistic/kkk.dat' +'\n');
        writeInputWrap.write('./para_estim_probabilistic/eee.dat' +'\n');
        writeInputWrap.write('./para_estim_probabilistic/rrr.dat' +'\n');
        writeInputWrap.end('num_rigid' +'\n');

        //setup data for start simulation
        const fileName = 'wraper.exe';
        const params = ['input_warp.dat'];
        const path = './para_estim_probabilistic/'
        let promise = new Promise((resolve, reject) => {
            exec(fileName, params, { cwd: path }, (err, data) => {
                if (err) reject(err);
                /*else {
                    const simulationPath = fs.readFileSync('./para_estim_probabilistic_out/5.DAT').toString().split('\n');
                    let simulation = [];
                    simulationPath.forEach(dat => {
                        const splitData = dat.split('      ');
                        simulation.push({
                            col1: splitData[1],
                            col2: splitData[2],
                            col3: splitData[3],
                            col4: splitData[4],
                        });
                    });
                    const options = {
                        userID: userID,
                        simulation: simulation
                    };
                    return loopback.getModel('para_estim_probabilistic_out').create(options)
                    .then(simulationData => {
                        const histogramPath = fs.readFileSync('./para_estim_probabilistic_out/5-histogram.DAT').toString().split('\n');
                        let histogram = [];
                        histogramPath.forEach(dat => {
                            const splitData = dat.split('      ');
                            histogram.push({
                                col1: splitData[1],
                                col2: splitData[2],
                                col3: splitData[3],
                                col4: splitData[4],
                                col5: splitData[5],
                                col6: splitData[6],
                                col7: splitData[7],
                                col8: splitData[8],
                            });
                        });
                        const options = {
                            userID: userID,
                            histogram: histogram
                        };
                        return loopback.getModel('para_estim_probabilistic_out_histogram').create(options)
                        .then(histogramData => {
                            return resolve(data);
                        })
                    })
                }*/
            });
    
        });
        return promise;
    }

    //Save input data for simulation
    para_estim_probabilistic.saveInputData = (
        userID, s, kb_plus, kb_minus, kt_plus, kt_minus, k1_plus, k1_minus, k2_plus, k2_minus, actin, myosin, type_of_simulation, epsilon, number_of_iteration, final_myosin,
        kb_fiting, kb_guess_of_params1, kb_guess_of_params2, kb_lower_bound, kb_upper_bound,
        kb_minus_fiting, kb_minus_guess_of_params1, kb_minus_guess_of_params2, kb_minus_lower_bound, kb_minus_upper_bound,
        kt_fiting, kt_guess_of_params1, kt_guess_of_params2, kt_lower_bound, kt_upper_bound,
        kt_minus_fiting, kt_minus_guess_of_params1, kt_minus_guess_of_params2, kt_minus_lower_bound, kt_minus_upper_bound,
        k1_fiting, k1_guess_of_params1, k1_guess_of_params2, k1_lower_bound, k1_upper_bound,
        k1_minus_fiting, k1_minus_guess_of_params1, k1_minus_guess_of_params2, k1_minus_lower_bound, k1_minus_upper_bound,
        k2_fiting, k2_guess_of_params1, k2_guess_of_params2, k2_lower_bound, k2_upper_bound,
        k2_minus_fiting, k2_minus_guess_of_params1, k2_minus_guess_of_params2, k2_minus_lower_bound, k2_minus_upper_bound
    ) => {
        const options = {
            userID, s, kb_plus, kb_minus, kt_plus, kt_minus, k1_plus, k1_minus, k2_plus, k2_minus, actin, myosin, type_of_simulation, epsilon, number_of_iteration, final_myosin,
        kb_fiting, kb_guess_of_params1, kb_guess_of_params2, kb_lower_bound, kb_upper_bound,
        kb_minus_fiting, kb_minus_guess_of_params1, kb_minus_guess_of_params2, kb_minus_lower_bound, kb_minus_upper_bound,
        kt_fiting, kt_guess_of_params1, kt_guess_of_params2, kt_lower_bound, kt_upper_bound,
        kt_minus_fiting, kt_minus_guess_of_params1, kt_minus_guess_of_params2, kt_minus_lower_bound, kt_minus_upper_bound,
        k1_fiting, k1_guess_of_params1, k1_guess_of_params2, k1_lower_bound, k1_upper_bound,
        k1_minus_fiting, k1_minus_guess_of_params1, k1_minus_guess_of_params2, k1_minus_lower_bound, k1_minus_upper_bound,
        k2_fiting, k2_guess_of_params1, k2_guess_of_params2, k2_lower_bound, k2_upper_bound,
        k2_minus_fiting, k2_minus_guess_of_params1, k2_minus_guess_of_params2, k2_minus_lower_bound, k2_minus_upper_bound
        };
        return loopback.getModel('para_estim_probabilistic').create(options)
        .then( data => {
            return data;
        });
    };


    para_estim_probabilistic.returnInputData = (userID) => {
        return loopback.getModel('para_estim_probabilistic').findOne({ where: { userID: userID }})
        .then(data => {
            return data;
        }) 
    }

    /*para_estim_probabilistic.returnOutPutData = (userID) => {
        const path = './para_estim_probabilistic_out/';
        const data = fs.readFileSync('./para_estim_probabilistic_out/5.DAT').toString().split('\n');
        let test2 = [];
        data.forEach(dat => {
            const test = dat.split('      ');
            test2.push({
                col1: test[1],
                col2: test[2],
                col3: test[3],
                col4: test[4],
            });
        });
        return test2;
    }*/

    /** API for run simulation */
    para_estim_probabilistic.remoteMethod('runSimulation', {
        http: { verb: 'POST', path: '/runSimulation' },
        accepts: [
            { arg: 'userID', type: 'string', required: true, description: '' },
            { arg: 's', type: 'number', required: true, description: '' },
            { arg: 'kb_plus', type: 'number', required: true, description: '' },
            { arg: 'kb_minus', type: 'number', required: true, description: '' },
            { arg: 'kt_plus', type: 'number', required: true, description: '' },
            { arg: 'kt_minus', type: 'number', required: true, description: '' },
            { arg: 'k1_plus', type: 'string', required: true, description: '' },
            { arg: 'k1_minus', type: 'number', required: true, description: '' },
            { arg: 'k2_plus', type: 'number', required: true, description: '' },
            { arg: 'k2_minus', type: 'number', required: true, description: '' },
            { arg: 'actin', type: 'number', required: true, description: '' },
            { arg: 'myosin', type: 'number', required: true, description: '' },
            { arg: 'type_of_simulation', type: 'string', required: true, description: '' },
            { arg: 'epsilon', type: 'number', required: false, description: '' },
            { arg: 'number_of_iteration', type: 'number', required: false, description: '' },
            { arg: 'final_myosin', type: 'number', required: false, description: '' },
            { arg: 'kb_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'kb_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'kb_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'kb_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'kb_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'kb_minus_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'kb_minus_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'kb_minus_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'kb_minus_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'kb_minus_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'kt_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'kt_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'kt_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'kt_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'kt_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'kt_minus_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'kt_minus_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'kt_minus_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'kt_minus_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'kt_minus_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'k1_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'k1_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'k1_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'k1_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'k1_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'k1_minus_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'k1_minus_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'k1_minus_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'k1_minus_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'k1_minus_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'k2_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'k2_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'k2_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'k2_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'k2_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'k2_minus_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'k2_minus_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'k2_minus_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'k2_minus_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'k2_minus_upper_bound', type: 'number', required: false, description: '' }
         ],
        returns: { arg: 'data', type: 'object', root: true },
    });

    para_estim_probabilistic.remoteMethod('saveInputData', {
        http: { verb: 'POST', path: '/saveInputData' },
        accepts: [
            { arg: 'userID', type: 'string', required: true, description: '' },
            { arg: 's', type: 'number', required: true, description: '' },
            { arg: 'kb_plus', type: 'number', required: true, description: '' },
            { arg: 'kb_minus', type: 'number', required: true, description: '' },
            { arg: 'kt_plus', type: 'number', required: true, description: '' },
            { arg: 'kt_minus', type: 'number', required: true, description: '' },
            { arg: 'k1_plus', type: 'string', required: true, description: '' },
            { arg: 'k1_minus', type: 'number', required: true, description: '' },
            { arg: 'k2_plus', type: 'number', required: true, description: '' },
            { arg: 'k2_minus', type: 'number', required: true, description: '' },
            { arg: 'actin', type: 'number', required: true, description: '' },
            { arg: 'myosin', type: 'number', required: true, description: '' },
            { arg: 'type_of_simulation', type: 'string', required: true, description: '' },
            { arg: 'epsilon', type: 'number', required: false, description: '' },
            { arg: 'number_of_iteration', type: 'number', required: false, description: '' },
            { arg: 'final_myosin', type: 'number', required: false, description: '' },
            { arg: 'kb_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'kb_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'kb_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'kb_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'kb_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'kb_minus_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'kb_minus_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'kb_minus_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'kb_minus_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'kb_minus_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'kt_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'kt_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'kt_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'kt_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'kt_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'kt_minus_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'kt_minus_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'kt_minus_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'kt_minus_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'kt_minus_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'k1_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'k1_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'k1_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'k1_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'k1_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'k1_minus_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'k1_minus_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'k1_minus_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'k1_minus_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'k1_minus_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'k2_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'k2_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'k2_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'k2_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'k2_upper_bound', type: 'number', required: false, description: '' },
            { arg: 'k2_minus_fiting', type: 'boolean', required: false, description: '' },
            { arg: 'k2_minus_guess_of_params1', type: 'number', required: false, description: '' },
            { arg: 'k2_minus_guess_of_params2', type: 'number', required: false, description: '' },
            { arg: 'k2_minus_lower_bound', type: 'number', required: false, description: '' },
            { arg: 'k2_minus_upper_bound', type: 'number', required: false, description: '' }
         ],
        returns: { arg: 'data', type: 'object', root: true },
    });



      /** API for return input data for simulation */
      para_estim_probabilistic.remoteMethod('returnInputData', {
        http: { verb: 'GET', path: '/returnInputData' },
        accepts: [
            { arg: 'userID', type: 'string', required: true, description: '' }
        ],
        returns: { arg: 'data', type: 'object', root: true }
      });

      /** API for return output data for simulation */
      /*para_estim_probabilistic.remoteMethod('returnOutPutData', {
        http: { verb: 'GET', path: '/returnOutPutData' },
        accepts: [
            { arg: 'userID', type: 'string', required: true, description: '' }
        ],
        returns: { arg: 'data', type: 'object', root: true }
      });*/

      para_estim_probabilistic.sharedClass.methods().forEach((method) => {
        if (!_.contains(['runSimulation', 'saveInputData', 'returnInputData'], method.name)) {
            para_estim_probabilistic.disableRemoteMethodByName(method.name, method.isStatic);
        }
      });
}