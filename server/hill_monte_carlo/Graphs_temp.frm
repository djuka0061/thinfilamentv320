VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{D940E4E4-6079-11CE-88CB-0020AF6845F6}#1.6#0"; "cwui.ocx"
Begin VB.Form Graphs_temp 
   Caption         =   "Graphs"
   ClientHeight    =   8775
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14280
   LinkTopic       =   "Form1"
   ScaleHeight     =   8775
   ScaleWidth      =   14280
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton APComB1 
      Caption         =   "Add Plot"
      Height          =   495
      Left            =   12720
      TabIndex        =   5
      Top             =   1440
      Width           =   1215
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Time Course"
      Height          =   255
      Index           =   0
      Left            =   12600
      TabIndex        =   4
      Top             =   360
      Value           =   -1  'True
      Width           =   1455
   End
   Begin VB.OptionButton Option1 
      Caption         =   "Titration"
      Height          =   255
      Index           =   1
      Left            =   12600
      TabIndex        =   3
      Top             =   720
      Width           =   1095
   End
   Begin VB.CommandButton CloseB 
      Caption         =   "Close"
      Height          =   495
      Left            =   12720
      TabIndex        =   2
      Top             =   2880
      Width           =   1215
   End
   Begin VB.CommandButton Print_Button 
      Caption         =   "Print Graph"
      Height          =   555
      Left            =   12720
      TabIndex        =   1
      Top             =   2160
      Visible         =   0   'False
      Width           =   1200
   End
   Begin CWUIControlsLib.CWGraph CWGraph1 
      Height          =   8100
      Left            =   210
      TabIndex        =   0
      Top             =   105
      Width           =   11355
      _Version        =   458752
      _ExtentX        =   20029
      _ExtentY        =   14287
      _StockProps     =   71
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Reset_0         =   0   'False
      CompatibleVers_0=   458752
      Graph_0         =   1
      ClassName_1     =   "CCWGraphFrame"
      opts_1          =   62
      C[0]_1          =   16777215
      C[1]_1          =   -2147483643
      Event_1         =   2
      ClassName_2     =   "CCWGFPlotEvent"
      Owner_2         =   1
      Plots_1         =   3
      ClassName_3     =   "CCWDataPlots"
      Array_3         =   4
      Editor_3        =   4
      ClassName_4     =   "CCWGFPlotArrayEditor"
      Owner_4         =   1
      Array[0]_3      =   5
      ClassName_5     =   "CCWDataPlot"
      opts_5          =   4194367
      Name_5          =   "Plot-1"
      C[0]_5          =   8388736
      C[1]_5          =   8388736
      C[2]_5          =   8388736
      C[3]_5          =   8388736
      Event_5         =   2
      X_5             =   6
      ClassName_6     =   "CCWAxis"
      opts_6          =   575
      Name_6          =   "XAxis"
      Orientation_6   =   2944
      format_6        =   7
      ClassName_7     =   "CCWFormat"
      Scale_6         =   8
      ClassName_8     =   "CCWScale"
      opts_8          =   90112
      rMin_8          =   25
      rMax_8          =   725
      dMax_8          =   10
      discInterval_8  =   1
      Radial_6        =   0
      Enum_6          =   9
      ClassName_9     =   "CCWEnum"
      Editor_9        =   10
      ClassName_10    =   "CCWEnumArrayEditor"
      Owner_10        =   6
      Font_6          =   0
      tickopts_6      =   2711
      major_6         =   1
      minor_6         =   0.5
      Caption_6       =   11
      ClassName_11    =   "CCWDrawObj"
      opts_11         =   62
      C[0]_11         =   -2147483640
      Image_11        =   12
      ClassName_12    =   "CCWTextImage"
      style_12        =   16777217
      font_12         =   0
      Animator_11     =   0
      Blinker_11      =   0
      Y_5             =   13
      ClassName_13    =   "CCWAxis"
      opts_13         =   575
      Name_13         =   "YAxis-1"
      C[2]_13         =   8388736
      Orientation_13  =   2067
      format_13       =   14
      ClassName_14    =   "CCWFormat"
      Scale_13        =   15
      ClassName_15    =   "CCWScale"
      opts_15         =   122880
      rMin_15         =   11
      rMax_15         =   513
      dMax_15         =   10
      discInterval_15 =   1
      Radial_13       =   0
      Enum_13         =   16
      ClassName_16    =   "CCWEnum"
      Editor_16       =   17
      ClassName_17    =   "CCWEnumArrayEditor"
      Owner_17        =   13
      Font_13         =   0
      tickopts_13     =   2711
      major_13        =   1
      minor_13        =   0.5
      Caption_13      =   18
      ClassName_18    =   "CCWDrawObj"
      opts_18         =   62
      C[0]_18         =   8388736
      Image_18        =   19
      ClassName_19    =   "CCWTextImage"
      style_19        =   16777217
      font_19         =   0
      Animator_18     =   0
      Blinker_18      =   0
      LineStyle_5     =   1
      LineWidth_5     =   1
      BasePlot_5      =   0
      DefaultXInc_5   =   1
      DefaultPlotPerRow_5=   -1  'True
      Array[1]_3      =   20
      ClassName_20    =   "CCWDataPlot"
      opts_20         =   4194367
      Name_20         =   "Plot-2"
      C[0]_20         =   32768
      C[1]_20         =   32768
      C[2]_20         =   32768
      C[3]_20         =   32768
      Event_20        =   2
      X_20            =   6
      Y_20            =   21
      ClassName_21    =   "CCWAxis"
      opts_21         =   575
      Name_21         =   "YAxis-2"
      C[2]_21         =   32768
      Orientation_21  =   2076
      format_21       =   22
      ClassName_22    =   "CCWFormat"
      Scale_21        =   23
      ClassName_23    =   "CCWScale"
      opts_23         =   122880
      rMin_23         =   11
      rMax_23         =   513
      dMax_23         =   100
      discInterval_23 =   1
      Radial_21       =   0
      Enum_21         =   24
      ClassName_24    =   "CCWEnum"
      Editor_24       =   25
      ClassName_25    =   "CCWEnumArrayEditor"
      Owner_25        =   21
      Font_21         =   0
      tickopts_21     =   2711
      major_21        =   10
      minor_21        =   5
      Caption_21      =   26
      ClassName_26    =   "CCWDrawObj"
      opts_26         =   62
      C[0]_26         =   32768
      Image_26        =   27
      ClassName_27    =   "CCWTextImage"
      font_27         =   0
      Animator_26     =   0
      Blinker_26      =   0
      LineStyle_20    =   1
      LineWidth_20    =   1
      BasePlot_20     =   0
      DefaultXInc_20  =   1
      DefaultPlotPerRow_20=   -1  'True
      Array[2]_3      =   28
      ClassName_28    =   "CCWDataPlot"
      opts_28         =   4194367
      Name_28         =   "Plot-3"
      C[0]_28         =   16711680
      C[1]_28         =   16711680
      C[2]_28         =   16711680
      C[3]_28         =   16711680
      Event_28        =   2
      X_28            =   6
      Y_28            =   21
      LineStyle_28    =   1
      LineWidth_28    =   1
      BasePlot_28     =   0
      DefaultXInc_28  =   1
      DefaultPlotPerRow_28=   -1  'True
      Array[3]_3      =   29
      ClassName_29    =   "CCWDataPlot"
      opts_29         =   4194367
      Name_29         =   "Plot-4"
      C[0]_29         =   255
      C[1]_29         =   255
      C[2]_29         =   255
      C[3]_29         =   255
      Event_29        =   2
      X_29            =   6
      Y_29            =   21
      LineStyle_29    =   1
      LineWidth_29    =   1
      BasePlot_29     =   0
      DefaultXInc_29  =   1
      DefaultPlotPerRow_29=   -1  'True
      Axes_1          =   30
      ClassName_30    =   "CCWAxes"
      Array_30        =   3
      Editor_30       =   31
      ClassName_31    =   "CCWGFAxisArrayEditor"
      Owner_31        =   1
      Array[0]_30     =   6
      Array[1]_30     =   13
      Array[2]_30     =   21
      DefaultPlot_1   =   32
      ClassName_32    =   "CCWDataPlot"
      opts_32         =   4194367
      Name_32         =   "[Template]"
      C[0]_32         =   65280
      C[1]_32         =   255
      C[2]_32         =   16711680
      C[3]_32         =   16776960
      Event_32        =   2
      X_32            =   6
      Y_32            =   13
      LineStyle_32    =   1
      LineWidth_32    =   1
      BasePlot_32     =   0
      DefaultXInc_32  =   1
      DefaultPlotPerRow_32=   -1  'True
      Cursors_1       =   33
      ClassName_33    =   "CCWCursors"
      Editor_33       =   34
      ClassName_34    =   "CCWGFCursorArrayEditor"
      Owner_34        =   1
      TrackMode_1     =   2
      GraphBackground_1=   0
      GraphFrame_1    =   35
      ClassName_35    =   "CCWDrawObj"
      opts_35         =   62
      C[0]_35         =   -2147483643
      C[1]_35         =   -2147483643
      Image_35        =   36
      ClassName_36    =   "CCWPictImage"
      opts_36         =   1280
      Rows_36         =   1
      Cols_36         =   1
      F_36            =   -2147483643
      ColorReplaceWith_36=   8421504
      ColorReplace_36 =   8421504
      Tolerance_36    =   2
      Animator_35     =   0
      Blinker_35      =   0
      PlotFrame_1     =   37
      ClassName_37    =   "CCWDrawObj"
      opts_37         =   62
      C[0]_37         =   -2147483643
      C[1]_37         =   16777215
      Image_37        =   38
      ClassName_38    =   "CCWPictImage"
      opts_38         =   1280
      Rows_38         =   1
      Cols_38         =   1
      Pict_38         =   1
      F_38            =   -2147483643
      B_38            =   16777215
      ColorReplaceWith_38=   8421504
      ColorReplace_38 =   8421504
      Tolerance_38    =   2
      Animator_37     =   0
      Blinker_37      =   0
      Caption_1       =   39
      ClassName_39    =   "CCWDrawObj"
      opts_39         =   62
      C[0]_39         =   -2147483640
      Image_39        =   40
      ClassName_40    =   "CCWTextImage"
      font_40         =   0
      Animator_39     =   0
      Blinker_39      =   0
      DefaultXInc_1   =   1
      DefaultPlotPerRow_1=   -1  'True
      Bindings_1      =   41
      ClassName_41    =   "CCWBindingHolderArray"
      Editor_41       =   42
      ClassName_42    =   "CCWBindingHolderArrayEditor"
      Owner_42        =   1
      Annotations_1   =   43
      ClassName_43    =   "CCWAnnotations"
      Array_43        =   4
      Editor_43       =   44
      ClassName_44    =   "CCWAnnotationArrayEditor"
      Owner_44        =   1
      Array[0]_43     =   45
      ClassName_45    =   "CCWAnnotation"
      opts_45         =   63
      Name_45         =   "Annotation-1"
      CoordinateType_45=   2
      Plot_45         =   32
      Text_45         =   "Time vs. Fraction of Unbound Actin"
      TextXPoint_45   =   450
      TextYPoint_45   =   100
      TextColor_45    =   8388736
      TextFont_45     =   46
      ClassName_46    =   "CCWFont"
      bFont_46        =   -1  'True
      BeginProperty Font_46 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_45 =   47
      ClassName_47    =   "CDataBuffer"
      Type_47         =   5
      m_cDims;_47     =   1
      ShapeYPoints_45 =   48
      ClassName_48    =   "CDataBuffer"
      Type_48         =   5
      m_cDims;_48     =   1
      ShapeFillColor_45=   16777215
      ShapeLineColor_45=   16777215
      ShapeLineWidth_45=   1
      ShapeLineStyle_45=   1
      ShapePointStyle_45=   10
      ShapeImage_45   =   49
      ClassName_49    =   "CCWDrawObj"
      opts_49         =   62
      Image_49        =   50
      ClassName_50    =   "CCWPictImage"
      opts_50         =   1280
      Rows_50         =   1
      Cols_50         =   1
      Pict_50         =   7
      F_50            =   -2147483633
      B_50            =   -2147483633
      ColorReplaceWith_50=   8421504
      ColorReplace_50 =   8421504
      Tolerance_50    =   2
      Animator_49     =   0
      Blinker_49      =   0
      ArrowColor_45   =   8388608
      ArrowWidth_45   =   1
      ArrowLineStyle_45=   1
      ArrowHeadStyle_45=   1
      Array[1]_43     =   51
      ClassName_51    =   "CCWAnnotation"
      opts_51         =   63
      Name_51         =   "Annotation-2"
      CoordinateType_51=   2
      Plot_51         =   32
      Text_51         =   "Time vs. Fraction of S1 Bound"
      TextXPoint_51   =   450
      TextYPoint_51   =   125
      TextColor_51    =   32768
      TextFont_51     =   52
      ClassName_52    =   "CCWFont"
      bFont_52        =   -1  'True
      BeginProperty Font_52 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_51 =   53
      ClassName_53    =   "CDataBuffer"
      Type_53         =   5
      m_cDims;_53     =   1
      ShapeYPoints_51 =   54
      ClassName_54    =   "CDataBuffer"
      Type_54         =   5
      m_cDims;_54     =   1
      ShapeFillColor_51=   16777215
      ShapeLineColor_51=   16777215
      ShapeLineWidth_51=   1
      ShapeLineStyle_51=   1
      ShapePointStyle_51=   10
      ShapeImage_51   =   55
      ClassName_55    =   "CCWDrawObj"
      opts_55         =   62
      Image_55        =   56
      ClassName_56    =   "CCWPictImage"
      opts_56         =   1280
      Rows_56         =   1
      Cols_56         =   1
      Pict_56         =   7
      F_56            =   -2147483633
      B_56            =   -2147483633
      ColorReplaceWith_56=   8421504
      ColorReplace_56 =   8421504
      Tolerance_56    =   2
      Animator_55     =   0
      Blinker_55      =   0
      ArrowColor_51   =   8388736
      ArrowWidth_51   =   1
      ArrowLineStyle_51=   1
      ArrowHeadStyle_51=   1
      Array[2]_43     =   57
      ClassName_57    =   "CCWAnnotation"
      opts_57         =   63
      Name_57         =   "Annotation-3"
      CoordinateType_57=   2
      Plot_57         =   32
      Text_57         =   "Time vs. Fraction of S1 in A state"
      TextXPoint_57   =   450
      TextYPoint_57   =   150
      TextColor_57    =   16711680
      TextFont_57     =   58
      ClassName_58    =   "CCWFont"
      bFont_58        =   -1  'True
      BeginProperty Font_58 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_57 =   59
      ClassName_59    =   "CDataBuffer"
      Type_59         =   5
      m_cDims;_59     =   1
      ShapeYPoints_57 =   60
      ClassName_60    =   "CDataBuffer"
      Type_60         =   5
      m_cDims;_60     =   1
      ShapeFillColor_57=   16777215
      ShapeLineColor_57=   16777215
      ShapeLineWidth_57=   1
      ShapeLineStyle_57=   1
      ShapePointStyle_57=   10
      ShapeImage_57   =   61
      ClassName_61    =   "CCWDrawObj"
      opts_61         =   62
      Image_61        =   62
      ClassName_62    =   "CCWPictImage"
      opts_62         =   1280
      Rows_62         =   1
      Cols_62         =   1
      Pict_62         =   7
      F_62            =   -2147483633
      B_62            =   -2147483633
      ColorReplaceWith_62=   8421504
      ColorReplace_62 =   8421504
      Tolerance_62    =   2
      Animator_61     =   0
      Blinker_61      =   0
      ArrowColor_57   =   255
      ArrowWidth_57   =   1
      ArrowLineStyle_57=   1
      ArrowHeadStyle_57=   1
      Array[3]_43     =   63
      ClassName_63    =   "CCWAnnotation"
      opts_63         =   63
      Name_63         =   "Annotation-4"
      CoordinateType_63=   2
      Plot_63         =   32
      Text_63         =   "Time vs. Fraction of S1 in R state"
      TextXPoint_63   =   450
      TextYPoint_63   =   175
      TextColor_63    =   255
      TextFont_63     =   64
      ClassName_64    =   "CCWFont"
      bFont_64        =   -1  'True
      BeginProperty Font_64 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_63 =   65
      ClassName_65    =   "CDataBuffer"
      Type_65         =   5
      m_cDims;_65     =   1
      ShapeYPoints_63 =   66
      ClassName_66    =   "CDataBuffer"
      Type_66         =   5
      m_cDims;_66     =   1
      ShapeFillColor_63=   16777215
      ShapeLineColor_63=   16777215
      ShapeLineWidth_63=   1
      ShapeLineStyle_63=   1
      ShapePointStyle_63=   10
      ShapeImage_63   =   67
      ClassName_67    =   "CCWDrawObj"
      opts_67         =   62
      Image_67        =   68
      ClassName_68    =   "CCWPictImage"
      opts_68         =   1280
      Rows_68         =   1
      Cols_68         =   1
      Pict_68         =   7
      F_68            =   -2147483633
      B_68            =   -2147483633
      ColorReplaceWith_68=   8421504
      ColorReplace_68 =   8421504
      Tolerance_68    =   2
      Animator_67     =   0
      Blinker_67      =   0
      ArrowColor_63   =   32768
      ArrowWidth_63   =   1
      ArrowLineStyle_63=   1
      ArrowHeadStyle_63=   1
      AnnotationTemplate_1=   69
      ClassName_69    =   "CCWAnnotation"
      opts_69         =   63
      Name_69         =   "[Template]"
      Plot_69         =   32
      Text_69         =   "[Template]"
      TextXPoint_69   =   6.7
      TextYPoint_69   =   6.7
      TextColor_69    =   16777215
      TextFont_69     =   70
      ClassName_70    =   "CCWFont"
      bFont_70        =   -1  'True
      BeginProperty Font_70 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_69 =   71
      ClassName_71    =   "CDataBuffer"
      Type_71         =   5
      m_cDims;_71     =   1
      ShapeYPoints_69 =   72
      ClassName_72    =   "CDataBuffer"
      Type_72         =   5
      m_cDims;_72     =   1
      ShapeFillColor_69=   16777215
      ShapeLineColor_69=   16777215
      ShapeLineWidth_69=   1
      ShapeLineStyle_69=   1
      ShapePointStyle_69=   10
      ShapeImage_69   =   73
      ClassName_73    =   "CCWDrawObj"
      opts_73         =   62
      Image_73        =   74
      ClassName_74    =   "CCWPictImage"
      opts_74         =   1280
      Rows_74         =   1
      Cols_74         =   1
      Pict_74         =   7
      F_74            =   -2147483633
      B_74            =   -2147483633
      ColorReplaceWith_74=   8421504
      ColorReplace_74 =   8421504
      Tolerance_74    =   2
      Animator_73     =   0
      Blinker_73      =   0
      ArrowColor_69   =   16777215
      ArrowWidth_69   =   1
      ArrowLineStyle_69=   1
      ArrowHeadStyle_69=   1
   End
   Begin MSComDlg.CommonDialog opencd1 
      Left            =   11880
      Top             =   1560
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Frame Frame6 
      Caption         =   "Type"
      Height          =   1095
      Left            =   11760
      TabIndex        =   6
      Top             =   120
      Width           =   2415
   End
End
Attribute VB_Name = "Graphs_temp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim Time() As Double
Dim CM() As Double
Dim FCM() As Double
Dim FLU() As Double
Dim OCU() As Double
Dim Unbound_Actin() As Double
'Dim Plot_Value() As Variant
    


Private Sub APComB1_Click()
    Dim OutputFileName, OutputFileTitle As String
    Dim Msg As String

    Dim i As Long, j As Long
    Dim OpenFileNumber As Integer                                   ' Import data to a file
    Dim num_Rows As Long
    Dim SearchText As String

    ' Set CancelError is True
    opencd1.CancelError = True
    On Error GoTo ErrHandler
    ' Set flags
    opencd1.Flags = cdlOFNHideReadOnly
    ' Set filters
    opencd1.Filter = " *.dat |*"
    ' Specify default filter
    opencd1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the Save dialog box
    opencd1.ShowSave
    ' Display name of selected file (with the path)
    OutputFileName = opencd1.FileName
    ' Display name of selected file (without the path)
    OutputFileTitle = opencd1.FileTitle
    
    
    num_Rows = 0
    OpenFileNumber = FreeFile
    Open OutputFileName For Input As #OpenFileNumber          ' Open file

    While Not EOF(OpenFileNumber)
        Line Input #OpenFileNumber, SearchText
        num_Rows = num_Rows + 1
    Wend

    
        ReDim Time(1 To num_Rows - 1)
        ReDim CM(1 To num_Rows - 1)
        ReDim FCM(1 To num_Rows - 1)
        ReDim OCU(1 To num_Rows - 1)
        ReDim FLU(1 To num_Rows - 1)
        ReDim Unbound_Actin(1 To num_Rows - 1)
        'ReDim Plot_Value(1 To num_Rows - 1, 1 To 2)

        
        Seek #OpenFileNumber, 1
        Line Input #OpenFileNumber, SearchText          ' First Line
        i = 0
        While Not EOF(OpenFileNumber)
            i = i + 1
            Input #OpenFileNumber, Time(i), CM(i), FCM(i), FLU(i), _
                                OCU(i)
        Wend
        Close #OpenFileNumber
    
        For i = 1 To num_Rows - 1
            FLU(i) = 1# - FLU(i) / FLU(num_Rows - 1)
            OCU(i) = OCU(i) / OCU(num_Rows - 1)
            Unbound_Actin(i) = 1# - OCU(i)
        Next i

    
        'CWGraph1.Plots.Item(3).PlotXvsY Time, Unbound_Actin
        CWGraph1.Plots.Item(1).PlotXvsY Time, FLU
        'CWGraph1.Plots.Item(2).PlotXvsY Time, OCU
        'CWGraph1.Plots.Item(4).PlotXvsY Time, FCM

        'CWGraph1.Annotations.Item(3).Caption.Text = "Time vs. Fraction of Unbound Actin"
        CWGraph1.Annotations.Item(1).Caption.Text = "Time vs. Fractional Fluorescence"
        'CWGraph1.Annotations.Item(2).Caption.Text = "Time vs. Fraction Saturation"
        'CWGraph1.Annotations.Item(4).Caption.Text = "Time vs. FreeMyosin Concentration"

        CWGraph1.Axes.Item(1).Caption = "Time [s]"
        CWGraph1.Axes.Item(2).Caption = "Fractional Fluorescence"
        'CWGraph1.Axes.Item(3).Visible = True
        'CWGraph1.Axes.Item(3).Caption = "Fraction of Others"

        CWGraph1.Axes.Item(1).AutoScaleNow
        CWGraph1.Axes.Item(2).AutoScaleNow
        'CWGraph1.Axes.Item(3).AutoScaleNow

      
    
ErrHandler:
    'User pressed the Cancel button
    Exit Sub
End Sub

Private Sub CloseB_Click()
    Graphs_temp.Hide
    Para_MGNUM_Form.Show
End Sub

Private Sub Form_Load()
    CWGraph1.ClearData
End Sub

Private Sub Option1_Click(Index As Integer)
    Select Case Index
    Case 0
        Option1(0).Value = False
        Option1(1).Value = True
    Case 1
        Option1(1).Value = False
        Option1(0).Value = True
    End Select
End Sub

Private Sub Print_Button_Click()
    Dim BeginPage, EndPage, NumCopies, Orientation, i

    ' Set Cancel to True.
    CommonDialog1.CancelError = True
    On Error GoTo ErrHandler
    ' Display the Print dialog box.
    CommonDialog1.ShowPrinter
    ' Get user-selected values from the dialog box.
    BeginPage = CommonDialog1.FromPage
    EndPage = CommonDialog1.ToPage
    NumCopies = CommonDialog1.Copies
    Orientation = CommonDialog1.Orientation
    For i = 1 To NumCopies
    ' Put code here to send data to your printer.
    Next
    Exit Sub
ErrHandler:
    ' User pressed Cancel button.
    Exit Sub

End Sub

