(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-widgets-widgets-module"],{

/***/ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js":
/*!************************************************************************************************!*\
  !*** ./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
   true ? factory(exports) :
  undefined;
}(this, (function (exports) { 'use strict';

  /**
   * --------------------------------------------------------------------------
   * CoreUI Plugins - Custom Tooltips for Chart.js (v1.2.0): custom-tooltips.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */
  function CustomTooltips(tooltipModel) {
    var _this = this;

    // Add unique id if not exist
    var _setCanvasId = function _setCanvasId() {
      var _idMaker = function _idMaker() {
        var _hex = 16;
        var _multiplier = 0x10000;
        return ((1 + Math.random()) * _multiplier | 0).toString(_hex);
      };

      var _canvasId = "_canvas-" + (_idMaker() + _idMaker());

      _this._chart.canvas.id = _canvasId;
      return _canvasId;
    };

    var ClassName = {
      ABOVE: 'above',
      BELOW: 'below',
      CHARTJS_TOOLTIP: 'chartjs-tooltip',
      NO_TRANSFORM: 'no-transform',
      TOOLTIP_BODY: 'tooltip-body',
      TOOLTIP_BODY_ITEM: 'tooltip-body-item',
      TOOLTIP_BODY_ITEM_COLOR: 'tooltip-body-item-color',
      TOOLTIP_BODY_ITEM_LABEL: 'tooltip-body-item-label',
      TOOLTIP_BODY_ITEM_VALUE: 'tooltip-body-item-value',
      TOOLTIP_HEADER: 'tooltip-header',
      TOOLTIP_HEADER_ITEM: 'tooltip-header-item'
    };
    var Selector = {
      DIV: 'div',
      SPAN: 'span',
      TOOLTIP: (this._chart.canvas.id || _setCanvasId()) + "-tooltip"
    };
    var tooltip = document.getElementById(Selector.TOOLTIP);

    if (!tooltip) {
      tooltip = document.createElement('div');
      tooltip.id = Selector.TOOLTIP;
      tooltip.className = ClassName.CHARTJS_TOOLTIP;

      this._chart.canvas.parentNode.appendChild(tooltip);
    } // Hide if no tooltip


    if (tooltipModel.opacity === 0) {
      tooltip.style.opacity = 0;
      return;
    } // Set caret Position


    tooltip.classList.remove(ClassName.ABOVE, ClassName.BELOW, ClassName.NO_TRANSFORM);

    if (tooltipModel.yAlign) {
      tooltip.classList.add(tooltipModel.yAlign);
    } else {
      tooltip.classList.add(ClassName.NO_TRANSFORM);
    } // Set Text


    if (tooltipModel.body) {
      var titleLines = tooltipModel.title || [];
      var tooltipHeader = document.createElement(Selector.DIV);
      tooltipHeader.className = ClassName.TOOLTIP_HEADER;
      titleLines.forEach(function (title) {
        var tooltipHeaderTitle = document.createElement(Selector.DIV);
        tooltipHeaderTitle.className = ClassName.TOOLTIP_HEADER_ITEM;
        tooltipHeaderTitle.innerHTML = title;
        tooltipHeader.appendChild(tooltipHeaderTitle);
      });
      var tooltipBody = document.createElement(Selector.DIV);
      tooltipBody.className = ClassName.TOOLTIP_BODY;
      var tooltipBodyItems = tooltipModel.body.map(function (item) {
        return item.lines;
      });
      tooltipBodyItems.forEach(function (item, i) {
        var tooltipBodyItem = document.createElement(Selector.DIV);
        tooltipBodyItem.className = ClassName.TOOLTIP_BODY_ITEM;
        var colors = tooltipModel.labelColors[i];
        var tooltipBodyItemColor = document.createElement(Selector.SPAN);
        tooltipBodyItemColor.className = ClassName.TOOLTIP_BODY_ITEM_COLOR;
        tooltipBodyItemColor.style.backgroundColor = colors.backgroundColor;
        tooltipBodyItem.appendChild(tooltipBodyItemColor);

        if (item[0].split(':').length > 1) {
          var tooltipBodyItemLabel = document.createElement(Selector.SPAN);
          tooltipBodyItemLabel.className = ClassName.TOOLTIP_BODY_ITEM_LABEL;
          tooltipBodyItemLabel.innerHTML = item[0].split(': ')[0];
          tooltipBodyItem.appendChild(tooltipBodyItemLabel);
          var tooltipBodyItemValue = document.createElement(Selector.SPAN);
          tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          tooltipBodyItemValue.innerHTML = item[0].split(': ').pop();
          tooltipBodyItem.appendChild(tooltipBodyItemValue);
        } else {
          var _tooltipBodyItemValue = document.createElement(Selector.SPAN);

          _tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          _tooltipBodyItemValue.innerHTML = item[0];
          tooltipBodyItem.appendChild(_tooltipBodyItemValue);
        }

        tooltipBody.appendChild(tooltipBodyItem);
      });
      tooltip.innerHTML = '';
      tooltip.appendChild(tooltipHeader);
      tooltip.appendChild(tooltipBody);
    }

    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft; // Display, position, and set styles for font

    tooltip.style.opacity = 1;
    tooltip.style.left = positionX + tooltipModel.caretX + "px";
    tooltip.style.top = positionY + tooltipModel.caretY + "px";
  }

  exports.CustomTooltips = CustomTooltips;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=custom-tooltips.js.map


/***/ }),

/***/ "./node_modules/@coreui/coreui/dist/js/coreui-utilities.js":
/*!*****************************************************************!*\
  !*** ./node_modules/@coreui/coreui/dist/js/coreui-utilities.js ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

/*!
  * CoreUI v2.1.9 (https://coreui.io)
  * Copyright 2019 Łukasz Holeczek
  * Licensed under MIT (https://coreui.io)
  */
(function (global, factory) {
   true ? factory(exports) :
  undefined;
}(this, function (exports) { 'use strict';

  /**
   * --------------------------------------------------------------------------
   * CoreUI Utilities (v2.1.9): classes.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */
  var sidebarCssClasses = ['sidebar-show', 'sidebar-sm-show', 'sidebar-md-show', 'sidebar-lg-show', 'sidebar-xl-show'];
  var asideMenuCssClasses = ['aside-menu-show', 'aside-menu-sm-show', 'aside-menu-md-show', 'aside-menu-lg-show', 'aside-menu-xl-show'];
  var validBreakpoints = ['sm', 'md', 'lg', 'xl'];
  function checkBreakpoint(breakpoint, list) {
    return list.indexOf(breakpoint) > -1;
  }

  function createCommonjsModule(fn, module) {
  	return module = { exports: {} }, fn(module, module.exports), module.exports;
  }

  var _core = createCommonjsModule(function (module) {
  var core = module.exports = { version: '2.6.5' };
  if (typeof __e == 'number') __e = core; // eslint-disable-line no-undef
  });
  var _core_1 = _core.version;

  var _global = createCommonjsModule(function (module) {
  // https://github.com/zloirock/core-js/issues/86#issuecomment-115759028
  var global = module.exports = typeof window != 'undefined' && window.Math == Math
    ? window : typeof self != 'undefined' && self.Math == Math ? self
    // eslint-disable-next-line no-new-func
    : Function('return this')();
  if (typeof __g == 'number') __g = global; // eslint-disable-line no-undef
  });

  var _shared = createCommonjsModule(function (module) {
  var SHARED = '__core-js_shared__';
  var store = _global[SHARED] || (_global[SHARED] = {});

  (module.exports = function (key, value) {
    return store[key] || (store[key] = value !== undefined ? value : {});
  })('versions', []).push({
    version: _core.version,
    mode: 'global',
    copyright: '© 2019 Denis Pushkarev (zloirock.ru)'
  });
  });

  var id = 0;
  var px = Math.random();
  var _uid = function (key) {
    return 'Symbol('.concat(key === undefined ? '' : key, ')_', (++id + px).toString(36));
  };

  var _wks = createCommonjsModule(function (module) {
  var store = _shared('wks');

  var Symbol = _global.Symbol;
  var USE_SYMBOL = typeof Symbol == 'function';

  var $exports = module.exports = function (name) {
    return store[name] || (store[name] =
      USE_SYMBOL && Symbol[name] || (USE_SYMBOL ? Symbol : _uid)('Symbol.' + name));
  };

  $exports.store = store;
  });

  var _isObject = function (it) {
    return typeof it === 'object' ? it !== null : typeof it === 'function';
  };

  var _anObject = function (it) {
    if (!_isObject(it)) throw TypeError(it + ' is not an object!');
    return it;
  };

  var _fails = function (exec) {
    try {
      return !!exec();
    } catch (e) {
      return true;
    }
  };

  // Thank's IE8 for his funny defineProperty
  var _descriptors = !_fails(function () {
    return Object.defineProperty({}, 'a', { get: function () { return 7; } }).a != 7;
  });

  var document$1 = _global.document;
  // typeof document.createElement is 'object' in old IE
  var is = _isObject(document$1) && _isObject(document$1.createElement);
  var _domCreate = function (it) {
    return is ? document$1.createElement(it) : {};
  };

  var _ie8DomDefine = !_descriptors && !_fails(function () {
    return Object.defineProperty(_domCreate('div'), 'a', { get: function () { return 7; } }).a != 7;
  });

  // 7.1.1 ToPrimitive(input [, PreferredType])

  // instead of the ES6 spec version, we didn't implement @@toPrimitive case
  // and the second argument - flag - preferred type is a string
  var _toPrimitive = function (it, S) {
    if (!_isObject(it)) return it;
    var fn, val;
    if (S && typeof (fn = it.toString) == 'function' && !_isObject(val = fn.call(it))) return val;
    if (typeof (fn = it.valueOf) == 'function' && !_isObject(val = fn.call(it))) return val;
    if (!S && typeof (fn = it.toString) == 'function' && !_isObject(val = fn.call(it))) return val;
    throw TypeError("Can't convert object to primitive value");
  };

  var dP = Object.defineProperty;

  var f = _descriptors ? Object.defineProperty : function defineProperty(O, P, Attributes) {
    _anObject(O);
    P = _toPrimitive(P, true);
    _anObject(Attributes);
    if (_ie8DomDefine) try {
      return dP(O, P, Attributes);
    } catch (e) { /* empty */ }
    if ('get' in Attributes || 'set' in Attributes) throw TypeError('Accessors not supported!');
    if ('value' in Attributes) O[P] = Attributes.value;
    return O;
  };

  var _objectDp = {
  	f: f
  };

  var _propertyDesc = function (bitmap, value) {
    return {
      enumerable: !(bitmap & 1),
      configurable: !(bitmap & 2),
      writable: !(bitmap & 4),
      value: value
    };
  };

  var _hide = _descriptors ? function (object, key, value) {
    return _objectDp.f(object, key, _propertyDesc(1, value));
  } : function (object, key, value) {
    object[key] = value;
    return object;
  };

  // 22.1.3.31 Array.prototype[@@unscopables]
  var UNSCOPABLES = _wks('unscopables');
  var ArrayProto = Array.prototype;
  if (ArrayProto[UNSCOPABLES] == undefined) _hide(ArrayProto, UNSCOPABLES, {});
  var _addToUnscopables = function (key) {
    ArrayProto[UNSCOPABLES][key] = true;
  };

  var _iterStep = function (done, value) {
    return { value: value, done: !!done };
  };

  var _iterators = {};

  var toString = {}.toString;

  var _cof = function (it) {
    return toString.call(it).slice(8, -1);
  };

  // fallback for non-array-like ES3 and non-enumerable old V8 strings

  // eslint-disable-next-line no-prototype-builtins
  var _iobject = Object('z').propertyIsEnumerable(0) ? Object : function (it) {
    return _cof(it) == 'String' ? it.split('') : Object(it);
  };

  // 7.2.1 RequireObjectCoercible(argument)
  var _defined = function (it) {
    if (it == undefined) throw TypeError("Can't call method on  " + it);
    return it;
  };

  // to indexed object, toObject with fallback for non-array-like ES3 strings


  var _toIobject = function (it) {
    return _iobject(_defined(it));
  };

  var hasOwnProperty = {}.hasOwnProperty;
  var _has = function (it, key) {
    return hasOwnProperty.call(it, key);
  };

  var _functionToString = _shared('native-function-to-string', Function.toString);

  var _redefine = createCommonjsModule(function (module) {
  var SRC = _uid('src');

  var TO_STRING = 'toString';
  var TPL = ('' + _functionToString).split(TO_STRING);

  _core.inspectSource = function (it) {
    return _functionToString.call(it);
  };

  (module.exports = function (O, key, val, safe) {
    var isFunction = typeof val == 'function';
    if (isFunction) _has(val, 'name') || _hide(val, 'name', key);
    if (O[key] === val) return;
    if (isFunction) _has(val, SRC) || _hide(val, SRC, O[key] ? '' + O[key] : TPL.join(String(key)));
    if (O === _global) {
      O[key] = val;
    } else if (!safe) {
      delete O[key];
      _hide(O, key, val);
    } else if (O[key]) {
      O[key] = val;
    } else {
      _hide(O, key, val);
    }
  // add fake Function#toString for correct work wrapped methods / constructors with methods like LoDash isNative
  })(Function.prototype, TO_STRING, function toString() {
    return typeof this == 'function' && this[SRC] || _functionToString.call(this);
  });
  });

  var _aFunction = function (it) {
    if (typeof it != 'function') throw TypeError(it + ' is not a function!');
    return it;
  };

  // optional / simple context binding

  var _ctx = function (fn, that, length) {
    _aFunction(fn);
    if (that === undefined) return fn;
    switch (length) {
      case 1: return function (a) {
        return fn.call(that, a);
      };
      case 2: return function (a, b) {
        return fn.call(that, a, b);
      };
      case 3: return function (a, b, c) {
        return fn.call(that, a, b, c);
      };
    }
    return function (/* ...args */) {
      return fn.apply(that, arguments);
    };
  };

  var PROTOTYPE = 'prototype';

  var $export = function (type, name, source) {
    var IS_FORCED = type & $export.F;
    var IS_GLOBAL = type & $export.G;
    var IS_STATIC = type & $export.S;
    var IS_PROTO = type & $export.P;
    var IS_BIND = type & $export.B;
    var target = IS_GLOBAL ? _global : IS_STATIC ? _global[name] || (_global[name] = {}) : (_global[name] || {})[PROTOTYPE];
    var exports = IS_GLOBAL ? _core : _core[name] || (_core[name] = {});
    var expProto = exports[PROTOTYPE] || (exports[PROTOTYPE] = {});
    var key, own, out, exp;
    if (IS_GLOBAL) source = name;
    for (key in source) {
      // contains in native
      own = !IS_FORCED && target && target[key] !== undefined;
      // export native or passed
      out = (own ? target : source)[key];
      // bind timers to global for call from export context
      exp = IS_BIND && own ? _ctx(out, _global) : IS_PROTO && typeof out == 'function' ? _ctx(Function.call, out) : out;
      // extend global
      if (target) _redefine(target, key, out, type & $export.U);
      // export
      if (exports[key] != out) _hide(exports, key, exp);
      if (IS_PROTO && expProto[key] != out) expProto[key] = out;
    }
  };
  _global.core = _core;
  // type bitmap
  $export.F = 1;   // forced
  $export.G = 2;   // global
  $export.S = 4;   // static
  $export.P = 8;   // proto
  $export.B = 16;  // bind
  $export.W = 32;  // wrap
  $export.U = 64;  // safe
  $export.R = 128; // real proto method for `library`
  var _export = $export;

  // 7.1.4 ToInteger
  var ceil = Math.ceil;
  var floor = Math.floor;
  var _toInteger = function (it) {
    return isNaN(it = +it) ? 0 : (it > 0 ? floor : ceil)(it);
  };

  // 7.1.15 ToLength

  var min = Math.min;
  var _toLength = function (it) {
    return it > 0 ? min(_toInteger(it), 0x1fffffffffffff) : 0; // pow(2, 53) - 1 == 9007199254740991
  };

  var max = Math.max;
  var min$1 = Math.min;
  var _toAbsoluteIndex = function (index, length) {
    index = _toInteger(index);
    return index < 0 ? max(index + length, 0) : min$1(index, length);
  };

  // false -> Array#indexOf
  // true  -> Array#includes



  var _arrayIncludes = function (IS_INCLUDES) {
    return function ($this, el, fromIndex) {
      var O = _toIobject($this);
      var length = _toLength(O.length);
      var index = _toAbsoluteIndex(fromIndex, length);
      var value;
      // Array#includes uses SameValueZero equality algorithm
      // eslint-disable-next-line no-self-compare
      if (IS_INCLUDES && el != el) while (length > index) {
        value = O[index++];
        // eslint-disable-next-line no-self-compare
        if (value != value) return true;
      // Array#indexOf ignores holes, Array#includes - not
      } else for (;length > index; index++) if (IS_INCLUDES || index in O) {
        if (O[index] === el) return IS_INCLUDES || index || 0;
      } return !IS_INCLUDES && -1;
    };
  };

  var shared = _shared('keys');

  var _sharedKey = function (key) {
    return shared[key] || (shared[key] = _uid(key));
  };

  var arrayIndexOf = _arrayIncludes(false);
  var IE_PROTO = _sharedKey('IE_PROTO');

  var _objectKeysInternal = function (object, names) {
    var O = _toIobject(object);
    var i = 0;
    var result = [];
    var key;
    for (key in O) if (key != IE_PROTO) _has(O, key) && result.push(key);
    // Don't enum bug & hidden keys
    while (names.length > i) if (_has(O, key = names[i++])) {
      ~arrayIndexOf(result, key) || result.push(key);
    }
    return result;
  };

  // IE 8- don't enum bug keys
  var _enumBugKeys = (
    'constructor,hasOwnProperty,isPrototypeOf,propertyIsEnumerable,toLocaleString,toString,valueOf'
  ).split(',');

  // 19.1.2.14 / 15.2.3.14 Object.keys(O)



  var _objectKeys = Object.keys || function keys(O) {
    return _objectKeysInternal(O, _enumBugKeys);
  };

  var _objectDps = _descriptors ? Object.defineProperties : function defineProperties(O, Properties) {
    _anObject(O);
    var keys = _objectKeys(Properties);
    var length = keys.length;
    var i = 0;
    var P;
    while (length > i) _objectDp.f(O, P = keys[i++], Properties[P]);
    return O;
  };

  var document$2 = _global.document;
  var _html = document$2 && document$2.documentElement;

  // 19.1.2.2 / 15.2.3.5 Object.create(O [, Properties])



  var IE_PROTO$1 = _sharedKey('IE_PROTO');
  var Empty = function () { /* empty */ };
  var PROTOTYPE$1 = 'prototype';

  // Create object with fake `null` prototype: use iframe Object with cleared prototype
  var createDict = function () {
    // Thrash, waste and sodomy: IE GC bug
    var iframe = _domCreate('iframe');
    var i = _enumBugKeys.length;
    var lt = '<';
    var gt = '>';
    var iframeDocument;
    iframe.style.display = 'none';
    _html.appendChild(iframe);
    iframe.src = 'javascript:'; // eslint-disable-line no-script-url
    // createDict = iframe.contentWindow.Object;
    // html.removeChild(iframe);
    iframeDocument = iframe.contentWindow.document;
    iframeDocument.open();
    iframeDocument.write(lt + 'script' + gt + 'document.F=Object' + lt + '/script' + gt);
    iframeDocument.close();
    createDict = iframeDocument.F;
    while (i--) delete createDict[PROTOTYPE$1][_enumBugKeys[i]];
    return createDict();
  };

  var _objectCreate = Object.create || function create(O, Properties) {
    var result;
    if (O !== null) {
      Empty[PROTOTYPE$1] = _anObject(O);
      result = new Empty();
      Empty[PROTOTYPE$1] = null;
      // add "__proto__" for Object.getPrototypeOf polyfill
      result[IE_PROTO$1] = O;
    } else result = createDict();
    return Properties === undefined ? result : _objectDps(result, Properties);
  };

  var def = _objectDp.f;

  var TAG = _wks('toStringTag');

  var _setToStringTag = function (it, tag, stat) {
    if (it && !_has(it = stat ? it : it.prototype, TAG)) def(it, TAG, { configurable: true, value: tag });
  };

  var IteratorPrototype = {};

  // 25.1.2.1.1 %IteratorPrototype%[@@iterator]()
  _hide(IteratorPrototype, _wks('iterator'), function () { return this; });

  var _iterCreate = function (Constructor, NAME, next) {
    Constructor.prototype = _objectCreate(IteratorPrototype, { next: _propertyDesc(1, next) });
    _setToStringTag(Constructor, NAME + ' Iterator');
  };

  // 7.1.13 ToObject(argument)

  var _toObject = function (it) {
    return Object(_defined(it));
  };

  // 19.1.2.9 / 15.2.3.2 Object.getPrototypeOf(O)


  var IE_PROTO$2 = _sharedKey('IE_PROTO');
  var ObjectProto = Object.prototype;

  var _objectGpo = Object.getPrototypeOf || function (O) {
    O = _toObject(O);
    if (_has(O, IE_PROTO$2)) return O[IE_PROTO$2];
    if (typeof O.constructor == 'function' && O instanceof O.constructor) {
      return O.constructor.prototype;
    } return O instanceof Object ? ObjectProto : null;
  };

  var ITERATOR = _wks('iterator');
  var BUGGY = !([].keys && 'next' in [].keys()); // Safari has buggy iterators w/o `next`
  var FF_ITERATOR = '@@iterator';
  var KEYS = 'keys';
  var VALUES = 'values';

  var returnThis = function () { return this; };

  var _iterDefine = function (Base, NAME, Constructor, next, DEFAULT, IS_SET, FORCED) {
    _iterCreate(Constructor, NAME, next);
    var getMethod = function (kind) {
      if (!BUGGY && kind in proto) return proto[kind];
      switch (kind) {
        case KEYS: return function keys() { return new Constructor(this, kind); };
        case VALUES: return function values() { return new Constructor(this, kind); };
      } return function entries() { return new Constructor(this, kind); };
    };
    var TAG = NAME + ' Iterator';
    var DEF_VALUES = DEFAULT == VALUES;
    var VALUES_BUG = false;
    var proto = Base.prototype;
    var $native = proto[ITERATOR] || proto[FF_ITERATOR] || DEFAULT && proto[DEFAULT];
    var $default = $native || getMethod(DEFAULT);
    var $entries = DEFAULT ? !DEF_VALUES ? $default : getMethod('entries') : undefined;
    var $anyNative = NAME == 'Array' ? proto.entries || $native : $native;
    var methods, key, IteratorPrototype;
    // Fix native
    if ($anyNative) {
      IteratorPrototype = _objectGpo($anyNative.call(new Base()));
      if (IteratorPrototype !== Object.prototype && IteratorPrototype.next) {
        // Set @@toStringTag to native iterators
        _setToStringTag(IteratorPrototype, TAG, true);
        // fix for some old engines
        if (typeof IteratorPrototype[ITERATOR] != 'function') _hide(IteratorPrototype, ITERATOR, returnThis);
      }
    }
    // fix Array#{values, @@iterator}.name in V8 / FF
    if (DEF_VALUES && $native && $native.name !== VALUES) {
      VALUES_BUG = true;
      $default = function values() { return $native.call(this); };
    }
    // Define iterator
    if (BUGGY || VALUES_BUG || !proto[ITERATOR]) {
      _hide(proto, ITERATOR, $default);
    }
    // Plug for library
    _iterators[NAME] = $default;
    _iterators[TAG] = returnThis;
    if (DEFAULT) {
      methods = {
        values: DEF_VALUES ? $default : getMethod(VALUES),
        keys: IS_SET ? $default : getMethod(KEYS),
        entries: $entries
      };
      if (FORCED) for (key in methods) {
        if (!(key in proto)) _redefine(proto, key, methods[key]);
      } else _export(_export.P + _export.F * (BUGGY || VALUES_BUG), NAME, methods);
    }
    return methods;
  };

  // 22.1.3.4 Array.prototype.entries()
  // 22.1.3.13 Array.prototype.keys()
  // 22.1.3.29 Array.prototype.values()
  // 22.1.3.30 Array.prototype[@@iterator]()
  var es6_array_iterator = _iterDefine(Array, 'Array', function (iterated, kind) {
    this._t = _toIobject(iterated); // target
    this._i = 0;                   // next index
    this._k = kind;                // kind
  // 22.1.5.2.1 %ArrayIteratorPrototype%.next()
  }, function () {
    var O = this._t;
    var kind = this._k;
    var index = this._i++;
    if (!O || index >= O.length) {
      this._t = undefined;
      return _iterStep(1);
    }
    if (kind == 'keys') return _iterStep(0, index);
    if (kind == 'values') return _iterStep(0, O[index]);
    return _iterStep(0, [index, O[index]]);
  }, 'values');

  // argumentsList[@@iterator] is %ArrayProto_values% (9.4.4.6, 9.4.4.7)
  _iterators.Arguments = _iterators.Array;

  _addToUnscopables('keys');
  _addToUnscopables('values');
  _addToUnscopables('entries');

  var ITERATOR$1 = _wks('iterator');
  var TO_STRING_TAG = _wks('toStringTag');
  var ArrayValues = _iterators.Array;

  var DOMIterables = {
    CSSRuleList: true, // TODO: Not spec compliant, should be false.
    CSSStyleDeclaration: false,
    CSSValueList: false,
    ClientRectList: false,
    DOMRectList: false,
    DOMStringList: false,
    DOMTokenList: true,
    DataTransferItemList: false,
    FileList: false,
    HTMLAllCollection: false,
    HTMLCollection: false,
    HTMLFormElement: false,
    HTMLSelectElement: false,
    MediaList: true, // TODO: Not spec compliant, should be false.
    MimeTypeArray: false,
    NamedNodeMap: false,
    NodeList: true,
    PaintRequestList: false,
    Plugin: false,
    PluginArray: false,
    SVGLengthList: false,
    SVGNumberList: false,
    SVGPathSegList: false,
    SVGPointList: false,
    SVGStringList: false,
    SVGTransformList: false,
    SourceBufferList: false,
    StyleSheetList: true, // TODO: Not spec compliant, should be false.
    TextTrackCueList: false,
    TextTrackList: false,
    TouchList: false
  };

  for (var collections = _objectKeys(DOMIterables), i = 0; i < collections.length; i++) {
    var NAME = collections[i];
    var explicit = DOMIterables[NAME];
    var Collection = _global[NAME];
    var proto = Collection && Collection.prototype;
    var key;
    if (proto) {
      if (!proto[ITERATOR$1]) _hide(proto, ITERATOR$1, ArrayValues);
      if (!proto[TO_STRING_TAG]) _hide(proto, TO_STRING_TAG, NAME);
      _iterators[NAME] = ArrayValues;
      if (explicit) for (key in es6_array_iterator) if (!proto[key]) _redefine(proto, key, es6_array_iterator[key], true);
    }
  }

  // getting tag from 19.1.3.6 Object.prototype.toString()

  var TAG$1 = _wks('toStringTag');
  // ES3 wrong here
  var ARG = _cof(function () { return arguments; }()) == 'Arguments';

  // fallback for IE11 Script Access Denied error
  var tryGet = function (it, key) {
    try {
      return it[key];
    } catch (e) { /* empty */ }
  };

  var _classof = function (it) {
    var O, T, B;
    return it === undefined ? 'Undefined' : it === null ? 'Null'
      // @@toStringTag case
      : typeof (T = tryGet(O = Object(it), TAG$1)) == 'string' ? T
      // builtinTag case
      : ARG ? _cof(O)
      // ES3 arguments fallback
      : (B = _cof(O)) == 'Object' && typeof O.callee == 'function' ? 'Arguments' : B;
  };

  // 19.1.3.6 Object.prototype.toString()

  var test = {};
  test[_wks('toStringTag')] = 'z';
  if (test + '' != '[object z]') {
    _redefine(Object.prototype, 'toString', function toString() {
      return '[object ' + _classof(this) + ']';
    }, true);
  }

  // most Object methods by ES6 should accept primitives



  var _objectSap = function (KEY, exec) {
    var fn = (_core.Object || {})[KEY] || Object[KEY];
    var exp = {};
    exp[KEY] = exec(fn);
    _export(_export.S + _export.F * _fails(function () { fn(1); }), 'Object', exp);
  };

  // 19.1.2.14 Object.keys(O)



  _objectSap('keys', function () {
    return function keys(it) {
      return _objectKeys(_toObject(it));
    };
  });

  var f$1 = Object.getOwnPropertySymbols;

  var _objectGops = {
  	f: f$1
  };

  var f$2 = {}.propertyIsEnumerable;

  var _objectPie = {
  	f: f$2
  };

  // 19.1.2.1 Object.assign(target, source, ...)





  var $assign = Object.assign;

  // should work with symbols and should have deterministic property order (V8 bug)
  var _objectAssign = !$assign || _fails(function () {
    var A = {};
    var B = {};
    // eslint-disable-next-line no-undef
    var S = Symbol();
    var K = 'abcdefghijklmnopqrst';
    A[S] = 7;
    K.split('').forEach(function (k) { B[k] = k; });
    return $assign({}, A)[S] != 7 || Object.keys($assign({}, B)).join('') != K;
  }) ? function assign(target, source) { // eslint-disable-line no-unused-vars
    var T = _toObject(target);
    var aLen = arguments.length;
    var index = 1;
    var getSymbols = _objectGops.f;
    var isEnum = _objectPie.f;
    while (aLen > index) {
      var S = _iobject(arguments[index++]);
      var keys = getSymbols ? _objectKeys(S).concat(getSymbols(S)) : _objectKeys(S);
      var length = keys.length;
      var j = 0;
      var key;
      while (length > j) if (isEnum.call(S, key = keys[j++])) T[key] = S[key];
    } return T;
  } : $assign;

  // 19.1.3.1 Object.assign(target, source)


  _export(_export.S + _export.F, 'Object', { assign: _objectAssign });

  var deepObjectsMerge = function deepObjectsMerge(target, source) {
    // Iterate through `source` properties and if an `Object` set property to merge of `target` and `source` properties
    var _arr = Object.keys(source);

    for (var _i = 0; _i < _arr.length; _i++) {
      var key = _arr[_i];

      if (source[key] instanceof Object) {
        Object.assign(source[key], deepObjectsMerge(target[key], source[key]));
      }
    } // Join `target` and modified `source`


    Object.assign(target || {}, source);
    return target;
  };

  // true  -> String#at
  // false -> String#codePointAt
  var _stringAt = function (TO_STRING) {
    return function (that, pos) {
      var s = String(_defined(that));
      var i = _toInteger(pos);
      var l = s.length;
      var a, b;
      if (i < 0 || i >= l) return TO_STRING ? '' : undefined;
      a = s.charCodeAt(i);
      return a < 0xd800 || a > 0xdbff || i + 1 === l || (b = s.charCodeAt(i + 1)) < 0xdc00 || b > 0xdfff
        ? TO_STRING ? s.charAt(i) : a
        : TO_STRING ? s.slice(i, i + 2) : (a - 0xd800 << 10) + (b - 0xdc00) + 0x10000;
    };
  };

  var at = _stringAt(true);

   // `AdvanceStringIndex` abstract operation
  // https://tc39.github.io/ecma262/#sec-advancestringindex
  var _advanceStringIndex = function (S, index, unicode) {
    return index + (unicode ? at(S, index).length : 1);
  };

  var builtinExec = RegExp.prototype.exec;

   // `RegExpExec` abstract operation
  // https://tc39.github.io/ecma262/#sec-regexpexec
  var _regexpExecAbstract = function (R, S) {
    var exec = R.exec;
    if (typeof exec === 'function') {
      var result = exec.call(R, S);
      if (typeof result !== 'object') {
        throw new TypeError('RegExp exec method returned something other than an Object or null');
      }
      return result;
    }
    if (_classof(R) !== 'RegExp') {
      throw new TypeError('RegExp#exec called on incompatible receiver');
    }
    return builtinExec.call(R, S);
  };

  // 21.2.5.3 get RegExp.prototype.flags

  var _flags = function () {
    var that = _anObject(this);
    var result = '';
    if (that.global) result += 'g';
    if (that.ignoreCase) result += 'i';
    if (that.multiline) result += 'm';
    if (that.unicode) result += 'u';
    if (that.sticky) result += 'y';
    return result;
  };

  var nativeExec = RegExp.prototype.exec;
  // This always refers to the native implementation, because the
  // String#replace polyfill uses ./fix-regexp-well-known-symbol-logic.js,
  // which loads this file before patching the method.
  var nativeReplace = String.prototype.replace;

  var patchedExec = nativeExec;

  var LAST_INDEX = 'lastIndex';

  var UPDATES_LAST_INDEX_WRONG = (function () {
    var re1 = /a/,
        re2 = /b*/g;
    nativeExec.call(re1, 'a');
    nativeExec.call(re2, 'a');
    return re1[LAST_INDEX] !== 0 || re2[LAST_INDEX] !== 0;
  })();

  // nonparticipating capturing group, copied from es5-shim's String#split patch.
  var NPCG_INCLUDED = /()??/.exec('')[1] !== undefined;

  var PATCH = UPDATES_LAST_INDEX_WRONG || NPCG_INCLUDED;

  if (PATCH) {
    patchedExec = function exec(str) {
      var re = this;
      var lastIndex, reCopy, match, i;

      if (NPCG_INCLUDED) {
        reCopy = new RegExp('^' + re.source + '$(?!\\s)', _flags.call(re));
      }
      if (UPDATES_LAST_INDEX_WRONG) lastIndex = re[LAST_INDEX];

      match = nativeExec.call(re, str);

      if (UPDATES_LAST_INDEX_WRONG && match) {
        re[LAST_INDEX] = re.global ? match.index + match[0].length : lastIndex;
      }
      if (NPCG_INCLUDED && match && match.length > 1) {
        // Fix browsers whose `exec` methods don't consistently return `undefined`
        // for NPCG, like IE8. NOTE: This doesn' work for /(.?)?/
        // eslint-disable-next-line no-loop-func
        nativeReplace.call(match[0], reCopy, function () {
          for (i = 1; i < arguments.length - 2; i++) {
            if (arguments[i] === undefined) match[i] = undefined;
          }
        });
      }

      return match;
    };
  }

  var _regexpExec = patchedExec;

  _export({
    target: 'RegExp',
    proto: true,
    forced: _regexpExec !== /./.exec
  }, {
    exec: _regexpExec
  });

  var SPECIES = _wks('species');

  var REPLACE_SUPPORTS_NAMED_GROUPS = !_fails(function () {
    // #replace needs built-in support for named groups.
    // #match works fine because it just return the exec results, even if it has
    // a "grops" property.
    var re = /./;
    re.exec = function () {
      var result = [];
      result.groups = { a: '7' };
      return result;
    };
    return ''.replace(re, '$<a>') !== '7';
  });

  var SPLIT_WORKS_WITH_OVERWRITTEN_EXEC = (function () {
    // Chrome 51 has a buggy "split" implementation when RegExp#exec !== nativeExec
    var re = /(?:)/;
    var originalExec = re.exec;
    re.exec = function () { return originalExec.apply(this, arguments); };
    var result = 'ab'.split(re);
    return result.length === 2 && result[0] === 'a' && result[1] === 'b';
  })();

  var _fixReWks = function (KEY, length, exec) {
    var SYMBOL = _wks(KEY);

    var DELEGATES_TO_SYMBOL = !_fails(function () {
      // String methods call symbol-named RegEp methods
      var O = {};
      O[SYMBOL] = function () { return 7; };
      return ''[KEY](O) != 7;
    });

    var DELEGATES_TO_EXEC = DELEGATES_TO_SYMBOL ? !_fails(function () {
      // Symbol-named RegExp methods call .exec
      var execCalled = false;
      var re = /a/;
      re.exec = function () { execCalled = true; return null; };
      if (KEY === 'split') {
        // RegExp[@@split] doesn't call the regex's exec method, but first creates
        // a new one. We need to return the patched regex when creating the new one.
        re.constructor = {};
        re.constructor[SPECIES] = function () { return re; };
      }
      re[SYMBOL]('');
      return !execCalled;
    }) : undefined;

    if (
      !DELEGATES_TO_SYMBOL ||
      !DELEGATES_TO_EXEC ||
      (KEY === 'replace' && !REPLACE_SUPPORTS_NAMED_GROUPS) ||
      (KEY === 'split' && !SPLIT_WORKS_WITH_OVERWRITTEN_EXEC)
    ) {
      var nativeRegExpMethod = /./[SYMBOL];
      var fns = exec(
        _defined,
        SYMBOL,
        ''[KEY],
        function maybeCallNative(nativeMethod, regexp, str, arg2, forceStringMethod) {
          if (regexp.exec === _regexpExec) {
            if (DELEGATES_TO_SYMBOL && !forceStringMethod) {
              // The native String method already delegates to @@method (this
              // polyfilled function), leasing to infinite recursion.
              // We avoid it by directly calling the native @@method method.
              return { done: true, value: nativeRegExpMethod.call(regexp, str, arg2) };
            }
            return { done: true, value: nativeMethod.call(str, regexp, arg2) };
          }
          return { done: false };
        }
      );
      var strfn = fns[0];
      var rxfn = fns[1];

      _redefine(String.prototype, KEY, strfn);
      _hide(RegExp.prototype, SYMBOL, length == 2
        // 21.2.5.8 RegExp.prototype[@@replace](string, replaceValue)
        // 21.2.5.11 RegExp.prototype[@@split](string, limit)
        ? function (string, arg) { return rxfn.call(string, this, arg); }
        // 21.2.5.6 RegExp.prototype[@@match](string)
        // 21.2.5.9 RegExp.prototype[@@search](string)
        : function (string) { return rxfn.call(string, this); }
      );
    }
  };

  var max$1 = Math.max;
  var min$2 = Math.min;
  var floor$1 = Math.floor;
  var SUBSTITUTION_SYMBOLS = /\$([$&`']|\d\d?|<[^>]*>)/g;
  var SUBSTITUTION_SYMBOLS_NO_NAMED = /\$([$&`']|\d\d?)/g;

  var maybeToString = function (it) {
    return it === undefined ? it : String(it);
  };

  // @@replace logic
  _fixReWks('replace', 2, function (defined, REPLACE, $replace, maybeCallNative) {
    return [
      // `String.prototype.replace` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.replace
      function replace(searchValue, replaceValue) {
        var O = defined(this);
        var fn = searchValue == undefined ? undefined : searchValue[REPLACE];
        return fn !== undefined
          ? fn.call(searchValue, O, replaceValue)
          : $replace.call(String(O), searchValue, replaceValue);
      },
      // `RegExp.prototype[@@replace]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@replace
      function (regexp, replaceValue) {
        var res = maybeCallNative($replace, regexp, this, replaceValue);
        if (res.done) return res.value;

        var rx = _anObject(regexp);
        var S = String(this);
        var functionalReplace = typeof replaceValue === 'function';
        if (!functionalReplace) replaceValue = String(replaceValue);
        var global = rx.global;
        if (global) {
          var fullUnicode = rx.unicode;
          rx.lastIndex = 0;
        }
        var results = [];
        while (true) {
          var result = _regexpExecAbstract(rx, S);
          if (result === null) break;
          results.push(result);
          if (!global) break;
          var matchStr = String(result[0]);
          if (matchStr === '') rx.lastIndex = _advanceStringIndex(S, _toLength(rx.lastIndex), fullUnicode);
        }
        var accumulatedResult = '';
        var nextSourcePosition = 0;
        for (var i = 0; i < results.length; i++) {
          result = results[i];
          var matched = String(result[0]);
          var position = max$1(min$2(_toInteger(result.index), S.length), 0);
          var captures = [];
          // NOTE: This is equivalent to
          //   captures = result.slice(1).map(maybeToString)
          // but for some reason `nativeSlice.call(result, 1, result.length)` (called in
          // the slice polyfill when slicing native arrays) "doesn't work" in safari 9 and
          // causes a crash (https://pastebin.com/N21QzeQA) when trying to debug it.
          for (var j = 1; j < result.length; j++) captures.push(maybeToString(result[j]));
          var namedCaptures = result.groups;
          if (functionalReplace) {
            var replacerArgs = [matched].concat(captures, position, S);
            if (namedCaptures !== undefined) replacerArgs.push(namedCaptures);
            var replacement = String(replaceValue.apply(undefined, replacerArgs));
          } else {
            replacement = getSubstitution(matched, S, position, captures, namedCaptures, replaceValue);
          }
          if (position >= nextSourcePosition) {
            accumulatedResult += S.slice(nextSourcePosition, position) + replacement;
            nextSourcePosition = position + matched.length;
          }
        }
        return accumulatedResult + S.slice(nextSourcePosition);
      }
    ];

      // https://tc39.github.io/ecma262/#sec-getsubstitution
    function getSubstitution(matched, str, position, captures, namedCaptures, replacement) {
      var tailPos = position + matched.length;
      var m = captures.length;
      var symbols = SUBSTITUTION_SYMBOLS_NO_NAMED;
      if (namedCaptures !== undefined) {
        namedCaptures = _toObject(namedCaptures);
        symbols = SUBSTITUTION_SYMBOLS;
      }
      return $replace.call(replacement, symbols, function (match, ch) {
        var capture;
        switch (ch.charAt(0)) {
          case '$': return '$';
          case '&': return matched;
          case '`': return str.slice(0, position);
          case "'": return str.slice(tailPos);
          case '<':
            capture = namedCaptures[ch.slice(1, -1)];
            break;
          default: // \d\d?
            var n = +ch;
            if (n === 0) return match;
            if (n > m) {
              var f = floor$1(n / 10);
              if (f === 0) return match;
              if (f <= m) return captures[f - 1] === undefined ? ch.charAt(1) : captures[f - 1] + ch.charAt(1);
              return match;
            }
            capture = captures[n - 1];
        }
        return capture === undefined ? '' : capture;
      });
    }
  });

  // @@match logic
  _fixReWks('match', 1, function (defined, MATCH, $match, maybeCallNative) {
    return [
      // `String.prototype.match` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.match
      function match(regexp) {
        var O = defined(this);
        var fn = regexp == undefined ? undefined : regexp[MATCH];
        return fn !== undefined ? fn.call(regexp, O) : new RegExp(regexp)[MATCH](String(O));
      },
      // `RegExp.prototype[@@match]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@match
      function (regexp) {
        var res = maybeCallNative($match, regexp, this);
        if (res.done) return res.value;
        var rx = _anObject(regexp);
        var S = String(this);
        if (!rx.global) return _regexpExecAbstract(rx, S);
        var fullUnicode = rx.unicode;
        rx.lastIndex = 0;
        var A = [];
        var n = 0;
        var result;
        while ((result = _regexpExecAbstract(rx, S)) !== null) {
          var matchStr = String(result[0]);
          A[n] = matchStr;
          if (matchStr === '') rx.lastIndex = _advanceStringIndex(S, _toLength(rx.lastIndex), fullUnicode);
          n++;
        }
        return n === 0 ? null : A;
      }
    ];
  });

  // 7.2.8 IsRegExp(argument)


  var MATCH = _wks('match');
  var _isRegexp = function (it) {
    var isRegExp;
    return _isObject(it) && ((isRegExp = it[MATCH]) !== undefined ? !!isRegExp : _cof(it) == 'RegExp');
  };

  // 7.3.20 SpeciesConstructor(O, defaultConstructor)


  var SPECIES$1 = _wks('species');
  var _speciesConstructor = function (O, D) {
    var C = _anObject(O).constructor;
    var S;
    return C === undefined || (S = _anObject(C)[SPECIES$1]) == undefined ? D : _aFunction(S);
  };

  var $min = Math.min;
  var $push = [].push;
  var $SPLIT = 'split';
  var LENGTH = 'length';
  var LAST_INDEX$1 = 'lastIndex';
  var MAX_UINT32 = 0xffffffff;

  // babel-minify transpiles RegExp('x', 'y') -> /x/y and it causes SyntaxError
  var SUPPORTS_Y = !_fails(function () { });

  // @@split logic
  _fixReWks('split', 2, function (defined, SPLIT, $split, maybeCallNative) {
    var internalSplit;
    if (
      'abbc'[$SPLIT](/(b)*/)[1] == 'c' ||
      'test'[$SPLIT](/(?:)/, -1)[LENGTH] != 4 ||
      'ab'[$SPLIT](/(?:ab)*/)[LENGTH] != 2 ||
      '.'[$SPLIT](/(.?)(.?)/)[LENGTH] != 4 ||
      '.'[$SPLIT](/()()/)[LENGTH] > 1 ||
      ''[$SPLIT](/.?/)[LENGTH]
    ) {
      // based on es5-shim implementation, need to rework it
      internalSplit = function (separator, limit) {
        var string = String(this);
        if (separator === undefined && limit === 0) return [];
        // If `separator` is not a regex, use native split
        if (!_isRegexp(separator)) return $split.call(string, separator, limit);
        var output = [];
        var flags = (separator.ignoreCase ? 'i' : '') +
                    (separator.multiline ? 'm' : '') +
                    (separator.unicode ? 'u' : '') +
                    (separator.sticky ? 'y' : '');
        var lastLastIndex = 0;
        var splitLimit = limit === undefined ? MAX_UINT32 : limit >>> 0;
        // Make `global` and avoid `lastIndex` issues by working with a copy
        var separatorCopy = new RegExp(separator.source, flags + 'g');
        var match, lastIndex, lastLength;
        while (match = _regexpExec.call(separatorCopy, string)) {
          lastIndex = separatorCopy[LAST_INDEX$1];
          if (lastIndex > lastLastIndex) {
            output.push(string.slice(lastLastIndex, match.index));
            if (match[LENGTH] > 1 && match.index < string[LENGTH]) $push.apply(output, match.slice(1));
            lastLength = match[0][LENGTH];
            lastLastIndex = lastIndex;
            if (output[LENGTH] >= splitLimit) break;
          }
          if (separatorCopy[LAST_INDEX$1] === match.index) separatorCopy[LAST_INDEX$1]++; // Avoid an infinite loop
        }
        if (lastLastIndex === string[LENGTH]) {
          if (lastLength || !separatorCopy.test('')) output.push('');
        } else output.push(string.slice(lastLastIndex));
        return output[LENGTH] > splitLimit ? output.slice(0, splitLimit) : output;
      };
    // Chakra, V8
    } else if ('0'[$SPLIT](undefined, 0)[LENGTH]) {
      internalSplit = function (separator, limit) {
        return separator === undefined && limit === 0 ? [] : $split.call(this, separator, limit);
      };
    } else {
      internalSplit = $split;
    }

    return [
      // `String.prototype.split` method
      // https://tc39.github.io/ecma262/#sec-string.prototype.split
      function split(separator, limit) {
        var O = defined(this);
        var splitter = separator == undefined ? undefined : separator[SPLIT];
        return splitter !== undefined
          ? splitter.call(separator, O, limit)
          : internalSplit.call(String(O), separator, limit);
      },
      // `RegExp.prototype[@@split]` method
      // https://tc39.github.io/ecma262/#sec-regexp.prototype-@@split
      //
      // NOTE: This cannot be properly polyfilled in engines that don't support
      // the 'y' flag.
      function (regexp, limit) {
        var res = maybeCallNative(internalSplit, regexp, this, limit, internalSplit !== $split);
        if (res.done) return res.value;

        var rx = _anObject(regexp);
        var S = String(this);
        var C = _speciesConstructor(rx, RegExp);

        var unicodeMatching = rx.unicode;
        var flags = (rx.ignoreCase ? 'i' : '') +
                    (rx.multiline ? 'm' : '') +
                    (rx.unicode ? 'u' : '') +
                    (SUPPORTS_Y ? 'y' : 'g');

        // ^(? + rx + ) is needed, in combination with some S slicing, to
        // simulate the 'y' flag.
        var splitter = new C(SUPPORTS_Y ? rx : '^(?:' + rx.source + ')', flags);
        var lim = limit === undefined ? MAX_UINT32 : limit >>> 0;
        if (lim === 0) return [];
        if (S.length === 0) return _regexpExecAbstract(splitter, S) === null ? [S] : [];
        var p = 0;
        var q = 0;
        var A = [];
        while (q < S.length) {
          splitter.lastIndex = SUPPORTS_Y ? q : 0;
          var z = _regexpExecAbstract(splitter, SUPPORTS_Y ? S : S.slice(q));
          var e;
          if (
            z === null ||
            (e = $min(_toLength(splitter.lastIndex + (SUPPORTS_Y ? 0 : q)), S.length)) === p
          ) {
            q = _advanceStringIndex(S, q, unicodeMatching);
          } else {
            A.push(S.slice(p, q));
            if (A.length === lim) return A;
            for (var i = 1; i <= z.length - 1; i++) {
              A.push(z[i]);
              if (A.length === lim) return A;
            }
            q = p = e;
          }
        }
        A.push(S.slice(p));
        return A;
      }
    ];
  });

  /**
   * --------------------------------------------------------------------------
   * CoreUI Utilities (v2.1.9): get-css-custom-properties.js
   * Licensed under MIT (https://coreui.io/license)
   * @returns {string} css custom property name
   * --------------------------------------------------------------------------
   */
  var getCssCustomProperties = function getCssCustomProperties() {
    var cssCustomProperties = {};
    var sheets = document.styleSheets;
    var cssText = '';

    for (var i = sheets.length - 1; i > -1; i--) {
      var rules = sheets[i].cssRules;

      for (var j = rules.length - 1; j > -1; j--) {
        if (rules[j].selectorText === '.ie-custom-properties') {
          cssText = rules[j].cssText;
          break;
        }
      }

      if (cssText) {
        break;
      }
    }

    cssText = cssText.substring(cssText.lastIndexOf('{') + 1, cssText.lastIndexOf('}'));
    cssText.split(';').forEach(function (property) {
      if (property) {
        var name = property.split(': ')[0];
        var value = property.split(': ')[1];

        if (name && value) {
          cssCustomProperties["--" + name.trim()] = value.trim();
        }
      }
    });
    return cssCustomProperties;
  };

  var minIEVersion = 10;

  var isIE1x = function isIE1x() {
    return Boolean(document.documentMode) && document.documentMode >= minIEVersion;
  };

  var isCustomProperty = function isCustomProperty(property) {
    return property.match(/^--.*/i);
  };

  var getStyle = function getStyle(property, element) {
    if (element === void 0) {
      element = document.body;
    }

    var style;

    if (isCustomProperty(property) && isIE1x()) {
      var cssCustomProperties = getCssCustomProperties();
      style = cssCustomProperties[property];
    } else {
      style = window.getComputedStyle(element, null).getPropertyValue(property).replace(/^\s/, '');
    }

    return style;
  };

  /**
   * --------------------------------------------------------------------------
   * CoreUI Utilities (v2.1.9): get-color.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */

  var getColor = function getColor(rawProperty, element) {
    if (element === void 0) {
      element = document.body;
    }

    var property = "--" + rawProperty;
    var style = getStyle(property, element);
    return style ? style : rawProperty;
  };

  /**
   * --------------------------------------------------------------------------
   * CoreUI Utilities (v2.1.9): hex-to-rgb.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */

  /* eslint-disable no-magic-numbers */
  var hexToRgb = function hexToRgb(color) {
    if (typeof color === 'undefined') {
      throw new Error('Hex color is not defined');
    }

    var hex = color.match(/^#(?:[0-9a-f]{3}){1,2}$/i);

    if (!hex) {
      throw new Error(color + " is not a valid hex color");
    }

    var r;
    var g;
    var b;

    if (color.length === 7) {
      r = parseInt(color.substring(1, 3), 16);
      g = parseInt(color.substring(3, 5), 16);
      b = parseInt(color.substring(5, 7), 16);
    } else {
      r = parseInt(color.substring(1, 2), 16);
      g = parseInt(color.substring(2, 3), 16);
      b = parseInt(color.substring(3, 5), 16);
    }

    return "rgba(" + r + ", " + g + ", " + b + ")";
  };

  /**
   * --------------------------------------------------------------------------
   * CoreUI Utilities (v2.1.9): hex-to-rgba.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */

  /* eslint-disable no-magic-numbers */
  var hexToRgba = function hexToRgba(color, opacity) {
    if (opacity === void 0) {
      opacity = 100;
    }

    if (typeof color === 'undefined') {
      throw new Error('Hex color is not defined');
    }

    var hex = color.match(/^#(?:[0-9a-f]{3}){1,2}$/i);

    if (!hex) {
      throw new Error(color + " is not a valid hex color");
    }

    var r;
    var g;
    var b;

    if (color.length === 7) {
      r = parseInt(color.substring(1, 3), 16);
      g = parseInt(color.substring(3, 5), 16);
      b = parseInt(color.substring(5, 7), 16);
    } else {
      r = parseInt(color.substring(1, 2), 16);
      g = parseInt(color.substring(2, 3), 16);
      b = parseInt(color.substring(3, 5), 16);
    }

    return "rgba(" + r + ", " + g + ", " + b + ", " + opacity / 100 + ")";
  };

  // 21.2.5.3 get RegExp.prototype.flags()
  if (_descriptors && /./g.flags != 'g') _objectDp.f(RegExp.prototype, 'flags', {
    configurable: true,
    get: _flags
  });

  var TO_STRING = 'toString';
  var $toString = /./[TO_STRING];

  var define = function (fn) {
    _redefine(RegExp.prototype, TO_STRING, fn, true);
  };

  // 21.2.5.14 RegExp.prototype.toString()
  if (_fails(function () { return $toString.call({ source: 'a', flags: 'b' }) != '/a/b'; })) {
    define(function toString() {
      var R = _anObject(this);
      return '/'.concat(R.source, '/',
        'flags' in R ? R.flags : !_descriptors && R instanceof RegExp ? _flags.call(R) : undefined);
    });
  // FF44- RegExp#toString has a wrong name
  } else if ($toString.name != TO_STRING) {
    define(function toString() {
      return $toString.call(this);
    });
  }

  /**
   * --------------------------------------------------------------------------
   * CoreUI (v2.1.9): rgb-to-hex.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */

  /* eslint-disable no-magic-numbers */
  var rgbToHex = function rgbToHex(color) {
    if (typeof color === 'undefined') {
      throw new Error('Hex color is not defined');
    }

    if (color === 'transparent') {
      return '#00000000';
    }

    var rgb = color.match(/^rgba?[\s+]?\([\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?,[\s+]?(\d+)[\s+]?/i);

    if (!rgb) {
      throw new Error(color + " is not a valid rgb color");
    }

    var r = "0" + parseInt(rgb[1], 10).toString(16);
    var g = "0" + parseInt(rgb[2], 10).toString(16);
    var b = "0" + parseInt(rgb[3], 10).toString(16);
    return "#" + r.slice(-2) + g.slice(-2) + b.slice(-2);
  };

  exports.asideMenuCssClasses = asideMenuCssClasses;
  exports.checkBreakpoint = checkBreakpoint;
  exports.deepObjectsMerge = deepObjectsMerge;
  exports.getColor = getColor;
  exports.getStyle = getStyle;
  exports.hexToRgb = hexToRgb;
  exports.hexToRgba = hexToRgba;
  exports.rgbToHex = rgbToHex;
  exports.sidebarCssClasses = sidebarCssClasses;
  exports.validBreakpoints = validBreakpoints;

  Object.defineProperty(exports, '__esModule', { value: true });

}));
//# sourceMappingURL=coreui-utilities.js.map


/***/ }),

/***/ "./src/app/service/atphase.service.ts":
/*!********************************************!*\
  !*** ./src/app/service/atphase.service.ts ***!
  \********************************************/
/*! exports provided: ATPhaseService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ATPhaseService", function() { return ATPhaseService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");




var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].server.host;
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
headers.set('enctype', 'multipart/form-data');
var ATPhaseService = /** @class */ (function () {
    function ATPhaseService(httpClient) {
        this.httpClient = httpClient;
    }
    ATPhaseService.prototype.getConfig = function () {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get('');
    };
    ATPhaseService.prototype.getData = function (json) {
        return this.httpClient.post(host + 'atphases/users/test21/runSimulation', json, httpOptions);
    };
    ATPhaseService.prototype.getInputData = function (userID) {
        return this.httpClient.get(host + 'atphases/users/test21/returnInputData');
    };
    ATPhaseService.prototype.download = function (userID) {
        return this.httpClient.get(host + 'atphases/users/test21/download');
    };
    ATPhaseService.prototype.upload = function (fileToUpload) {
        console.log(fileToUpload);
        var formData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        console.log(formData.getAll('file'));
        return this.httpClient.post(host + 'atphases/users/test21/upload', formData, { headers: headers });
    };
    ATPhaseService.prototype.saveSimulation = function (body) {
        return this.httpClient.post(host + 'atphases/users/test21/saveData', body, httpOptions);
    };
    ATPhaseService.prototype.openSaveSimulation = function (userID) {
        return this.httpClient.get(host + 'atphases/users/test21/returnSaveData');
    };
    ATPhaseService.prototype.resetSimulation = function (userID) {
        return this.httpClient.get(host + 'atphases/users/test21/download');
    };
    ATPhaseService.prototype.saveDefaultValue = function (userID) {
        return this.httpClient.get(host + 'atphases/users/test21/download');
    };
    ATPhaseService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ATPhaseService);
    return ATPhaseService;
}());



/***/ }),

/***/ "./src/app/views/widgets/widgets-routing.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/widgets/widgets-routing.module.ts ***!
  \*********************************************************/
/*! exports provided: WidgetsRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetsRoutingModule", function() { return WidgetsRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _widgets_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./widgets.component */ "./src/app/views/widgets/widgets.component.ts");




var routes = [
    {
        path: '',
        component: _widgets_component__WEBPACK_IMPORTED_MODULE_3__["WidgetsComponent"],
        data: {
            title: 'Widgets'
        }
    }
];
var WidgetsRoutingModule = /** @class */ (function () {
    function WidgetsRoutingModule() {
    }
    WidgetsRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], WidgetsRoutingModule);
    return WidgetsRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/widgets/widgets.component.html":
/*!******************************************************!*\
  !*** ./src/app/views/widgets/widgets.component.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn d-flex\">\r\n  <div class=\"d-flex w-100 flex-wrap \">\r\n    <!--Parameters-->\r\n    <div class=\"d-flex w-30 p-2 align-self-start flex-min-width\">\r\n      <div class=\"d-flex card card-accent-primary \">\r\n        <div class=\"card-header\">\r\n          SIMULATION AND MODEL DATA\r\n        </div>\r\n        <div class=\"card-body d-flex w-100  \">\r\n          <div class=\"d-flex flex-column w-100\">\r\n            <div class=\"d-flex w-100 pt-1 align-content-end\">\r\n              <div class=\"d-flex w-60\"> &nbsp; </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"saveSimulation()\">SAVE</button>\r\n              </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"openSaveSimulation()\">OPEN</button>\r\n              </div>\r\n            </div>\r\n\r\n            <form [formGroup]=\"profileForm\" (ngSubmit)=\"onSubmit()\">\r\n\r\n              <div class=\"d-flex flex-column w-100\">\r\n\r\n\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Simulation parameters and experimental data\r\n                </div>\r\n                <div class=\"d-flex flex-column w-100 pt-2\">\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Epsilon:</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.epsilon}}\r\n                        formControlName=\"epsilon\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Number of iterations:</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.number_of_iteration}}\r\n                        formControlName=\"number_of_iteration\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Experimental Data File:</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100\" type=\"file\" id=\"a_t\" (change)=\"handleFileInput($event.target.files)\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Fixed model parameters\r\n                </div>\r\n                <div class=\"d-flex w-100 pt-2\">\r\n                  <!-- LEVA KOLONA-->\r\n                  <div class=\"d-flex flex-column w-40\">\r\n                    <div class=\"d-flex w-100\">&nbsp;</div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">KD*:</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-50 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kd_star}}\r\n                          formControlName=\"kd_star\">\r\n                      </div>\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">kD*-:</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-50 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kd_star_minus}}\r\n                          formControlName=\"kd_star_minus\">\r\n                      </div>\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">KD: </span>\r\n                      </div>\r\n                      <div class=\"d-flex w-50 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kd}}\r\n                          formControlName=\"kd\">\r\n                      </div>\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">kD-:</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-50 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kd_minus}}\r\n                          formControlName=\"kd_minus\">\r\n                      </div>\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n\r\n                  </div>\r\n                  <!-- DESNA KOLONA-->\r\n                  <div class=\"d-flex flex-column w-60\">\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-52 p-1\">\r\n                        <span style=\"vertical-align: middle\">Concentration of Actin:</span>\r\n                      </div>\r\n                      <div class=\"w-35 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.actin}}\r\n                          formControlName=\"actin\">\r\n                      </div>\r\n                      <div class=\"w-13 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-52 p-1\">\r\n                        <span style=\"vertical-align: middle\">Concentration of Myosin:</span>\r\n                      </div>\r\n                      <div class=\"w-35 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                          formControlName=\"myosin\">\r\n                      </div>\r\n                      <div class=\"w-13 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-52 p-1\">\r\n                        <span style=\"vertical-align: middle\">Concentration of Pi:</span>\r\n                      </div>\r\n                      <div class=\"w-35 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.pi}}\r\n                          formControlName=\"pi\">\r\n                      </div>\r\n                      <div class=\"w-13 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <!--div class=\"d-flex w-10\"></div-->\r\n                      <div class=\"w-52 p-1\">\r\n                        <span style=\"vertical-align: middle\">Concentration of DPi:</span>\r\n                      </div>\r\n                      <div class=\"w-35 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.dpi}}\r\n                          formControlName=\"dpi\">\r\n                      </div>\r\n                      <div class=\"w-13 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-52 p-1\">\r\n                        <span style=\"vertical-align: middle\">Concentration of TPi:</span>\r\n                      </div>\r\n                      <div class=\"w-35 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.tpi}}\r\n                          formControlName=\"tpi\">\r\n                      </div>\r\n                      <div class=\"w-13 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n\r\n              <div class=\"d-flex flex-column w-100 pt-2\">\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Parameters for estimation\r\n                </div>\r\n                <!--<div class=\"d-flex text-center pt-2 text-bottom\"> -->\r\n                <div class=\"d-flex text-center pt-2 my-font-sm75\">\r\n                  <div class=\"w-40 p-1\"></div>\r\n                  <div class=\"w-10 p-1 align-self-end\">Fitting</div>\r\n                  <div class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1 align-self-end\">Guess 1</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Guess 2</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Lower Bound</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Upper Bound</div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">KA:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.ka}}\r\n                        formControlName=\"ka\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.ka_fiting\"\r\n                      formControlName=\"ka_fiting\" (ngModelChange)=\"onCheckboxChange('ka_fiting', 'ka_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.ka_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.ka_guess_of_params1}}\r\n                        formControlName=\"ka_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.ka_guess_of_params2}}\r\n                        formControlName=\"ka_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.ka_lower_bound}}\r\n                        formControlName=\"ka_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.ka_upper_bound}}\r\n                        formControlName=\"ka_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kA-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.ka_minus}}\r\n                        formControlName=\"ka_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.ka_minus_fiting\"\r\n                      formControlName=\"ka_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('ka_minus_fiting', 'ka_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.ka_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.ka_minus_guess_of_params1}}\r\n                        formControlName=\"ka_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.ka_minus_guess_of_params2}}\r\n                        formControlName=\"ka_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.ka_minus_lower_bound}}\r\n                        formControlName=\"ka_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.ka_minus_upper_bound}}\r\n                        formControlName=\"ka_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">KPi:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kpi}}\r\n                        formControlName=\"kpi\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kpi_fiting\"\r\n                      formControlName=\"kpi_fiting\" (ngModelChange)=\"onCheckboxChange('kpi_fiting', 'kpi_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kpi_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kpi_guess_of_params1}}\r\n                        formControlName=\"kpi_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kpi_guess_of_params2}}\r\n                        formControlName=\"kpi_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kpi_lower_bound}}\r\n                        formControlName=\"kpi_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kpi_upper_bound}}\r\n                        formControlName=\"kpi_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kPi-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kpi_minus}}\r\n                        formControlName=\"kpi_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kpi_minus_fiting\"\r\n                      formControlName=\"kpi_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kpi_minus_fiting', 'kpi_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kpi_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kpi_minus_guess_of_params1}}\r\n                        formControlName=\"kpi_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kpi_minus_guess_of_params2}}\r\n                        formControlName=\"kpi_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kpi_minus_lower_bound}} formControlName=\"kpi_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kpi_minus_upper_bound}} formControlName=\"kpi_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">KT:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt}}\r\n                        formControlName=\"kt\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_fiting\"\r\n                      formControlName=\"kt_fiting\" (ngModelChange)=\"onCheckboxChange('kt_fiting', 'kt_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_guess_of_params1}}\r\n                        formControlName=\"kt_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_guess_of_params2}}\r\n                        formControlName=\"kt_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_lower_bound}}\r\n                        formControlName=\"kt_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_upper_bound}}\r\n                        formControlName=\"kt_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kT-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_minus}}\r\n                        formControlName=\"kt_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center \">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_minus_fiting\"\r\n                      formControlName=\"kt_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kt_minus_fiting', 'kt_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_minus_guess_of_params1}}\r\n                        formControlName=\"kt_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_minus_guess_of_params2}}\r\n                        formControlName=\"kt_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_lower_bound}}\r\n                        formControlName=\"kt_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_upper_bound}}\r\n                        formControlName=\"kt_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">KT*:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_star}}\r\n                        formControlName=\"kt_star\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_star_fiting\"\r\n                      formControlName=\"kt_star_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kt_star_fiting', 'kt_star_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_star_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_guess_of_params1}} formControlName=\"kt_star_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_guess_of_params2}} formControlName=\"kt_star_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_star_lower_bound}}\r\n                        formControlName=\"kt_star_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_star_upper_bound}}\r\n                        formControlName=\"kt_star_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kT*-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_star_minus}}\r\n                        formControlName=\"kt_star_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                      [(ngModel)]=\"this.inputData.kt_star_minus_fiting\" formControlName=\"kt_star_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kt_star_minus_fiting', 'kt_star_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_star_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_minus_guess_of_params1}}\r\n                        formControlName=\"kt_star_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_minus_guess_of_params2}}\r\n                        formControlName=\"kt_star_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_minus_lower_bound}}\r\n                        formControlName=\"kt_star_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_minus_upper_bound}}\r\n                        formControlName=\"kt_star_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">KT**:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_star_star}}\r\n                        formControlName=\"kt_star_star\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                      [(ngModel)]=\"this.inputData.kt_star_star_fiting\" formControlName=\"kt_star_star_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kt_star_star_fiting', 'kt_star_star_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_star_star_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_star_guess_of_params1}}\r\n                        formControlName=\"kt_star_star_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_star_guess_of_params2}}\r\n                        formControlName=\"kt_star_star_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_star_lower_bound}} formControlName=\"kt_star_star_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_star_upper_bound}} formControlName=\"kt_star_star_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kT**-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_star_star_minus}}\r\n                        formControlName=\"kt_star_star_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                      [(ngModel)]=\"this.inputData.kt_star_star_minus_fiting\" formControlName=\"kt_star_star_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kt_star_star_minus_fiting', 'kt_star_star_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_star_star_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_star_minus_guess_of_params1}}\r\n                        formControlName=\"kt_star_star_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_star_minus_guess_of_params2}}\r\n                        formControlName=\"kt_star_star_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_star_minus_lower_bound}}\r\n                        formControlName=\"kt_star_star_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_star_star_minus_upper_bound}}\r\n                        formControlName=\"kt_star_star_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">KH:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kh}}\r\n                        formControlName=\"kh\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kh_fiting\"\r\n                      formControlName=\"kh_fiting\" (ngModelChange)=\"onCheckboxChange('kh_fiting', 'kh_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kh_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kh_guess_of_params1}}\r\n                        formControlName=\"kh_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kh_guess_of_params2}}\r\n                        formControlName=\"kh_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kh_lower_bound}}\r\n                        formControlName=\"kh_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kh_upper_bound}}\r\n                        formControlName=\"kh_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kH-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kh_minus}}\r\n                        formControlName=\"kh_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kh_minus_fiting\"\r\n                      formControlName=\"kh_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kh_minus_fiting', 'kh_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kh_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kh_minus_guess_of_params1}}\r\n                        formControlName=\"kh_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kh_minus_guess_of_params2}}\r\n                        formControlName=\"kh_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kh_minus_lower_bound}}\r\n                        formControlName=\"kh_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kh_minus_upper_bound}}\r\n                        formControlName=\"kh_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">KAh:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kah}}\r\n                        formControlName=\"kah\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kah_fiting\"\r\n                      formControlName=\"kah_fiting\" (ngModelChange)=\"onCheckboxChange('kah_fiting', 'kah_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kah_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kah_guess_of_params1}}\r\n                        formControlName=\"kah_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kah_guess_of_params2}}\r\n                        formControlName=\"kah_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kah_lower_bound}}\r\n                        formControlName=\"kah_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kah_upper_bound}}\r\n                        formControlName=\"kah_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kAh-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kah_minus}}\r\n                        formControlName=\"kah_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kah_minus_fiting\"\r\n                      formControlName=\"kah_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kah_minus_fiting', 'kah_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kah_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kah_minus_guess_of_params1}}\r\n                        formControlName=\"kah_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kah_minus_guess_of_params2}}\r\n                        formControlName=\"kah_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kah_minus_lower_bound}} formControlName=\"kah_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kah_minus_upper_bound}} formControlName=\"kah_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"d-flex w-100 pt-3\">\r\n                <div class=\"d-flex w-80\"> &nbsp; </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button id=\"dugmeRun\" type=\"submit w-25\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"dugmeRun\">{{run_button}}</button>\r\n                </div>\r\n                <!--div class=\"d-flex w-25 \">\r\n                  <button type=\"submit\" class=\"btn btn-sm btn-primary\" [disabled]=\"!profileForm.valid\">SAVE Current\r\n                    Test</button>\r\n                </div>\r\n                <div class=\"d-flex w-25 \">\r\n                  <button type=\"submit\" class=\"btn btn-sm btn-primary\" [disabled]=\"!profileForm.valid\">OPEN Saved\r\n                    Test</button>\r\n                </div-->\r\n              </div>\r\n\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"d-flex w-70 p-2\">\r\n      <div class=\"d-flex card card-accent-primary w-100\">\r\n        <div class=\"card-header\">\r\n          RESULTS\r\n          <span class=\"float-right\" tooltip=\"Download graphic data\" placement=\"left\">\r\n            <i (click)=\"download()\" class=\"fa fa-cloud-download\" style=\"font-size:16px\"></i>\r\n          </span>\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex flex-column w-100 mv-400\">\r\n            <div class=\"d-flex w-100 h-5 justify-content-around\">\r\n\r\n            </div>\r\n            <div class=\"d-flex flex-column w-100 pt-2\">\r\n              <div class=\"d-flex w-100 flex-wrap justify-content-around\" >\r\n\r\n                <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n                <div class=\"w-25 tbl-10\">\r\n                  <!--style=\"height:300px;margin-top:40px;\"-->\r\n                  <div class=\"d-flex pb-10 title-div-table\">\r\n                    Estimated values\r\n                  </div>\r\n                  <!--div class=\"table-responsive\">-->\r\n                  <div class=\"tbl-10\">\r\n                    <table class=\"table table-condensed \" *ngIf=\"isVisible\">\r\n                      <!--table-striped-->\r\n                      <tbody>\r\n                        <tr *ngFor=\"let row of dataSource\">\r\n                          <td *ngFor=\"let col of row\" class=\"td td-1\">{{col}}</td>\r\n                        </tr>\r\n                      </tbody>\r\n                    </table>\r\n                  </div>\r\n                </div>\r\n                <div class=\"w-60 tbl-10 \">\r\n                  <div class=\"d-flex pb-10 title-div-table\">\r\n                    Sensitivity matrix\r\n                  </div>\r\n                  <div class=\"tbl-8kolona\">\r\n                    <table class=\"table table-condensed\" *ngIf=\"isVisible\">\r\n                      <tbody>\r\n                        <tr *ngFor=\"let row of sensitivityMatrix\">\r\n                          <td *ngFor=\"let col of row\" class=\"td td-1\">{{col}}</td>\r\n                        </tr>\r\n                      </tbody>\r\n                    </table>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <!--div class=\"card-body d-flex w-100\"-->\r\n              <div class=\"d-flex w-100 flex-wrap justify-content-around align-content-start\">\r\n                <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n                <div class=\"w-100 p-2 mw-350\">\r\n                  <div class=\"chart-wrapper\">\r\n                    <!--style=\"height:300px;margin-top:40px;\"-->\r\n                    <canvas *ngIf=\"graphs[0]\" baseChart class=\"chart\" [datasets]=\"graphs[0].datasets\"\r\n                      [labels]=\"graphs[0].labels\" [options]=\"graphs[0].options\" [colors]=\"graphs[0].colors\"\r\n                      [legend]=\"graphs[0].legend\" [chartType]=\"graphs[0].chartType\">\r\n                    </canvas>\r\n                    <canvas *ngIf=\"!graphs[0]\" baseChart class=\"chart\"></canvas>\r\n                  </div>\r\n                </div>\r\n                <!--/div-->\r\n                <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n                <!-- <div *ngFor=\"let graph of errorGraph\" class=\"w-50 p-2 mw-350\">\r\n                  <div class=\"chart-wrapper\">\r\n                    <canvas baseChart class=\"chart\" [datasets]=\"graph.datasets\" [labels]=\"graph.labels\"\r\n                      [options]=\"graph.options\" [colors]=\"graph.colors\" [legend]=\"graph.legend\"\r\n                      [chartType]=\"graph.chartType\">\r\n                    </canvas>\r\n                  </div>\r\n                </div> -->\r\n              </div>\r\n            </div>\r\n            <div class=\"d-flex title-div-2 w-100 h-20 p-3 justify-content-around\">\r\n              Estimated parameters convergation\r\n            </div>\r\n            <!--div class=\"d-flex flex-column w-100 pt-2\"></div-->\r\n            <div class=\"d-inline-flex p-2 w-100 flex-wrap justify-content-center\">\r\n              <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n              <div class=\"w-100\">\r\n                <div class=\"chart-wrapper\">\r\n                  <!--style=\"height:300px;margin-top:40px;\"-->\r\n                  <canvas *ngIf=\"graphs[1]\" baseChart class=\"chart\" [datasets]=\"graphs[1].datasets\"\r\n                    [labels]=\"graphs[1].labels\" [options]=\"graphs[1].options\" [colors]=\"graphs[1].colors\"\r\n                    [legend]=\"graphs[1].legend\" [chartType]=\"graphs[1].chartType\">\r\n                  </canvas>\r\n                  <canvas *ngIf=\"!graphs[1]\" baseChart class=\"chart\"></canvas>\r\n                </div>\r\n              </div>\r\n            </div>\r\n\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>"

/***/ }),

/***/ "./src/app/views/widgets/widgets.component.scss":
/*!******************************************************!*\
  !*** ./src/app/views/widgets/widgets.component.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column; }\n\n.example-container > * {\n  width: 100%; }\n\n.example-container form {\n  margin-bottom: 20px; }\n\n.example-container form > * {\n  margin: 5px 0; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.table_form {\n  min-width: 550px;\n  padding: 0px; }\n\n.table_form p {\n  font-size: 10px;\n  text-align: right; }\n\n.table_form input {\n  height: 25px;\n  text-align: center;\n  max-width: 100px; }\n\n/* Customize the label (the container) */\n\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n/* Hide the browser's default checkbox */\n\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0; }\n\n/* Create a custom checkbox */\n\n.checkmark {\n  position: absolute;\n  margin-top: 5px;\n  top: 0;\n  left: 0;\n  height: 20 px;\n  width: 20px;\n  background-color: #eee; }\n\n/* On mouse-over, add a grey background color */\n\n.container:hover input ~ .checkmark {\n  background-color: #ccc; }\n\n/* When the checkbox is checked, add a blue background */\n\n.container input:checked ~ .checkmark {\n  background-color: #2196F3; }\n\n/* Create the checkmark/indicator (hidden when not checked) */\n\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none; }\n\n/* Show the checkmark when checked */\n\n.container input:checked ~ .checkmark:after {\n  display: block; }\n\n/* Style the checkmark/indicator */\n\n.container .checkmark:after {\n  left: 9px;\n  top: 5px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg); }\n\n.title-div-centar {\n  font-size: 1rem;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\n.title-div {\n  color: cadetblue;\n  font-style: 1rem;\n  border-bottom: 1px dashed lightblue; }\n\n.title-div-table {\n  font-size: 1rem;\n  color: cadetblue;\n  text-align: center !important; }\n\n.title-div-2 {\n  font-size: 1.2rem;\n  color: cadetblue; }\n\ninput {\n  background-color: #c3e4e5;\n  border: 1px solid \t#c3e4e5 !important; }\n\n.text-align-right {\n  text-align: right;\n  padding-right: 2px !important; }\n\n.text-align-center {\n  text-align: center; }\n\n.flex-min-width {\n  min-width: 470px; }\n\n.max-width-70posto {\n  max-width: 70%; }\n\n.over-auto {\n  overflow: visible; }\n\n.chart-wrapper {\n  border: 1px solid lightblue !important; }\n\n.mw-350 {\n  min-width: 280px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-30posto {\n  min-width: 370px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-400 {\n  min-width: 280px; }\n\n.tbl-10 {\n  min-width: 300px !important; }\n\n.tbl-8kolona {\n  min-width: 300px !important; }\n\n.td-1 {\n  padding: 0.4rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3Mvd2lkZ2V0cy9GOlxcdGhpbmZpbGFtZW50djMyMFxcbmV3X2NsaWVudC9zcmNcXGFwcFxcdmlld3NcXHdpZGdldHNcXHdpZGdldHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFhO0VBQ2Isc0JBQXNCLEVBQUE7O0FBR3hCO0VBQ0UsV0FBVyxFQUFBOztBQUdiO0VBQ0UsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsYUFBYSxFQUFBOztBQUdmO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixXQUFXLEVBQUE7O0FBR2I7RUFDRSxXQUFXLEVBQUE7O0FBR2I7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBSW5CLHdDQUFBOztBQUNEO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBR25CLHdDQUFBOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUztFQUNULFFBQVEsRUFBQTs7QUFHViw2QkFBQTs7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsTUFBTTtFQUNOLE9BQU87RUFDUCxhQUFhO0VBQ2IsV0FBVztFQUNYLHNCQUFzQixFQUFBOztBQUd4QiwrQ0FBQTs7QUFDQTtFQUNFLHNCQUFzQixFQUFBOztBQUd4Qix3REFBQTs7QUFDQTtFQUNFLHlCQUF5QixFQUFBOztBQUczQiw2REFBQTs7QUFDQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsYUFBYSxFQUFBOztBQUdmLG9DQUFBOztBQUNBO0VBQ0UsY0FBYyxFQUFBOztBQUdoQixrQ0FBQTs7QUFDQTtFQUNFLFNBQVM7RUFDVCxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLGdDQUFnQztFQUVoQyx3QkFBd0IsRUFBQTs7QUFHMUI7RUFFRSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUl0QjtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsbUNBQW1DLEVBQUE7O0FBR3JDO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiw2QkFBNkIsRUFBQTs7QUFHL0I7RUFDRSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBSWxCO0VBRUUseUJBQTBCO0VBQzFCLHFDQUFxQyxFQUFBOztBQUd2QztFQUNFLGlCQUFpQjtFQUNqQiw2QkFBNkIsRUFBQTs7QUFHL0I7RUFDRSxrQkFBa0IsRUFBQTs7QUFJcEI7RUFDRSxnQkFBZ0IsRUFBQTs7QUFTbEI7RUFDRSxjQUFjLEVBQUE7O0FBRWhCO0VBQ0UsaUJBQWlCLEVBQUE7O0FBR25CO0VBRUUsc0NBQXNDLEVBQUE7O0FBR3hDO0VBQ0UsZ0JBQWdCO0VBQ2hCLDJCQUFtQjtFQUFuQix3QkFBbUI7RUFBbkIsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsZ0JBQWdCO0VBQ2hCLDJCQUFtQjtFQUFuQix3QkFBbUI7RUFBbkIsbUJBQW1CLEVBQUE7O0FBS3JCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBRUUsMkJBQTJCLEVBQUE7O0FBRTdCO0VBRUUsMkJBQTJCLEVBQUE7O0FBSTdCO0VBQ0UsMEJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC92aWV3cy93aWRnZXRzL3dpZGdldHMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciBmb3JtIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciBmb3JtID4gKiB7XHJcbiAgICBtYXJnaW46IDVweCAwO1xyXG4gIH1cclxuICBcclxuICAuZXhhbXBsZS1mb3JtIHtcclxuICAgIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIC50YWJsZV9mb3JtIHtcclxuICAgIG1pbi13aWR0aDogNTUwcHg7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC50YWJsZV9mb3JtIHAge1xyXG4gICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgfVxyXG4gIFxyXG4gIC50YWJsZV9mb3JtIGlucHV0IHtcclxuICAgIGhlaWdodDogMjVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1heC13aWR0aDogMTAwcHg7XHJcbiAgfVxyXG5cclxuXHJcbiAvKiBDdXN0b21pemUgdGhlIGxhYmVsICh0aGUgY29udGFpbmVyKSAqL1xyXG4uY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZy1sZWZ0OiAzNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGZvbnQtc2l6ZTogMjJweDtcclxuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4gIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gIHVzZXItc2VsZWN0OiBub25lO1xyXG59XHJcblxyXG4vKiBIaWRlIHRoZSBicm93c2VyJ3MgZGVmYXVsdCBjaGVja2JveCAqL1xyXG4uY29udGFpbmVyIGlucHV0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgb3BhY2l0eTogMDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgaGVpZ2h0OiAwO1xyXG4gIHdpZHRoOiAwO1xyXG59XHJcblxyXG4vKiBDcmVhdGUgYSBjdXN0b20gY2hlY2tib3ggKi9cclxuLmNoZWNrbWFyayB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIG1hcmdpbi10b3A6IDVweDtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxuICBoZWlnaHQ6IDIwIHB4O1xyXG4gIHdpZHRoOiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XHJcbn1cclxuXHJcbi8qIE9uIG1vdXNlLW92ZXIsIGFkZCBhIGdyZXkgYmFja2dyb3VuZCBjb2xvciAqL1xyXG4uY29udGFpbmVyOmhvdmVyIGlucHV0IH4gLmNoZWNrbWFyayB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NjYztcclxufVxyXG5cclxuLyogV2hlbiB0aGUgY2hlY2tib3ggaXMgY2hlY2tlZCwgYWRkIGEgYmx1ZSBiYWNrZ3JvdW5kICovXHJcbi5jb250YWluZXIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcmsge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyMTk2RjM7XHJcbn1cclxuXHJcbi8qIENyZWF0ZSB0aGUgY2hlY2ttYXJrL2luZGljYXRvciAoaGlkZGVuIHdoZW4gbm90IGNoZWNrZWQpICovXHJcbi5jaGVja21hcms6YWZ0ZXIge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi8qIFNob3cgdGhlIGNoZWNrbWFyayB3aGVuIGNoZWNrZWQgKi9cclxuLmNvbnRhaW5lciBpbnB1dDpjaGVja2VkIH4gLmNoZWNrbWFyazphZnRlciB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi8qIFN0eWxlIHRoZSBjaGVja21hcmsvaW5kaWNhdG9yICovXHJcbi5jb250YWluZXIgLmNoZWNrbWFyazphZnRlciB7XHJcbiAgbGVmdDogOXB4O1xyXG4gIHRvcDogNXB4O1xyXG4gIHdpZHRoOiA1cHg7XHJcbiAgaGVpZ2h0OiAxMHB4O1xyXG4gIGJvcmRlcjogc29saWQgd2hpdGU7XHJcbiAgYm9yZGVyLXdpZHRoOiAwIDNweCAzcHggMDtcclxuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG4gIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxufVxyXG5cclxuLnRpdGxlLWRpdi1jZW50YXIge1xyXG4gIC8vY29sb3I6IGNhZGV0Ymx1ZTtcclxuICBmb250LXNpemU6IDFyZW07XHJcbiAgcGFkZGluZy10b3A6IDFyZW07XHJcbiAgcGFkZGluZy1ib3R0b206IDFyZW07XHJcbiAgLy9ib3JkZXItYm90dG9tOiAxcHggZGFzaGVkIGxpZ2h0Ymx1ZTtcclxufVxyXG5cclxuLnRpdGxlLWRpdiB7XHJcbiAgY29sb3I6IGNhZGV0Ymx1ZTtcclxuICBmb250LXN0eWxlOiAxcmVtO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBkYXNoZWQgbGlnaHRibHVlO1xyXG59XHJcblxyXG4udGl0bGUtZGl2LXRhYmxlIHtcclxuICBmb250LXNpemU6IDFyZW07IFxyXG4gIGNvbG9yOiBjYWRldGJsdWU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50aXRsZS1kaXYtMiB7XHJcbiAgZm9udC1zaXplOiAxLjJyZW07IFxyXG4gIGNvbG9yOiBjYWRldGJsdWU7XHJcbiAvLyB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW5wdXQge1xyXG4gIC8vYm9yZGVyOiBub25lO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IFx0I2MzZTRlNTtcclxuICBib3JkZXI6IDFweCBzb2xpZCBcdCNjM2U0ZTUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtYWxpZ24tcmlnaHQge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDJweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGV4dC1hbGlnbi1jZW50ZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbn1cclxuXHJcbi5mbGV4LW1pbi13aWR0aCB7XHJcbiAgbWluLXdpZHRoOiA0NzBweDtcclxuLy8gIG1heC13aWR0aDogNjAwcHg7XHJcbiAgLy96LWluZGV4OiAxMDAwO1xyXG59XHJcblxyXG4uZmxleC1taW4td2lkdGggKiB7XHJcbiAgLy96LWluZGV4OiAxMDAwO1xyXG59XHJcblxyXG4ubWF4LXdpZHRoLTcwcG9zdG8ge1xyXG4gIG1heC13aWR0aDogNzAlO1xyXG59XHJcbi5vdmVyLWF1dG8ge1xyXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xyXG59XHJcblxyXG4uY2hhcnQtd3JhcHBlciB7XHJcbiAgLy9ib3JkZXItdG9wLXN0eWxlOiBncm9vdmUgIWltcG9ydGFudDtcclxuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGJsdWUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm13LTM1MCB7XHJcbiAgbWluLXdpZHRoOiAyODBweDtcclxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xyXG59XHJcblxyXG4ubXctMzBwb3N0byB7XHJcbiAgbWluLXdpZHRoOiAzNzBweDtcclxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gIC8vZm9udC1zaXplOiAwLjVyZW0gIWltcG9ydGFudDtcclxufVxyXG5cclxuXHJcbi5tdy00MDAge1xyXG4gIG1pbi13aWR0aDogMjgwcHg7XHJcbn1cclxuXHJcbi50YmwtMTAge1xyXG4gLy8gbWFyZ2luOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgbWluLXdpZHRoOiAzMDBweCAhaW1wb3J0YW50O1xyXG59XHJcbi50YmwtOGtvbG9uYSB7XHJcbiAvLyBtYXJnaW46IDEwcHggIWltcG9ydGFudDtcclxuICBtaW4td2lkdGg6IDMwMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG4udGQtMXtcclxuICBwYWRkaW5nOiAwLjRyZW0gIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/views/widgets/widgets.component.ts":
/*!****************************************************!*\
  !*** ./src/app/views/widgets/widgets.component.ts ***!
  \****************************************************/
/*! exports provided: WidgetsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetsComponent", function() { return WidgetsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @coreui/coreui/dist/js/coreui-utilities */ "./node_modules/@coreui/coreui/dist/js/coreui-utilities.js");
/* harmony import */ var _coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @coreui/coreui-plugin-chartjs-custom-tooltips */ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_atphase_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../service/atphase.service */ "./src/app/service/atphase.service.ts");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");








var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_7__["environment"].server.host;
var WidgetsComponent = /** @class */ (function () {
    // displayedColumns: string[] = ['col1', 'col2', 'col3', 'col4', 'col5', 'col6', 'col7', 'col8'];
    function WidgetsComponent(fb, atPhaseService) {
        this.fb = fb;
        this.atPhaseService = atPhaseService;
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            ka: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            ka_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kpi_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kt_star_star_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kh_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kah_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kd_star: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kd: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kd_star_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            kd_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            epsilon: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            number_of_iteration: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            actin: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            pi: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            dpi: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            tpi: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](''),
            file: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"]('')
        });
        this.inputData = this.profileForm;
        this.dugmeRun = false;
        this.isVisible = true;
        this.run_button = "RUN";
        this.fileToUpload = null;
        this.numberOfGraphs = 2;
        this.graphs = [];
        this.atModelGraph = [];
        this.errorGraph = [];
        // mainChart
        this.mainChartElements = 27;
        this.mainChartData1 = [
            { x: 0, y: 0 },
            { x: 2, y: 1.5 },
            { x: 3, y: 2 },
            { x: 4, y: 2.5 }
        ];
        this.podaci2 = [
            { x: 0, y: 0 },
            { x: 2, y: 1 },
            { x: 3, y: 3 },
            { x: 4, y: 3.5 }
        ];
        this.mainChartData2 = [
            {
                data: this.podaci2,
                label: 'Labela',
            }
        ];
        this.mainChartData = [
            {
                data: this.mainChartData1,
                label: 'Labela',
            }
        ];
        /* tslint:disable:max-line-length */
        this.mainChartLabels = [];
        /* tslint:enable:max-line-length */
        this.mainChartOptions = {
            title: {
                display: true,
                text: 'Error ',
                // fontColor: 'blue',
                fontStyle: 'small-caps',
            },
            tooltips: {
                enabled: false,
                custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_3__["CustomTooltips"],
                intersect: true,
                mode: 'index',
                position: 'nearest',
                callbacks: {
                    labelColor: function (tooltipItem, chart) {
                        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                    }
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 15,
                    top: 0,
                    bottom: 5
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        type: 'linear',
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            //drawticks: false,
                            tickMarkLength: 5
                        },
                        ticks: {
                            padding: 5,
                            beginAtZero: false,
                            min: 1,
                            stepSize: 0.5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC1 (%)',
                            fontSize: 11
                            //fontColor: 'red'
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            beginAtZero: true,
                            padding: 5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC2 (%)',
                            fontSize: 11
                            //fontColor: 'red'
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: false,
            }
        };
        this.MainChartOptions2 = this.mainChartOptions;
        this.mainChartColours = [
            {
                borderColor: Object(_coreui_coreui_dist_js_coreui_utilities__WEBPACK_IMPORTED_MODULE_2__["getStyle"])('--info'),
                backgroundColor: 'transparent'
            }
        ];
        this.mainChartLegend = true;
        this.mainChartType = 'line';
    }
    WidgetsComponent.prototype.random = function (min, max) {
        return Math.floor(Math.random() * (max - min + 1) + min);
    };
    WidgetsComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.atPhaseService.getInputData('test').subscribe(function (data) {
            if (data) {
                _this.inputData = data;
            }
        });
    };
    WidgetsComponent.prototype.ngOnInit = function () {
        var test = 'false';
        // this.atModelGraph.push(this.initGraph(titles[14], labels[14], colors[14], 2));
        // this.errorGraph.push(this.initGraph(titles[15], labels[15], colors[15], 1));
    };
    WidgetsComponent.prototype.initGraph = function (newTitle, newLabels, newColors, numberOfLines) {
        var basicGraph = {};
        basicGraph.options = {
            title: {
                display: true,
                text: 'Error',
                // fontColor: 'blue',
                fontStyle: 'small-caps',
            },
            tooltips: {
                enabled: false,
                custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_3__["CustomTooltips"],
                intersect: true,
                mode: 'index',
                position: 'nearest',
                callbacks: {
                    labelColor: function (tooltipItem, chart) {
                        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                    }
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 15,
                    top: 0,
                    bottom: 5
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        type: 'linear',
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            padding: 5,
                            beginAtZero: false,
                            stepSize: numberOfLines === 2 ? 1000 : 1,
                            fontSize: 12,
                            min: 1
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC1 (%)',
                            fontSize: 11
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            beginAtZero: true,
                            padding: 5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC2 (%)',
                            fontSize: 11
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: true,
                position: 'right'
            }
        };
        var newOptions = basicGraph.options;
        newOptions.title.text = newTitle;
        newOptions.scales.xAxes[0].type = newLabels.xAxes.type;
        newOptions.scales.xAxes[0].scaleLabel.labelString = newLabels.xAxes.title;
        newOptions.scales.yAxes[0].type = newLabels.yAxes.type;
        newOptions.scales.yAxes[0].scaleLabel.display = newLabels.yAxes.displayTitle;
        newOptions.scales.yAxes[0].scaleLabel.labelString = newLabels.yAxes.title;
        basicGraph.options = newOptions;
        basicGraph.colors = [];
        basicGraph.datasets = [];
        for (var i = 0; i < numberOfLines; i++) {
            basicGraph.datasets.push({ data: [] });
            basicGraph.colors.push(newColors[i]);
        }
        basicGraph.chartType = 'line';
        basicGraph.labels = [];
        basicGraph.legend = false;
        basicGraph.visibility = true;
        return basicGraph;
    };
    WidgetsComponent.prototype.prepareGraphs = function (graphLines) {
        var titles = [
            'Experiment VS Model',
            'Error & Parameters convergence'
        ];
        var labels = [
            { xAxes: { type: 'linear', title: 'Time [s]' }, yAxes: { type: 'linear', displayTitle: true, title: 'Fractional Fluorescence' } },
            { xAxes: { type: 'linear', title: 'Iteration' }, yAxes: { type: 'logarithmic', displayTitle: false, title: '' } }
        ];
        var colors = [
            [
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'yellow', backgroundColor: 'transparent' }
            ],
            [
                { borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' },
                { borderColor: 'yellow', backgroundColor: 'transparent' },
                { borderColor: 'oragne', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'black', backgroundColor: 'transparent' },
                { borderColor: 'brown', backgroundColor: 'transparent' },
            ]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i], graphLines[i]));
        }
    };
    WidgetsComponent.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
    };
    WidgetsComponent.prototype.onSubmit = function () {
        var _this = this;
        this.dugmeRun = true;
        this.run_button = "RUNNING";
        this.isVisible = false;
        this.graphs = [];
        console.log('start simulation');
        var body = this.profileForm.value;
        var atData = [];
        console.log(this.profileForm.value.file);
        var file = this.profileForm.value.file;
        this.atPhaseService.upload(this.fileToUpload).subscribe(function (upload) {
            console.log(upload);
            _this.atPhaseService.getData(body).subscribe(function (data) {
                console.log('finish simulation');
                var a_t = data.a_t;
                var models = data.model;
                console.log(data);
                var vidljivo = [
                    Boolean(body.ka_fiting),
                    Boolean(body.ka_minus_fiting),
                    Boolean(body.kpi_fiting),
                    Boolean(body.kpi_minus_fiting),
                    Boolean(body.kt_fiting),
                    Boolean(body.kt_minus_fiting),
                    Boolean(body.kt_star_fiting),
                    Boolean(body.kt_star_minus_fiting),
                    Boolean(body.kt_star_star_fiting),
                    Boolean(body.kt_star_star_minus_fiting),
                    Boolean(body.kh_fiting),
                    Boolean(body.kh_minus_fiting),
                    Boolean(body.kah_fiting),
                    Boolean(body.kah_minus_fiting),
                ];
                var podaci = [
                    { data: data.ka, label: 'KA' },
                    { data: data.ka_minus, label: 'kA-' },
                    { data: data.kpi, label: 'KPi' },
                    { data: data.kpi_minus, label: 'kPi-' },
                    { data: data.kt, label: 'KT' },
                    { data: data.kt_minus, label: 'kT-' },
                    { data: data.kt_star, label: 'KT*' },
                    { data: data.kt_star_minus, label: 'kT*-' },
                    { data: data.kt_star_2, label: 'KT**' },
                    { data: data.kt_star_minus_2, label: 'kT**-' },
                    { data: data.kh, label: 'KH' },
                    { data: data.kh_minus, label: 'kH-' },
                    { data: data.kah, label: 'KAh' },
                    { data: data.kah_minus, label: 'kAh-' }
                ];
                var datasets = [
                    [
                        { data: a_t, label: 'Experiment' },
                        { data: models, label: 'Model' }
                    ],
                    []
                ];
                for (var i = 0; i < 14; i++) {
                    if (vidljivo[i]) {
                        datasets[1].push(podaci[i]);
                    }
                }
                datasets[1].push({ data: data.error, label: 'Error' });
                // this.atModelGraph[0].datasets = [{ data: a_t },
                // { data: models }];
                // console.log(this.atModelGraph[0].datasets);
                // this.errorGraph[0].datasets = [{ data: atData }]
                _this.graphs = [];
                _this.prepareGraphs([datasets[0].length, datasets[1].length]);
                for (var i = 0; i < _this.numberOfGraphs; i++) {
                    _this.graphs[i].datasets = datasets[i];
                    _this.graphs[i].legend = true;
                }
                _this.isVisible = true;
                console.log(_this.graphs);
                for (var i = 1; i < data.estimated_values.length; i++) {
                    data.estimated_values[i][1] = data.estimated_values[i][1].toFixed(6);
                    data.estimated_values[i][2] = data.estimated_values[i][2].toFixed(6);
                    data.estimated_values[i][3] = data.estimated_values[i][3].toFixed(6);
                }
                _this.dataSource = data.estimated_values;
                _this.sensitivityMatrix = data.sensitivity;
                _this.dugmeRun = false;
                _this.run_button = "RUN";
            });
        });
    };
    WidgetsComponent.prototype.onCheckboxChange = function (box1, box2) {
        if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
            if (this.inputData[box1])
                this.inputData[box2] = false;
        }
    };
    WidgetsComponent.prototype.download = function () {
        window.open(host + 'atphases/users/test21/download');
    };
    WidgetsComponent.prototype.saveSimulation = function () {
        var body = this.profileForm.value;
        this.atPhaseService.saveSimulation(body).subscribe(function (data) {
            console.log(data);
        });
    };
    WidgetsComponent.prototype.openSaveSimulation = function () {
        var _this = this;
        this.graphs = [];
        this.atPhaseService.openSaveSimulation('test21').subscribe(function (data) {
            var inputDatas = data.parameters;
            _this.inputData = data.parameters;
            var vidljivo = [
                Boolean(inputDatas.ka_fiting),
                Boolean(inputDatas.ka_minus_fiting),
                Boolean(inputDatas.kpi_fiting),
                Boolean(inputDatas.kpi_minus_fiting),
                Boolean(inputDatas.kt_fiting),
                Boolean(inputDatas.kt_minus_fiting),
                Boolean(inputDatas.kt_star_fiting),
                Boolean(inputDatas.kt_star_minus_fiting),
                Boolean(inputDatas.kt_star_star_fiting),
                Boolean(inputDatas.kt_star_star_minus_fiting),
                Boolean(inputDatas.kh_fiting),
                Boolean(inputDatas.kh_minus_fiting),
                Boolean(inputDatas.kah_fiting),
                Boolean(inputDatas.kah_minus_fiting),
            ];
            var params = data.inputData;
            var podaci = [
                { data: params.ka, label: 'KA' },
                { data: params.ka_minus, label: 'kA-' },
                { data: params.kpi, label: 'KPi' },
                { data: params.kpi_minus, label: 'kPi-' },
                { data: params.kt, label: 'KT' },
                { data: params.kt_minus, label: 'kT-' },
                { data: params.kt_star, label: 'KT*' },
                { data: params.kt_star_minus, label: 'kT*-' },
                { data: params.kt_star_2, label: 'KT**' },
                { data: params.kt_star_minus_2, label: 'kT**-' },
                { data: params.kh, label: 'KH' },
                { data: params.kh_minus, label: 'kH-' },
                { data: params.kah, label: 'KAh' },
                { data: params.kah_minus, label: 'kAh-' }
            ];
            var datasets = [
                [
                    { data: params.a_t, label: 'Experiment' },
                    { data: params.models, label: 'Model' }
                ],
                []
            ];
            for (var i = 0; i < 14; i++) {
                if (vidljivo[i]) {
                    datasets[1].push(podaci[i]);
                }
            }
            datasets[1].push({ data: params.error, label: 'Error' });
            // this.atModelGraph[0].datasets = [{ data: a_t },
            // { data: models }];
            // console.log(this.atModelGraph[0].datasets);
            // this.errorGraph[0].datasets = [{ data: atData }]
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length, datasets[1].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
                _this.graphs[i].legend = true;
            }
            for (var i = 1; i < data.estimated_values.length; i++) {
                data.estimated_values[i][1] = data.estimated_values[i][1].toFixed(6);
                data.estimated_values[i][2] = data.estimated_values[i][2].toFixed(6);
                data.estimated_values[i][3] = data.estimated_values[i][3].toFixed(6);
            }
            _this.dataSource = params.estimated_values;
            _this.sensitivityMatrix = params.sensitivity;
            _this.dugmeRun = false;
            _this.run_button = "RUN";
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_6__["BaseChartDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
    ], WidgetsComponent.prototype, "charts", void 0);
    WidgetsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! ./widgets.component.html */ "./src/app/views/widgets/widgets.component.html"),
            styles: [__webpack_require__(/*! ./widgets.component.scss */ "./src/app/views/widgets/widgets.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormBuilder"], _service_atphase_service__WEBPACK_IMPORTED_MODULE_5__["ATPhaseService"]])
    ], WidgetsComponent);
    return WidgetsComponent;
}());



/***/ }),

/***/ "./src/app/views/widgets/widgets.module.ts":
/*!*************************************************!*\
  !*** ./src/app/views/widgets/widgets.module.ts ***!
  \*************************************************/
/*! exports provided: WidgetsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WidgetsModule", function() { return WidgetsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _widgets_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./widgets.component */ "./src/app/views/widgets/widgets.component.ts");
/* harmony import */ var _widgets_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./widgets-routing.module */ "./src/app/views/widgets/widgets-routing.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");











var WidgetsModule = /** @class */ (function () {
    function WidgetsModule() {
    }
    WidgetsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_4__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["ReactiveFormsModule"],
                _widgets_routing_module__WEBPACK_IMPORTED_MODULE_6__["WidgetsRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_2__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_3__["BsDropdownModule"],
                _angular_material_table__WEBPACK_IMPORTED_MODULE_7__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"]
            ],
            declarations: [_widgets_component__WEBPACK_IMPORTED_MODULE_5__["WidgetsComponent"]],
            exports: [
                _angular_material_table__WEBPACK_IMPORTED_MODULE_7__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_8__["MatPaginatorModule"]
            ]
        })
    ], WidgetsModule);
    return WidgetsModule;
}());



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    server: {
        host: 'http://localhost:3000/api/',
    }
};


/***/ })

}]);
//# sourceMappingURL=views-widgets-widgets-module.js.map