import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';

import { HillStochasticalComponent } from './hill-stochastical.component';
import { HillStochasticalRoutingModule } from './hill-stochastical-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';

@NgModule({
 imports: [
   FormsModule,
   CommonModule,
   ReactiveFormsModule,
   HillStochasticalRoutingModule,
   ChartsModule,
   BsDropdownModule,
   ButtonsModule.forRoot(),
   MatTableModule,
   MatPaginatorModule
 ],
 declarations: [ HillStochasticalComponent ],
 exports: [
   MatTableModule,
   MatPaginatorModule
 ]
})
export class HillStochasticalModule { }