'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const multer = require('multer');
const zippingFile = require('../../middleware/zipping');
const returnOutputData = require('../../middleware/return_data');
const winston = require('winston')
const fsExtra = require('fs-extra')

//Local include

module.exports = function (mg_pobabilistic) {

  const exeSimulation = (id, runSimulationId, parameters) => {
    const path = `./${runSimulationId}`

    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
    }

    const archive = {
      simulationId: runSimulationId,
      simulationType: 'MG Stochastic DLS',
      status: 'In Progress',
      runDate: new Date(),
      simulation: 'mg-probabilistic'
    }
    return loopback.getModel('arhive').create(archive)
      .then(arhiveData => {
        return fsExtra.copy('./atphase/', path, (err) => {
          if (err) {
            winston.info(err)
            const response = {
              statusCode: 400,
              message: 'Simulation failed'
            }
            return arhiveData.updateAttributes({ status: 'Failed' })
              .then(() => {
                return response
              })
          }
          //setup data for start simulation
          const fileName = 'paramestim.exe';
          const params = [];
          return exec(fileName, params, { cwd: path }, (err, data) => {
            if (err) {
              winston.error(err)
              const response = {
                statusCode: 400,
                message: 'Simulation failed'
              }
              return arhiveData.updateAttributes({ status: 'Failed' })
                .then(() => {
                  return response
                })
            }
            else {
              winston.info(data)
              winston.info('Simulation mg probabilistic successfully finish')
              //checked estimation params
              const cekiranaPolja = [Boolean(parameters.kb_fiting), Boolean(parameters.kb_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting), Boolean(parameters.k1_fiting), Boolean(parameters.k1_minus_fiting),
              Boolean(parameters.k2_fiting), Boolean(parameters.k2_minus_fiting)];

              returnOutputData.removeData('mg_probabilistic')
              //call function for create output data
              return returnOutputData.returnData('mg_probabilistic', cekiranaPolja, parameters.number_of_iteration)
                .then(outputData => {
                  //call function for zip output data
                  zippingFile.zipFile('mg_probabilistic', cekiranaPolja, parameters, outputData);

                  const options = {
                    userID: id,
                    input_id: runSimulationId,
                    ...outputData
                  }
                  return loopback.getModel('mg_probabilistic_output').create(options)
                    .then(datas => {
                      return arhiveData.updateAttributes({ status: 'Complete' })
                        .then(() => {
                          return fsExtra.remove(path)
                            .then(() => {
                              return datas
                            })
                        })
                    })
                })
            }
          })
        })
      })
  }

  //function for run simulation
  mg_pobabilistic.runSimulation = (id, parameters) => {
    console.log(parameters)

    //create input file
    let writeStream = fs.createWriteStream('./mg_probabilistic/tmtn-ca.dat');
    writeStream.write(parameters.s + '\n');
    writeStream.write(parameters.kb + '\n');
    writeStream.write(parameters.kb_minus + '\n');
    writeStream.write(parameters.kt + '\n');
    writeStream.write(parameters.kt_minus + '\n');
    writeStream.write(parameters.k1 + '\n');
    writeStream.write(parameters.k1_minus + '\n');
    writeStream.write(parameters.k2 + '\n');
    writeStream.write(parameters.k2_minus + '\n');
    writeStream.write(parameters.actin + '\n');
    writeStream.write(parameters.myosin + '\n');
    writeStream.end((parameters.final_myosin ? parameters.final_myosin : parameters.myosin) + '\n');

    //create input warp
    let inputWarp = fs.createWriteStream('./mg_probabilistic/input_wrap');
    inputWarp.write('8\n');
    inputWarp.write((parameters.kb_fiting ? parameters.kb_guess_of_params1 : parameters.kb) + '\n');
    inputWarp.write((parameters.kb_fiting ? parameters.kb_guess_of_params2 : parameters.kb) + '\n');
    inputWarp.write((parameters.kb_fiting ? parameters.kb_lower_bound : parameters.kb) + '\n');
    inputWarp.write((parameters.kb_fiting ? parameters.kb_upper_bound : parameters.kb) + '\n');
    inputWarp.write((parameters.kb_minus_fiting ? parameters.kb_minus_guess_of_params1 : parameters.kb_minus) + '\n');
    inputWarp.write((parameters.kb_minus_fiting ? parameters.kb_minus_guess_of_params2 : parameters.kb_minus) + '\n');
    inputWarp.write((parameters.kb_minus_fiting ? parameters.kb_minus_lower_bound : parameters.kb_minus) + '\n');
    inputWarp.write((parameters.kb_minus_fiting ? parameters.kb_minus_upper_bound : parameters.kb_minus) + '\n');
    inputWarp.write((parameters.kt_fiting ? parameters.kt_guess_of_params1 : parameters.kt) + '\n');
    inputWarp.write((parameters.kt_fiting ? parameters.kt_guess_of_params2 : parameters.kt) + '\n');
    inputWarp.write((parameters.kt_fiting ? parameters.kt_lower_bound : parameters.kt) + '\n');
    inputWarp.write((parameters.kt_fiting ? parameters.kt_upper_bound : parameters.kt) + '\n');
    inputWarp.write((parameters.kt_minus_fiting ? parameters.kt_minus_guess_of_params1 : parameters.kt_minus) + '\n');
    inputWarp.write((parameters.kt_minus_fiting ? parameters.kt_minus_guess_of_params2 : parameters.kt_minus) + '\n');
    inputWarp.write((parameters.kt_minus_fiting ? parameters.kt_minus_lower_bound : parameters.kt_minus) + '\n');
    inputWarp.write((parameters.kt_minus_fiting ? parameters.kt_minus_upper_bound : parameters.kt_minus) + '\n');
    inputWarp.write((parameters.k1_fiting ? parameters.k1_guess_of_params1 : parameters.k1) + '\n');
    inputWarp.write((parameters.k1_fiting ? parameters.k1_guess_of_params2 : parameters.k1) + '\n');
    inputWarp.write((parameters.k1_fiting ? parameters.k1_lower_bound : parameters.k1) + '\n');
    inputWarp.write((parameters.k1_fiting ? parameters.k1_upper_bound : parameters.k1) + '\n');
    inputWarp.write((parameters.k1_minus_fiting ? parameters.k1_minus_guess_of_params1 : parameters.k1_minus) + '\n');
    inputWarp.write((parameters.k1_minus_fiting ? parameters.k1_minus_guess_of_params2 : parameters.k1_minus) + '\n');
    inputWarp.write((parameters.k1_minus_fiting ? parameters.k1_minus_lower_bound : parameters.k1_minus) + '\n');
    inputWarp.write((parameters.k1_minus_fiting ? parameters.k1_minus_upper_bound : parameters.k1_minus) + '\n');
    inputWarp.write((parameters.k2_fiting ? parameters.k2_guess_of_params1 : parameters.k2) + '\n');
    inputWarp.write((parameters.k2_fiting ? parameters.k2_guess_of_params2 : parameters.k2) + '\n');
    inputWarp.write((parameters.k2_fiting ? parameters.k2_lower_bound : parameters.k2) + '\n');
    inputWarp.write((parameters.k2_fiting ? parameters.k2_upper_bound : parameters.k2) + '\n');
    inputWarp.write((parameters.k2_minus_fiting ? parameters.k2_minus_guess_of_params1 : parameters.k2_minus) + '\n');
    inputWarp.write((parameters.k2_minus_fiting ? parameters.k2_minus_guess_of_params2 : parameters.k2_minus) + '\n');
    inputWarp.write((parameters.k2_minus_fiting ? parameters.k2_minus_lower_bound : parameters.k2_minus) + '\n');
    inputWarp.write((parameters.k2_minus_fiting ? parameters.k2_minus_upper_bound : parameters.k2_minus) + '\n');
    inputWarp.write(parameters.epsilon + '\n');
    inputWarp.write(parameters.number_of_iteration + '\n');
    inputWarp.write('\'a_t.txt\'' + '\n');
    inputWarp.write('\'kkk.dat\'' + '\n');
    inputWarp.write('\'eee.dat\'' + '\n');
    inputWarp.write('\'rrr.dat\'' + '\n');
    inputWarp.end('\'num_rigid\'' + '\n');

    //setup data for start simulation

    const options = {
      userID: id,
      ...parameters
    }

    return loopback.getModel('mg_pobabilistic').create(options)
      .then(data => {
        exeSimulation(id, data._id, parameters)
        winston.info('Simulation mg probabilistic successfully start')
        const response = {
          statusCode: 200,
          message: "Successfull run simulation",
          simulationId: data._id
        }

        return response
      })
  }


  mg_pobabilistic.saveData = (id, parameters) => {
    //checked estimation params
    const cekiranaPolja = [Boolean(parameters.kb_fiting), Boolean(parameters.kb_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting), Boolean(parameters.k1_fiting), Boolean(parameters.k1_minus_fiting),
    Boolean(parameters.k2_fiting), Boolean(parameters.k2_minus_fiting)];

    //call function for create sensitivity matrix
    pravljenjeSensitivityMatrice.createMatrix('./mg_pobabilistic/new_eee.dat', './mg_pobabilistic/new_kkk.dat', './mg_pobabilistic/new_rrr.dat', parameters.number_of_iteration, cekiranaPolja);

    return returnOutputData.returnData('mg_pobabilistic', cekiranaPolja, parameters.number_of_iteration)
      .then(outputData => {
        if (!outputData && outputData !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for mg probabilistic simulation'
          }
          winston.info('Not found output data for mg probabilistic simulation')
          return response
        }
        const mg_pobabilisticParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel(mg_pobabilistic).create(mg_pobabilisticParams)
          .then(atp => {
            const options = {
              userID: id,
              input_id: atp._id,
              ...outputData
            }
            return loopback.getModel('mg_probabilistic_output').create(options)
              .then(data => {
                winston.info('Simulation mg probabilistic successfully save data')
                data.statusCode = 200;
                return data
              })
          })
      })
  }

  mg_pobabilistic.returnSaveData = (id) => {
    return loopback.getModel('mg_probabilistic_output').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found mg probabilistic simulation'
          }
          winston.info('Not found return data for mg probabilistic simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id
        return loopback.getModel('mg_pobabilistic').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found mg probabilistic simulation'
              }
              winston.info('Not found input data for mg probabilistic simulation')
              return response
            }
            const response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response
          })
      })
  }

  mg_pobabilistic.returnData = (simulationId) => {
    let response = {}
    return loopback.getModel('mg_pobabilistic').findOne({ where: { _id: simulationId } })
      .then(data => {
        if (!data) {
          response = {
            statusCode: 404,
            message: `Not found mg probabilistic simulation with simulation id: ${simulationId}`
          }
          winston.info(`Not found input data for mg probabilistic simulation with simulation id: ${simulationId}`)
          return response
        }
        return loopback.getModel('mg_probabilistic_output').findOne({ where: { input_id: simulationId } })
          .then(estimatedParams => {
            if (!estimatedParams) {
              response = {
                statusCode: 404,
                message: `Not found mg probabilistic simulation with simulation id: ${simulationId}`
              }
              winston.info(`Not found estimated data for mg probabilistic simulation with simulation id: ${simulationId}`)
              return response
            }
            response = {
              statusCode: 200,
              parameters: data,
              estimatedParams: estimatedParams
            }
            return response
          })
      })
  }

  mg_pobabilistic.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    }
    return loopback.getModel('mg_pobabilistic').create(options)
      .then(data => {
        winston.info('Simulation mg probabilistic successfully save input data')
        return data;
      });
  }

  mg_pobabilistic.returnInputData = (id) => {
    return loopback.getModel('mg_pobabilistic').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found mg probabilistic input data`
          }
          winston.info('Not found mg probabilistic input data')
          return response
        }
        return data[data.length - 1];
      })
  }

  mg_pobabilistic.saveDefaulValue = (id, parameters) => {
    const options = {
      userID: id,
      default_values: true,
      ...parameters
    }
    return loopback.getModel('mg_pobabilistic').create(options)
      .then(data => {
        winston.info('Simulation mg probabilistic successfully save default values')
        return data;
      })
  }

  mg_pobabilistic.returnSaveDefaulValue = (id) => {
    return loopback.getModel('mg_pobabilistic').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found mg probabilistic default values`
          }
          winston.info('Not found mg probabilistic default values')
          return response
        }
        return data[data.length - 1];
      })
  }

  mg_pobabilistic.download = (id, res) => {
    winston.info('Simulation mg probabilistic successfully download data')
    res.download('./mg_probabilistic/output_files.zip');
  };

  mg_pobabilistic.upload = (id, req, res) => {
    var storage = multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './mg_probabilistic');
      },
      filename: function (req, file, callback) {
        callback(null, 'a_t.txt');
      }
    });

    let upload = multer({ storage: storage }).single('file');

    upload(req, res, function (err) {
      if (err) {
        winston.info(err)
        return res.end({ message: "Error uploading file." });
      }
      winston.info('Simulation mg probabilistic successfully update a_t')
      res.json({ message: "File is uploaded" });
    });
  }

  /** API for run simulation */
  mg_pobabilistic.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  mg_pobabilistic.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  mg_pobabilistic.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  /** API for return input data for simulation */
  mg_pobabilistic.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  mg_pobabilistic.remoteMethod('returnData', {
    http: { verb: 'GET', path: '/users/:simulationId/returnData' },
    accepts: [
      { arg: 'simulationId', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  mg_pobabilistic.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  mg_pobabilistic.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  mg_pobabilistic.remoteMethod('upload',
    {
      http: { verb: 'POST', path: '/users/:id/upload' },
      accepts: [
        { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
        { arg: 'req', type: 'object', http: { source: 'req' } },
        { arg: 'res', type: 'object', http: { source: 'res' } }
      ],
      returns: { arg: 'data', type: 'string', root: true }
    }
  );

  mg_pobabilistic.remoteMethod('saveDefaulValue', {
    http: { verb: 'POST', path: '/users/:id/saveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  mg_pobabilistic.remoteMethod('returnSaveDefaulValue', {
    http: { verb: 'GET', path: '/users/:id/returnSaveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  mg_pobabilistic.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download', 'upload', 'returnSaveDefaulValue', 'saveDefaulValue', 'returnData'], method.name)) {
      mg_pobabilistic.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}