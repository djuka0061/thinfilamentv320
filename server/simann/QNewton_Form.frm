VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form QNewton_Form 
   Caption         =   "Quasi-Newton Optimized Method"
   ClientHeight    =   9390
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9390
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   7
      Left            =   2400
      TabIndex        =   72
      Text            =   "20"
      Top             =   4800
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   6
      Left            =   2430
      TabIndex        =   71
      Text            =   "3760"
      Top             =   4440
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   5
      Left            =   2400
      TabIndex        =   70
      Text            =   "20"
      Top             =   3960
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   4
      Left            =   2430
      TabIndex        =   69
      Text            =   "2.e6"
      Top             =   3600
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   3
      Left            =   2400
      TabIndex        =   68
      Text            =   "3000"
      Top             =   3120
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   2
      Left            =   2430
      TabIndex        =   67
      Text            =   "0.013"
      Top             =   2760
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   1
      Left            =   2400
      TabIndex        =   66
      Text            =   "100"
      Top             =   2280
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   0
      Left            =   2430
      TabIndex        =   65
      Text            =   "0.3"
      Top             =   1920
      Width           =   1485
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   5280
      TabIndex        =   60
      Text            =   "80"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   5280
      TabIndex        =   59
      Text            =   "0.02"
      Top             =   2760
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   5280
      TabIndex        =   58
      Text            =   "1000"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   4
      Left            =   5280
      TabIndex        =   57
      Text            =   "2.0e6"
      Top             =   3600
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   4
      Left            =   6960
      TabIndex        =   56
      Text            =   "1.0e4"
      Top             =   3600
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   6960
      TabIndex        =   55
      Text            =   "50"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   6960
      TabIndex        =   54
      Text            =   "1.0e-5"
      Top             =   2760
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   6960
      TabIndex        =   53
      Text            =   "500"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   8640
      TabIndex        =   52
      Text            =   "2000"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   8640
      TabIndex        =   51
      Text            =   "0.5"
      Top             =   2760
      Width           =   1095
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   8640
      TabIndex        =   50
      Text            =   "6000"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   4
      Left            =   8640
      TabIndex        =   49
      Text            =   "5.0e6"
      Top             =   3600
      Width           =   1095
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   8640
      TabIndex        =   48
      Text            =   "200"
      Top             =   1920
      Width           =   1095
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   5
      Left            =   8640
      TabIndex        =   47
      Text            =   "100"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   6
      Left            =   8640
      TabIndex        =   46
      Text            =   "5000"
      Top             =   4440
      Width           =   1095
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   7
      Left            =   8640
      TabIndex        =   45
      Text            =   "100"
      Top             =   4800
      Visible         =   0   'False
      Width           =   1095
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   6960
      TabIndex        =   44
      Text            =   "0.0001"
      Top             =   1920
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   5
      Left            =   6960
      TabIndex        =   43
      Text            =   "5"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   6
      Left            =   6960
      TabIndex        =   42
      Text            =   "500"
      Top             =   4440
      Width           =   1215
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   7
      Left            =   6960
      TabIndex        =   41
      Text            =   "1"
      Top             =   4800
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   5280
      TabIndex        =   40
      Text            =   "10"
      Top             =   1920
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   5
      Left            =   5280
      TabIndex        =   39
      Text            =   "15"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   6
      Left            =   5280
      TabIndex        =   38
      Text            =   "800"
      Top             =   4440
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   7
      Left            =   5280
      TabIndex        =   37
      Text            =   "10"
      Top             =   4800
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   7
      Left            =   4440
      TabIndex        =   36
      Top             =   4800
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   6
      Left            =   4440
      TabIndex        =   35
      Top             =   4440
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   5
      Left            =   4440
      TabIndex        =   34
      Top             =   3960
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   4
      Left            =   4440
      TabIndex        =   33
      Top             =   3600
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   3
      Left            =   4440
      TabIndex        =   32
      Top             =   3120
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   2
      Left            =   4440
      TabIndex        =   31
      Top             =   2760
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   1
      Left            =   4440
      TabIndex        =   30
      Top             =   2280
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   0
      Left            =   4440
      TabIndex        =   29
      Top             =   1920
      Value           =   1  'Checked
      Width           =   375
   End
   Begin MSComDlg.CommonDialog CommonDialog2 
      Left            =   360
      Top             =   8160
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command2 
      Caption         =   "........"
      Height          =   375
      Left            =   9720
      TabIndex        =   28
      Top             =   6360
      Width           =   615
   End
   Begin VB.CommandButton Command1 
      Caption         =   "......."
      Height          =   375
      Left            =   9720
      TabIndex        =   27
      Top             =   5880
      Width           =   615
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2430
      TabIndex        =   12
      Text            =   "2"
      Top             =   1500
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2430
      TabIndex        =   11
      Text            =   "0.5"
      Top             =   5520
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2400
      TabIndex        =   10
      Text            =   "5.0"
      Top             =   6570
      Width           =   1485
   End
   Begin VB.TextBox Text15 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   300
      Left            =   2400
      TabIndex        =   9
      Text            =   "0.25"
      Top             =   7080
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run"
      Height          =   435
      Left            =   4800
      TabIndex        =   8
      Top             =   6960
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type of Simulation"
      Height          =   645
      Left            =   480
      TabIndex        =   5
      Top             =   5880
      Width           =   3270
      Begin VB.OptionButton Time_Option 
         Caption         =   "Time Course"
         Height          =   225
         Left            =   525
         TabIndex        =   7
         Top             =   315
         Value           =   -1  'True
         Width           =   1275
      End
      Begin VB.OptionButton Titration_Option 
         Caption         =   "Titration"
         Height          =   225
         Left            =   1995
         TabIndex        =   6
         Top             =   315
         Width           =   960
      End
   End
   Begin VB.CommandButton Ret_comm 
      Caption         =   "Return to Main"
      Height          =   435
      Left            =   5520
      TabIndex        =   4
      Top             =   7680
      Width           =   1485
   End
   Begin VB.TextBox Text20 
      Height          =   300
      Left            =   5880
      TabIndex        =   3
      Text            =   "kkk_4b.dat"
      Top             =   6360
      Width           =   3765
   End
   Begin VB.TextBox Text30 
      Height          =   300
      Left            =   5880
      TabIndex        =   2
      Text            =   "a_t.txt"
      Top             =   5880
      Width           =   3765
   End
   Begin VB.CommandButton Display 
      Caption         =   "Show Results"
      Height          =   495
      Left            =   6840
      TabIndex        =   1
      Top             =   6960
      Width           =   1575
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label32 
      Alignment       =   2  'Center
      Caption         =   "Upper Bound:"
      Height          =   285
      Left            =   8520
      TabIndex        =   64
      Top             =   1560
      Width           =   1155
   End
   Begin VB.Label Label22 
      Alignment       =   2  'Center
      Caption         =   "Lower Bound:"
      Height          =   285
      Left            =   6720
      TabIndex        =   63
      Top             =   1560
      Width           =   1515
   End
   Begin VB.Label Label23 
      Alignment       =   2  'Center
      Caption         =   "Guess:"
      Height          =   285
      Left            =   5160
      TabIndex        =   62
      Top             =   1560
      Width           =   1515
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Fitting"
      Height          =   255
      Left            =   4200
      TabIndex        =   61
      Top             =   1560
      Width           =   615
   End
   Begin VB.Label Label16 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   600
      TabIndex        =   26
      Top             =   1560
      Width           =   1635
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Kb:"
      Height          =   285
      Left            =   480
      TabIndex        =   25
      Top             =   1920
      Width           =   1755
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "kb-:"
      Height          =   285
      Left            =   600
      TabIndex        =   24
      Top             =   2400
      Width           =   1635
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Kt:"
      Height          =   285
      Left            =   600
      TabIndex        =   23
      Top             =   2760
      Width           =   1635
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "kt-:"
      Height          =   285
      Left            =   600
      TabIndex        =   22
      Top             =   3240
      Width           =   1635
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "k1+    [1/(M*s)]:"
      Height          =   285
      Left            =   600
      TabIndex        =   21
      Top             =   3585
      Width           =   1635
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "k2+:"
      Height          =   285
      Left            =   600
      TabIndex        =   20
      Top             =   4425
      Width           =   1755
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "[Actin] in uM:"
      Height          =   285
      Left            =   480
      TabIndex        =   19
      Top             =   5505
      Width           =   1875
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "[Myosin] in uM:"
      Height          =   285
      Left            =   600
      TabIndex        =   18
      Top             =   6600
      Width           =   1635
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "k2- :"
      Height          =   285
      Left            =   600
      TabIndex        =   17
      Top             =   4845
      Width           =   1755
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "k1-:"
      Height          =   285
      Left            =   600
      TabIndex        =   16
      Top             =   4005
      Width           =   1635
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Experimentical Data File:"
      Height          =   285
      Left            =   4080
      TabIndex        =   15
      Top             =   5880
      Width           =   1755
   End
   Begin VB.Label Label26 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   285
      Left            =   4440
      TabIndex        =   14
      Top             =   6360
      Width           =   1395
   End
   Begin VB.Label Label36 
      Alignment       =   1  'Right Justify
      Caption         =   "[Myosin] in uM:"
      Height          =   285
      Left            =   720
      TabIndex        =   13
      Top             =   7080
      Visible         =   0   'False
      Width           =   1635
   End
   Begin VB.Label Label2 
      Caption         =   "Quasi-Newton Optimized Method"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   960
      TabIndex        =   0
      Top             =   720
      Width           =   7575
   End
End
Attribute VB_Name = "QNewton_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Check1_Click(Index As Integer)
    If (Index Mod 2) = 0 Then
        If Check1(Index).Value = vbChecked Then
                Text1(Index).Visible = True
                Text2(Index).Visible = True
                Text3(Index).Visible = True
                Check1(Index + 1).Value = vbUnchecked
        Else
                Text1(Index).Visible = False
                Text2(Index).Visible = False
                Text3(Index).Visible = False
        End If
    Else
        If Check1(Index).Value = vbChecked Then
            Text1(Index).Visible = True
            Text2(Index).Visible = True
            Text3(Index).Visible = True
            Check1(Index - 1).Value = vbUnchecked
        Else
            Text1(Index).Visible = False
            Text2(Index).Visible = False
            Text3(Index).Visible = False
        End If
    End If
    
    
End Sub
   
Private Sub Command1_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text30.Text = CommonDialog1.FileName
    
End Sub

Private Sub Command2_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text20.Text = CommonDialog1.FileName
End Sub



Private Sub Display_Click()
    Result_Form.Show
    QNewton_Form.Hide
 End Sub

Private Sub Form_Load()
    Text30.Text = App.path & "\a_t.txt"
    Text20.Text = App.path & "\opt_output"
    Run_Button.Enabled = True
    Display.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub



Private Sub Ret_comm_Click()
    Main_Form.Show
    QNewton_Form.Hide
End Sub

Private Sub Run_Button_Click()
    Dim ip As Integer
    Dim FileNumber As Integer
    Dim Input_File_Name As String
    Dim position(4), real_vs, kp As Integer

    ChDrive App.path
    ChDir App.path
    
    For ip = 1 To 4
        position(ip) = 1
    Next ip
    
    On Error GoTo ErrHandler

    If Text20.Text <> "" Then

        If Time_Option.Value = True Then
            Text15.Text = Text14.Text
            Text15.Enabled = False
            Text15.Visible = False
            Label13.Caption = "[Myosin] in uM:"
            Label36.Visible = False
        End If

        kp = 0
        For i = 0 To 7
            If Check1(i).Value = vbUnchecked Then
                Text1(i).Text = Text5(i).Text
                Text2(i).Text = Text5(i).Text
                Text3(i).Text = Text5(i).Text
            Else
                kp = kp + 1
                position(kp) = i + 1
            End If
               
        Next i
        real_vs = kp
        
        Input_File_Name = App.path & "\Input_params.dat"
        
        'Export data to a file
        FileNumber = FreeFile
        Open Input_File_Name For Output As #FileNumber
        Print #FileNumber, "#Experimental and Output File Name:"
        Print #FileNumber, Text20.Text
        Print #FileNumber, Text30.Text
        Print #FileNumber, "#Number of parameters:"
        Print #FileNumber, "8"
        Print #FileNumber, "#Total Time [s]:"
        Print #FileNumber, Text4.Text
        Print #FileNumber, "#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):"
        For i = 0 To 7
            Print #FileNumber, Text5(i).Text
        Next i
        Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text13.Text
        Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text15.Text
        Print #FileNumber, "#Parameters for guess, low and upper bound:"
        For ip = 0 To 7
            Print #FileNumber, Text1(ip).Text
            Print #FileNumber, Text2(ip).Text
            Print #FileNumber, Text3(ip).Text
        Next ip
        
        Close #FileNumber

        QNewton_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        Display.Enabled = False
        

        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        'Call ExecCmd(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34))
        Call ExecCmd(App.path & "\opt_var.exe " & Chr(34) & Input_File_Name & Chr(34))
        
        QNewton_Form.MousePointer = vbArrow
        
        FileNumber = FreeFile
        File_name = App.path & "\ps.dat"
        Open File_name For Output As #FileNumber
            Print #FileNumber, real_vs
            For ip = 1 To 4
                Print #FileNumber, position(ip)
            Next ip
        Close #FileNumber
        
        Run_Button.Enabled = True
        Display.Enabled = True
       
    End If

    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub



Private Sub Time_Option_Click()
    If Time_Option.Value = True Then
        Text14.Text = 0.5
        Text15.Text = Text14.Text
        Text4.Text = 5
        Text15.Enabled = False
        Text15.Visible = False
        Label13.Caption = "[Myosin] in uM:"
        Label36.Visible = False
    End If
End Sub



Private Sub Titration_Option_Click()
    If Titration_Option.Value = True Then
        Text4.Text = 250
        Text14.Text = 0
        Text15.Enabled = True
        Text15.Visible = True
        Label36.Caption = "[Myosin] Final:"
        Label13.Caption = "[Myosin] Init."
        Label36.Visible = True
    End If
End Sub
