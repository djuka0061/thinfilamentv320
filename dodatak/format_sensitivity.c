#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[])
{
  FILE *f = fopen("param_est.dat", "r+");
  FILE *g = fopen("new_param_est.dat", "w+");
  FILE *s = fopen("new_sensitivity.dat", "w+");
  int n = atoi(argv[1]);
  double br;
  int i = 0, j = 0;
  
  while (fscanf(f, "%Lf", &br) > 0)
  {
  	printf("%e\n", br);
    if (j < (n + 1))
    {
      if (i < 2)
      {
        fprintf(g, "%e,", br);
        i++;
      }
      else
      {
        i = 0;
        j++;
        fprintf(g, "%e\n", br);
      }
    }
    else
    {
    	printf("%d\n",i);
      if (i < n-1)
      {
        fprintf(s, "%e,", br);
        printf("%e,", br);
		i++;
      }
      else
      {
        i = 0;
        j++;
        fprintf(s, "%e\n", br);
		printf("%Lf\n", br);
      }
    }
  }
  fclose(f);
  fclose(g);
  fclose(s);
}
