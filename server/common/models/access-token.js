'use strict';

// Global include
const jwt = require('jsonwebtoken');
// Local include
const configuration = require('../../config/config');
let data = {};

module.exports = (accessToken) => {
  accessToken.resolve = (id, cb) => {
    return jwt.verify(id, configuration.loopback.config.secret, (err, decodedJwt) => {
      if (err) {
        const e = new Error('Invalid Access Token');
        e.status = e.statusCode = 401;
        e.code = 'INVALID_TOKEN';
        return cb(e);
      }
      if (decodedJwt && decodedJwt.id) {
        id = decodedJwt.id;
        data = decodedJwt;
      }
      accessToken.findById(id, (err, token) => {
        if (err) {
          cb(err);
        } else if (token) {
          token.validate((err, isValid) => {
            if (err) {
              cb(err);
            } else if (isValid) {
              token = data.__data || data;
              cb(null, token);
            } else {
              const e = new Error('Invalid Access Token');
              e.status = e.statusCode = 401;
              e.code = 'INVALID_TOKEN';
              cb(e);
            }
          });
        } else {
          cb();
        }
      });
      return null;
    });
  };

  accessToken.getIdForRequest = (req, options) => {
    options = options || {};
    let params = options.params || [];
    let headers = options.headers || [];
    let cookies = options.cookies || [];
    let i = 0;
    let length, id;
    if (options.searchDefaultTokenKeys !== false) {
      params = params.concat(['access_token']);
      headers = headers.concat(['X-Access-Token', 'authorization']);
      cookies = cookies.concat(['access_token', 'authorization']);
    }
    for (length = params.length; i < length; i++) {
      const param = params[i];
      id = req.params && req.params[param] !== undefined ? req.params[param] : req.body && req.body[param] !== undefined ? req.body[param] : req.query && req.query[param] !== undefined ? req.query[param] : undefined;
      if (typeof id === 'string') {
        return id;
      }
    }
    for (i = 0, length = headers.length; i < length; i++) {
      id = req.header(headers[i]);

      if (typeof id === 'string') {
        if (id.indexOf('Bearer ') === 0) {
          id = id.substring(7);
          return id;
        } else if (/^Basic /i.test(id)) {
          id = id.substring(6);
          id = (new Buffer(id, 'base64')).toString('utf8');
          const parts = /^([^:]*):(.*)$/.exec(id);
          if (parts) {
            id = parts[2].length > parts[1].length ? parts[2] : parts[1];
          }
        }
        return id;
      }
    }
    return null;
  };
};
