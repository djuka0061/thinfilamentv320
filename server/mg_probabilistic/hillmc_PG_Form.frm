VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{D940E4E4-6079-11CE-88CB-0020AF6845F6}#1.6#0"; "cwui.ocx"
Begin VB.Form hillmc_PG_Form 
   Caption         =   "Graphs"
   ClientHeight    =   8370
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   14220
   LinkTopic       =   "Form1"
   ScaleHeight     =   8370
   ScaleWidth      =   14220
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton CompareComm 
      Caption         =   "Compare Results"
      Height          =   615
      Left            =   12000
      TabIndex        =   8
      Top             =   5520
      Width           =   1815
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Error Changes"
      ForeColor       =   &H00FF00FF&
      Height          =   330
      Index           =   4
      Left            =   11880
      TabIndex        =   7
      Top             =   2280
      Width           =   2050
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   13320
      Top             =   3000
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Ret_comm 
      Caption         =   "Return"
      Height          =   495
      Left            =   12000
      TabIndex        =   6
      Top             =   6600
      Width           =   1815
   End
   Begin VB.Frame Frame5 
      Caption         =   "Select Graphs"
      Height          =   2595
      Left            =   11760
      TabIndex        =   1
      Top             =   105
      Width           =   2220
      Begin VB.CheckBox Check1 
         Caption         =   "K2 Changes"
         ForeColor       =   &H000000FF&
         Height          =   330
         Index           =   3
         Left            =   105
         TabIndex        =   5
         Top             =   1680
         Width           =   2050
      End
      Begin VB.CheckBox Check1 
         Caption         =   "K1 Changes"
         ForeColor       =   &H00FF0000&
         Height          =   330
         Index           =   2
         Left            =   105
         TabIndex        =   4
         Top             =   1225
         Width           =   2050
      End
      Begin VB.CheckBox Check1 
         Caption         =   "Y Changes"
         ForeColor       =   &H0000C000&
         Height          =   330
         Index           =   1
         Left            =   105
         TabIndex        =   3
         Top             =   770
         Width           =   2050
      End
      Begin VB.CheckBox Check1 
         Caption         =   "L' Changes"
         ForeColor       =   &H00800080&
         Height          =   330
         Index           =   0
         Left            =   105
         TabIndex        =   2
         Top             =   315
         Value           =   1  'Checked
         Width           =   2050
      End
   End
   Begin CWUIControlsLib.CWGraph CWGraph1 
      Height          =   8100
      Left            =   210
      TabIndex        =   0
      Top             =   105
      Width           =   11355
      _Version        =   393218
      _ExtentX        =   20029
      _ExtentY        =   14287
      _StockProps     =   71
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Reset_0         =   0   'False
      CompatibleVers_0=   393218
      Graph_0         =   1
      ClassName_1     =   "CCWGraphFrame"
      opts_1          =   62
      C[0]_1          =   16777215
      C[1]_1          =   -2147483643
      Event_1         =   2
      ClassName_2     =   "CCWGFPlotEvent"
      Owner_2         =   1
      Plots_1         =   3
      ClassName_3     =   "CCWDataPlots"
      Array_3         =   6
      Editor_3        =   4
      ClassName_4     =   "CCWGFPlotArrayEditor"
      Owner_4         =   1
      Array[0]_3      =   5
      ClassName_5     =   "CCWDataPlot"
      opts_5          =   4194367
      Name_5          =   "Plot-1"
      C[0]_5          =   8388736
      C[1]_5          =   8388736
      C[2]_5          =   8388736
      C[3]_5          =   8388736
      Event_5         =   2
      X_5             =   6
      ClassName_6     =   "CCWAxis"
      opts_6          =   575
      Name_6          =   "XAxis"
      Orientation_6   =   2944
      format_6        =   7
      ClassName_7     =   "CCWFormat"
      Scale_6         =   8
      ClassName_8     =   "CCWScale"
      opts_8          =   90112
      rMin_8          =   28
      rMax_8          =   746
      dMin_8          =   1
      dMax_8          =   10
      discInterval_8  =   1
      Radial_6        =   0
      Enum_6          =   9
      ClassName_9     =   "CCWEnum"
      Editor_9        =   10
      ClassName_10    =   "CCWEnumArrayEditor"
      Owner_10        =   6
      Font_6          =   0
      tickopts_6      =   2711
      major_6         =   1
      minor_6         =   0.5
      Caption_6       =   11
      ClassName_11    =   "CCWDrawObj"
      opts_11         =   62
      C[0]_11         =   -2147483640
      Image_11        =   12
      ClassName_12    =   "CCWTextImage"
      style_12        =   16777217
      font_12         =   0
      Animator_11     =   0
      Blinker_11      =   0
      Y_5             =   13
      ClassName_13    =   "CCWAxis"
      opts_13         =   575
      Name_13         =   "YAxis-1"
      C[2]_13         =   8388736
      Orientation_13  =   2067
      format_13       =   14
      ClassName_14    =   "CCWFormat"
      Scale_13        =   15
      ClassName_15    =   "CCWScale"
      opts_15         =   122880
      rMin_15         =   11
      rMax_15         =   513
      dMin_15         =   1
      dMax_15         =   10
      discInterval_15 =   1
      Radial_13       =   0
      Enum_13         =   16
      ClassName_16    =   "CCWEnum"
      Editor_16       =   17
      ClassName_17    =   "CCWEnumArrayEditor"
      Owner_17        =   13
      Font_13         =   0
      tickopts_13     =   2711
      major_13        =   0.5
      minor_13        =   0.25
      Caption_13      =   18
      ClassName_18    =   "CCWDrawObj"
      opts_18         =   62
      C[0]_18         =   8388736
      Image_18        =   19
      ClassName_19    =   "CCWTextImage"
      style_19        =   16777217
      font_19         =   0
      Animator_18     =   0
      Blinker_18      =   0
      LineStyle_5     =   1
      LineWidth_5     =   1
      BaseVal_5       =   2.2250738585072E-308
      BasePlot_5      =   0
      DefaultXInc_5   =   1
      DefaultPlotPerRow_5=   -1  'True
      Array[1]_3      =   20
      ClassName_20    =   "CCWDataPlot"
      opts_20         =   4194367
      Name_20         =   "Plot-2"
      C[0]_20         =   32768
      C[1]_20         =   32768
      C[2]_20         =   32768
      C[3]_20         =   32768
      Event_20        =   2
      X_20            =   6
      Y_20            =   13
      LineStyle_20    =   1
      LineWidth_20    =   1
      BasePlot_20     =   0
      DefaultXInc_20  =   1
      DefaultPlotPerRow_20=   -1  'True
      Array[2]_3      =   21
      ClassName_21    =   "CCWDataPlot"
      opts_21         =   4194367
      Name_21         =   "Plot-3"
      C[0]_21         =   16711680
      C[1]_21         =   16711680
      C[2]_21         =   16711680
      C[3]_21         =   16711680
      Event_21        =   2
      X_21            =   6
      Y_21            =   13
      LineStyle_21    =   1
      LineWidth_21    =   1
      BasePlot_21     =   0
      DefaultXInc_21  =   1
      DefaultPlotPerRow_21=   -1  'True
      Array[3]_3      =   22
      ClassName_22    =   "CCWDataPlot"
      opts_22         =   4194367
      Name_22         =   "Plot-4"
      C[0]_22         =   255
      C[1]_22         =   255
      C[2]_22         =   255
      C[3]_22         =   255
      Event_22        =   2
      X_22            =   6
      Y_22            =   13
      LineStyle_22    =   1
      LineWidth_22    =   1
      BasePlot_22     =   0
      DefaultXInc_22  =   1
      DefaultPlotPerRow_22=   -1  'True
      Array[4]_3      =   23
      ClassName_23    =   "CCWDataPlot"
      opts_23         =   4194367
      Name_23         =   "Plot-5"
      C[0]_23         =   16711935
      C[1]_23         =   255
      C[2]_23         =   16711680
      C[3]_23         =   16776960
      Event_23        =   2
      X_23            =   6
      Y_23            =   13
      LineStyle_23    =   1
      LineWidth_23    =   1
      BasePlot_23     =   0
      DefaultXInc_23  =   1
      DefaultPlotPerRow_23=   -1  'True
      Array[5]_3      =   24
      ClassName_24    =   "CCWDataPlot"
      opts_24         =   4194367
      Name_24         =   "Plot-6"
      C[0]_24         =   16776960
      C[1]_24         =   255
      C[2]_24         =   16711680
      C[3]_24         =   16776960
      Event_24        =   2
      X_24            =   6
      Y_24            =   13
      LineStyle_24    =   1
      LineWidth_24    =   1
      BasePlot_24     =   0
      DefaultXInc_24  =   1
      DefaultPlotPerRow_24=   -1  'True
      Axes_1          =   25
      ClassName_25    =   "CCWAxes"
      Array_25        =   2
      Editor_25       =   26
      ClassName_26    =   "CCWGFAxisArrayEditor"
      Owner_26        =   1
      Array[0]_25     =   6
      Array[1]_25     =   13
      DefaultPlot_1   =   27
      ClassName_27    =   "CCWDataPlot"
      opts_27         =   4194367
      Name_27         =   "[Template]"
      C[0]_27         =   65280
      C[1]_27         =   255
      C[2]_27         =   16711680
      C[3]_27         =   16776960
      Event_27        =   2
      X_27            =   6
      Y_27            =   13
      LineStyle_27    =   1
      LineWidth_27    =   1
      BasePlot_27     =   0
      DefaultXInc_27  =   1
      DefaultPlotPerRow_27=   -1  'True
      Cursors_1       =   28
      ClassName_28    =   "CCWCursors"
      Editor_28       =   29
      ClassName_29    =   "CCWGFCursorArrayEditor"
      Owner_29        =   1
      TrackMode_1     =   2
      GraphBackground_1=   0
      GraphFrame_1    =   30
      ClassName_30    =   "CCWDrawObj"
      opts_30         =   62
      C[0]_30         =   -2147483643
      C[1]_30         =   -2147483643
      Image_30        =   31
      ClassName_31    =   "CCWPictImage"
      opts_31         =   1280
      Rows_31         =   1
      Cols_31         =   1
      F_31            =   -2147483643
      ColorReplaceWith_31=   8421504
      ColorReplace_31 =   8421504
      Tolerance_31    =   2
      Animator_30     =   0
      Blinker_30      =   0
      PlotFrame_1     =   32
      ClassName_32    =   "CCWDrawObj"
      opts_32         =   62
      C[0]_32         =   -2147483643
      C[1]_32         =   16777215
      Image_32        =   33
      ClassName_33    =   "CCWPictImage"
      opts_33         =   1280
      Rows_33         =   1
      Cols_33         =   1
      Pict_33         =   1
      F_33            =   -2147483633
      B_33            =   16777215
      ColorReplaceWith_33=   8421504
      ColorReplace_33 =   8421504
      Tolerance_33    =   2
      Animator_32     =   0
      Blinker_32      =   0
      Caption_1       =   34
      ClassName_34    =   "CCWDrawObj"
      opts_34         =   62
      C[0]_34         =   -2147483640
      Image_34        =   35
      ClassName_35    =   "CCWTextImage"
      font_35         =   0
      Animator_34     =   0
      Blinker_34      =   0
      DefaultXInc_1   =   1
      DefaultPlotPerRow_1=   -1  'True
      Bindings_1      =   36
      ClassName_36    =   "CCWBindingHolderArray"
      Editor_36       =   37
      ClassName_37    =   "CCWBindingHolderArrayEditor"
      Owner_37        =   1
      Annotations_1   =   38
      ClassName_38    =   "CCWAnnotations"
      Array_38        =   6
      Editor_38       =   39
      ClassName_39    =   "CCWAnnotationArrayEditor"
      Owner_39        =   1
      Array[0]_38     =   40
      ClassName_40    =   "CCWAnnotation"
      opts_40         =   63
      Name_40         =   "Annotation-1"
      CoordinateType_40=   2
      Plot_40         =   27
      Text_40         =   "Time vs. Fraction of Unbound Actin"
      TextXPoint_40   =   350
      TextYPoint_40   =   100
      TextColor_40    =   8388736
      TextFont_40     =   41
      ClassName_41    =   "CCWFont"
      bFont_41        =   -1  'True
      BeginProperty Font_41 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_40 =   42
      ClassName_42    =   "CDataBuffer"
      Type_42         =   5
      m_cDims;_42     =   1
      ShapeYPoints_40 =   43
      ClassName_43    =   "CDataBuffer"
      Type_43         =   5
      m_cDims;_43     =   1
      ShapeFillColor_40=   16777215
      ShapeLineColor_40=   16777215
      ShapeLineWidth_40=   1
      ShapeLineStyle_40=   1
      ShapePointStyle_40=   10
      ShapeImage_40   =   44
      ClassName_44    =   "CCWDrawObj"
      opts_44         =   62
      Image_44        =   45
      ClassName_45    =   "CCWPictImage"
      opts_45         =   1280
      Rows_45         =   1
      Cols_45         =   1
      Pict_45         =   7
      F_45            =   -2147483633
      B_45            =   -2147483633
      ColorReplaceWith_45=   8421504
      ColorReplace_45 =   8421504
      Tolerance_45    =   2
      Animator_44     =   0
      Blinker_44      =   0
      ArrowColor_40   =   8388608
      ArrowWidth_40   =   1
      ArrowLineStyle_40=   1
      ArrowHeadStyle_40=   1
      Array[1]_38     =   46
      ClassName_46    =   "CCWAnnotation"
      opts_46         =   63
      Name_46         =   "Annotation-2"
      CoordinateType_46=   2
      Plot_46         =   27
      Text_46         =   "Annotation-2"
      TextXPoint_46   =   350
      TextYPoint_46   =   125
      TextColor_46    =   32768
      TextFont_46     =   47
      ClassName_47    =   "CCWFont"
      bFont_47        =   -1  'True
      BeginProperty Font_47 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_46 =   48
      ClassName_48    =   "CDataBuffer"
      Type_48         =   5
      m_cDims;_48     =   1
      ShapeYPoints_46 =   49
      ClassName_49    =   "CDataBuffer"
      Type_49         =   5
      m_cDims;_49     =   1
      ShapeFillColor_46=   16777215
      ShapeLineColor_46=   16777215
      ShapeLineWidth_46=   1
      ShapeLineStyle_46=   1
      ShapePointStyle_46=   10
      ShapeImage_46   =   50
      ClassName_50    =   "CCWDrawObj"
      opts_50         =   62
      Image_50        =   51
      ClassName_51    =   "CCWPictImage"
      opts_51         =   1280
      Rows_51         =   1
      Cols_51         =   1
      Pict_51         =   7
      F_51            =   -2147483633
      B_51            =   -2147483633
      ColorReplaceWith_51=   8421504
      ColorReplace_51 =   8421504
      Tolerance_51    =   2
      Animator_50     =   0
      Blinker_50      =   0
      ArrowColor_46   =   8388736
      ArrowWidth_46   =   1
      ArrowLineStyle_46=   1
      ArrowHeadStyle_46=   1
      Array[2]_38     =   52
      ClassName_52    =   "CCWAnnotation"
      opts_52         =   63
      Name_52         =   "Annotation-3"
      CoordinateType_52=   2
      Plot_52         =   27
      Text_52         =   "Time vs. Fraction of S1 in A state"
      TextXPoint_52   =   350
      TextYPoint_52   =   150
      TextColor_52    =   16711680
      TextFont_52     =   53
      ClassName_53    =   "CCWFont"
      bFont_53        =   -1  'True
      BeginProperty Font_53 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_52 =   54
      ClassName_54    =   "CDataBuffer"
      Type_54         =   5
      m_cDims;_54     =   1
      ShapeYPoints_52 =   55
      ClassName_55    =   "CDataBuffer"
      Type_55         =   5
      m_cDims;_55     =   1
      ShapeFillColor_52=   16777215
      ShapeLineColor_52=   16777215
      ShapeLineWidth_52=   1
      ShapeLineStyle_52=   1
      ShapePointStyle_52=   10
      ShapeImage_52   =   56
      ClassName_56    =   "CCWDrawObj"
      opts_56         =   62
      Image_56        =   57
      ClassName_57    =   "CCWPictImage"
      opts_57         =   1280
      Rows_57         =   1
      Cols_57         =   1
      Pict_57         =   7
      F_57            =   -2147483633
      B_57            =   -2147483633
      ColorReplaceWith_57=   8421504
      ColorReplace_57 =   8421504
      Tolerance_57    =   2
      Animator_56     =   0
      Blinker_56      =   0
      ArrowColor_52   =   255
      ArrowWidth_52   =   1
      ArrowLineStyle_52=   1
      ArrowHeadStyle_52=   1
      Array[3]_38     =   58
      ClassName_58    =   "CCWAnnotation"
      opts_58         =   63
      Name_58         =   "Annotation-4"
      CoordinateType_58=   2
      Plot_58         =   27
      Text_58         =   "Time vs. Fraction of S1 in R state"
      TextXPoint_58   =   350
      TextYPoint_58   =   175
      TextColor_58    =   255
      TextFont_58     =   59
      ClassName_59    =   "CCWFont"
      bFont_59        =   -1  'True
      BeginProperty Font_59 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_58 =   60
      ClassName_60    =   "CDataBuffer"
      Type_60         =   5
      m_cDims;_60     =   1
      ShapeYPoints_58 =   61
      ClassName_61    =   "CDataBuffer"
      Type_61         =   5
      m_cDims;_61     =   1
      ShapeFillColor_58=   16777215
      ShapeLineColor_58=   16777215
      ShapeLineWidth_58=   1
      ShapeLineStyle_58=   1
      ShapePointStyle_58=   10
      ShapeImage_58   =   62
      ClassName_62    =   "CCWDrawObj"
      opts_62         =   62
      Image_62        =   63
      ClassName_63    =   "CCWPictImage"
      opts_63         =   1280
      Rows_63         =   1
      Cols_63         =   1
      Pict_63         =   7
      F_63            =   -2147483633
      B_63            =   -2147483633
      ColorReplaceWith_63=   8421504
      ColorReplace_63 =   8421504
      Tolerance_63    =   2
      Animator_62     =   0
      Blinker_62      =   0
      ArrowColor_58   =   32768
      ArrowWidth_58   =   1
      ArrowLineStyle_58=   1
      ArrowHeadStyle_58=   1
      Array[4]_38     =   64
      ClassName_64    =   "CCWAnnotation"
      opts_64         =   63
      Name_64         =   "Annotation-5"
      CoordinateType_64=   2
      Plot_64         =   27
      Text_64         =   "Annotation-5"
      TextXPoint_64   =   350
      TextYPoint_64   =   200
      TextColor_64    =   16711935
      TextFont_64     =   65
      ClassName_65    =   "CCWFont"
      bFont_65        =   -1  'True
      BeginProperty Font_65 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_64 =   66
      ClassName_66    =   "CDataBuffer"
      Type_66         =   5
      m_cDims;_66     =   1
      ShapeYPoints_64 =   67
      ClassName_67    =   "CDataBuffer"
      Type_67         =   5
      m_cDims;_67     =   1
      ShapeFillColor_64=   16777215
      ShapeLineColor_64=   16777215
      ShapeLineWidth_64=   1
      ShapeLineStyle_64=   1
      ShapePointStyle_64=   10
      ShapeImage_64   =   68
      ClassName_68    =   "CCWDrawObj"
      opts_68         =   62
      Image_68        =   69
      ClassName_69    =   "CCWPictImage"
      opts_69         =   1280
      Rows_69         =   1
      Cols_69         =   1
      Pict_69         =   7
      F_69            =   -2147483633
      B_69            =   -2147483633
      ColorReplaceWith_69=   8421504
      ColorReplace_69 =   8421504
      Tolerance_69    =   2
      Animator_68     =   0
      Blinker_68      =   0
      ArrowColor_64   =   16777215
      ArrowWidth_64   =   1
      ArrowLineStyle_64=   1
      ArrowHeadStyle_64=   1
      Array[5]_38     =   70
      ClassName_70    =   "CCWAnnotation"
      opts_70         =   63
      Name_70         =   "Annotation-6"
      CoordinateType_70=   2
      Plot_70         =   27
      Text_70         =   "Annotation-6"
      TextXPoint_70   =   350
      TextYPoint_70   =   225
      TextColor_70    =   16776960
      TextFont_70     =   71
      ClassName_71    =   "CCWFont"
      bFont_71        =   -1  'True
      BeginProperty Font_71 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_70 =   72
      ClassName_72    =   "CDataBuffer"
      Type_72         =   5
      m_cDims;_72     =   1
      ShapeYPoints_70 =   73
      ClassName_73    =   "CDataBuffer"
      Type_73         =   5
      m_cDims;_73     =   1
      ShapeFillColor_70=   16777215
      ShapeLineColor_70=   16777215
      ShapeLineWidth_70=   1
      ShapeLineStyle_70=   1
      ShapePointStyle_70=   10
      ShapeImage_70   =   74
      ClassName_74    =   "CCWDrawObj"
      opts_74         =   62
      Image_74        =   75
      ClassName_75    =   "CCWPictImage"
      opts_75         =   1280
      Rows_75         =   1
      Cols_75         =   1
      Pict_75         =   7
      F_75            =   -2147483633
      B_75            =   -2147483633
      ColorReplaceWith_75=   8421504
      ColorReplace_75 =   8421504
      Tolerance_75    =   2
      Animator_74     =   0
      Blinker_74      =   0
      ArrowColor_70   =   16777215
      ArrowWidth_70   =   1
      ArrowLineStyle_70=   1
      ArrowHeadStyle_70=   1
      AnnotationTemplate_1=   76
      ClassName_76    =   "CCWAnnotation"
      opts_76         =   63
      Name_76         =   "[Template]"
      Plot_76         =   27
      Text_76         =   "[Template]"
      TextXPoint_76   =   6.7
      TextYPoint_76   =   6.7
      TextColor_76    =   16777215
      TextFont_76     =   77
      ClassName_77    =   "CCWFont"
      bFont_77        =   -1  'True
      BeginProperty Font_77 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_76 =   78
      ClassName_78    =   "CDataBuffer"
      Type_78         =   5
      m_cDims;_78     =   1
      ShapeYPoints_76 =   79
      ClassName_79    =   "CDataBuffer"
      Type_79         =   5
      m_cDims;_79     =   1
      ShapeFillColor_76=   16777215
      ShapeLineColor_76=   16777215
      ShapeLineWidth_76=   1
      ShapeLineStyle_76=   1
      ShapePointStyle_76=   10
      ShapeImage_76   =   80
      ClassName_80    =   "CCWDrawObj"
      opts_80         =   62
      Image_80        =   81
      ClassName_81    =   "CCWPictImage"
      opts_81         =   1280
      Rows_81         =   1
      Cols_81         =   1
      Pict_81         =   7
      F_81            =   -2147483633
      B_81            =   -2147483633
      ColorReplaceWith_81=   8421504
      ColorReplace_81 =   8421504
      Tolerance_81    =   2
      Animator_80     =   0
      Blinker_80      =   0
      ArrowColor_76   =   16777215
      ArrowWidth_76   =   1
      ArrowLineStyle_76=   1
      ArrowHeadStyle_76=   1
   End
End
Attribute VB_Name = "hillmc_PG_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
    Dim tkb(100), tkt(100), tk1(100) As Double
    Dim tk2(100), ter(100) As Double
    Dim Time() As Double
    Dim FLU() As Double
    Dim OutputFileName, OutputFiletitle As String
    Dim citem(9) As String
    Dim position(5), real_vs As Integer
    
        

  Private Sub Check1_Click(Index As Integer)
    Dim ip, jp As Integer
    Dim ftitle As String
    
    CWGraph1.Axes.Item(2).Log = True
    
    If Check1(Index).Value = vbChecked Then
    '    CWGraph1.ClearData
    '    CWGraph1.Plots.Item(3).PlotXvsY Time, Unbound_Actin
       
        If (Index + 1 <= real_vs) Then
        Select Case Index + 1
            Case 1
                CWGraph1.Plots.Item(1).PlotY tkb
                ftitle = citem(position(1))
            Case 2
                CWGraph1.Plots.Item(2).PlotY tkt
                ftitle = citem(position(2))
            Case 3
                CWGraph1.Plots.Item(3).PlotY tk1
                ftitle = citem(position(3))
            Case 4
                CWGraph1.Plots.Item(4).PlotY tk2
                ftitle = citem(position(4))
            End Select
        Else
            
            CWGraph1.Plots.Item(real_vs + 1).PlotY ter
            ftitle = "Error"
            
        End If
         
    '    CWGraph1.Plots.Item(2).PlotXvsY Time, OCU
    '    CWGraph1.Plots.Item(4).PlotXvsY Time, FCM

    '    CWGraph1.Annotations.Item(3).Caption.Text = "Time vs. Fraction of Unbound Actin"
        CWGraph1.Annotations.Item(Index + 1).Caption.Text = ftitle & " vs. Iteration"
    '    CWGraph1.Annotations.Item(2).Caption.Text = "Time vs. Fraction Saturation"
    '   CWGraph1.Annotations.Item(4).Caption.Text = "Time vs. FreeMyosin Concentration"

    '    Frame5.Visible = True

        CWGraph1.Axes.Item(1).Caption = "Iteration"
        'CWGraph1.Axes.Item(2).Caption = ftitle
        CWGraph1.Axes.Item(1).AutoScaleNow
        CWGraph1.Axes.Item(2).AutoScaleNow
     
     
     Else
        'CWGraph1.ClearData
        CWGraph1.Annotations.Item(Index + 1).Caption.Text = ""
        CWGraph1.Plots.Item(Index + 1).ClearData
        CWGraph1.Axes.Item(2).AutoScaleNow
        CWGraph1.Axes.Item(1).AutoScaleNow
     End If
    
    Exit Sub



ErrorHandler:                                                       ' Error-handling routine.

    'Display unanticipated error message.
    Msg = "Error " & Err.Number & ": " & Err.Description
    MsgBox Msg, vbExclamation

    
    
End Sub



Private Sub CompareComm_Click()
    Dim Msg As String

    Dim i As Long, j As Long
    Dim OpenFileNumber As Integer
    ' Import data to a file
    Dim num_Rows As Long
    Dim SearchText As String
    
    CWGraph1.ClearData
    CWGraph1.Axes.Item(2).Log = False
    
    num_Rows = 0
    OpenFileNumber = FreeFile
    OutputFileName = Para_HILLSTO_Form.Text30.Text
    Open OutputFileName For Input As #OpenFileNumber
    ' Open file

    While Not EOF(OpenFileNumber)
        Line Input #OpenFileNumber, SearchText
        num_Rows = num_Rows + 1
    Wend
 
    ReDim Time(1 To num_Rows - 1)
    ReDim FLU(1 To num_Rows - 1)
    
    Seek #OpenFileNumber, 1
    Line Input #OpenFileNumber, SearchText          ' First Line
    i = 0
    While Not EOF(OpenFileNumber)
         i = i + 1
         Input #OpenFileNumber, Time(i), FLU(i)
    Wend
    Close #OpenFileNumber
    
    CWGraph1.Plots.Item(1).PlotXvsY Time, FLU
    CWGraph1.Annotations.Item(1).Caption.Text = OutputFileName _
                & ": " & " Time vs. Fractional Fluorescence"
   
    CWGraph1.Axes.Item(1).Caption = "Time [s]"
    CWGraph1.Axes.Item(2).Caption = "Fractional Fluorescence"
    
    ChDrive App.path
    ChDir App.path
    
    num_Rows = 0
    OpenFileNumber = FreeFile
    OutputFileName = App.path & "\model0001.dat"
    Open OutputFileName For Input As #OpenFileNumber
    ' Open file

    While Not EOF(OpenFileNumber)
        Line Input #OpenFileNumber, SearchText
        num_Rows = num_Rows + 1
    Wend
 
    ReDim Time(1 To num_Rows - 1)
    ReDim FLU(1 To num_Rows - 1)
    
    Seek #OpenFileNumber, 1
    Line Input #OpenFileNumber, SearchText          ' First Line
    i = 0
    While Not EOF(OpenFileNumber)
         i = i + 1
         Input #OpenFileNumber, Time(i), FLU(i)
    Wend
    Close #OpenFileNumber
    
    CWGraph1.Plots.Item(2).PlotXvsY Time, FLU
    CWGraph1.Annotations.Item(2).Caption.Text = OutputFileName _
                & ": " & " Time vs. Fractional Fluorescence"
    CWGraph1.Axes.Item(1).AutoScaleNow
    CWGraph1.Axes.Item(2).AutoScaleNow
    
    
    
End Sub

Private Sub Form_Activate()
    Call Form_Load
End Sub

Private Sub Form_Load()

    Dim File_name As String
    Dim FileNumber, ip, jp As Integer
    Dim flen As Integer
    Dim int_max As Integer
    
    int_max = CInt(Para_HILLSTO_Form.Text19.Text)
    
    ChDrive App.path
    ChDir App.path
    
    citem(1) = "alphao: "
    citem(2) = "beta0: "
    citem(3) = "y1: "
    citem(4) = "y2: "
    citem(5) = "k1: "
    citem(6) = "k1': "
    citem(7) = "k2: "
    citem(8) = "k2': "
    
    For i = 0 To 4
        Check1(i).Enabled = False
        Check1(i).Visible = False
    Next i
 
    
    CWGraph1.ClearData
    For i = 1 To 6
        CWGraph1.Annotations.Item(i).Caption.Text = ""
    Next i
    CWGraph1.Axes.Item(1).Caption = "Iteration"
    'CWGraph1.Axes.Item(2).Caption = "Kb"
    CWGraph1.Axes.Item(2).Log = True
       
    FileNumber = FreeFile
    File_name = App.path & "\ps.dat"
   
    Open File_name For Input As #FileNumber
        Input #FileNumber, real_vs
        For ip = 1 To real_vs
            Input #FileNumber, position(ip)
        Next ip
    Close #FileNumber
    
    For i = 0 To real_vs - 1
        Check1(i).Caption = citem(position(i + 1))
        Check1(i).Enabled = True
        Check1(i).Visible = True
    Next i
    Check1(real_vs).Caption = "Error:"
    Check1(real_vs).Enabled = True
    Check1(real_vs).Visible = True
       
    FileNumber = FreeFile
    File_name = App.path & "\kb.dat"
   
    Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            Input #FileNumber, tkb(ip)
        Next ip
    Close #FileNumber
  
    CWGraph1.Plots.Item(1).PlotY tkb
    CWGraph1.Axes.Item(1).AutoScaleNow
    CWGraph1.Axes.Item(2).AutoScaleNow
    CWGraph1.Annotations.Item(1).Caption.Text = "Kb  vs. Iteration"
    
    File_name = App.path & "\kt.dat"
    Open File_name For Input As #FileNumber
    For ip = 1 To int_max
          Input #FileNumber, tkt(ip)
    Next ip
    Close #FileNumber
    
    
    File_name = App.path & "\k1.dat"
    Open File_name For Input As #FileNumber
    For ip = 1 To int_max
          Input #FileNumber, tk1(ip)
    Next ip
    Close #FileNumber
    
    
    File_name = App.path & "\k2.dat"
    Open File_name For Input As #FileNumber
    For ip = 1 To int_max
          Input #FileNumber, tk2(ip)
    Next ip
    Close #FileNumber
   
    File_name = App.path & "\er.dat"
    Open File_name For Input As #FileNumber
    For ip = 1 To int_max
          Input #FileNumber, ter(ip)
    Next ip
    Close #FileNumber
    
    
   
End Sub



Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub



Private Sub Ret_comm_Click()
    Para_HILLSTO_Form.Show
    hillmc_PG_Form.Hide
End Sub
