(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-home-home-module"],{

/***/ "./src/app/views/home/home-routing.module.ts":
/*!***************************************************!*\
  !*** ./src/app/views/home/home-routing.module.ts ***!
  \***************************************************/
/*! exports provided: HomeRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeRoutingModule", function() { return HomeRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.component */ "./src/app/views/home/home.component.ts");




var routes = [
    {
        path: '',
        component: _home_component__WEBPACK_IMPORTED_MODULE_3__["HomeComponent"],
        data: {
            title: 'Home'
        }
    }
];
var HomeRoutingModule = /** @class */ (function () {
    function HomeRoutingModule() {
    }
    HomeRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], HomeRoutingModule);
    return HomeRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/home/home.component.html":
/*!************************************************!*\
  !*** ./src/app/views/home/home.component.html ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn d-flex\">\r\n  <div class=\"d-flex w-100 \">\r\n    <div class=\"d-flex w-100 card card-accent-primary \">\r\n      <div class=\"card-header\">\r\n        Software Description\r\n      </div>\r\n      <div class=\"card-body d-flex w-100  \">\r\n        <div class=\"d-flex flex-column\">\r\n          <div class=\"d-flex flex-column text-primary title-center\">\r\n            <h2> Thin Filament Regulation v4.0</h2>\r\n          </div>\r\n          <div class=\"text-div w-100\">\r\n            This is a web version of Thin Filament Regulation v3.0 software developed in 2006. by Eduard Xiaochuan\r\n            Li, Juan Del Alamo De Petro, Aleksandar Marinkovic, Adriano Alencar, Srboljub Mijailovich. The software provides:\r\n          </div>\r\n          <div class=\"text-div pl-5 pb-2 w-100\">\r\n            <strong> Simulatations of kinetical binding of myosin S1 with actin filaments in regulated muscle. </strong> <br /> The\r\n                three-state Geeve's Model is simulated through numerical and stochatical methods.\r\n                <br /> Reference: <a class=\"navlink-active\" href=\"https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1304903/\">Robin Maytum etc., Biochemistry, 38, 1102-1110, 1999</a> </div>\r\n                <div class=\"text-div pl-5 pb-2 w-100\">\r\n              <strong>The two-state Hill's Model, which is simulated in numerical and stochatical approaches.</strong> <br />The\r\n                cooperativity of Hill's model is implemented in stochatical approach.\r\n                <br /> Reference:  <a class=\"navlink-active\" href=\"https://www.ncbi.nlm.nih.gov/pmc/articles/PMC1304587/\">Yi-der Chen etc., Biophysical Journal, 80, 2338-2349, 2001</a></div>\r\n                <div class=\"text-div pl-5 pb-2 w-100\">\r\n              <strong>Estimation of the Kb, Kt, K1+ and K2+, based on Geeve's Model.</strong> <br /> Estimations are conducted through a\r\n                Sensitivity Matrix parameter estimation method, a Quasi-Newton method, and a Simulated Annealing\r\n                (SIMANN)\r\n                method.\r\n            \r\n\r\n          </div>\r\n          <div class=\"d-flex flex-column text-div pb-2 w-100\">\r\n\r\n            <div class=\"d-flex w-100\">\r\n              <p>Following simulations and parameter estimations are available:</p>\r\n            </div>\r\n            <!-- 1 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/probabilistic\">MG Model –\r\n                  Probabilistic</a> </div>\r\n              <div class=\"d-flex w-50\">The three-state MG model of myosin S1 binding with regulated Actin7. TmTn is\r\n                simulated. The kinetic equations are solved by numerical method.</div>\r\n            </div>\r\n            <!-- 2 -->\r\n            <div class=\"d-flex w-100 bp-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/stochastical\">MG Model -\r\n                  Stochastical</a> </div>\r\n              <div class=\"d-flex w-50\">The three-state MG model of myosin S1 binding with regulated Actin7. TmTn is\r\n                simulated. The kinetic equations are solved by Monte Carlo approach.</div>\r\n            </div>\r\n            <!-- 3 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/hill-stochastical\">Hill Model\r\n                  - Stochastical</a> </div>\r\n              <div class=\"d-flex w-50\">The two-state Hill model of myosin S1 binding with regulated Actin7. TmTn is\r\n                simulated. The kinetic equations are solved by Monte Carlo approach. The cooperativity is implemented.\r\n              </div>\r\n            </div>\r\n            <!-- 4 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/hill-probabilistic\">Hill Model\r\n                  - Probabilistic</a> </div>\r\n              <div class=\"d-flex w-50\">The two-state Hill model of myosin S1 binding with regulated Actin7. TmTn is\r\n                simulated. The kinetic equations are solved by numerical method.</div>\r\n            </div>\r\n            <!-- 5 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/mg-probabilistic\">Parameter\r\n                  Estimation <br /> MG Prob DLS</a> </div>\r\n              <div class=\"d-flex w-50\">Kinetic Parameters, kb, kt, k1+, k2+ of MG model are matched with the\r\n                experimental data. The resolution matrix of those parameters are calculated.</div>\r\n            </div>\r\n            <!-- 6 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/monte-carlo\">Parameter\r\n                  Estimation\r\n                  <br /> MG Sto DLS</a> </div>\r\n              <div class=\"d-flex w-50\">Kinetic Parameters, kb, kt, k1+, etc. based on MG model are matched with the\r\n                experimental data. The resolution matrix of those parameters are calculated through the Monte Carlo\r\n                simulation.</div>\r\n            </div>\r\n            <!-- 7 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/hill-monte-carlo\">Parameter\r\n                  Estimation <br />\r\n                  Hill Sto DLS</a> </div>\r\n              <div class=\"d-flex w-50\">Hill Model Parameters K1, K2, Y, L' are estimated by DLS method against\r\n                Experimental Data through Monte Carlo simulation.</div>\r\n            </div>\r\n            <!-- 8 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/simulated-annealing\">Parameter\r\n                  Optimization <br /> SIMANN</a> </div>\r\n              <div class=\"d-flex w-50\">Kinetic Parameters, kb, kt, k1+, k2+ of MG model are optimazed by simulated\r\n                annealing (SIMANN) global method against the experimental data.</div>\r\n            </div>\r\n            <!-- 9 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/quasi-newton\">Parameter\r\n                  Optimization <br /> Quasi-Newton</a> </div>\r\n              <div class=\"d-flex w-50\">Kinetic Parameters, kb, kt, k1+, k2+ of MG model are optimazed by Quasi-Newton\r\n                method against the experimental data.</div>\r\n            </div>\r\n            <!-- 10 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/david\">Parameter Estimation\r\n                  <br /> David\r\n                  DLS</a> </div>\r\n              <div class=\"d-flex w-50\">Kinetic Parameters, Kti, GAMA of David model are matched with the experimental\r\n                data.</div>\r\n            </div>\r\n            <!-- 11 -->\r\n            <div class=\"d-flex w-100 pb-3\">\r\n              <div class=\"d-flex w-20  pl-5 text-bold\"> <a class=\"navlink-active\" href=\"#/widgets\">Parameter Estimation\r\n                  <br /> ATPase\r\n                  DLS</a> </div>\r\n              <div class=\"d-flex w-50\">Kinetic Parameters of ATPase model are matched with the experimental data.</div>\r\n            </div>\r\n          </div>\r\n\r\n          <div class=\"d-flex text-div w-100\">\r\n              Output data of all simulations as well as parameter convergence data are\r\n                  presented in a form of graphs,\r\n                  but also available in csv format ready for download.\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/home/home.component.scss":
/*!************************************************!*\
  !*** ./src/app/views/home/home.component.scss ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".text-div {\n  font-size: 1.1rem !important; }\n\n.title-center {\n  text-align: center !important;\n  margin-bottom: 3rem !important;\n  margin-top: 3rem !important; }\n\n.text-bold {\n  font-weight: bold; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaG9tZS9GOlxcdGhpbmZpbGFtZW50djMyMFxcbmV3X2NsaWVudC9zcmNcXGFwcFxcdmlld3NcXGhvbWVcXGhvbWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFFSSw0QkFBNEIsRUFBQTs7QUFLaEM7RUFDSSw2QkFBNkI7RUFDN0IsOEJBQThCO0VBQzlCLDJCQUEyQixFQUFBOztBQUcvQjtFQUNJLGlCQUFpQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvaG9tZS9ob21lLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnRleHQtZGl2IHtcclxuICAgIC8vY29sb3I6IGNhZGV0Ymx1ZTtcclxuICAgIGZvbnQtc2l6ZTogMS4xcmVtICFpbXBvcnRhbnQ7XHJcblxyXG4gICAgLy9ib3JkZXItYm90dG9tOiAxcHggZGFzaGVkIGxpZ2h0Ymx1ZTtcclxuICB9XHJcblxyXG4udGl0bGUtY2VudGVyIHtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLWJvdHRvbTogM3JlbSAhaW1wb3J0YW50O1xyXG4gICAgbWFyZ2luLXRvcDogM3JlbSAhaW1wb3J0YW50O1xyXG59XHJcbiAgXHJcbi50ZXh0LWJvbGQge1xyXG4gICAgZm9udC13ZWlnaHQ6IGJvbGQ7XHJcbn0iXX0= */"

/***/ }),

/***/ "./src/app/views/home/home.component.ts":
/*!**********************************************!*\
  !*** ./src/app/views/home/home.component.ts ***!
  \**********************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var HomeComponent = /** @class */ (function () {
    function HomeComponent() {
    }
    HomeComponent.prototype.ngOnInit = function () {
    };
    HomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/views/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.scss */ "./src/app/views/home/home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/views/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/views/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomeModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeModule", function() { return HomeModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _home_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.component */ "./src/app/views/home/home.component.ts");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./home-routing.module */ "./src/app/views/home/home-routing.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");











var HomeModule = /** @class */ (function () {
    function HomeModule() {
    }
    HomeModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _home_routing_module__WEBPACK_IMPORTED_MODULE_7__["HomeRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(),
                _angular_material_table__WEBPACK_IMPORTED_MODULE_8__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatPaginatorModule"]
            ],
            declarations: [_home_component__WEBPACK_IMPORTED_MODULE_6__["HomeComponent"]],
            exports: [
                _angular_material_table__WEBPACK_IMPORTED_MODULE_8__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_9__["MatPaginatorModule"]
            ]
        })
    ], HomeModule);
    return HomeModule;
}());



/***/ })

}]);
//# sourceMappingURL=views-home-home-module.js.map