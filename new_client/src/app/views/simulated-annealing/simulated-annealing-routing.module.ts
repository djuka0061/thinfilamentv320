import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SimulatedAnnealingComponent } from './simulated-annealing.component';

const routes: Routes = [
  {
    path: '',
    component: SimulatedAnnealingComponent,
    data: {
      title: 'Simulated Annealing'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SimulatedAnnealingRoutingModule {}
