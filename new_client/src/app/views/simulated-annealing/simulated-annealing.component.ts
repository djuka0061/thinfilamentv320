import { Component, OnInit, QueryList, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { SimulatedAnnealingService } from '../../service/simulated_annealing.service';
import { environment } from '../../../environments/environment.prod';
import { Chart } from 'chart.js'

const host = environment.server.host;

export interface Graph {
  datasets: Array<any>;
  options: any;
  chartType: string;
  colors: Array<any>;
  legend: boolean;
  labels: Array<any>;
  visibility: Boolean;
}

@Component({
  selector: 'app-simulated-annealing',
  templateUrl: './simulated-annealing.component.html',
  styleUrls: ['./simulated-annealing.component.scss']
})
export class SimulatedAnnealingComponent implements OnInit, AfterViewInit {
  angForm: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    kb_plus: new FormControl(''),
    kb_fiting: new FormControl(''),
    kb_guess: new FormControl(''),
    kb_lower_bound: new FormControl(''),
    kb_upper_bound: new FormControl(''),
    kb_minus: new FormControl(''),
    kb_minus_fiting: new FormControl(''),
    kb_minus_guess: new FormControl(''),
    kb_minus_lower_bound: new FormControl(''),
    kb_minus_upper_bound: new FormControl(''),
    kt_plus: new FormControl(''),
    kt_fiting: new FormControl(''),
    kt_guess: new FormControl(''),
    kt_lower_bound: new FormControl(''),
    kt_upper_bound: new FormControl(''),
    kt_minus: new FormControl(''),
    kt_minus_fiting: new FormControl(''),
    kt_minus_guess: new FormControl(''),
    kt_minus_lower_bound: new FormControl(''),
    kt_minus_upper_bound: new FormControl(''),
    k1_plus: new FormControl(''),
    k1_fiting: new FormControl(''),
    k1_guess: new FormControl(''),
    k1_lower_bound: new FormControl(''),
    k1_upper_bound: new FormControl(''),
    k1_minus: new FormControl(''),
    k1_minus_fiting: new FormControl(''),
    k1_minus_guess: new FormControl(''),
    k1_minus_lower_bound: new FormControl(''),
    k1_minus_upper_bound: new FormControl(''),
    k2_plus: new FormControl(''),
    k2_fiting: new FormControl(''),
    k2_guess: new FormControl(''),
    k2_lower_bound: new FormControl(''),
    k2_upper_bound: new FormControl(''),
    k2_minus: new FormControl(''),
    k2_minus_fiting: new FormControl(''),
    k2_minus_guess: new FormControl(''),
    k2_minus_lower_bound: new FormControl(''),
    k2_minus_upper_bound: new FormControl(''),
    actin: new FormControl(''),
    myosin: new FormControl(''),
    type_of_simulation: new FormControl(''),
    final_myosin: new FormControl(''),
    file: new FormControl('')
  });

  constructor(private fb: FormBuilder, private SimulatedAnnealingService: SimulatedAnnealingService) { }

  public monteCarlo: any;
  public dataSource;
  public sensitivityMatrix;
  public inputData = this.profileForm;;
  public dugmeRun: Boolean = false;
  public displayedColumns: string[];
  public isVisible = true;
  public run_button = "RUN";
  public typeOfSimulation: Number = 1;
  fileToUpload: File = null;

  public experimentalModelGraph

  ngOnInit() {
    this.SimulatedAnnealingService.getInputData('test').subscribe(data => {
      if (data) {
        this.inputData = data;
      }
    });
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {})
  }

  ngAfterViewInit(): void {

  }

  download() {
    window.open(host + 'simulated_annealings/users/test21/download');
  }

  onSubmit() {
    this.dugmeRun = true;
    this.run_button = "RUNNING";
    this.isVisible = false;
    const body = this.profileForm.value;
    const file = this.profileForm.value.file
    this.experimentalModelGraph.destroy()
    this.SimulatedAnnealingService.upload(this.fileToUpload).subscribe(upload => {
      this.SimulatedAnnealingService.getData(body).subscribe(data => {
        const a_t = data.a_t;

        this.experimentAndModelGraph(a_t)
        this.isVisible = true;

        for (let i = 1; i < data.estimated_values.length; i++) {
          data.estimated_values[i][1] = data.estimated_values[i][1].toFixed(6)
        }

        this.dataSource = data.estimated_values;
        this.dugmeRun = false;
        this.run_button = "RUN";
      });
    });
  }

  saveSimulation() {
    const body = this.profileForm.value;
    this.SimulatedAnnealingService.saveSimulation(body).subscribe(data => {
    })
  }

  openSaveSimulation(file) {
    this.experimentalModelGraph.destroy()
    const fileData = file.target.files
    var reader = new FileReader();
    reader.readAsText(fileData[0]);
    reader.onload = (_event) => {
      const data = JSON.parse(reader.result.toString());
      if (data.a_t) {
        const a_t = data.a_t;

        this.experimentAndModelGraph(a_t)
        this.isVisible = true;

        for (let i = 1; i < data.estimated_values.length; i++) {
          data.estimated_values[i][1] = data.estimated_values[i][1].toFixed(6)
        }
        this.dataSource = data.estimated_values;
      }
    };
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onCheckboxChange(box1: string, box2: string) {
    if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
      if (this.inputData[box1])
        this.inputData[box2] = false;
    }
  }

  experimentAndModelGraph(experiment: any) {
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {
      type: 'line',
      data: {
        datasets: [{
          label: 'Experiment',
          data: experiment,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Actin concentration [microM]'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Fractional Fluorescence'
            }
          }]
        }
      }
    })
  }
}
