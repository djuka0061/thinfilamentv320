'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const returnOutputData = require('../../middleware/return_data');
const zip = require('../../middleware/zipping');
const winston = require('winston')

//Local include

module.exports = function (hill_probabilistic) {

  //function for run simulation
  hill_probabilistic.runSimulation = (id, parameters) => {
    //create input file
    let writeStream = fs.createWriteStream('./hill_probabilistic/Input_params.dat');
    writeStream.write("#Output File Name:\n");
    writeStream.write('./Hillnum_output\n');
    writeStream.write('#Total Time [s]:\n');
    writeStream.write(parameters.s + '\n');
    writeStream.write('#Molar Concentration of Actin [uM]:\n');
    writeStream.write(parameters.actin + '\n');
    writeStream.write('#Molar Concentration of Myosin Initial / Final [uM / uM]:\n');
    writeStream.write(parameters.myosin + '\n');
    writeStream.write(parameters.myosin + '\n');
    writeStream.write('#The Rate Constants Beetwen ON and OFF States (Alpha0 / Beta0):\n');
    writeStream.write(parameters.alpha_zero + '\n');
    writeStream.write(parameters.beta_zero + '\n');
    writeStream.write('#Constants Y1 and Y2:\n');
    writeStream.write(parameters.constant_y1 + '\n');
    writeStream.write(parameters.constant_y2 + '\n');
    writeStream.write('#The Foward/Backward Rate Constants of OFF State (K1 / K1\'):\n');
    writeStream.write(parameters.constant_k1 + '\n');
    writeStream.write(parameters.constant_k1_prim + '\n');
    writeStream.write('#The Foward/Backward Rate Constants of ON State (K2/ K2\'):\n')
    writeStream.write(parameters.constant_k2 + '\n');
    writeStream.write(parameters.constant_k2_prim + '\n');
    writeStream.write('#Parameter Gama:\n');
    writeStream.write(parameters.parameter_gama + '\n');
    writeStream.write('#Cooperativity Paramater Delta:\n')
    writeStream.end(parameters.parameter_delta + '\n');

    const options = {
      userID: id,
      ...parameters
    };

    //setup data for start simulation
    const fileName = 'num_hill.exe';
    const params = ['Input_params.dat'];
    const path = './hill_probabilistic/'
    let promise = new Promise((resolve, reject) => {
      exec(fileName, params, { cwd: path }, (err, data) => {
        winston.info('Simulation hill probabilistic successfully start')
        if (err) {
          winston.info(err)
          const response = {
            statusCode: 400,
            message: 'Simulation failed'
          }
          return response
        } else {
          winston.info('Simulation hill probabilistic successfully finish')
          return loopback.getModel('hill_probabilistic').create(options)
            .then(data => {
              return loopback.getModel('hill_probabilistic').create(options)
                .then(data => {
                  return returnOutputData.returnDataForFirstFourSimulation('hill_probabilistic', 'Hillnum_output.dat', 'Hillnum_output-histogram.DAT')
                    .then(data => {
                      zip.zippingFile('hill_probabilistic', 'Hillnum_output', 'Hillnum_output-histogram', data);
                      return resolve(data);
                    });
                });
            })
        }
      });

    });
    return promise;
  }

  //Save input data for simulation
  hill_probabilistic.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    };
    return loopback.getModel('hill_probabilistic').create(options)
      .then(data => {
        winston.info('Simulation hill probabilistic successfully save input data')
        return data;
      });
  };

  hill_probabilistic.saveData = (id, parameters) => {
    return returnOutputData.returnDataForFirstFourSimulation('hill_probabilistic', 'Hillnum_output.dat', 'Hillnum_output-histogram.DAT')
      .then(data => {
        if (!data && data !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for hill probabilistic simulation'
          }
          winston.info('Not found output data for hill probabilistic simulation')
          return response
        }
        const inputParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel('hill_probabilistic').create(inputParams)
          .then(inputData => {
            const options = {
              userID: id,
              input_id: inputData._id,
              ...data
            };
            return loopback.getModel('hill_probabilistic_out').create(options)
              .then(saveData => {
                winston.info('Simulation hill probabilistic successfully save data')
                return saveData;
              })
          })
      });
  }

  hill_probabilistic.returnSaveData = (id) => {
    let response
    return loopback.getModel('hill_probabilistic_out').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found hill probabilistic simulation'
          }
          winston.info('Not found return data for hill probabilistic simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id;
        return loopback.getModel('hill_probabilistic').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found hill probabilistic simulation save data'
              }
              winston.info('Not found input data for hill probabilistic simulation')
              return response
            }
            response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response;
          });
      });
  }

  hill_probabilistic.returnInputData = (id) => {
    return loopback.getModel('hill_probabilistic').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          response = {
            statusCode: 404,
            message: `Not found hill probabilistic simulation input data`
          }
          winston.info('Not found hill probabilistic input data')
          return response
        }
        return data[data.length - 1];
      })
  }


  hill_probabilistic.download = (id, res) => {
    winston.info('Simulation hill probabilistic successfully download data')
    res.download('./hill_probabilistic/output_files.zip');
  };

  /** API for run simulation */
  hill_probabilistic.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }

    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_probabilistic.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_probabilistic.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });


  /** API for return input data for simulation */
  hill_probabilistic.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  hill_probabilistic.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });


  hill_probabilistic.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_probabilistic.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download'], method.name)) {
      hill_probabilistic.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}