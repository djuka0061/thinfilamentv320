import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { ProbabilisticService } from '../service/probabilistic.service';


@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.scss'],
})
export class InputFormComponent implements OnInit {
  options: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    kb_plus: new FormControl(''),
    kb_minus: new FormControl(''),
    kt_plus: new FormControl(''),
    kt_minus: new FormControl(''),
    k1_plus: new FormControl(''),
    k1_minus: new FormControl(''),
    k2_plus: new FormControl(''),
    k2_minus: new FormControl(''),
    molar_concentration_of_actin: new FormControl(''),
    molar_concentration_of_myosin: new FormControl(''),
    type_of_simulation: new FormControl('')
  });

  constructor(fb: FormBuilder, private ProbabilisticService: ProbabilisticService) {
    this.options = fb.group({
      hideRequired: false,
      floatLabel: 'auto',
    })
  }

  ngOnInit() {
    
  }

  public porbabilistic: any;

  onSubmit() {
    console.log(this.profileForm.value)
    const body = 'userID=test21&s='+ this.profileForm.value.s +'&kb_plus='+ this.profileForm.value.kb_plus +'&kb_minus='+ this.profileForm.value.kb_minus +'&kt_plus='+ this.profileForm.value.kt_plus +'&kt_minus='+ this.profileForm.value.kt_minus + '&k1_plus='+ this.profileForm.value.k1_plus +'&k1_minus='+ this.profileForm.value.k1_minus +'&k2_plus='+ this.profileForm.value.k2_plus +'&k2_minus='+ this.profileForm.value.k2_minus +'&molar_concentration_of_actin='+ this.profileForm.value.molar_concentration_of_actin +'&molar_concentration_of_myosin='+ this.profileForm.value.molar_concentration_of_myosin +'&type_of_simulation=test'

    this.ProbabilisticService.getData(body).subscribe(data => {
      this.porbabilistic = data;
      console.log(data);
    })
  }

  

}
