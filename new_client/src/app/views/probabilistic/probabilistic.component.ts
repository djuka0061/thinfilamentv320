import { Component, OnInit, QueryList, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { ProbabilisticService } from '../../service/probabilistic.service';
import { environment } from '../../../environments/environment.prod';
import { Chart } from 'chart.js'

const host = environment.server.host;

export interface Graph {
  datasets: Array<any>;
  options: any;
  chartType: string;
  colors: Array<any>;
  legend: boolean;
  labels: Array<any>;
  visibility: Boolean;
}

@Component({
  templateUrl: 'probabilistic.component.html',
  styleUrls: ['./probabilistic.component.scss'],
})
export class ProbabilisticComponent implements OnInit, AfterViewInit {

  angForm: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    kb_plus: new FormControl(''),
    kb_minus: new FormControl(''),
    kt_plus: new FormControl(''),
    kt_minus: new FormControl(''),
    k1_plus: new FormControl(''),
    k1_minus: new FormControl(''),
    k2_plus: new FormControl(''),
    k2_minus: new FormControl(''),
    molar_concentration_of_actin: new FormControl(''),
    molar_concentration_of_myosin: new FormControl(''),
    final_myosin: new FormControl(''),
    type_of_simulation: new FormControl('')
  });

  // displayedColumns: string[] = ['col1', 'col2', 'col3', 'col4', 'col5', 'col6', 'col7', 'col8'];
  constructor(private fb: FormBuilder, private probabilisticService: ProbabilisticService) {
  }

  public porbabilistic: any;
  public dataSource;
  public inputData = this.profileForm;
  public displayedColumns: string[];
  public typeOfSimulation: Number = 1;

  public run_button = "RUN";
  public isVisible = false;
  public dugmeRun: Boolean = false;

  public graphData

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.probabilisticService.getInputData('test21').subscribe(data => {
      if (data) {
        this.inputData = data;
      }
    });

    this.graphData = new Chart('graphData', {})
  }

  onSubmit() {
    const body = this.profileForm.value;
    body.type_of_simulation = 'titration';

    this.isVisible = false;
    this.run_button = "RUNNING";
    this.dugmeRun = true;
    this.graphData.destroy()
    this.probabilisticService.getData(body).subscribe(data => {
      this.porbabilistic = data;

      const datasets = [
        {
          data: data.fractional_fluorescence,
          label: 'Fractional Fluorescence',
          fill: false,
          lineTension: 0.2,
          borderColor: 'blue',
          borderWidth: 1,
        },
        {
          data: data.fraction_saturation,
          label: 'Fraction Saturation',
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        },
        {
          data: data.fraction_of_unbound_actin,
          label: 'Fraction of Unbound Actin',
          fill: false,
          lineTension: 0.2,
          borderColor: 'green',
          borderWidth: 1,
        },
        {
          data: data.free_myosin_concentration,
          label: 'Free Myosin Concentration',
          fill: false,
          lineTension: 0.2,
          borderColor: 'orange',
          borderWidth: 1,
        }
      ];

      this.parameterForGraph(datasets)
      this.isVisible = true;
      this.dugmeRun = false;
      this.run_button = "RUN";
    });
  }

  download() {
    window.open(host + 'probabilistics/users/test21/download');
  }

  saveSimulation() {
    const body = this.profileForm.value;
    this.probabilisticService.saveSimulation(body).subscribe(data => {
    })
  }

  openSaveSimulation(file) {
    this.graphData.destroy()
    const fileData = file.target.files
    var reader = new FileReader();
    reader.readAsText(fileData[0]);
    reader.onload = (_event) => {
      const data = JSON.parse(reader.result.toString());
      this.porbabilistic = data;
      if (data.fractional_fluorescence && data.fraction_saturation && data.fraction_of_unbound_actin && data.free_myosin_concentration) {
        const datasets = [
          {
            data: data.fractional_fluorescence,
            label: 'Fractional Fluorescence',
            fill: false,
            lineTension: 0.2,
            borderColor: 'blue',
            borderWidth: 1,
          },
          {
            data: data.fraction_saturation,
            label: 'Fraction Saturation',
            fill: false,
            lineTension: 0.2,
            borderColor: 'red',
            borderWidth: 1,
          },
          {
            data: data.fraction_of_unbound_actin,
            label: 'Fraction of Unbound Actin',
            fill: false,
            lineTension: 0.2,
            borderColor: 'green',
            borderWidth: 1,
          },
          {
            data: data.free_myosin_concentration,
            label: 'Free Myosin Concentration',
            fill: false,
            lineTension: 0.2,
            borderColor: 'orange',
            borderWidth: 1,
          }
        ];

        this.parameterForGraph(datasets)
      }
    };
  }

  parameterForGraph(datasets: any) {
    this.graphData = new Chart('graphData', {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Iteration'
            }
          }], yAxes: [{
            ticks: {
              callback: function (value) {
                return value.toExponential(2)
              }
            }
          }]
        }
      }
    })
  }
}
