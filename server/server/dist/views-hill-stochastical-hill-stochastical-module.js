(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-hill-stochastical-hill-stochastical-module"],{

/***/ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js":
/*!************************************************************************************************!*\
  !*** ./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
   true ? factory(exports) :
  undefined;
}(this, (function (exports) { 'use strict';

  /**
   * --------------------------------------------------------------------------
   * CoreUI Plugins - Custom Tooltips for Chart.js (v1.2.0): custom-tooltips.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */
  function CustomTooltips(tooltipModel) {
    var _this = this;

    // Add unique id if not exist
    var _setCanvasId = function _setCanvasId() {
      var _idMaker = function _idMaker() {
        var _hex = 16;
        var _multiplier = 0x10000;
        return ((1 + Math.random()) * _multiplier | 0).toString(_hex);
      };

      var _canvasId = "_canvas-" + (_idMaker() + _idMaker());

      _this._chart.canvas.id = _canvasId;
      return _canvasId;
    };

    var ClassName = {
      ABOVE: 'above',
      BELOW: 'below',
      CHARTJS_TOOLTIP: 'chartjs-tooltip',
      NO_TRANSFORM: 'no-transform',
      TOOLTIP_BODY: 'tooltip-body',
      TOOLTIP_BODY_ITEM: 'tooltip-body-item',
      TOOLTIP_BODY_ITEM_COLOR: 'tooltip-body-item-color',
      TOOLTIP_BODY_ITEM_LABEL: 'tooltip-body-item-label',
      TOOLTIP_BODY_ITEM_VALUE: 'tooltip-body-item-value',
      TOOLTIP_HEADER: 'tooltip-header',
      TOOLTIP_HEADER_ITEM: 'tooltip-header-item'
    };
    var Selector = {
      DIV: 'div',
      SPAN: 'span',
      TOOLTIP: (this._chart.canvas.id || _setCanvasId()) + "-tooltip"
    };
    var tooltip = document.getElementById(Selector.TOOLTIP);

    if (!tooltip) {
      tooltip = document.createElement('div');
      tooltip.id = Selector.TOOLTIP;
      tooltip.className = ClassName.CHARTJS_TOOLTIP;

      this._chart.canvas.parentNode.appendChild(tooltip);
    } // Hide if no tooltip


    if (tooltipModel.opacity === 0) {
      tooltip.style.opacity = 0;
      return;
    } // Set caret Position


    tooltip.classList.remove(ClassName.ABOVE, ClassName.BELOW, ClassName.NO_TRANSFORM);

    if (tooltipModel.yAlign) {
      tooltip.classList.add(tooltipModel.yAlign);
    } else {
      tooltip.classList.add(ClassName.NO_TRANSFORM);
    } // Set Text


    if (tooltipModel.body) {
      var titleLines = tooltipModel.title || [];
      var tooltipHeader = document.createElement(Selector.DIV);
      tooltipHeader.className = ClassName.TOOLTIP_HEADER;
      titleLines.forEach(function (title) {
        var tooltipHeaderTitle = document.createElement(Selector.DIV);
        tooltipHeaderTitle.className = ClassName.TOOLTIP_HEADER_ITEM;
        tooltipHeaderTitle.innerHTML = title;
        tooltipHeader.appendChild(tooltipHeaderTitle);
      });
      var tooltipBody = document.createElement(Selector.DIV);
      tooltipBody.className = ClassName.TOOLTIP_BODY;
      var tooltipBodyItems = tooltipModel.body.map(function (item) {
        return item.lines;
      });
      tooltipBodyItems.forEach(function (item, i) {
        var tooltipBodyItem = document.createElement(Selector.DIV);
        tooltipBodyItem.className = ClassName.TOOLTIP_BODY_ITEM;
        var colors = tooltipModel.labelColors[i];
        var tooltipBodyItemColor = document.createElement(Selector.SPAN);
        tooltipBodyItemColor.className = ClassName.TOOLTIP_BODY_ITEM_COLOR;
        tooltipBodyItemColor.style.backgroundColor = colors.backgroundColor;
        tooltipBodyItem.appendChild(tooltipBodyItemColor);

        if (item[0].split(':').length > 1) {
          var tooltipBodyItemLabel = document.createElement(Selector.SPAN);
          tooltipBodyItemLabel.className = ClassName.TOOLTIP_BODY_ITEM_LABEL;
          tooltipBodyItemLabel.innerHTML = item[0].split(': ')[0];
          tooltipBodyItem.appendChild(tooltipBodyItemLabel);
          var tooltipBodyItemValue = document.createElement(Selector.SPAN);
          tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          tooltipBodyItemValue.innerHTML = item[0].split(': ').pop();
          tooltipBodyItem.appendChild(tooltipBodyItemValue);
        } else {
          var _tooltipBodyItemValue = document.createElement(Selector.SPAN);

          _tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          _tooltipBodyItemValue.innerHTML = item[0];
          tooltipBodyItem.appendChild(_tooltipBodyItemValue);
        }

        tooltipBody.appendChild(tooltipBodyItem);
      });
      tooltip.innerHTML = '';
      tooltip.appendChild(tooltipHeader);
      tooltip.appendChild(tooltipBody);
    }

    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft; // Display, position, and set styles for font

    tooltip.style.opacity = 1;
    tooltip.style.left = positionX + tooltipModel.caretX + "px";
    tooltip.style.top = positionY + tooltipModel.caretY + "px";
  }

  exports.CustomTooltips = CustomTooltips;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=custom-tooltips.js.map


/***/ }),

/***/ "./src/app/service/hill_stochastical.service.ts":
/*!******************************************************!*\
  !*** ./src/app/service/hill_stochastical.service.ts ***!
  \******************************************************/
/*! exports provided: HillStochasticalService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HillStochasticalService", function() { return HillStochasticalService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");




var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].server.host;
var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var HillStochasticalService = /** @class */ (function () {
    function HillStochasticalService(httpClient) {
        this.httpClient = httpClient;
    }
    HillStochasticalService.prototype.getConfig = function () {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get('');
    };
    HillStochasticalService.prototype.getData = function (json) {
        return this.httpClient.post(host + 'hill_stochasticals/users/test21/runSimulation', json, httpOptions);
    };
    HillStochasticalService.prototype.getInputData = function (userID) {
        return this.httpClient.get(host + 'hill_stochasticals/users/test21/returnInputData');
    };
    HillStochasticalService.prototype.download = function (userID) {
        return this.httpClient.get(host + 'hill_stochasticals/users/test21/download');
    };
    HillStochasticalService.prototype.saveSimulation = function (body) {
        return this.httpClient.post(host + 'hill_stochasticals/users/test21/saveData', body, httpOptions);
    };
    HillStochasticalService.prototype.openSaveSimulation = function (userID) {
        return this.httpClient.get(host + 'hill_stochasticals/users/test21/returnSaveData');
    };
    HillStochasticalService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HillStochasticalService);
    return HillStochasticalService;
}());



/***/ }),

/***/ "./src/app/views/hill-stochastical/hill-stochastical-routing.module.ts":
/*!*****************************************************************************!*\
  !*** ./src/app/views/hill-stochastical/hill-stochastical-routing.module.ts ***!
  \*****************************************************************************/
/*! exports provided: HillStochasticalRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HillStochasticalRoutingModule", function() { return HillStochasticalRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _hill_stochastical_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hill-stochastical.component */ "./src/app/views/hill-stochastical/hill-stochastical.component.ts");




var routes = [
    {
        path: '',
        component: _hill_stochastical_component__WEBPACK_IMPORTED_MODULE_3__["HillStochasticalComponent"],
        data: {
            title: 'HillStochastical'
        }
    }
];
var HillStochasticalRoutingModule = /** @class */ (function () {
    function HillStochasticalRoutingModule() {
    }
    HillStochasticalRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], HillStochasticalRoutingModule);
    return HillStochasticalRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/hill-stochastical/hill-stochastical.component.html":
/*!**************************************************************************!*\
  !*** ./src/app/views/hill-stochastical/hill-stochastical.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn col-sm-12\">\r\n  <div class=\"d-flex w-100 flex-wrap\">\r\n    <!--Parameters-->\r\n    <div class=\"d-flex w-30 p-2 align-self-start flex-min-width\">\r\n      <div class=\"d-flex w-100 card card-accent-primary\">\r\n        <div class=\"card-header\">\r\n          SIMULATION AND MODEL DATA\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex flex-column w-100\">\r\n            <div class=\"d-flex w-100 pt-1 align-content-end\">\r\n              <div class=\"d-flex w-60\"> &nbsp; </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"saveSimulation()\">SAVE</button>\r\n              </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"openSaveSimulation()\">OPEN</button>\r\n              </div>\r\n            </div>\r\n            <form [formGroup]=\"profileForm\" (ngSubmit)=\"onSubmit()\">\r\n              <div class=\"d-flex flex-column w-100\">\r\n\r\n\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Simulation paramaters\r\n                </div>\r\n                <div class=\"d-flex w-100 pt-2\">\r\n\r\n                  <div class=\"d-flex flex-column w-45\">\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-35 p-1\">\r\n                        <span style=\"vertical-align: middle\">Total time</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-45 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.s}}\r\n                          formControlName=\"s\">\r\n                      </div>\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">[s]</span>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"d-flex w-100 pt-2\">\r\n                      <div class=\"d-flex w-35\"><span>Type of Simulation</span></div>\r\n                      <div class=\"d-flex flex-column w-65\">\r\n                        <div class=\"form-check\">\r\n                          <input name=\"radios\" class=\"form-check-input\" id=\"radio1\" type=\"radio\" value=\"true\"\r\n                            checked=\"true\">\r\n                          <label class=\"form-check-label\" for=\"radio1\">Time Course</label>\r\n                        </div>\r\n                        <div class=\"form-check\">\r\n                          <input name=\"radios\" class=\"form-check-input\" id=\"radio3\" type=\"radio\" value=\"true\">\r\n                          <label class=\"form-check-label\" for=\"radio3\">Titration</label>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n\r\n                  </div>\r\n                  <div class=\"d-flex flex-column w-55\">\r\n                    <div class=\"w-100 p-1\">\r\n                      <span style=\"vertical-align: middle\">&nbsp;</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Initial)\">\r\n                        <span style=\"vertical-align: middle\">Myosin Initial</span>\r\n                      </div>\r\n                      <div class=\"w-37 p-1\">\r\n                        <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                          formControlName=\"myosin\"-->\r\n                        <input class=\"w-100 text-align-right\" type=\"text\">\r\n                      </div>\r\n                      <div class=\"w-20 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Final)\">\r\n                        <span style=\"vertical-align: middle\">Myosin Final</span>\r\n                      </div>\r\n                      <div class=\"w-37 p-1\">\r\n                        <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                          formControlName=\"myosin\"-->\r\n                        <input class=\"w-100 text-align-right\" type=\"text\">\r\n                      </div>\r\n                      <div class=\"w-20 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n\r\n              <div class=\"d-flex title-div pb-1 pt-3\">\r\n                Fixed model parameters\r\n              </div>\r\n\r\n              <div class=\"d-flex w-100 pt-2\">\r\n                <!-- LEVA KOLONA-->\r\n                <div class=\"d-flex flex-column w-50\">\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Rate Constant Alpha 0\">\r\n                      <span style=\"vertical-align: middle\">Alpha 0</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.alpha_zero}}\r\n                        formControlName=\"alpha_zero\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Constant Y1\">\r\n                      <span style=\"vertical-align: middle\">Y1</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_y1}}\r\n                        formControlName=\"constant_y1\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Forward Rate Constant K1\">\r\n                      <span style=\"vertical-align: middle\">K1</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_k1}}\r\n                        formControlName=\"constant_k1\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Forward Rate Constant K2\">\r\n                      <span style=\"vertical-align: middle\">K2</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_k2}}\r\n                        formControlName=\"constant_k2\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Parameter Gama\">\r\n                      <span style=\"vertical-align: middle\">Gama</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.parameter_gama}}\r\n                        formControlName=\"parameter_gama\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n\r\n                </div>\r\n                <!-- DESNA KOLONA-->\r\n                <div class=\"d-flex flex-column w-50\">\r\n                  <!--div class=\"d-flex w-100\"> &nbsp;</div-->\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Rate Constant Beta 0\">\r\n                      <span style=\"vertical-align: middle\">Beta 0</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.beta_zero}}\r\n                        formControlName=\"beta_zero\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Constant Y2\">\r\n                      <span style=\"vertical-align: middle\">Y2</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_y2}}\r\n                        formControlName=\"constant_y2\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Backward Rate Constant K1'\">\r\n                      <span style=\"vertical-align: middle\">K1'</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_k1_prim}}\r\n                        formControlName=\"constant_k1_prim\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Backward Rate Constant K2'\">\r\n                      <span style=\"vertical-align: middle\">K2'</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_k2_prim}}\r\n                        formControlName=\"constant_k2_prim\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Cooperativity Parameter Delta\">\r\n                      <span style=\"vertical-align: middle\">Delta</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.parameter_delta}}\r\n                        formControlName=\"parameter_delta\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"d-flex w-100 pt-3\">\r\n                <div class=\"d-flex flex-column w-45\">\r\n                </div>\r\n                <div class=\"d-flex flex-column w-55\">\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Actin\">\r\n                      <span style=\"vertical-align: middle\">Actin</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-37 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.actin}}\r\n                        formControlName=\"actin\">\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-36 p-1\" tooltip=\"Number of Filaments\">\r\n                      <span style=\"vertical-align: middle\">Filaments</span>\r\n                    </div>\r\n                    <div class=\"w-37 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.number_of_filaments}}\r\n                        formControlName=\"number_of_filaments\">\r\n                    </div>\r\n                    <div class=\"w-10 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-36 p-1\" tooltip=\"Number of TmTn Units\">\r\n                      <span style=\"vertical-align: middle\">TmTn Units</span>\r\n                    </div>\r\n                    <div class=\"w-37 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.number_of_tm_tn_units}}\r\n                        formControlName=\"number_of_tm_tn_units\">\r\n                    </div>\r\n                    <div class=\"w-10 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-36 p-1\" tooltip=\"Number of Actin Per Unit\">\r\n                      <span style=\"vertical-align: middle\">Actin Per Unit</span>\r\n                    </div>\r\n                    <div class=\"w-37 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\"\r\n                        ngModel={{this.inputData.number_of_actin_per_unit}} formControlName=\"number_of_actin_per_unit\">\r\n                    </div>\r\n                    <div class=\"w-10 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n\r\n\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"d-flex w-100 pt-3 \">\r\n                <div class=\"d-flex w-80\"> &nbsp; </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"!profileForm.valid\">RUN </button>\r\n                </div>\r\n              </div>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!--Gaphs-->\r\n    <div class=\"d-flex w-70 p-2\">\r\n      <div class=\"d-flex card card-accent-primary\">\r\n        <div class=\"card-header\">\r\n          RESULTS\r\n          <span class=\"float-right\" tooltip=\"Download graphic data\" placement=\"left\">\r\n            <i (click)=\"download()\" class=\"fa fa-cloud-download\" style=\"font-size:16px\"></i>\r\n          </span>\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex w-100 flex-wrap justify-content-around align-content-start\">\r\n            <div *ngFor=\"let graph of graphs\" class=\"w-50 p-2 mw-350\">\r\n              <div class=\"chart-wrapper\">\r\n                <canvas baseChart class=\"chart\" [datasets]=\"graph.datasets\" [labels]=\"graph.labels\"\r\n                  [options]=\"graph.options\" [colors]=\"graph.colors\" [legend]=\"graph.legend\"\r\n                  [chartType]=\"graph.chartType\">\r\n                </canvas>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/hill-stochastical/hill-stochastical.component.scss":
/*!**************************************************************************!*\
  !*** ./src/app/views/hill-stochastical/hill-stochastical.component.scss ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column; }\n\ninput[type=number] {\n  -moz-appearance: textfield !important;\n       appearance: textfield !important;\n  -webkit-appearance: textfield !important;\n  margin-right: 2px !important;\n  text-align: right; }\n\ninput[_ngcontent-c1]::-webkit-outer-spin-button {\n  -webkit-appearance: texfield !important;\n  -moz-appearance: textfield !important;\n  appearance: none;\n  margin: 0; }\n\n.example-container > * {\n  width: 100%; }\n\n.example-container form {\n  margin-bottom: 20px; }\n\n.example-container form > * {\n  margin: 5px 0; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.table_form {\n  min-width: 550px;\n  padding: 0px; }\n\n.table_form p {\n  font-size: 10px;\n  text-align: right; }\n\n.table_form input {\n  height: 25px;\n  text-align: center;\n  max-width: 100px; }\n\n/* Customize the label (the container) */\n\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n/* Hide the browser's default checkbox */\n\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0; }\n\n/* Create a custom checkbox */\n\n.checkmark {\n  position: absolute;\n  margin-top: 5px;\n  top: 0;\n  left: 0;\n  height: 20 px;\n  width: 20px;\n  background-color: #eee; }\n\n/* On mouse-over, add a grey background color */\n\n.container:hover input ~ .checkmark {\n  background-color: #ccc; }\n\n/* When the checkbox is checked, add a blue background */\n\n.container input:checked ~ .checkmark {\n  background-color: #2196F3; }\n\n.btn-align {\n  text-align: center !important; }\n\n/* Create the checkmark/indicator (hidden when not checked) */\n\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none; }\n\n/* Show the checkmark when checked */\n\n.container input:checked ~ .checkmark:after {\n  display: block; }\n\n/* Style the checkmark/indicator */\n\n.container .checkmark:after {\n  left: 9px;\n  top: 5px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg); }\n\n.title-div-centar {\n  font-size: 1rem;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\n.title-div {\n  color: cadetblue;\n  font-style: 1rem;\n  border-bottom: 1px dashed lightblue; }\n\n.title-div-table {\n  font-size: 1rem;\n  color: cadetblue;\n  text-align: center !important; }\n\n.title-div-2 {\n  font-size: 1.2rem;\n  color: cadetblue; }\n\ninput {\n  background-color: #c3e4e5;\n  border: 1px solid \t#c3e4e5 !important;\n  padding-right: 2px !important; }\n\n.text-align-right {\n  text-align: right !important; }\n\n.text-align-center {\n  text-align: center; }\n\n.flex-min-width {\n  min-width: 470px; }\n\n.max-width-70posto {\n  max-width: 70%; }\n\n.over-auto {\n  overflow: visible; }\n\n.chart-wrapper {\n  border: 1px solid lightblue !important; }\n\n.mw-350 {\n  min-width: 700px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-30posto {\n  min-width: 370px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-400 {\n  min-width: 280px; }\n\n.tbl-10 {\n  min-width: 300px !important; }\n\n.tbl-8kolona {\n  min-width: 300px !important; }\n\n.td-1 {\n  padding: 0.4rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaGlsbC1zdG9jaGFzdGljYWwvRjpcXHRoaW5maWxhbWVudHYzMjBcXG5ld19jbGllbnQvc3JjXFxhcHBcXHZpZXdzXFxoaWxsLXN0b2NoYXN0aWNhbFxcaGlsbC1zdG9jaGFzdGljYWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCLEVBQUE7O0FBSXhCO0VBR0UscUNBQWdDO09BQWhDLGdDQUFnQztFQUNoQyx3Q0FBd0M7RUFDeEMsNEJBQTRCO0VBQzVCLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLHVDQUF1QztFQUN2QyxxQ0FBcUM7RUFDckMsZ0JBQWdCO0VBQ2hCLFNBQVMsRUFBQTs7QUFFWDtFQUNFLFdBQVcsRUFBQTs7QUFHYjtFQUNFLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsV0FBVyxFQUFBOztBQUdiO0VBQ0UsV0FBVyxFQUFBOztBQUdiO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQTs7QUFHZDtFQUNFLGVBQWU7RUFDZixpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUlsQix3Q0FBQTs7QUFDQTtFQUNBLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIscUJBQXFCO0VBQ3JCLGlCQUFpQixFQUFBOztBQUdqQix3Q0FBQTs7QUFDQTtFQUNBLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsZUFBZTtFQUNmLFNBQVM7RUFDVCxRQUFRLEVBQUE7O0FBR1IsNkJBQUE7O0FBQ0E7RUFDQSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLE1BQU07RUFDTixPQUFPO0VBQ1AsYUFBYTtFQUNiLFdBQVc7RUFDWCxzQkFBc0IsRUFBQTs7QUFHdEIsK0NBQUE7O0FBQ0E7RUFDQSxzQkFBc0IsRUFBQTs7QUFHdEIsd0RBQUE7O0FBQ0E7RUFDQSx5QkFBeUIsRUFBQTs7QUFHekI7RUFDRSw2QkFBOEIsRUFBQTs7QUFHaEMsNkRBQUE7O0FBQ0E7RUFDQSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGFBQWEsRUFBQTs7QUFHYixvQ0FBQTs7QUFDQTtFQUNBLGNBQWMsRUFBQTs7QUFHZCxrQ0FBQTs7QUFDQTtFQUNBLFNBQVM7RUFDVCxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLGdDQUFnQztFQUVoQyx3QkFBd0IsRUFBQTs7QUFHeEI7RUFFQSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUlwQjtFQUNBLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsbUNBQW1DLEVBQUE7O0FBR25DO0VBQ0EsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiw2QkFBNkIsRUFBQTs7QUFHN0I7RUFDQSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBSWhCO0VBRUEseUJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyw2QkFBNkIsRUFBQTs7QUFHN0I7RUFDQSw0QkFBNEIsRUFBQTs7QUFJNUI7RUFDQSxrQkFBa0IsRUFBQTs7QUFJbEI7RUFDQSxnQkFBZ0IsRUFBQTs7QUFTaEI7RUFDQSxjQUFjLEVBQUE7O0FBRWQ7RUFDQSxpQkFBaUIsRUFBQTs7QUFHakI7RUFFQSxzQ0FBc0MsRUFBQTs7QUFHdEM7RUFDQSxnQkFBZ0I7RUFDaEIsMkJBQW1CO0VBQW5CLHdCQUFtQjtFQUFuQixtQkFBbUIsRUFBQTs7QUFHbkI7RUFDQSxnQkFBZ0I7RUFDaEIsMkJBQW1CO0VBQW5CLHdCQUFtQjtFQUFuQixtQkFBbUIsRUFBQTs7QUFLbkI7RUFDQSxnQkFBZ0IsRUFBQTs7QUFHaEI7RUFFQSwyQkFBMkIsRUFBQTs7QUFFM0I7RUFFQSwyQkFBMkIsRUFBQTs7QUFJM0I7RUFDQSwwQkFBMEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2hpbGwtc3RvY2hhc3RpY2FsL2hpbGwtc3RvY2hhc3RpY2FsLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmV4YW1wbGUtY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBmbGV4O1xyXG4gIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbn1cclxuXHJcblxyXG5pbnB1dFt0eXBlPW51bWJlcl0geyBcclxuICAvLy13ZWJraXQtYXBwZWFyYW5jZTogdGV4ZmllbGQgIWltcG9ydGFudDtcclxuIC8vIC1tb3otYXBwZWFyYW5jZTogdGV4dGZpZWxkICFpbXBvcnRhbnQ7XHJcbiAgYXBwZWFyYW5jZTogdGV4dGZpZWxkICFpbXBvcnRhbnQ7XHJcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiB0ZXh0ZmllbGQgIWltcG9ydGFudDtcclxuICBtYXJnaW4tcmlnaHQ6IDJweCAhaW1wb3J0YW50OyBcclxuICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG5cclxuaW5wdXRbX25nY29udGVudC1jMV06Oi13ZWJraXQtb3V0ZXItc3Bpbi1idXR0b24geyBcclxuICAtd2Via2l0LWFwcGVhcmFuY2U6IHRleGZpZWxkICFpbXBvcnRhbnQ7XHJcbiAgLW1vei1hcHBlYXJhbmNlOiB0ZXh0ZmllbGQgIWltcG9ydGFudDtcclxuICBhcHBlYXJhbmNlOiBub25lO1xyXG4gIG1hcmdpbjogMDsgXHJcbn1cclxuLmV4YW1wbGUtY29udGFpbmVyID4gKiB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciBmb3JtIHtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4uZXhhbXBsZS1jb250YWluZXIgZm9ybSA+ICoge1xyXG4gIG1hcmdpbjogNXB4IDA7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZvcm0ge1xyXG4gIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgbWF4LXdpZHRoOiA1MDBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi50YWJsZV9mb3JtIHtcclxuICBtaW4td2lkdGg6IDU1MHB4O1xyXG4gIHBhZGRpbmc6IDBweDtcclxufVxyXG5cclxuLnRhYmxlX2Zvcm0gcCB7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG4udGFibGVfZm9ybSBpbnB1dCB7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXgtd2lkdGg6IDEwMHB4O1xyXG59XHJcblxyXG5cclxuLyogQ3VzdG9taXplIHRoZSBsYWJlbCAodGhlIGNvbnRhaW5lcikgKi9cclxuLmNvbnRhaW5lciB7XHJcbmRpc3BsYXk6IGJsb2NrO1xyXG5wb3NpdGlvbjogcmVsYXRpdmU7XHJcbnBhZGRpbmctbGVmdDogMzVweDtcclxubWFyZ2luLWJvdHRvbTogMTJweDtcclxuY3Vyc29yOiBwb2ludGVyO1xyXG5mb250LXNpemU6IDIycHg7XHJcbi13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbi1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbi1tcy11c2VyLXNlbGVjdDogbm9uZTtcclxudXNlci1zZWxlY3Q6IG5vbmU7XHJcbn1cclxuXHJcbi8qIEhpZGUgdGhlIGJyb3dzZXIncyBkZWZhdWx0IGNoZWNrYm94ICovXHJcbi5jb250YWluZXIgaW5wdXQge1xyXG5wb3NpdGlvbjogYWJzb2x1dGU7XHJcbm9wYWNpdHk6IDA7XHJcbmN1cnNvcjogcG9pbnRlcjtcclxuaGVpZ2h0OiAwO1xyXG53aWR0aDogMDtcclxufVxyXG5cclxuLyogQ3JlYXRlIGEgY3VzdG9tIGNoZWNrYm94ICovXHJcbi5jaGVja21hcmsge1xyXG5wb3NpdGlvbjogYWJzb2x1dGU7XHJcbm1hcmdpbi10b3A6IDVweDtcclxudG9wOiAwO1xyXG5sZWZ0OiAwO1xyXG5oZWlnaHQ6IDIwIHB4O1xyXG53aWR0aDogMjBweDtcclxuYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcclxufVxyXG5cclxuLyogT24gbW91c2Utb3ZlciwgYWRkIGEgZ3JleSBiYWNrZ3JvdW5kIGNvbG9yICovXHJcbi5jb250YWluZXI6aG92ZXIgaW5wdXQgfiAuY2hlY2ttYXJrIHtcclxuYmFja2dyb3VuZC1jb2xvcjogI2NjYztcclxufVxyXG5cclxuLyogV2hlbiB0aGUgY2hlY2tib3ggaXMgY2hlY2tlZCwgYWRkIGEgYmx1ZSBiYWNrZ3JvdW5kICovXHJcbi5jb250YWluZXIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcmsge1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjMjE5NkYzO1xyXG59XHJcblxyXG4uYnRuLWFsaWduIHtcclxuICB0ZXh0LWFsaWduOiAgY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi8qIENyZWF0ZSB0aGUgY2hlY2ttYXJrL2luZGljYXRvciAoaGlkZGVuIHdoZW4gbm90IGNoZWNrZWQpICovXHJcbi5jaGVja21hcms6YWZ0ZXIge1xyXG5jb250ZW50OiBcIlwiO1xyXG5wb3NpdGlvbjogYWJzb2x1dGU7XHJcbmRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi8qIFNob3cgdGhlIGNoZWNrbWFyayB3aGVuIGNoZWNrZWQgKi9cclxuLmNvbnRhaW5lciBpbnB1dDpjaGVja2VkIH4gLmNoZWNrbWFyazphZnRlciB7XHJcbmRpc3BsYXk6IGJsb2NrO1xyXG59XHJcblxyXG4vKiBTdHlsZSB0aGUgY2hlY2ttYXJrL2luZGljYXRvciAqL1xyXG4uY29udGFpbmVyIC5jaGVja21hcms6YWZ0ZXIge1xyXG5sZWZ0OiA5cHg7XHJcbnRvcDogNXB4O1xyXG53aWR0aDogNXB4O1xyXG5oZWlnaHQ6IDEwcHg7XHJcbmJvcmRlcjogc29saWQgd2hpdGU7XHJcbmJvcmRlci13aWR0aDogMCAzcHggM3B4IDA7XHJcbi13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG4tbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG50cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XHJcbn1cclxuXHJcbi50aXRsZS1kaXYtY2VudGFyIHtcclxuLy9jb2xvcjogY2FkZXRibHVlO1xyXG5mb250LXNpemU6IDFyZW07XHJcbnBhZGRpbmctdG9wOiAxcmVtO1xyXG5wYWRkaW5nLWJvdHRvbTogMXJlbTtcclxuLy9ib3JkZXItYm90dG9tOiAxcHggZGFzaGVkIGxpZ2h0Ymx1ZTtcclxufVxyXG5cclxuLnRpdGxlLWRpdiB7XHJcbmNvbG9yOiBjYWRldGJsdWU7XHJcbmZvbnQtc3R5bGU6IDFyZW07XHJcbmJvcmRlci1ib3R0b206IDFweCBkYXNoZWQgbGlnaHRibHVlO1xyXG59XHJcblxyXG4udGl0bGUtZGl2LXRhYmxlIHtcclxuZm9udC1zaXplOiAxcmVtOyBcclxuY29sb3I6IGNhZGV0Ymx1ZTtcclxudGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50aXRsZS1kaXYtMiB7XHJcbmZvbnQtc2l6ZTogMS4ycmVtOyBcclxuY29sb3I6IGNhZGV0Ymx1ZTtcclxuLy8gdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbmlucHV0IHtcclxuLy9ib3JkZXI6IG5vbmU7XHJcbmJhY2tncm91bmQtY29sb3I6IFx0I2MzZTRlNTtcclxuYm9yZGVyOiAxcHggc29saWQgXHQjYzNlNGU1ICFpbXBvcnRhbnQ7XHJcbnBhZGRpbmctcmlnaHQ6IDJweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGV4dC1hbGlnbi1yaWdodCB7XHJcbnRleHQtYWxpZ246IHJpZ2h0ICFpbXBvcnRhbnQ7XHJcbi8vcGFkZGluZy1yaWdodDogMnB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50ZXh0LWFsaWduLWNlbnRlciB7XHJcbnRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbn1cclxuXHJcbi5mbGV4LW1pbi13aWR0aCB7XHJcbm1pbi13aWR0aDogNDcwcHg7XHJcbi8vICBtYXgtd2lkdGg6IDYwMHB4O1xyXG4vL3otaW5kZXg6IDEwMDA7XHJcbn1cclxuXHJcbi5mbGV4LW1pbi13aWR0aCAqIHtcclxuLy96LWluZGV4OiAxMDAwO1xyXG59XHJcblxyXG4ubWF4LXdpZHRoLTcwcG9zdG8ge1xyXG5tYXgtd2lkdGg6IDcwJTtcclxufVxyXG4ub3Zlci1hdXRvIHtcclxub3ZlcmZsb3c6IHZpc2libGU7XHJcbn1cclxuXHJcbi5jaGFydC13cmFwcGVyIHtcclxuLy9ib3JkZXItdG9wLXN0eWxlOiBncm9vdmUgIWltcG9ydGFudDtcclxuYm9yZGVyOiAxcHggc29saWQgbGlnaHRibHVlICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5tdy0zNTAge1xyXG5taW4td2lkdGg6IDcwMHB4O1xyXG5oZWlnaHQ6IGZpdC1jb250ZW50O1xyXG59XHJcblxyXG4ubXctMzBwb3N0byB7XHJcbm1pbi13aWR0aDogMzcwcHg7XHJcbmhlaWdodDogZml0LWNvbnRlbnQ7XHJcbi8vZm9udC1zaXplOiAwLjVyZW0gIWltcG9ydGFudDtcclxufVxyXG5cclxuXHJcbi5tdy00MDAge1xyXG5taW4td2lkdGg6IDI4MHB4O1xyXG59XHJcblxyXG4udGJsLTEwIHtcclxuLy8gbWFyZ2luOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbm1pbi13aWR0aDogMzAwcHggIWltcG9ydGFudDtcclxufVxyXG4udGJsLThrb2xvbmEge1xyXG4vLyBtYXJnaW46IDEwcHggIWltcG9ydGFudDtcclxubWluLXdpZHRoOiAzMDBweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuLnRkLTF7XHJcbnBhZGRpbmc6IDAuNHJlbSAhaW1wb3J0YW50O1xyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./src/app/views/hill-stochastical/hill-stochastical.component.ts":
/*!************************************************************************!*\
  !*** ./src/app/views/hill-stochastical/hill-stochastical.component.ts ***!
  \************************************************************************/
/*! exports provided: HillStochasticalComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HillStochasticalComponent", function() { return HillStochasticalComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @coreui/coreui-plugin-chartjs-custom-tooltips */ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_hill_stochastical_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/hill_stochastical.service */ "./src/app/service/hill_stochastical.service.ts");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");







var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].server.host;
var HillStochasticalComponent = /** @class */ (function () {
    function HillStochasticalComponent(fb, HillStochasticalService) {
        this.fb = fb;
        this.HillStochasticalService = HillStochasticalService;
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            s: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            alpha_zero: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            beta_zero: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_y1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_y2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_k1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_k1_prim: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_k2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_k2_prim: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            parameter_gama: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            parameter_delta: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            actin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            number_of_filaments: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            number_of_tm_tn_units: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            number_of_actin_per_unit: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            cooperativity_on: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.numberOfGraphs = 1;
        this.graphs = [];
        this.inputData = this.profileForm;
        this.typeOfSimulation = 1;
        this.test = [
            [1, 2, 3, 4],
            [1, 2, 3, 4],
            [1, 6, 3, 4],
            [5, 2, 3, 4],
            [1, 2, 3, 4],
        ];
    }
    HillStochasticalComponent.prototype.testFja = function () {
        this.test = [
            [1, 2, 3, 4, 4, 5],
            [1, 2, 3, 4, 5, 3]
        ];
    };
    HillStochasticalComponent.prototype.ngOnInit = function () {
        var _this = this;
        var titles = [
            'Fractional Fluorescence',
            'Fraction Saturation',
            'Fraction of Unbound Actin',
            'Free Myosin Concentration'
        ];
        var labels = [
            { xAxes: 'Time [s]', yAxes: 'Unit X1' },
            { xAxes: 'Time [s]', yAxes: 'Unit X2' },
            { xAxes: 'Time [s]', yAxes: 'Unit X3' },
            { xAxes: 'Time [s]', yAxes: 'Unit X4' }
        ];
        var colors = [
            [{ borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' }]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i]));
        }
        console.log(this.graphs);
        this.HillStochasticalService.getInputData('test21').subscribe(function (data) {
            if (data) {
                _this.inputData = data;
            }
        });
    };
    HillStochasticalComponent.prototype.prepareGraphs = function (graphLines) {
        var titles = [
            'Fractional Fluorescence',
            'Fraction Saturation',
            'Fraction of Unbound Actin',
            'Free Myosin Concentration'
        ];
        var labels = [
            { xAxes: 'Time [s]', yAxes: 'Unit X1' },
            { xAxes: 'Time [s]', yAxes: 'Unit X2' },
            { xAxes: 'Time [s]', yAxes: 'Unit X3' },
            { xAxes: 'Time [s]', yAxes: 'Unit X4' }
        ];
        var colors = [
            [{ borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' }]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i]));
        }
    };
    HillStochasticalComponent.prototype.initGraph = function (newTitle, newLabels, newColors) {
        var basicGraph = {};
        basicGraph.options = {
            title: {
                display: true,
                text: 'Error',
                // fontColor: 'blue',
                fontStyle: 'small-caps',
            },
            tooltips: {
                enabled: false,
                custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                intersect: true,
                mode: 'index',
                position: 'nearest',
                callbacks: {
                    labelColor: function (tooltipItem, chart) {
                        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                    }
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 15,
                    top: 0,
                    bottom: 5
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        type: 'linear',
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            padding: 5,
                            beginAtZero: true,
                            stepSize: 0.5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC1 (%)',
                            fontSize: 11
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            beginAtZero: true,
                            padding: 5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC2 (%)',
                            fontSize: 11
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: false
            }
        };
        var newOptions = basicGraph.options;
        newOptions.title.text = newTitle;
        newOptions.scales.xAxes[0].scaleLabel.labelString = newLabels.xAxes;
        newOptions.scales.yAxes[0].scaleLabel.labelString = newLabels.yAxes;
        basicGraph.options = newOptions;
        basicGraph.colors = newColors;
        basicGraph.datasets = [{
                data: []
            }];
        basicGraph.chartType = 'line';
        basicGraph.labels = [];
        return basicGraph;
    };
    HillStochasticalComponent.prototype.onSubmit = function () {
        var _this = this;
        var body = this.profileForm.value;
        body.type_of_simulation = 'titration';
        this.HillStochasticalService.getData(body).subscribe(function (data) {
            console.log('podaci', data);
            _this.stochastical = data;
            var datasets = [
                [{ data: data.fractional_fluorescence },
                    { data: data.fraction_saturation },
                    { data: data.fraction_of_unbound_actin },
                    { data: data.free_myosin_concentration }]
            ];
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
            }
            _this.graphs[0].options = {
                title: {
                    display: true,
                    text: 'Error',
                    // fontColor: 'blue',
                    fontStyle: 'small-caps',
                },
                tooltips: {
                    enabled: false,
                    custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                    intersect: true,
                    mode: 'index',
                    position: 'nearest',
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                        }
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 15,
                        top: 0,
                        bottom: 5
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                            type: 'linear',
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                padding: 5,
                                beginAtZero: true,
                                stepSize: 0.5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC1 (%)',
                                fontSize: 11
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                beginAtZero: true,
                                padding: 5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC555 (%)',
                                fontSize: 11
                            }
                        }]
                },
                elements: {
                    line: {
                        borderWidth: 2
                    },
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4,
                        hoverBorderWidth: 3,
                    }
                },
                legend: {
                    display: false
                }
            };
        });
    };
    HillStochasticalComponent.prototype.download = function () {
        window.open(host + 'hill_stochasticals/users/test21/download');
    };
    HillStochasticalComponent.prototype.saveSimulation = function () {
        var body = this.profileForm.value;
        this.HillStochasticalService.saveSimulation(body).subscribe(function (data) {
            console.log(data);
        });
    };
    HillStochasticalComponent.prototype.openSaveSimulation = function () {
        var _this = this;
        this.HillStochasticalService.openSaveSimulation('test21').subscribe(function (data) {
            console.log('podaci', data);
            _this.stochastical = data;
            _this.inputData = data.parameters;
            var datasets = [
                [{ data: data.inputData.fractional_fluorescence },
                    { data: data.inputData.fraction_saturation },
                    { data: data.inputData.fraction_of_unbound_actin },
                    { data: data.inputData.free_myosin_concentration }]
            ];
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
            }
            _this.graphs[0].options = {
                title: {
                    display: true,
                    text: 'Error',
                    // fontColor: 'blue',
                    fontStyle: 'small-caps',
                },
                tooltips: {
                    enabled: false,
                    custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                    intersect: true,
                    mode: 'index',
                    position: 'nearest',
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                        }
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 15,
                        top: 0,
                        bottom: 5
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                            type: 'linear',
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                padding: 5,
                                beginAtZero: true,
                                stepSize: 0.5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC1 (%)',
                                fontSize: 11
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                beginAtZero: true,
                                padding: 5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC555 (%)',
                                fontSize: 11
                            }
                        }]
                },
                elements: {
                    line: {
                        borderWidth: 2
                    },
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4,
                        hoverBorderWidth: 3,
                    }
                },
                legend: {
                    display: false
                }
            };
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__["BaseChartDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
    ], HillStochasticalComponent.prototype, "charts", void 0);
    HillStochasticalComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-hill-stochastical',
            template: __webpack_require__(/*! ./hill-stochastical.component.html */ "./src/app/views/hill-stochastical/hill-stochastical.component.html"),
            styles: [__webpack_require__(/*! ./hill-stochastical.component.scss */ "./src/app/views/hill-stochastical/hill-stochastical.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _service_hill_stochastical_service__WEBPACK_IMPORTED_MODULE_4__["HillStochasticalService"]])
    ], HillStochasticalComponent);
    return HillStochasticalComponent;
}());



/***/ }),

/***/ "./src/app/views/hill-stochastical/hill-stochastical.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/views/hill-stochastical/hill-stochastical.module.ts ***!
  \*********************************************************************/
/*! exports provided: HillStochasticalModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HillStochasticalModule", function() { return HillStochasticalModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _hill_stochastical_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./hill-stochastical.component */ "./src/app/views/hill-stochastical/hill-stochastical.component.ts");
/* harmony import */ var _hill_stochastical_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./hill-stochastical-routing.module */ "./src/app/views/hill-stochastical/hill-stochastical-routing.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");












var HillStochasticalModule = /** @class */ (function () {
    function HillStochasticalModule() {
    }
    HillStochasticalModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _hill_stochastical_routing_module__WEBPACK_IMPORTED_MODULE_8__["HillStochasticalRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(),
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ],
            declarations: [_hill_stochastical_component__WEBPACK_IMPORTED_MODULE_7__["HillStochasticalComponent"]],
            exports: [
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ]
        })
    ], HillStochasticalModule);
    return HillStochasticalModule;
}());



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    server: {
        host: 'http://localhost:3000/api/',
    }
};


/***/ })

}]);
//# sourceMappingURL=views-hill-stochastical-hill-stochastical-module.js.map