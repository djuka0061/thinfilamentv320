import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HillProbabilisticComponent } from './hill-probabilistic.component';

const routes: Routes = [
  {
    path: '',
    component: HillProbabilisticComponent,
    data: {
      title: 'Hill Probabilistic'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HillProbabilisticRoutingModule {}
