import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import { DefaultLayoutComponent } from './containers';
import { PlainLayoutComponent} from './containers/plain-layout'

import { P404Component } from './views/error/404.component';
import { P500Component } from './views/error/500.component';
import { LoginComponent } from './views/login/login.component';
import { RegisterComponent } from './views/register/register.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'probabilistic',
    pathMatch: 'full',
  },
  {
    path: '404',
    component: P404Component,
    data: {
      title: 'Page 404'
    }
  },
  {
    path: '500',
    component: P500Component,
    data: {
      title: 'Page 500'
    }
  },
  {
    path: 'login',
    component: LoginComponent,
    data: {
      title: 'Login Page'
    }
  },
  {
    path: 'register',
    component: RegisterComponent,
    data: {
      title: 'Register Page'
    }
  },
  {
    path: '',
    component: DefaultLayoutComponent,
    data: {
      title: 'Models'
    },
    children: [
      {
        path: 'probabilistic',
        loadChildren: './views/probabilistic/probabilistic.module#ProbabilisticModule'
      },
      {
        path: 'widgets',
        loadChildren: './views/widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'stochastical',
        loadChildren: './views/stochastical/stochastical.module#StochasticalModule'
      },
      {
        path: 'david',
        loadChildren: './views/david/david.module#DavidModule'
      },
      {
        path: 'hill-monte-carlo',
        loadChildren: './views/hill-monte-carlo/hill-monte-carlo.module#HillMonteCarloModule'
      },
      {
        path: 'hill-probabilistic',
        loadChildren: './views/hill-probabilistic/hill-probabilistic.module#HillProbabilisticModule'
      },
      {
        path: 'hill-stochastical',
        loadChildren: './views/hill-stochastical/hill-stochastical.module#HillStochasticalModule'
      },
      {
        path: 'monte-carlo',
        loadChildren: './views/monte-carlo/monte-carlo.module#MonteCarloModule'
      },
      {
        path: 'simulated-annealing',
        loadChildren: './views/simulated-annealing/simulated-annealing.module#SimulatedAnnealingModule'
      },
      {
        path: 'mg-probabilistic',
        loadChildren: './views/mg-probabilistic/mg-probabilistic.module#MgProbabilisticModule'
      },
      {
        path: 'quasi-newton',
        loadChildren: './views/quasi-newton/quasi-newton.module#QuasiNewtonModule'
      },
      {
        path: 'home',
        loadChildren: './views/home/home.module#HomeModule'
      },
      {
        path: 'help',
        loadChildren: './views/help/help.module#HelpModule'
      },
      {
        path: 'archive',
        loadChildren: './views/archive/archive.module#ArchiveModule'
      },
      {
        path: 'about',
        loadChildren: './views/about/about.module#AboutModule'
      },
    ]
  },
  {
    path: '',
    component: PlainLayoutComponent,
    data: {
      title: 'Models'
    },
    children: [
      {
        path: 'models/probabilistic',
        loadChildren: './views/probabilistic/probabilistic.module#ProbabilisticModule'
      },
      {
        path: 'models/widgets',
        loadChildren: './views/widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'models/stochastical',
        loadChildren: './views/stochastical/stochastical.module#StochasticalModule'
      },
      {
        path: 'models/david',
        loadChildren: './views/david/david.module#DavidModule'
      },
      {
        path: 'models/hill-monte-carlo',
        loadChildren: './views/hill-monte-carlo/hill-monte-carlo.module#HillMonteCarloModule'
      },
      {
        path: 'models/hill-probabilistic',
        loadChildren: './views/hill-probabilistic/hill-probabilistic.module#HillProbabilisticModule'
      },
      {
        path: 'models/hill-stochastical',
        loadChildren: './views/hill-stochastical/hill-stochastical.module#HillStochasticalModule'
      },
      {
        path: 'models/monte-carlo',
        loadChildren: './views/monte-carlo/monte-carlo.module#MonteCarloModule'
      },
      {
        path: 'models/simulated-annealing',
        loadChildren: './views/simulated-annealing/simulated-annealing.module#SimulatedAnnealingModule'
      },
      {
        path: 'models/mg-probabilistic',
        loadChildren: './views/mg-probabilistic/mg-probabilistic.module#MgProbabilisticModule'
      },
      {
        path: 'models/quasi-newton',
        loadChildren: './views/quasi-newton/quasi-newton.module#QuasiNewtonModule'
      },
      {
        path: 'models/archive',
        loadChildren: './views/archive-plain/archive-plain.module#ArchivePlainModule'
      },
    ]
  },
  { path: '**', component: P404Component }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
