c ----------------------------------------------------------------------
c ----------------------------------------------------------------------
c --- interpola una tira
c ----------------------------------------------------------------------
c ----------------------------------------------------------------------
      subroutine interp(xa,ya,ma,x,y,m)
      integer i,m,ns,ns0
      real(8) dif,dift
      real(8) xa(ma),x(m)
      real(8) ya(ma),y(m)

      ns0=1
      do j=1,m
         ns=ns0
         dif=x(j)-xa(ns0)
cc       here we find the index ns of the closest table entry
         do i=ns0+1,ma
            dift=x(j)-xa(i)
            if (abs(dift).lt.abs(dif)) then
               ns=i
               dif=dift
            endif
         enddo
         ns0=ns
         if (dif.lt.0.0) then
            y(j) = (ya(ns-1)*(xa(ns  )-x(j)) +
     &              ya(ns  )*(x(j)-xa(ns-1))) /
     &             (xa(ns) - xa(ns-1))
         elseif(dif.gt.0.0) then
            y(j) = (ya(ns+1)*(x(j)-xa(ns)) +
     &              ya(ns  )*(xa(ns+1)-x(j))) /
     &             (xa(ns+1) - xa(ns))
         else
            y(j) = ya(ns)
         endif
c         write(*,*) xa(j),ya(j)
c         write(*,*) x(j),y(j)
c         write(*,*)
      enddo
      end

