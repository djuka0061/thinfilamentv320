import { Component, OnInit, QueryList, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { MGProbabilisticService } from '../../service/mg_probabilistic.service';
import { BaseChartDirective } from 'ng2-charts/ng2-charts';
import { environment } from '../../../environments/environment.prod';
import { timer } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { Chart } from 'chart.js'
import { ActivatedRoute } from '@angular/router';


const host = environment.server.host;

export interface PeriodicElement {
  last: string;
  average: string;
  std_dev: string;
}

export interface Graph {
  datasets: Array<any>;
  options: any;
  chartType: string;
  colors: Array<any>;
  legend: boolean;
  labels: Array<any>;
  visibility: Boolean;
}

@Component({
  selector: 'app-mg-probabilistic',
  templateUrl: './mg-probabilistic.component.html',
  styleUrls: ['./mg-probabilistic.component.scss']
})
export class MgProbabilisticComponent implements OnInit, AfterViewInit {
  mgProbabilisticForm: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    kb: new FormControl(''),
    kb_minus: new FormControl(''),
    kt: new FormControl(''),
    kt_minus: new FormControl(''),
    k1: new FormControl(''),
    k1_minus: new FormControl(''),
    k2: new FormControl(''),
    k2_minus: new FormControl(''),
    actin: new FormControl(''),
    myosin: new FormControl(''),
    epsilon: new FormControl(''),
    number_of_iteration: new FormControl(''),
    kb_fiting: new FormControl(''),
    kb_guess_of_params1: new FormControl(''),
    kb_guess_of_params2: new FormControl(''),
    kb_lower_bound: new FormControl(''),
    kb_upper_bound: new FormControl(''),
    kb_minus_fiting: new FormControl(''),
    kb_minus_guess_of_params1: new FormControl(''),
    kb_minus_guess_of_params2: new FormControl(''),
    kb_minus_lower_bound: new FormControl(''),
    kb_minus_upper_bound: new FormControl(''),
    kt_fiting: new FormControl(''),
    kt_guess_of_params1: new FormControl(''),
    kt_guess_of_params2: new FormControl(''),
    kt_lower_bound: new FormControl(''),
    kt_upper_bound: new FormControl(''),
    kt_minus_fiting: new FormControl(''),
    kt_minus_guess_of_params1: new FormControl(''),
    kt_minus_guess_of_params2: new FormControl(''),
    kt_minus_lower_bound: new FormControl(''),
    kt_minus_upper_bound: new FormControl(''),
    k1_fiting: new FormControl(''),
    k1_guess_of_params1: new FormControl(''),
    k1_guess_of_params2: new FormControl(''),
    k1_lower_bound: new FormControl(''),
    k1_upper_bound: new FormControl(''),
    k1_minus_fiting: new FormControl(''),
    k1_minus_guess_of_params1: new FormControl(''),
    k1_minus_guess_of_params2: new FormControl(''),
    k1_minus_lower_bound: new FormControl(''),
    k1_minus_upper_bound: new FormControl(''),
    k2_fiting: new FormControl(''),
    k2_guess_of_params1: new FormControl(''),
    k2_guess_of_params2: new FormControl(''),
    k2_lower_bound: new FormControl(''),
    k2_upper_bound: new FormControl(''),
    k2_minus_fiting: new FormControl(''),
    k2_minus_guess_of_params1: new FormControl(''),
    k2_minus_guess_of_params2: new FormControl(''),
    k2_minus_lower_bound: new FormControl(''),
    k2_minus_upper_bound: new FormControl(''),
    final_myosin: new FormControl(''),
    type_of_simulation: new FormControl(''),
    type_of_simulation_final_myosin: new FormControl('')
  })
  simulationId: string

  constructor(private fb: FormBuilder, private MGProbabilisticService: MGProbabilisticService, private route: ActivatedRoute) {
    this.route.queryParams.subscribe(params => {
      this.simulationId = params['simulationId'];
      if (this.simulationId) {
        this.openArchiveSimulation(this.simulationId)
      }
    });
  }

  public mgProbabilistic: any;
  public dataSource;
  public sensitivityMatrix;
  public inputData = this.profileForm;;
  public dugmeRun: Boolean = false;
  public displayedColumns: string[];
  public isVisible = true;
  public run_button = "RUN";
  fileToUpload: File = null;
  public typeOfSimulation: Number = 1;

  public experimentalModelGraph
  public parametersConvergance

  private colors = [
    { borderColor: 'blue', backgroundColor: 'transparent' },
    { borderColor: 'purple', backgroundColor: 'transparent' },
    { borderColor: 'yellow', backgroundColor: 'transparent' },
    { borderColor: 'orange', backgroundColor: 'transparent' },
    { borderColor: 'green', backgroundColor: 'transparent' },
    { borderColor: 'black', backgroundColor: 'transparent' },
    { borderColor: 'brown', backgroundColor: 'transparent' }
  ]

  ngOnInit() { }

  ngAfterViewInit(): void {
    this.MGProbabilisticService.getInputData('test').subscribe(data => {
      if (data) {
        this.inputData = data;
      }
    });
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {})
    this.parametersConvergance = new Chart('parametersConvergance', {})
  }

  returnData(simulationID) {
    let subrscription =
      timer(0, 10000).pipe(
        switchMap(() => this.MGProbabilisticService.returnData(simulationID))
      ).subscribe(data => {
        if (data !== null && data.estimatedParams !== null && data.statusCode < 400) {
          this.dugmeRun = false;
          const a_t = data.estimatedParams.a_t;
          const models = data.estimatedParams.model;
          const vidljivo = [
            Boolean(data.parameters.kb_fiting),
            Boolean(data.parameters.kb_minus_fiting),
            Boolean(data.parameters.kt_fiting),
            Boolean(data.parameters.kt_minus_fiting),
            Boolean(data.parameters.k1_fiting),
            Boolean(data.parameters.k1_minus_fiting),
            Boolean(data.parameters.k2_fiting),
            Boolean(data.parameters.k2_minus_fiting)
          ];

          const podaci = [
            { data: data.estimatedParams.kb, label: 'KB' },
            { data: data.estimatedParams.kb_minus, label: 'KB-' },
            { data: data.estimatedParams.kt, label: 'KT' },
            { data: data.estimatedParams.kt_minus, label: 'kT-' },
            { data: data.estimatedParams.k1, label: 'K1' },
            { data: data.estimatedParams.k1_minus, label: 'K1-' },
            { data: data.estimatedParams.k2, label: 'K2' },
            { data: data.estimatedParams.k2_minus, label: 'K2-' }
          ];

          let dataset = []
          this.experimentAndModelGraph(a_t, models)
          dataset.push({
            label: 'Error',
            data: data.estimatedParams.error,
            fill: false,
            lineTension: 0.2,
            borderColor: 'red',
            borderWidth: 1,
          })
          let j = 0;
          for (let i = 0; i < 8; i++) {
            if (vidljivo[i]) {
              dataset.push({
                label: podaci[i].label,
                data: podaci[i].data,
                fill: false,
                lineTension: 0.2,
                borderColor: this.colors[j].borderColor,
                borderWidth: 1,
              })
              j++;
            }
          }
          this.parametersConvergancePrepare(dataset)

          this.isVisible = true;

          for (let i = 1; i < data.estimatedParams.estimated_values.length; i++) {
            data.estimatedParams.estimated_values[i][1] = data.estimatedParams.estimated_values[i][1].toFixed(6)
            data.estimatedParams.estimated_values[i][2] = data.estimatedParams.estimated_values[i][2].toFixed(6)
            data.estimatedParams.estimated_values[i][3] = data.estimatedParams.estimated_values[i][3].toFixed(6)
          }

          this.dataSource = data.estimatedParams.estimated_values;
          this.sensitivityMatrix = data.estimatedParams.sensitivity;
          this.dugmeRun = false;
          this.run_button = "RUN";
          subrscription.unsubscribe();
        }
      })
  }

  download() {
    window.open(host + 'mg_pobabilistics/users/test21/download');
  }

  saveSimulation() {
    const body = this.profileForm.value;
    this.MGProbabilisticService.saveSimulation(body).subscribe(data => {
      console.log(data);
    })
  }

  openSaveSimulation(file) {
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    const fileData = file.target.files
    var reader = new FileReader();
    reader.readAsText(fileData[0]);
    reader.onload = (_event) => {
      const data = JSON.parse(reader.result.toString());
      this.inputData = data.parameters
      if (data.estimatedParams) {
        const a_t = data.estimatedParams.a_t;
        const models = data.estimatedParams.model;
        const vidljivo = [
          Boolean(data.parameters.kb_fiting),
          Boolean(data.parameters.kb_minus_fiting),
          Boolean(data.parameters.kt_fiting),
          Boolean(data.parameters.kt_minus_fiting),
          Boolean(data.parameters.k1_fiting),
          Boolean(data.parameters.k1_minus_fiting),
          Boolean(data.parameters.k2_fiting),
          Boolean(data.parameters.k2_minus_fiting)
        ];

        const podaci = [
          { data: data.estimatedParams.kb, label: 'KB' },
          { data: data.estimatedParams.kb_minus, label: 'KB-' },
          { data: data.estimatedParams.kt, label: 'KT' },
          { data: data.estimatedParams.kt_minus, label: 'kT-' },
          { data: data.estimatedParams.k1, label: 'K1' },
          { data: data.estimatedParams.k1_minus, label: 'K1-' },
          { data: data.estimatedParams.k2, label: 'K2' },
          { data: data.estimatedParams.k2_minus, label: 'K2-' }
        ];

        let dataset = []
        this.experimentAndModelGraph(a_t, models)
        dataset.push({
          label: 'Error',
          data: data.estimatedParams.error,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        })
        let j = 0;
        for (let i = 0; i < 8; i++) {
          if (vidljivo[i]) {
            dataset.push({
              label: podaci[i].label,
              data: podaci[i].data,
              fill: false,
              lineTension: 0.2,
              borderColor: this.colors[j].borderColor,
              borderWidth: 1,
            })
            j++;
          }
        }
        this.parametersConvergancePrepare(dataset)

        this.isVisible = true;

        for (let i = 1; i < data.estimatedParams.estimated_values.length; i++) {
          data.estimatedParams.estimated_values[i][1] = data.estimatedParams.estimated_values[i][1].toFixed(6)
          data.estimatedParams.estimated_values[i][2] = data.estimatedParams.estimated_values[i][2].toFixed(6)
          data.estimatedParams.estimated_values[i][3] = data.estimatedParams.estimated_values[i][3].toFixed(6)
        }

        this.dataSource = data.estimatedParams.estimated_values;
        this.sensitivityMatrix = data.estimatedParams.sensitivity;
      }
    };
  }

  openArchiveSimulation(simulationId) {
    this.MGProbabilisticService.returnData(simulationId).subscribe(data => {
      this.inputData = data.parameters
      if (data.estimatedParams) {
        const a_t = data.estimatedParams.a_t;
        const models = data.estimatedParams.model;
        const vidljivo = [
          Boolean(data.parameters.kb_fiting),
          Boolean(data.parameters.kb_minus_fiting),
          Boolean(data.parameters.kt_fiting),
          Boolean(data.parameters.kt_minus_fiting),
          Boolean(data.parameters.k1_fiting),
          Boolean(data.parameters.k1_minus_fiting),
          Boolean(data.parameters.k2_fiting),
          Boolean(data.parameters.k2_minus_fiting)
        ];

        const podaci = [
          { data: data.estimatedParams.kb, label: 'KB' },
          { data: data.estimatedParams.kb_minus, label: 'KB-' },
          { data: data.estimatedParams.kt, label: 'KT' },
          { data: data.estimatedParams.kt_minus, label: 'kT-' },
          { data: data.estimatedParams.k1, label: 'K1' },
          { data: data.estimatedParams.k1_minus, label: 'K1-' },
          { data: data.estimatedParams.k2, label: 'K2' },
          { data: data.estimatedParams.k2_minus, label: 'K2-' }
        ];

        let dataset = []
        this.experimentAndModelGraph(a_t, models)
        dataset.push({
          label: 'Error',
          data: data.estimatedParams.error,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        })
        let j = 0;
        for (let i = 0; i < 8; i++) {
          if (vidljivo[i]) {
            dataset.push({
              label: podaci[i].label,
              data: podaci[i].data,
              fill: false,
              lineTension: 0.2,
              borderColor: this.colors[j].borderColor,
              borderWidth: 1,
            })
            j++;
          }
        }
        this.parametersConvergancePrepare(dataset)

        this.isVisible = true;

        for (let i = 1; i < data.estimatedParams.estimated_values.length; i++) {
          data.estimatedParams.estimated_values[i][1] = data.estimatedParams.estimated_values[i][1].toFixed(6)
          data.estimatedParams.estimated_values[i][2] = data.estimatedParams.estimated_values[i][2].toFixed(6)
          data.estimatedParams.estimated_values[i][3] = data.estimatedParams.estimated_values[i][3].toFixed(6)
        }

        this.dataSource = data.estimatedParams.estimated_values;
        this.sensitivityMatrix = data.estimatedParams.sensitivity;
      }
    });
  }

  onSubmit() {
    this.dugmeRun = true;
    this.run_button = "RUNNING";
    this.isVisible = false;
    console.log('start simulation');
    const body = this.profileForm.value;
    const atData = [];
    console.log(this.profileForm.value.file);
    const file = this.profileForm.value.file
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    this.MGProbabilisticService.upload(this.fileToUpload).subscribe(upload => {
      console.log(upload);
      this.MGProbabilisticService.getData(body).subscribe(data => {
        this.returnData(data.simulationId)
      });
    });
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onCheckboxChange(box1: string, box2: string) {
    if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
      if (this.inputData[box1])
        this.inputData[box2] = false;
    }
  }

  experimentAndModelGraph(experiment: any, model: any) {
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {
      type: 'line',
      data: {
        datasets: [{
          label: 'Model',
          data: model,
          fill: false,
          lineTension: 0.2,
          borderColor: 'blue',
          borderWidth: 1,
          showLine: false
        }, {
          label: 'Experiment',
          data: experiment,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Actin concentration [microM]'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Fractional Fluorescence'
            }
          }]
        }
      }
    })
  }

  parametersConvergancePrepare(datasets: any) {
    this.parametersConvergance = new Chart('parametersConvergance', {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Iteration'
            }
          }], yAxes: [{
            ticks: {
              callback: function (value) {
                return value.toExponential(2)
              }
            }
          }]
        }
      }
    })
  }

}
