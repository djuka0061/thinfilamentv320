(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-quasi-newton-quasi-newton-module"],{

/***/ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js":
/*!************************************************************************************************!*\
  !*** ./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
   true ? factory(exports) :
  undefined;
}(this, (function (exports) { 'use strict';

  /**
   * --------------------------------------------------------------------------
   * CoreUI Plugins - Custom Tooltips for Chart.js (v1.2.0): custom-tooltips.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */
  function CustomTooltips(tooltipModel) {
    var _this = this;

    // Add unique id if not exist
    var _setCanvasId = function _setCanvasId() {
      var _idMaker = function _idMaker() {
        var _hex = 16;
        var _multiplier = 0x10000;
        return ((1 + Math.random()) * _multiplier | 0).toString(_hex);
      };

      var _canvasId = "_canvas-" + (_idMaker() + _idMaker());

      _this._chart.canvas.id = _canvasId;
      return _canvasId;
    };

    var ClassName = {
      ABOVE: 'above',
      BELOW: 'below',
      CHARTJS_TOOLTIP: 'chartjs-tooltip',
      NO_TRANSFORM: 'no-transform',
      TOOLTIP_BODY: 'tooltip-body',
      TOOLTIP_BODY_ITEM: 'tooltip-body-item',
      TOOLTIP_BODY_ITEM_COLOR: 'tooltip-body-item-color',
      TOOLTIP_BODY_ITEM_LABEL: 'tooltip-body-item-label',
      TOOLTIP_BODY_ITEM_VALUE: 'tooltip-body-item-value',
      TOOLTIP_HEADER: 'tooltip-header',
      TOOLTIP_HEADER_ITEM: 'tooltip-header-item'
    };
    var Selector = {
      DIV: 'div',
      SPAN: 'span',
      TOOLTIP: (this._chart.canvas.id || _setCanvasId()) + "-tooltip"
    };
    var tooltip = document.getElementById(Selector.TOOLTIP);

    if (!tooltip) {
      tooltip = document.createElement('div');
      tooltip.id = Selector.TOOLTIP;
      tooltip.className = ClassName.CHARTJS_TOOLTIP;

      this._chart.canvas.parentNode.appendChild(tooltip);
    } // Hide if no tooltip


    if (tooltipModel.opacity === 0) {
      tooltip.style.opacity = 0;
      return;
    } // Set caret Position


    tooltip.classList.remove(ClassName.ABOVE, ClassName.BELOW, ClassName.NO_TRANSFORM);

    if (tooltipModel.yAlign) {
      tooltip.classList.add(tooltipModel.yAlign);
    } else {
      tooltip.classList.add(ClassName.NO_TRANSFORM);
    } // Set Text


    if (tooltipModel.body) {
      var titleLines = tooltipModel.title || [];
      var tooltipHeader = document.createElement(Selector.DIV);
      tooltipHeader.className = ClassName.TOOLTIP_HEADER;
      titleLines.forEach(function (title) {
        var tooltipHeaderTitle = document.createElement(Selector.DIV);
        tooltipHeaderTitle.className = ClassName.TOOLTIP_HEADER_ITEM;
        tooltipHeaderTitle.innerHTML = title;
        tooltipHeader.appendChild(tooltipHeaderTitle);
      });
      var tooltipBody = document.createElement(Selector.DIV);
      tooltipBody.className = ClassName.TOOLTIP_BODY;
      var tooltipBodyItems = tooltipModel.body.map(function (item) {
        return item.lines;
      });
      tooltipBodyItems.forEach(function (item, i) {
        var tooltipBodyItem = document.createElement(Selector.DIV);
        tooltipBodyItem.className = ClassName.TOOLTIP_BODY_ITEM;
        var colors = tooltipModel.labelColors[i];
        var tooltipBodyItemColor = document.createElement(Selector.SPAN);
        tooltipBodyItemColor.className = ClassName.TOOLTIP_BODY_ITEM_COLOR;
        tooltipBodyItemColor.style.backgroundColor = colors.backgroundColor;
        tooltipBodyItem.appendChild(tooltipBodyItemColor);

        if (item[0].split(':').length > 1) {
          var tooltipBodyItemLabel = document.createElement(Selector.SPAN);
          tooltipBodyItemLabel.className = ClassName.TOOLTIP_BODY_ITEM_LABEL;
          tooltipBodyItemLabel.innerHTML = item[0].split(': ')[0];
          tooltipBodyItem.appendChild(tooltipBodyItemLabel);
          var tooltipBodyItemValue = document.createElement(Selector.SPAN);
          tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          tooltipBodyItemValue.innerHTML = item[0].split(': ').pop();
          tooltipBodyItem.appendChild(tooltipBodyItemValue);
        } else {
          var _tooltipBodyItemValue = document.createElement(Selector.SPAN);

          _tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          _tooltipBodyItemValue.innerHTML = item[0];
          tooltipBodyItem.appendChild(_tooltipBodyItemValue);
        }

        tooltipBody.appendChild(tooltipBodyItem);
      });
      tooltip.innerHTML = '';
      tooltip.appendChild(tooltipHeader);
      tooltip.appendChild(tooltipBody);
    }

    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft; // Display, position, and set styles for font

    tooltip.style.opacity = 1;
    tooltip.style.left = positionX + tooltipModel.caretX + "px";
    tooltip.style.top = positionY + tooltipModel.caretY + "px";
  }

  exports.CustomTooltips = CustomTooltips;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=custom-tooltips.js.map


/***/ }),

/***/ "./src/app/service/quasi_newton.service.ts":
/*!*************************************************!*\
  !*** ./src/app/service/quasi_newton.service.ts ***!
  \*************************************************/
/*! exports provided: QuasiNewtonService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuasiNewtonService", function() { return QuasiNewtonService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");




var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].server.host;
var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
headers.set('enctype', 'multipart/form-data');
var QuasiNewtonService = /** @class */ (function () {
    function QuasiNewtonService(httpClient) {
        this.httpClient = httpClient;
    }
    QuasiNewtonService.prototype.getConfig = function () {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get('');
    };
    QuasiNewtonService.prototype.getData = function (json) {
        return this.httpClient.post(host + 'quasi_newtons/users/test21/runSimulation', json, httpOptions);
    };
    QuasiNewtonService.prototype.getInputData = function (userID) {
        return this.httpClient.get(host + 'quasi_newtons/users/test21/returnInputData');
    };
    QuasiNewtonService.prototype.download = function (userID) {
        return this.httpClient.get(host + 'quasi_newtons/users/test21/download');
    };
    QuasiNewtonService.prototype.upload = function (fileToUpload) {
        console.log(fileToUpload);
        var formData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        console.log(formData.getAll('file'));
        return this.httpClient.post(host + 'quasi_newtons/users/test21/upload', formData, { headers: headers });
    };
    QuasiNewtonService.prototype.saveSimulation = function (body) {
        return this.httpClient.post(host + 'quasi_newtons/users/test21/saveData', body, httpOptions);
    };
    QuasiNewtonService.prototype.openSaveSimulation = function (userID) {
        return this.httpClient.get(host + 'quasi_newtons/users/test21/returnSaveData');
    };
    QuasiNewtonService.prototype.resetSimulation = function (userID) {
        return this.httpClient.get(host + 'quasi_newtons/users/test21/download');
    };
    QuasiNewtonService.prototype.saveDefaultValue = function (userID) {
        return this.httpClient.get(host + 'quasi_newtons/users/test21/download');
    };
    QuasiNewtonService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], QuasiNewtonService);
    return QuasiNewtonService;
}());



/***/ }),

/***/ "./src/app/views/quasi-newton/quasi-newton-routing.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/views/quasi-newton/quasi-newton-routing.module.ts ***!
  \*******************************************************************/
/*! exports provided: QuasiNewtonRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuasiNewtonRoutingModule", function() { return QuasiNewtonRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _quasi_newton_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./quasi-newton.component */ "./src/app/views/quasi-newton/quasi-newton.component.ts");




var routes = [
    {
        path: '',
        component: _quasi_newton_component__WEBPACK_IMPORTED_MODULE_3__["QuasiNewtonComponent"],
        data: {
            title: 'QuasiNewton'
        }
    }
];
var QuasiNewtonRoutingModule = /** @class */ (function () {
    function QuasiNewtonRoutingModule() {
    }
    QuasiNewtonRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], QuasiNewtonRoutingModule);
    return QuasiNewtonRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/quasi-newton/quasi-newton.component.html":
/*!****************************************************************!*\
  !*** ./src/app/views/quasi-newton/quasi-newton.component.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn d-flex\">\r\n  <div class=\"d-flex w-100 flex-wrap \">\r\n    <!--Parameters-->\r\n    <div class=\"d-flex w-30 p-2 align-self-start flex-min-width\">\r\n      <div class=\"d-flex card card-accent-primary \">\r\n        <div class=\"card-header\">\r\n          SIMULATION AND MODEL DATA\r\n        </div>\r\n        <div class=\"card-body d-flex w-100  \">\r\n          <!--div class=\"d-flex flex-column w-100\"-->\r\n          <form [formGroup]=\"profileForm\" (ngSubmit)=\"onSubmit()\">\r\n\r\n            <div class=\"d-flex flex-column w-100\">\r\n\r\n              <div class=\"d-flex w-100 pt-1 align-content-end\">\r\n                <div class=\"d-flex w-60\"> &nbsp; </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"!profileForm.valid\">SAVE</button>\r\n                </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"!profileForm.valid\">OPEN</button>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"d-flex title-div pb-1 pt-1\">\r\n                Simulation parameters and experimental data\r\n              </div>\r\n              <div class=\"d-flex flex-column w-100 pt-2\">\r\n\r\n                <div class=\"d-flex w-100\">\r\n                  <div class=\"w-30 p-1\">\r\n                    <span style=\"vertical-align: middle\">Total time</span>\r\n                  </div>\r\n                  <div class=\"d-flex w-15 p-1\">\r\n                    <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.s}} formControlName=\"s\">\r\n                  </div>\r\n                  <div class=\"w-25 p-1\">\r\n                    <span style=\"vertical-align: middle\">[s]</span>\r\n                  </div>\r\n                </div>\r\n                <div class=\"d-flex w-100\">\r\n                  <div class=\"w-30 p-1\">\r\n                    <span style=\"vertical-align: middle\">Actin</span>\r\n                  </div>\r\n                  <div class=\"d-flex w-15 p-1\">\r\n                    <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.actin}}\r\n                      formControlName=\"actin\">\r\n                  </div>\r\n                  <div class=\"w-25 p-1\">\r\n                    <span style=\"vertical-align: middle\">[s]</span>\r\n                  </div>\r\n                </div>\r\n                <div class=\"d-flex w-100\">\r\n                  <div class=\"w-30 p-1\">\r\n                    <span style=\"vertical-align: middle\">Experimental Data File</span>\r\n                  </div>\r\n                  <div class=\"w-40 p-1\">\r\n                    <input class=\"w-100\" type=\"file\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"d-flex w-100 pt-3\">\r\n                  <div class=\"d-flex w-50\">\r\n                    <div class=\"d-flex w-35\"><span>Type of Simulation</span></div>\r\n                    <div class=\"d-flex flex-column w-65\">\r\n                      <div class=\"form-check\">\r\n                        <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                          [(ngModel)]=\"this.inputData.type_of_simulation\" formControlName=\"type_of_simulation\"\r\n                          (ngModelChange)=\"onCheckboxChange('type_of_simulation', 'type_of_simulation_final_myosin')\">\r\n                        <label class=\"form-check-label\">Time Course</label>\r\n                      </div>\r\n                      <div class=\"form-check\">\r\n                        <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                          [(ngModel)]=\"this.inputData.type_of_simulation_final_myosin\"\r\n                          formControlName=\"type_of_simulation_final_myosin\"\r\n                          (ngModelChange)=\"onCheckboxChange('type_of_simulation_final_myosin', 'type_of_simulation')\">\r\n                        <label class=\"form-check-label\">Titration</label>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex flex-column w-50\">\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Initial)\">\r\n                        <span style=\"vertical-align: middle\">Myosin Initial</span>\r\n                      </div>\r\n                      <div class=\"w-37 p-1\">\r\n                        <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                                  formControlName=\"myosin\"-->\r\n                        <input class=\"w-100 text-align-right\" type=\"text\">\r\n                      </div>\r\n                      <div class=\"w-20 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"d-flex w-100\" *ngIf=\"this.inputData.type_of_simulation_final_myosin\">\r\n                      <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Final)\">\r\n                        <span style=\"vertical-align: middle\">Myosin Final</span>\r\n                      </div>\r\n                      <div class=\"w-37 p-1\">\r\n                        <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                                  formControlName=\"myosin\"-->\r\n                        <input class=\"w-100 text-align-right\" type=\"text\">\r\n                      </div>\r\n                      <div class=\"w-20 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"d-flex flex-column w-100 pt-2\">\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Parameters for estimation\r\n                </div>\r\n                <!--<div class=\"d-flex text-center pt-2 text-bottom\"> -->\r\n                <div class=\"d-flex text-center pt-2 my-font-sm75\">\r\n                  <div class=\"w-40 p-1\"></div>\r\n                  <div class=\"w-10 p-1 align-self-end\">Fitting</div>\r\n                  <div class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1 align-self-end\">Guess</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Lower Bound</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Upper Bound</div>\r\n                  </div>\r\n                </div>\r\n\r\n                <!-- novo -->\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">Kb</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kb_plus}}\r\n                        formControlName=\"kb_plus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kb_fiting\"\r\n                      formControlName=\"kb_fiting\" (ngModelChange)=\"onCheckboxChange('kb_fiting', 'kb_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kb_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_guess1}}\r\n                        formControlName=\"kb_guess\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_lower_bound}}\r\n                        formControlName=\"kb_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_upper_bound}}\r\n                        formControlName=\"kb_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kb-</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kb_minus}}\r\n                        formControlName=\"kb_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kb_minus_fiting\"\r\n                      formControlName=\"kb_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kb_minus_fiting', 'kb_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kb_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minus_guess}}\r\n                        formControlName=\"kb_minus_guess\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minus_lower_bound}}\r\n                        formControlName=\"kb_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minus_upper_bound}}\r\n                        formControlName=\"kb_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">Kt</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_plus}}\r\n                        formControlName=\"kt_plus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_fiting\"\r\n                      formControlName=\"kt_fiting\" (ngModelChange)=\"onCheckboxChange('kt_fiting', 'kt_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_guess}}\r\n                        formControlName=\"kt_guess\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_lower_bound}}\r\n                        formControlName=\"kt_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_upper_bound}}\r\n                        formControlName=\"kt_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kt-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_minus}}\r\n                        formControlName=\"kt_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_minus_fiting\"\r\n                      formControlName=\"kt_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kt_minus_fiting', 'kt_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_guess}}\r\n                        formControlName=\"kt_minus_guess\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_lower_bound}}\r\n                        formControlName=\"kt_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_upper_bound}}\r\n                        formControlName=\"kt_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k1+</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k1_plus}}\r\n                        formControlName=\"k1_plus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k1_fiting\"\r\n                      formControlName=\"k1_fiting\" (ngModelChange)=\"onCheckboxChange('k1_fiting', 'k1_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k1_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_guess}}\r\n                        formControlName=\"k1_guess\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_lower_bound}}\r\n                        formControlName=\"k1_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_upper_bound}}\r\n                        formControlName=\"k1_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k1-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k1_minus}}\r\n                        formControlName=\"k1_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k1_minus_fiting\"\r\n                      formControlName=\"k1_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('k1_minus_fiting', 'k1_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k1_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_guess}}\r\n                        formControlName=\"k1_minus_guess\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_lower_bound}}\r\n                        formControlName=\"k1_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_upper_bound}}\r\n                        formControlName=\"k1_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k2+</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k2_plus}}\r\n                        formControlName=\"k2_plus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k2_fiting\"\r\n                      formControlName=\"k2_fiting\" (ngModelChange)=\"onCheckboxChange('k2_fiting', 'k2_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k2_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_guess}}\r\n                        formControlName=\"k2_guess\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_lower_bound}}\r\n                        formControlName=\"k2_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_upper_bound}}\r\n                        formControlName=\"k2_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k2-</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k2_minus}}\r\n                        formControlName=\"k2_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k2_minus_fiting\"\r\n                      formControlName=\"k2_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('k2_minus_fiting', 'k2_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k2_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_guess}}\r\n                        formControlName=\"k2_minus_guess\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_lower_bound}}\r\n                        formControlName=\"k2_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_upper_bound}}\r\n                        formControlName=\"k2_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <!-- end novo-->\r\n\r\n              </div>\r\n\r\n              <div class=\"d-flex w-100 pt-3 \">\r\n                <div class=\"d-flex w-80\"> &nbsp; </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button id=\"dugmeRun\" type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"dugmeRun\">RUN </button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </form>\r\n          <!--/div-->\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"d-flex w-70 p-2\">\r\n      <div class=\"d-flex card card-accent-primary w-100\">\r\n        <div class=\"card-header\">\r\n          RESULTS\r\n          <span class=\"float-right\" tooltip=\"Download graphic data\" placement=\"left\">\r\n            <i (click)=\"download()\" class=\"fa fa-cloud-download\" style=\"font-size:16px\"></i>\r\n          </span>\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex flex-column w-100 mv-400\">\r\n            <div class=\"d-flex w-100 h-5 justify-content-around\">\r\n            </div>\r\n            <div class=\"d-flex flex-column w-100 pt-2\">\r\n              <!--div class=\"card-body d-flex w-100\"-->\r\n              <div class=\"d-flex w-100 flex-wrap justify-content-around align-content-start\">\r\n                <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n                <div class=\"w-100 p-2 mw-350\">\r\n                  <div class=\"chart-wrapper\">\r\n                    <!--style=\"height:300px;margin-top:40px;\"-->\r\n                    <canvas *ngIf=\"graphs[0]\" baseChart class=\"chart\" [datasets]=\"graphs[0].datasets\"\r\n                      [labels]=\"graphs[0].labels\" [options]=\"graphs[0].options\" [colors]=\"graphs[0].colors\"\r\n                      [legend]=\"graphs[0].legend\" [chartType]=\"graphs[0].chartType\">\r\n                    </canvas>\r\n                    <canvas *ngIf=\"!graphs[0]\" baseChart class=\"chart\"></canvas>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/quasi-newton/quasi-newton.component.scss":
/*!****************************************************************!*\
  !*** ./src/app/views/quasi-newton/quasi-newton.component.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column; }\n\n.example-container > * {\n  width: 100%; }\n\n.example-container form {\n  margin-bottom: 20px; }\n\n.example-container form > * {\n  margin: 5px 0; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.table_form {\n  min-width: 550px;\n  padding: 0px; }\n\n.table_form p {\n  font-size: 10px;\n  text-align: right; }\n\n.table_form input {\n  height: 25px;\n  text-align: center;\n  max-width: 100px; }\n\n/* Customize the label (the container) */\n\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n/* Hide the browser's default checkbox */\n\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0; }\n\n/* Create a custom checkbox */\n\n.checkmark {\n  position: absolute;\n  margin-top: 5px;\n  top: 0;\n  left: 0;\n  height: 20 px;\n  width: 20px;\n  background-color: #eee; }\n\n/* On mouse-over, add a grey background color */\n\n.container:hover input ~ .checkmark {\n  background-color: #ccc; }\n\n/* When the checkbox is checked, add a blue background */\n\n.container input:checked ~ .checkmark {\n  background-color: #2196F3; }\n\n/* Create the checkmark/indicator (hidden when not checked) */\n\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none; }\n\n/* Show the checkmark when checked */\n\n.container input:checked ~ .checkmark:after {\n  display: block; }\n\n/* Style the checkmark/indicator */\n\n.container .checkmark:after {\n  left: 9px;\n  top: 5px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg); }\n\n.title-div-centar {\n  font-size: 1rem;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\n.title-div {\n  color: cadetblue;\n  font-style: 1rem;\n  border-bottom: 1px dashed lightblue; }\n\n.title-div-table {\n  font-size: 1rem;\n  color: cadetblue;\n  text-align: center !important; }\n\n.title-div-2 {\n  font-size: 1.2rem;\n  color: cadetblue; }\n\ninput {\n  background-color: #c3e4e5;\n  border: 1px solid \t#c3e4e5 !important; }\n\n.text-align-right {\n  text-align: right;\n  padding-right: 2px !important; }\n\n.text-align-center {\n  text-align: center; }\n\n.flex-min-width {\n  min-width: 470px; }\n\n.max-width-70posto {\n  max-width: 70%; }\n\n.over-auto {\n  overflow: visible; }\n\n.chart-wrapper {\n  border: 1px solid lightblue !important; }\n\n.mw-350 {\n  min-width: 280px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-30posto {\n  min-width: 370px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-400 {\n  min-width: 280px; }\n\n.tbl-10 {\n  min-width: 300px !important; }\n\n.tbl-8kolona {\n  min-width: 300px !important; }\n\n.td-1 {\n  padding: 0.4rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvcXVhc2ktbmV3dG9uL0Y6XFx0aGluZmlsYW1lbnR2MzIwXFxuZXdfY2xpZW50L3NyY1xcYXBwXFx2aWV3c1xccXVhc2ktbmV3dG9uXFxxdWFzaS1uZXd0b24uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFhO0VBQ2Isc0JBQXNCLEVBQUE7O0FBR3hCO0VBQ0UsV0FBVyxFQUFBOztBQUdiO0VBQ0UsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsYUFBYSxFQUFBOztBQUdmO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixXQUFXLEVBQUE7O0FBR2I7RUFDRSxXQUFXLEVBQUE7O0FBR2I7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBSWxCLHdDQUFBOztBQUNBO0VBQ0EsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBR2pCLHdDQUFBOztBQUNBO0VBQ0Esa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUztFQUNULFFBQVEsRUFBQTs7QUFHUiw2QkFBQTs7QUFDQTtFQUNBLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsTUFBTTtFQUNOLE9BQU87RUFDUCxhQUFhO0VBQ2IsV0FBVztFQUNYLHNCQUFzQixFQUFBOztBQUd0QiwrQ0FBQTs7QUFDQTtFQUNBLHNCQUFzQixFQUFBOztBQUd0Qix3REFBQTs7QUFDQTtFQUNBLHlCQUF5QixFQUFBOztBQUd6Qiw2REFBQTs7QUFDQTtFQUNBLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsYUFBYSxFQUFBOztBQUdiLG9DQUFBOztBQUNBO0VBQ0EsY0FBYyxFQUFBOztBQUdkLGtDQUFBOztBQUNBO0VBQ0EsU0FBUztFQUNULFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsZ0NBQWdDO0VBRWhDLHdCQUF3QixFQUFBOztBQUd4QjtFQUVBLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsb0JBQW9CLEVBQUE7O0FBSXBCO0VBQ0EsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixtQ0FBbUMsRUFBQTs7QUFHbkM7RUFDQSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLDZCQUE2QixFQUFBOztBQUc3QjtFQUNBLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFJaEI7RUFFQSx5QkFBMEI7RUFDMUIscUNBQXFDLEVBQUE7O0FBR3JDO0VBQ0EsaUJBQWlCO0VBQ2pCLDZCQUE2QixFQUFBOztBQUc3QjtFQUNBLGtCQUFrQixFQUFBOztBQUlsQjtFQUNBLGdCQUFnQixFQUFBOztBQVNoQjtFQUNBLGNBQWMsRUFBQTs7QUFFZDtFQUNBLGlCQUFpQixFQUFBOztBQUdqQjtFQUVBLHNDQUFzQyxFQUFBOztBQUd0QztFQUNBLGdCQUFnQjtFQUNoQiwyQkFBbUI7RUFBbkIsd0JBQW1CO0VBQW5CLG1CQUFtQixFQUFBOztBQUduQjtFQUNBLGdCQUFnQjtFQUNoQiwyQkFBbUI7RUFBbkIsd0JBQW1CO0VBQW5CLG1CQUFtQixFQUFBOztBQUtuQjtFQUNBLGdCQUFnQixFQUFBOztBQUdoQjtFQUVBLDJCQUEyQixFQUFBOztBQUUzQjtFQUVBLDJCQUEyQixFQUFBOztBQUkzQjtFQUNBLDBCQUEwQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvcXVhc2ktbmV3dG9uL3F1YXNpLW5ld3Rvbi5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtY29udGFpbmVyID4gKiB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtY29udGFpbmVyIGZvcm0ge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtY29udGFpbmVyIGZvcm0gPiAqIHtcclxuICAgIG1hcmdpbjogNXB4IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWZvcm0ge1xyXG4gICAgbWluLXdpZHRoOiAxNTBweDtcclxuICAgIG1heC13aWR0aDogNTAwcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgXHJcbiAgLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICB9XHJcbiAgXHJcbiAgLnRhYmxlX2Zvcm0ge1xyXG4gICAgbWluLXdpZHRoOiA1NTBweDtcclxuICAgIHBhZGRpbmc6IDBweDtcclxuICB9XHJcbiAgXHJcbiAgLnRhYmxlX2Zvcm0gcCB7XHJcbiAgICBmb250LXNpemU6IDEwcHg7XHJcbiAgICB0ZXh0LWFsaWduOiByaWdodDtcclxuICB9XHJcbiAgXHJcbiAgLnRhYmxlX2Zvcm0gaW5wdXQge1xyXG4gICAgaGVpZ2h0OiAyNXB4O1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgbWF4LXdpZHRoOiAxMDBweDtcclxuICB9XHJcbiAgXHJcbiAgXHJcbiAgLyogQ3VzdG9taXplIHRoZSBsYWJlbCAodGhlIGNvbnRhaW5lcikgKi9cclxuICAuY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZy1sZWZ0OiAzNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGZvbnQtc2l6ZTogMjJweDtcclxuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4gIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gIHVzZXItc2VsZWN0OiBub25lO1xyXG4gIH1cclxuICBcclxuICAvKiBIaWRlIHRoZSBicm93c2VyJ3MgZGVmYXVsdCBjaGVja2JveCAqL1xyXG4gIC5jb250YWluZXIgaW5wdXQge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBvcGFjaXR5OiAwO1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBoZWlnaHQ6IDA7XHJcbiAgd2lkdGg6IDA7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIENyZWF0ZSBhIGN1c3RvbSBjaGVja2JveCAqL1xyXG4gIC5jaGVja21hcmsge1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBtYXJnaW4tdG9wOiA1cHg7XHJcbiAgdG9wOiAwO1xyXG4gIGxlZnQ6IDA7XHJcbiAgaGVpZ2h0OiAyMCBweDtcclxuICB3aWR0aDogMjBweDtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xyXG4gIH1cclxuICBcclxuICAvKiBPbiBtb3VzZS1vdmVyLCBhZGQgYSBncmV5IGJhY2tncm91bmQgY29sb3IgKi9cclxuICAuY29udGFpbmVyOmhvdmVyIGlucHV0IH4gLmNoZWNrbWFyayB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NjYztcclxuICB9XHJcbiAgXHJcbiAgLyogV2hlbiB0aGUgY2hlY2tib3ggaXMgY2hlY2tlZCwgYWRkIGEgYmx1ZSBiYWNrZ3JvdW5kICovXHJcbiAgLmNvbnRhaW5lciBpbnB1dDpjaGVja2VkIH4gLmNoZWNrbWFyayB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogIzIxOTZGMztcclxuICB9XHJcbiAgXHJcbiAgLyogQ3JlYXRlIHRoZSBjaGVja21hcmsvaW5kaWNhdG9yIChoaWRkZW4gd2hlbiBub3QgY2hlY2tlZCkgKi9cclxuICAuY2hlY2ttYXJrOmFmdGVyIHtcclxuICBjb250ZW50OiBcIlwiO1xyXG4gIHBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuICBkaXNwbGF5OiBub25lO1xyXG4gIH1cclxuICBcclxuICAvKiBTaG93IHRoZSBjaGVja21hcmsgd2hlbiBjaGVja2VkICovXHJcbiAgLmNvbnRhaW5lciBpbnB1dDpjaGVja2VkIH4gLmNoZWNrbWFyazphZnRlciB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbiAgfVxyXG4gIFxyXG4gIC8qIFN0eWxlIHRoZSBjaGVja21hcmsvaW5kaWNhdG9yICovXHJcbiAgLmNvbnRhaW5lciAuY2hlY2ttYXJrOmFmdGVyIHtcclxuICBsZWZ0OiA5cHg7XHJcbiAgdG9wOiA1cHg7XHJcbiAgd2lkdGg6IDVweDtcclxuICBoZWlnaHQ6IDEwcHg7XHJcbiAgYm9yZGVyOiBzb2xpZCB3aGl0ZTtcclxuICBib3JkZXItd2lkdGg6IDAgM3B4IDNweCAwO1xyXG4gIC13ZWJraXQtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG4gIC1tcy10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XHJcbiAgdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG4gIH1cclxuICBcclxuICAudGl0bGUtZGl2LWNlbnRhciB7XHJcbiAgLy9jb2xvcjogY2FkZXRibHVlO1xyXG4gIGZvbnQtc2l6ZTogMXJlbTtcclxuICBwYWRkaW5nLXRvcDogMXJlbTtcclxuICBwYWRkaW5nLWJvdHRvbTogMXJlbTtcclxuICAvL2JvcmRlci1ib3R0b206IDFweCBkYXNoZWQgbGlnaHRibHVlO1xyXG4gIH1cclxuICBcclxuICAudGl0bGUtZGl2IHtcclxuICBjb2xvcjogY2FkZXRibHVlO1xyXG4gIGZvbnQtc3R5bGU6IDFyZW07XHJcbiAgYm9yZGVyLWJvdHRvbTogMXB4IGRhc2hlZCBsaWdodGJsdWU7XHJcbiAgfVxyXG4gIFxyXG4gIC50aXRsZS1kaXYtdGFibGUge1xyXG4gIGZvbnQtc2l6ZTogMXJlbTsgXHJcbiAgY29sb3I6IGNhZGV0Ymx1ZTtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxuICB9XHJcbiAgXHJcbiAgLnRpdGxlLWRpdi0yIHtcclxuICBmb250LXNpemU6IDEuMnJlbTsgXHJcbiAgY29sb3I6IGNhZGV0Ymx1ZTtcclxuICAvLyB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxuICB9XHJcbiAgXHJcbiAgaW5wdXQge1xyXG4gIC8vYm9yZGVyOiBub25lO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IFx0I2MzZTRlNTtcclxuICBib3JkZXI6IDFweCBzb2xpZCBcdCNjM2U0ZTUgIWltcG9ydGFudDtcclxuICB9XHJcbiAgXHJcbiAgLnRleHQtYWxpZ24tcmlnaHQge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDJweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICAudGV4dC1hbGlnbi1jZW50ZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBcclxuICB9XHJcbiAgXHJcbiAgLmZsZXgtbWluLXdpZHRoIHtcclxuICBtaW4td2lkdGg6IDQ3MHB4O1xyXG4gIC8vICBtYXgtd2lkdGg6IDYwMHB4O1xyXG4gIC8vei1pbmRleDogMTAwMDtcclxuICB9XHJcbiAgXHJcbiAgLmZsZXgtbWluLXdpZHRoICoge1xyXG4gIC8vei1pbmRleDogMTAwMDtcclxuICB9XHJcbiAgXHJcbiAgLm1heC13aWR0aC03MHBvc3RvIHtcclxuICBtYXgtd2lkdGg6IDcwJTtcclxuICB9XHJcbiAgLm92ZXItYXV0byB7XHJcbiAgb3ZlcmZsb3c6IHZpc2libGU7XHJcbiAgfVxyXG4gIFxyXG4gIC5jaGFydC13cmFwcGVyIHtcclxuICAvL2JvcmRlci10b3Atc3R5bGU6IGdyb292ZSAhaW1wb3J0YW50O1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Ymx1ZSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICAubXctMzUwIHtcclxuICBtaW4td2lkdGg6IDI4MHB4O1xyXG4gIGhlaWdodDogZml0LWNvbnRlbnQ7XHJcbiAgfVxyXG4gIFxyXG4gIC5tdy0zMHBvc3RvIHtcclxuICBtaW4td2lkdGg6IDM3MHB4O1xyXG4gIGhlaWdodDogZml0LWNvbnRlbnQ7XHJcbiAgLy9mb250LXNpemU6IDAuNXJlbSAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICBcclxuICAubXctNDAwIHtcclxuICBtaW4td2lkdGg6IDI4MHB4O1xyXG4gIH1cclxuICBcclxuICAudGJsLTEwIHtcclxuICAvLyBtYXJnaW46IDEwcHggIWltcG9ydGFudDtcclxuICBtaW4td2lkdGg6IDMwMHB4ICFpbXBvcnRhbnQ7XHJcbiAgfVxyXG4gIC50YmwtOGtvbG9uYSB7XHJcbiAgLy8gbWFyZ2luOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgbWluLXdpZHRoOiAzMDBweCAhaW1wb3J0YW50O1xyXG4gIH1cclxuICBcclxuICBcclxuICAudGQtMXtcclxuICBwYWRkaW5nOiAwLjRyZW0gIWltcG9ydGFudDtcclxuICB9XHJcbiAgIl19 */"

/***/ }),

/***/ "./src/app/views/quasi-newton/quasi-newton.component.ts":
/*!**************************************************************!*\
  !*** ./src/app/views/quasi-newton/quasi-newton.component.ts ***!
  \**************************************************************/
/*! exports provided: QuasiNewtonComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuasiNewtonComponent", function() { return QuasiNewtonComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @coreui/coreui-plugin-chartjs-custom-tooltips */ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_quasi_newton_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/quasi_newton.service */ "./src/app/service/quasi_newton.service.ts");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");







var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].server.host;
var QuasiNewtonComponent = /** @class */ (function () {
    function QuasiNewtonComponent(fb, quasiNewtonService) {
        this.fb = fb;
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            s: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_plus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_guess: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_guess: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_plus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_guess: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_guess: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_plus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_guess: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_guess: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_plus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_guess: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_guess: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            actin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            type_of_simulation: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            type_of_simulation_final_myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.inputData = this.profileForm;
        this.dugmeRun = false;
        this.isVisible = true;
        this.run_button = "RUN";
        this.fileToUpload = null;
        this.numberOfGraphs = 2;
        this.graphs = [];
        this.atModelGraph = [];
        this.errorGraph = [];
    }
    ;
    QuasiNewtonComponent.prototype.ngOnInit = function () { };
    QuasiNewtonComponent.prototype.ngAfterViewInit = function () { };
    //Graph setup
    QuasiNewtonComponent.prototype.initGraph = function (newTitle, newLabels, newColors, numberOfLines) {
        var basicGraph = {};
        basicGraph.options = {
            title: {
                display: true,
                text: 'Error',
                // fontColor: 'blue',
                fontStyle: 'small-caps',
            },
            tooltips: {
                enabled: false,
                custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                intersect: true,
                mode: 'index',
                position: 'nearest',
                callbacks: {
                    labelColor: function (tooltipItem, chart) {
                        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                    }
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 15,
                    top: 0,
                    bottom: 5
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        type: 'linear',
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            padding: 5,
                            beginAtZero: false,
                            stepSize: numberOfLines === 2 ? 1000 : 1,
                            fontSize: 12,
                            min: 1
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC1 (%)',
                            fontSize: 11
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            beginAtZero: true,
                            padding: 5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC2 (%)',
                            fontSize: 11
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: true,
                position: 'right'
            }
        };
        var newOptions = basicGraph.options;
        newOptions.title.text = newTitle;
        newOptions.scales.xAxes[0].type = newLabels.xAxes.type;
        newOptions.scales.xAxes[0].scaleLabel.labelString = newLabels.xAxes.title;
        newOptions.scales.yAxes[0].type = newLabels.yAxes.type;
        newOptions.scales.yAxes[0].scaleLabel.display = newLabels.yAxes.displayTitle;
        newOptions.scales.yAxes[0].scaleLabel.labelString = newLabels.yAxes.title;
        basicGraph.options = newOptions;
        basicGraph.colors = [];
        basicGraph.datasets = [];
        for (var i = 0; i < numberOfLines; i++) {
            basicGraph.datasets.push({ data: [] });
            basicGraph.colors.push(newColors[i]);
        }
        basicGraph.chartType = 'line';
        basicGraph.labels = [];
        basicGraph.legend = false;
        basicGraph.visibility = true;
        return basicGraph;
    };
    QuasiNewtonComponent.prototype.prepareGraphs = function (graphLines) {
        var titles = [
            'Experiment VS Model',
            'Error & Parameters convergence'
        ];
        var labels = [
            { xAxes: { type: 'linear', title: 'Time [s]' }, yAxes: { type: 'linear', displayTitle: true, title: 'Fractional Fluorescence' } },
            { xAxes: { type: 'linear', title: 'Iteration' }, yAxes: { type: 'logarithmic', displayTitle: false, title: '' } }
        ];
        var colors = [
            [
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'yellow', backgroundColor: 'transparent' }
            ],
            [
                { borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' },
                { borderColor: 'yellow', backgroundColor: 'transparent' },
                { borderColor: 'oragne', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'black', backgroundColor: 'transparent' },
                { borderColor: 'brown', backgroundColor: 'transparent' },
            ]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i], graphLines[i]));
        }
    };
    QuasiNewtonComponent.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
    };
    QuasiNewtonComponent.prototype.onCheckboxChange = function (box1, box2) {
        if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
            if (this.inputData[box1])
                this.inputData[box2] = false;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__["BaseChartDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
    ], QuasiNewtonComponent.prototype, "charts", void 0);
    QuasiNewtonComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-quasi-newton',
            template: __webpack_require__(/*! ./quasi-newton.component.html */ "./src/app/views/quasi-newton/quasi-newton.component.html"),
            styles: [__webpack_require__(/*! ./quasi-newton.component.scss */ "./src/app/views/quasi-newton/quasi-newton.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _service_quasi_newton_service__WEBPACK_IMPORTED_MODULE_4__["QuasiNewtonService"]])
    ], QuasiNewtonComponent);
    return QuasiNewtonComponent;
}());



/***/ }),

/***/ "./src/app/views/quasi-newton/quasi-newton.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/views/quasi-newton/quasi-newton.module.ts ***!
  \***********************************************************/
/*! exports provided: QuasiNewtonModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "QuasiNewtonModule", function() { return QuasiNewtonModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _quasi_newton_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./quasi-newton.component */ "./src/app/views/quasi-newton/quasi-newton.component.ts");
/* harmony import */ var _quasi_newton_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./quasi-newton-routing.module */ "./src/app/views/quasi-newton/quasi-newton-routing.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");












var QuasiNewtonModule = /** @class */ (function () {
    function QuasiNewtonModule() {
    }
    QuasiNewtonModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _quasi_newton_routing_module__WEBPACK_IMPORTED_MODULE_8__["QuasiNewtonRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(),
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ],
            declarations: [_quasi_newton_component__WEBPACK_IMPORTED_MODULE_7__["QuasiNewtonComponent"]],
            exports: [
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ]
        })
    ], QuasiNewtonModule);
    return QuasiNewtonModule;
}());



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    server: {
        host: 'http://localhost:3000/api/',
    }
};


/***/ })

}]);
//# sourceMappingURL=views-quasi-newton-quasi-newton-module.js.map