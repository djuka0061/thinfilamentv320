VERSION 5.00
Begin VB.Form Readme_Form 
   Caption         =   "Read Me"
   ClientHeight    =   6285
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   8670
   LinkTopic       =   "Form1"
   ScaleHeight     =   6285
   ScaleWidth      =   8670
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Close 
      Caption         =   "Close"
      Height          =   375
      Left            =   3240
      TabIndex        =   1
      Top             =   5760
      Width           =   1455
   End
   Begin VB.TextBox Text1 
      Height          =   5295
      Left            =   240
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   240
      Width           =   8175
   End
End
Attribute VB_Name = "Readme_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Close_Click()
    Readme_Form.Hide
End Sub

Private Sub Form_Load()
    
    Dim File_name As String
    'Dim fso As New FileSystemObject
    'Dim txtfile As File
    'Dim ts As TextStream
    
    ChDrive App.path
    ChDir App.path
    
    Wrap$ = Chr$(13) + Chr$(10)   'create wrap characters
    File_name = App.path & "\README.tex"
    
    Open File_name For Input As #1
    Do Until EOF(1)
        Line Input #1, LineOfText$
        AllText$ = AllText$ & LineOfText & Wrap$
    Loop
    Close #1
    
    'txtfile = fso.GetFile(File_name)
    'ts = txtfile.OpenAsTextStream(ForReading)
    'Text1.Text = ts.ReadAll
    'ts.Close
    
    Text1.Text = AllText$
    
        
        
End Sub
