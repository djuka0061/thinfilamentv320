import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HillStochasticalComponent } from './hill-stochastical.component';

describe('HillStochasticalComponent', () => {
  let component: HillStochasticalComponent;
  let fixture: ComponentFixture<HillStochasticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HillStochasticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HillStochasticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
