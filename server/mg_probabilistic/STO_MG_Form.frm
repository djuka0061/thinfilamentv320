VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form STO_MG_Form 
   Caption         =   "MG Model Simulation Stochatical "
   ClientHeight    =   9900
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9900
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Run_Parallel 
      Caption         =   "Run Parallel"
      Height          =   435
      Left            =   8280
      TabIndex        =   42
      Top             =   7920
      Width           =   1455
   End
   Begin VB.CommandButton browse 
      Caption         =   "Browse"
      Height          =   495
      Left            =   6240
      TabIndex        =   41
      Top             =   1440
      Width           =   855
   End
   Begin VB.TextBox Text17 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   8520
      TabIndex        =   39
      Text            =   "1.0"
      Top             =   1440
      Width           =   855
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   8760
      TabIndex        =   35
      Text            =   "5"
      Top             =   2040
      Width           =   615
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   5880
      TabIndex        =   34
      Text            =   "100"
      Top             =   2040
      Width           =   1095
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   3360
      TabIndex        =   33
      Text            =   "100"
      Top             =   2040
      Width           =   1095
   End
   Begin VB.CommandButton Histogram 
      Caption         =   "Histogram"
      Height          =   435
      Left            =   6600
      TabIndex        =   32
      Top             =   8520
      Width           =   1485
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   1680
      TabIndex        =   31
      Text            =   "Text1"
      Top             =   1440
      Width           =   4455
   End
   Begin VB.CommandButton Return_comm 
      Caption         =   "Return Main"
      Height          =   435
      Left            =   8280
      TabIndex        =   15
      Top             =   8520
      Width           =   1485
   End
   Begin VB.TextBox Text10 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   6
      Text            =   "20"
      Top             =   5100
      Width           =   1485
   End
   Begin VB.TextBox Text12 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   8
      Text            =   "20"
      Top             =   5940
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type of Simulation"
      Height          =   645
      Left            =   3120
      TabIndex        =   27
      Top             =   6675
      Width           =   3270
      Begin VB.OptionButton Titration_Option 
         Caption         =   "Titration"
         Height          =   225
         Left            =   1995
         TabIndex        =   11
         Top             =   315
         Width           =   960
      End
      Begin VB.OptionButton Time_Option 
         Caption         =   "Time Course"
         Height          =   225
         Left            =   525
         TabIndex        =   10
         Top             =   315
         Value           =   -1  'True
         Width           =   1275
      End
   End
   Begin VB.CommandButton Graphs_Button 
      Caption         =   "Graphs"
      Enabled         =   0   'False
      Height          =   435
      Left            =   6600
      TabIndex        =   14
      Top             =   9240
      Width           =   1485
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run Serial"
      Enabled         =   0   'False
      Height          =   435
      Left            =   6600
      TabIndex        =   16
      Top             =   7920
      Width           =   1485
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Text15 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   300
      Left            =   8085
      TabIndex        =   13
      Text            =   "0.25"
      Top             =   7410
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   12
      Text            =   "0.25"
      Top             =   7410
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   9
      Text            =   "0.05"
      Top             =   6360
      Width           =   1485
   End
   Begin VB.TextBox Text11 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   7
      Text            =   "3760"
      Top             =   5520
      Width           =   1485
   End
   Begin VB.TextBox Text9 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   5
      Text            =   "2e6"
      Top             =   4680
      Width           =   1485
   End
   Begin VB.TextBox Text8 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   4
      Text            =   "3000"
      Top             =   4260
      Width           =   1485
   End
   Begin VB.TextBox Text7 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   3
      Text            =   "0.013"
      Top             =   3840
      Width           =   1485
   End
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   2
      Text            =   "100"
      Top             =   3420
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   1
      Text            =   "0.3"
      Top             =   3000
      Width           =   1485
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   0
      Text            =   "5"
      Top             =   2580
      Width           =   1485
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "Cooperativity Factor:"
      Height          =   375
      Left            =   7320
      TabIndex        =   40
      Top             =   1440
      Width           =   1095
   End
   Begin VB.Label Label14 
      Alignment       =   1  'Right Justify
      Caption         =   "Negative Cooperativity Factor: "
      Height          =   735
      Left            =   7440
      TabIndex        =   38
      Top             =   1920
      Width           =   1215
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of TmTn Units:"
      Height          =   375
      Left            =   4680
      TabIndex        =   37
      Top             =   2040
      Width           =   1095
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Filaments:"
      Height          =   255
      Left            =   840
      TabIndex        =   36
      Top             =   2040
      Width           =   2415
   End
   Begin VB.Label Label2 
      Caption         =   "Stochastical MG Model Simulation"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   960
      TabIndex        =   30
      Top             =   720
      Width           =   7575
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant of Myosin Weak Binding (k1- [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   29
      Top             =   5205
      Width           =   5595
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Isomerization Rate Constant (k2- [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   28
      Top             =   6045
      Width           =   5595
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Myosin [uM]:"
      Height          =   285
      Left            =   0
      TabIndex        =   26
      Top             =   7515
      Width           =   6435
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Actin [uM]:"
      Height          =   285
      Left            =   840
      TabIndex        =   25
      Top             =   6360
      Width           =   5595
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Isomerization Rate Constant (k2+ [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   24
      Top             =   5625
      Width           =   5595
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Rate Constant of Myosin Weak Binding (k1+ [1/(M*s)]):"
      Height          =   285
      Left            =   840
      TabIndex        =   23
      Top             =   4785
      Width           =   5595
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant Beetwen the Closed and Open States (kt-):"
      Height          =   285
      Left            =   840
      TabIndex        =   22
      Top             =   4365
      Width           =   5595
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Equlibrium Constant Beetwen the Closed and Open States (Kt):"
      Height          =   285
      Left            =   840
      TabIndex        =   21
      Top             =   3945
      Width           =   5595
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant Beetwen the Blocked and Closed States (kb-):"
      Height          =   285
      Left            =   840
      TabIndex        =   20
      Top             =   3525
      Width           =   5595
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Equilibrium Constant Beetwen the Blocked and Closed States (Kb):"
      Height          =   285
      Left            =   840
      TabIndex        =   19
      Top             =   3105
      Width           =   5595
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   840
      TabIndex        =   18
      Top             =   2685
      Width           =   5595
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   525
      Left            =   720
      TabIndex        =   17
      Top             =   1440
      Width           =   795
   End
End
Attribute VB_Name = "STO_MG_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub browse_Click()

    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text1.Text = CommonDialog1.FileName
    ' Display name of selected file (without the path)
    'OutputFiletitle = CommonDialog1.FileTitle
End Sub

Private Sub Form_Load()
    Text1.Text = App.path & "\Simu_output"
    Run_Button.Enabled = True
    Graphs_Button.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub

Private Sub Graphs_Button_Click()
    

    'Graphs_Button.Enabled = False

    Graphs_Form.Show
   STO_MG_Form.Hide

ErrHandler:
    'User pressed the Cancel button

End Sub


Private Sub Histogram_Click()
    Histogram_Form.Show
    STO_MG_Form.Hide

End Sub

Private Sub Return_comm_Click()
    Main_Form.Show
    STO_MG_Form.Hide
End Sub

Private Sub Run_Button_Click()
    
    RunT (1)
    
End Sub



Private Sub Run_Parallel_Click()
RunT (2)
End Sub

Private Sub Time_Option_Click()
    If Time_Option.Value = True Then
        Text14.Text = 0.5
        Text15.Text = Text14.Text
        Text4.Text = 5
        Text15.Enabled = False
        Text15.Visible = False
        Label13.Caption = "Molar Concentration of Myosin [uM]:"
    End If
End Sub



Private Sub Titration_Option_Click()
    If Titration_Option.Value = True Then
        Text4.Text = 250
        Text14.Text = 0
        Text15.Enabled = True
        Text15.Visible = True
        Label13.Caption = "(For Titration) Molar Concentration of Myosin Initial / Final [uM / uM]:"
    End If
End Sub
Private Function RunT(b)
Dim FileNumber As Integer
    Dim Input_File_Name As String
    
    ChDrive App.path
    ChDir App.path

    On Error GoTo ErrHandler

    If Text1.Text <> "" Then

        If Time_Option.Value = True Then
            Text15.Text = Text14.Text
            Text15.Enabled = False
            Text15.Visible = False
            Label13.Caption = "Molar Concentration of Myosin [uM]:"
        End If

        
        Input_File_Name = "pain.dat"
        
        'Export data to a file
        FileNumber = FreeFile
        Open Input_File_Name For Output As #FileNumber
        Print #FileNumber, "#Output File Name:"
        Print #FileNumber, Text1.Text
        Print #FileNumber, "#Number of Tmtn units:"
        Print #FileNumber, Text3.Text
        Print #FileNumber, "#Negative Cooperativity Factor:"
        Print #FileNumber, Text16.Text
        Print #FileNumber, "#Numer of Filaments:"
        Print #FileNumber, Text2.Text
        Print #FileNumber, "#Total Time [s]:"
        Print #FileNumber, Text4.Text
        Print #FileNumber, "#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):"
        Print #FileNumber, Text5.Text
        Print #FileNumber, Text6.Text
        Print #FileNumber, "#The Rate Constants Beetwen the Closed and Open States (Kt / kt-):"
        Print #FileNumber, Text7.Text
        Print #FileNumber, Text8.Text
        Print #FileNumber, "#The Rate Constants of Myosin Weak Binding (k1+ / k1-):"
        Print #FileNumber, Text9.Text
        Print #FileNumber, Text10.Text
        Print #FileNumber, "#Isomerization Rate Constants (k2+ / k2-):"
        Print #FileNumber, Text11.Text
        Print #FileNumber, Text12.Text
        Print #FileNumber, "#Cooperativity Factor:"
        Print #FileNumber, Text17.Text
        Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text13.Text
        Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text15.Text
        Close #FileNumber

        STO_MG_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        Run_Parallel.Enabled = False
        Graphs_Button.Enabled = False
        
        ffname = "input_wrap"
        FileNumber = FreeFile
        Open ffname For Output As #FileNumber
            Print #FileNumber, "pain.dat"
            If b = 1 Then Print #FileNumber, "rigidchain.exe"
             If b = 2 Then Print #FileNumber, "'mpiexec -n 4 ParallelMGStochastic'"
            Print #FileNumber, "1"
        Close #FileNumber

        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        Call ExecCmd(App.path & "\wraper.exe " & Chr(34))
        'Call ExecCmd(App.path & "\rigidchain.exe " & Chr(34) & Input_File_Name & Chr(34))
        
        STO_MG_Form.MousePointer = vbArrow
        
        Run_Button.Enabled = True
        Run_Parallel.Enabled = True
        Graphs_Button.Enabled = True
       
    End If

    Exit Function

ErrHandler:
    'User pressed the Cancel button

End Function
