#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main() {
    FILE *f=fopen("Hillnum_output.dat","r+");
    FILE *g=fopen("Simu_output_destiled.dat","w+");
    char linija[200];
    double br;
    double a[100];
    int n,prvi,tek;
    if (fgets(linija, 200, f)!=NULL)
    {
        printf("------- %s  ----- \n",linija);
        n=0;
        prvi=1;
        int i;
        for(i=0;i<strlen(linija);i++) {
            if (('0'<=linija[i] && linija[i]<='9')||linija[i]=='.' || linija[i]=='\n'){
                if (prvi){
                    prvi=0;
                    n++;
                }
            }
            else prvi=1;
        }
    }
    fclose(f);
    f=fopen("Simu_output.dat","r+");
    tek=0;
    while (fscanf(f,"%Lf",&br)>0)
    {   tek++;
        fprintf(g,"\t %e",br);
        if (tek==n) {
            fprintf(g,"\n");
            tek=0;
        }
    }
    fclose(f);
    fclose(g);
}
