
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/x-www-form-urlencoded'
    })
}

@Injectable({
	providedIn: 'root'
})
export class ProbabilisticService {
    constructor(private httpClient: HttpClient) {
    }
    
    getConfig(): Observable<any> {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get<any>('');

    }

    public getData(json): Observable<any> {
        return this.httpClient.post<any>('http://localhost:3000/api/probabilistics/runSimulation', json, httpOptions)
    }
}