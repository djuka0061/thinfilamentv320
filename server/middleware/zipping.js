'use strict';

//Global include
const fs = require("fs");
const archiver = require('archiver');

module.exports.zipFile = (path, checked_fitings, inputData, outputDatas) => {
  let loadA_T, loadEee, loadKkk, loadRrr, loadModel, loadSensMatrix, loadEstimValue;
  let a_t, eee, kkk, rrr, model, estimated_values, sensitivity, outputData;
  let count = 1;

  //create a_t.csv
  loadA_T = fs.readFileSync('./' + path + '/new_a_t.txt');
  a_t = fs.createWriteStream('./' + path + '/a_t.csv');
  a_t.write('Time, Experiment \n');
  a_t.write(loadA_T);
  a_t.end('\n');

  //create eee.csv
  loadEee = fs.readFileSync('./' + path + '/new_eee.dat').toString().split(',');
  eee = fs.createWriteStream('./' + path + '/eee.csv');
  eee.write('Iteration,Error\n');
  loadEee.forEach(element => {
    if (element != undefined && element != null && element != '') {
      eee.write(count + ',' + element + '\n');
      count++;
    }
  })
  eee.end('\n');

  //create kkk.csv
  count = 1;
  loadKkk = fs.readFileSync('./' + path + '/new_kkk.dat').toString().split('\n');
  kkk = fs.createWriteStream('./' + path + '/kkk.csv');
  if (path === 'atphase') {
    kkk.write('Iteration, KA, KA-, KPi, KPi-, kT, kT-, kT*, kT*-, kT**, kT**-, kH, kH-, kAh, kAh-\n');
  } else if (path === 'mg_probabilistic' || path === 'monte_carlo') {
    kkk.write('Iteration, KB, KB-, KT, KT-, k1, k1-, k2, k2-\n');
  } else if (path === 'hill_monte_carlo') {
    kkk.write('Iteration, Alpha0, Beta0, Y1, Y2, K1, K1\', K2, K2\'\n');
  } else if (path === 'david') {
    kkk.write('Iteration, Tni, Gama\n');
  }

  loadKkk.forEach(element => {
    if (element != undefined && element != null && element != '') {
      kkk.write(count + ',' + element + '\n');
      count++;
    }
  })
  kkk.end('\n');

  //create model.csv
  loadModel = fs.readFileSync('./' + path + '/NEW_MODEL0001.dat');
  model = fs.createWriteStream('./' + path + '/model.csv');
  model.write('Time, Model \n');
  model.write(loadModel);
  model.end('\n');

  let fitings = 0;
  const parameters_name = [];
  const parameters_name_sensitivity = [];

  if (path === 'atphase') {
    if (checked_fitings[0]) {
      parameters_name.push('KA');
      parameters_name_sensitivity.push('KA');
      fitings++;
    }

    if (checked_fitings[1]) {
      parameters_name.push('KA-');
      parameters_name_sensitivity.push('KA-');
      fitings++;
    }

    if (checked_fitings[2]) {
      parameters_name.push('KPI');
      parameters_name_sensitivity.push('KPI');
      fitings++;
    }

    if (checked_fitings[3]) {
      parameters_name.push('KPI-');
      parameters_name_sensitivity.push('KPI-');
      fitings++;
    }

    if (checked_fitings[4]) {
      parameters_name.push('KT');
      parameters_name_sensitivity.push('KT');
      fitings++;
    }

    if (checked_fitings[5]) {
      parameters_name.push('KT-');
      parameters_name_sensitivity.push('KT-');
      fitings++;
    }

    if (checked_fitings[6]) {
      parameters_name.push('KT*');
      parameters_name_sensitivity.push('KT*');
      fitings++;
    }

    if (checked_fitings[7]) {
      parameters_name.push('KT*-');
      parameters_name_sensitivity.push('KT*-');
      fitings++;
    }

    if (checked_fitings[8]) {
      parameters_name.push('KT**');
      parameters_name_sensitivity.push('KT**');
      fitings++;
    }

    if (checked_fitings[9]) {
      parameters_name.push('Kt**-');
      parameters_name_sensitivity.push('Kt**-');
      fitings++;
    }

    if (checked_fitings[10]) {
      parameters_name.push('KH');
      parameters_name_sensitivity.push('KH');
      fitings++;
    }

    if (checked_fitings[11]) {
      parameters_name.push('KH-');
      parameters_name_sensitivity.push('KH-');
      fitings++;
    }

    if (checked_fitings[12]) {
      parameters_name.push('KAH');
      parameters_name_sensitivity.push('KAH');
      fitings++;
    }

    if (checked_fitings[13]) {
      parameters_name.push('KAH-');
      parameters_name_sensitivity.push('KAH-');
      fitings++;
    }
  } else if (path === 'mg_probabilistic' || path === 'monte_carlo') {
    if (checked_fitings[0]) {
      parameters_name.push('KB');
      parameters_name_sensitivity.push('KB');
      fitings++;
    }

    if (checked_fitings[1]) {
      parameters_name.push('KB-');
      parameters_name_sensitivity.push('KB-');
      fitings++;
    }

    if (checked_fitings[2]) {
      parameters_name.push('KT');
      parameters_name_sensitivity.push('KT');
      fitings++;
    }

    if (checked_fitings[3]) {
      parameters_name.push('KT-');
      parameters_name_sensitivity.push('KT-');
      fitings++;
    }

    if (checked_fitings[4]) {
      parameters_name.push('K1');
      parameters_name_sensitivity.push('K1');
      fitings++;
    }

    if (checked_fitings[5]) {
      parameters_name.push('K1-');
      parameters_name_sensitivity.push('K1-');
      fitings++;
    }

    if (checked_fitings[6]) {
      parameters_name.push('K2');
      parameters_name_sensitivity.push('K2');
      fitings++;
    }

    if (checked_fitings[7]) {
      parameters_name.push('K2-');
      parameters_name_sensitivity.push('K2-');
      fitings++;
    }
  } else if (path === 'hill_monte_carlo') {
    if (checked_fitings[0]) {
      parameters_name.push('Alpha0');
      parameters_name_sensitivity.push('Alpha0');
      fitings++;
    }

    if (checked_fitings[1]) {
      parameters_name.push('Beta0');
      parameters_name_sensitivity.push('Beta0');
      fitings++;
    }

    if (checked_fitings[2]) {
      parameters_name.push('Y1');
      parameters_name_sensitivity.push('Y1');
      fitings++;
    }

    if (checked_fitings[3]) {
      parameters_name.push('Y2');
      parameters_name_sensitivity.push('Y2');
      fitings++;
    }

    if (checked_fitings[4]) {
      parameters_name.push('K1');
      parameters_name_sensitivity.push('K1');
      fitings++;
    }

    if (checked_fitings[5]) {
      parameters_name.push('K1\'');
      parameters_name_sensitivity.push('K1\'');
      fitings++;
    }

    if (checked_fitings[6]) {
      parameters_name.push('K2');
      parameters_name_sensitivity.push('K2');
      fitings++;
    }

    if (checked_fitings[7]) {
      parameters_name.push('K2\'');
      parameters_name_sensitivity.push('K2\'');
      fitings++;
    }
  } else if (path === 'david') {
    if (checked_fitings[0]) {
      parameters_name.push('Tni');
      parameters_name_sensitivity.push('Tni');
      fitings++;
    }

    if (checked_fitings[1]) {
      parameters_name.push('Gama');
      parameters_name_sensitivity.push('Gama');
      fitings++;
    }
  }
  parameters_name.push('Error');

  //create estimated_value.csv
  count = 0;
  loadEstimValue = fs.readFileSync('./' + path + '/new_param_est.dat').toString().split('\n');
  estimated_values = fs.createWriteStream('./' + path + '/estimated_value.csv');
  estimated_values.write('#,Last,Average,Standard Deviation\n');
  loadEstimValue.forEach(element => {
    if (element != undefined && element != null && element != '') {
      estimated_values.write(parameters_name[count] + ',' + element + '\n');
      count++;
    }
  });
  estimated_values.end('\n');

  //create resolution_matrix.csv
  count = 0;
  let header = '#';
  parameters_name_sensitivity.forEach(element => {
    header += ',' + element;
  })
  loadSensMatrix = fs.readFileSync('./' + path + '/new_sensitivity.dat').toString().split('\n');
  sensitivity = fs.createWriteStream('./' + path + '/resolution_matrix.csv');
  sensitivity.write(header + '\n');
  loadSensMatrix.forEach(element => {
    if (element != undefined && element != null && element != '') {
      sensitivity.write(parameters_name_sensitivity[count] + ',' + element + '\n');
      count++;
    }
  })
  sensitivity.end('\n');

  outputData = JSON.stringify({ parameters: inputData, estimatedParams: outputDatas })
  fs.writeFileSync('./' + path + '/outputData.json', outputData);

  const output = fs.createWriteStream('./' + path + '/output_files.zip');
  let archive = archiver('zip');

  output.on('close', function () {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
  });

  output.on('end', function () {
    console.log('Data has been drained');
  });

  archive.on('error', function (err) {
    throw err;
  });

  archive.pipe(output);

  archive.file('./' + path + '/a_t.csv', { name: 'experiment.csv' });
  archive.file('./' + path + '/eee.csv', { name: 'error.csv' });
  archive.file('./' + path + '/kkk.csv', { name: 'estimation_by_iteration.csv' });
  archive.file('./' + path + '/model.csv', { name: 'model.csv' });
  archive.file('./' + path + '/estimated_value.csv', { name: 'estimated_value.csv' });
  archive.file('./' + path + '/resolution_matrix.csv', { name: 'resolution_matrix.csv' });
  archive.file('./' + path + '/outputData.json', { name: 'outputData.json' });

  archive.finalize();
}

module.exports.zippingFile = (path, outputName, histogramName) => {
  let simuOut;
  let simulation;

  //create simuOut.csv
  simuOut = fs.readFileSync('./' + path + '/' + outputName + '.dat').toString().split('\n');
  simulation = fs.createWriteStream('./' + path + '/' + outputName + '.csv');
  simulation.write('time, fractional fluorescence, fraction saturation, fraction of unbound actin, free myosin concentration\n')
  simuOut.forEach(dat => {
    let splitData;
    if (path === 'hill_stochastical' || path === 'hill_probabilistic') {
      splitData = dat.split('     ');
    } else {
      splitData = dat.split('\t');
    }

    if (splitData[1] !== undefined && splitData[2] !== undefined && splitData[3] !== undefined && splitData[4] !== undefined && splitData[5] !== undefined) {
      simulation.write(parseFloat(splitData[1].replace(/\s/g, '')) + ',' + parseFloat(splitData[2].replace(/\s/g, '')) + ',' + parseFloat(splitData[3].replace(/\s/g, '')) + ',' + parseFloat(splitData[4].replace(/\s/g, '')) + ',' + parseFloat(splitData[5].replace(/\s/g, '')) + '\n')
    }
  });
  simulation.end('\n');

  const output = fs.createWriteStream('./' + path + '/output_files.zip');
  let archive = archiver('zip');

  output.on('close', function () {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
  });

  output.on('end', function () {
    console.log('Data has been drained');
  });

  archive.on('error', function (err) {
    throw err;
  });

  archive.pipe(output);

  archive.file('./' + path + '/' + outputName + '.csv', { name: outputName + '.csv' });

  archive.finalize();
}

module.exports.zipFileSimanQuasi = (path, checked_fitings) => {
  let loadA_T, loadEstimValue;
  let a_t, estimated_values;
  let count = 1;

  //create a_t.csv
  loadA_T = fs.readFileSync('./' + path + '/new_a_t.txt');
  a_t = fs.createWriteStream('./' + path + '/a_t.csv');
  a_t.write('Time, Experiment \n');
  a_t.write(loadA_T);
  a_t.end('\n');

  let fitings = 0;
  const parameters_name = [];

  if (path === 'simann') {
    if (checked_fitings[0]) {
      parameters_name.push('KB');
      fitings++;
    }

    if (checked_fitings[1]) {
      parameters_name.push('KB-');
      fitings++;
    }

    if (checked_fitings[2]) {
      parameters_name.push('KT');
      fitings++;
    }

    if (checked_fitings[3]) {
      parameters_name.push('KT-');
      fitings++;
    }

    if (checked_fitings[4]) {
      parameters_name.push('K1');
      fitings++;
    }

    if (checked_fitings[5]) {
      parameters_name.push('K1-');
      fitings++;
    }

    if (checked_fitings[6]) {
      parameters_name.push('K2');
      fitings++;
    }

    if (checked_fitings[7]) {
      parameters_name.push('K2-');
      fitings++;
    }
  } else if (path === 'quasi_newton') {
    if (checked_fitings[0]) {
      parameters_name.push('KB');
      fitings++;
    }

    if (checked_fitings[1]) {
      parameters_name.push('KB-');
      fitings++;
    }

    if (checked_fitings[2]) {
      parameters_name.push('KT');
      fitings++;
    }

    if (checked_fitings[3]) {
      parameters_name.push('KT-');
      fitings++;
    }

    if (checked_fitings[4]) {
      parameters_name.push('K1');
      fitings++;
    }

    if (checked_fitings[5]) {
      parameters_name.push('K1-');
      fitings++;
    }

    if (checked_fitings[6]) {
      parameters_name.push('K2');
      fitings++;
    }

    if (checked_fitings[7]) {
      parameters_name.push('K2-');
      fitings++;
    }
  }
  parameters_name.push('Error');

  //create estimated_value.csv
  count = 0;
  loadEstimValue = fs.readFileSync('./' + path + '/new_param_est.dat').toString().split('\n');
  estimated_values = fs.createWriteStream('./' + path + '/estimated_value.csv');
  estimated_values.write('#, Parameters Estimation\n');
  loadEstimValue.forEach(element => {
    if (element != undefined && element != null && element != '') {
      estimated_values.write(parameters_name[count] + ',' + element + '\n');
      count++;
    }
  });
  estimated_values.end('\n');

  const output = fs.createWriteStream('./' + path + '/output_files.zip');
  let archive = archiver('zip');

  output.on('close', function () {
    console.log(archive.pointer() + ' total bytes');
    console.log('archiver has been finalized and the output file descriptor has closed.');
  });

  output.on('end', function () {
    console.log('Data has been drained');
  });

  archive.on('error', function (err) {
    throw err;
  });

  archive.pipe(output);

  archive.file('./' + path + '/a_t.csv', { name: 'experiment.csv' });
  archive.file('./' + path + '/estimated_value.csv', { name: 'estimated_value.csv' });

  archive.finalize();
}
