import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CommonModule } from "@angular/common";

import { WidgetsComponent } from './widgets.component';
import { WidgetsRoutingModule } from './widgets-routing.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmationDialogComponent } from '../confirmation-dialog/confirmation-dialog.component';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    WidgetsRoutingModule,
    ChartsModule,
    BsDropdownModule,
    MatTableModule,
    MatPaginatorModule,
    NgbModule
    ],
  declarations: [ WidgetsComponent, ConfirmationDialogComponent ],
  exports: [
    MatTableModule,
    MatPaginatorModule
  ],
  entryComponents: [ ConfirmationDialogComponent ],
  providers: [ ConfirmationDialogService ],
})
export class WidgetsModule { }
