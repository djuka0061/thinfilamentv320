'use strict';

module.exports.configuration = () => {
    const configuration = {
        loopback: {
            datasources: {
                db: {
                    name: 'db',
                    connector: 'memory'
                },
                mongodb: {
                    host: process.env.MONGO_DB_HOST || 'mongodb-5387-0.cloudclusters.net:10015',
                    port: process.env.MONGO_DB_PORT || 10015,
                    url: process.env.MONGO_DB_URL || 'mongodb://test:test@mongodb-5387-0.cloudclusters.net:10015/ThinFilamenV320?authSource=admin',
                    database: process.env.MONGO_DB_DATABASE || 'ThinFilamenV320',
                    user: process.env.MONGO_DB_USER || 'test',
                    password: process.env.MONGO_DB_PASSWORD || 'test',
                    name: process.env.MONGO_DB_NAME || 'ThinFilamenV320',
                    connector: process.env.MONGO_DB_HOST || 'mongodb'
                }
            },
            config: {
                restApiRoot: process.env.REST_API_ROOT || '/api',
                host: process.env.HOST || '0.0.0.0',
                port: process.env.PORT || 3000,
                remoting: {
                    context: false,
                    rest: {
                        handleErrors: false,
                        normalizeHttpPath: false,
                        xml: false
                    },
                    json: {
                        strict: false,
                        limit: "1000kb"
                    },
                    urlencoded: {
                        extended: true,
                        limit: "1000kb"
                    },
                    cors: false
                }
            }
        }
    }

    return configuration;
}