
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod'

const host = environment.server.host;

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

const headers: HttpHeaders = new HttpHeaders();
headers.set('enctype', 'multipart/form-data');


@Injectable({
  providedIn: 'root'
})
export class QuasiNewtonService {
  constructor(private httpClient: HttpClient) {
  }

  getConfig(): Observable<any> {
    // tslint:disable-next-line:max-line-length
    return this.httpClient.get<any>('');

  }

  public getData(json): Observable<any> {
    return this.httpClient.post<any>(host + 'quasi_newtons/users/test21/runSimulation', json, httpOptions)
  }

  public getInputData(userID): Observable<any> {
    return this.httpClient.get<any>(host + 'quasi_newtons/users/test21/returnInputData')
  }

  public download(userID): Observable<any> {
    return this.httpClient.get<any>(host + 'quasi_newtons/users/test21/download')
  }

  public upload(fileToUpload: File): Observable<any> {
    console.log(fileToUpload)
    let formData: FormData = new FormData();
    formData.append('file', fileToUpload, fileToUpload.name);
    console.log(formData.getAll('file'))
    return this.httpClient.post<any>(host + 'quasi_newtons/users/test21/upload', formData, { headers: headers })
  }

  public saveSimulation(body): Observable<any> {
    return this.httpClient.post<any>(host + 'quasi_newtons/users/test21/saveData', body, httpOptions)
  }

  public openSaveSimulation(userID): Observable<any> {
    return this.httpClient.get<any>(host + 'quasi_newtons/users/test21/returnSaveData')
  }

  public resetSimulation(userID): Observable<any> {
    return this.httpClient.get<any>(host + 'quasi_newtons/users/test21/download')
  }

  public saveDefaultValue(userID): Observable<any> {
    return this.httpClient.get<any>(host + 'quasi_newtons/users/test21/download')
  }
}