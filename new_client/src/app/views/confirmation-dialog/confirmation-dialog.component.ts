import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
})
export class ConfirmationDialogComponent implements OnInit {
  confirmForm = new FormGroup({
    myTextarea: new FormControl('')
  })

  @Input() title: string;
  @Input() message: string;
  @Input() btnOkText: string;
  @Input() btnCancelText: string;

  constructor(private activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

  public decline() {
    this.activeModal.close({ answer: false, description: this.confirmForm.value.myTextarea });
  }

  public accept() {
    this.activeModal.close({ answer: true, description: this.confirmForm.value.myTextarea });
  }

  public dismiss() {
    this.activeModal.dismiss();
  }

}
