VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Para_MGSTO_Form 
   Caption         =   "Para_Estim MG STO Thin Filament Regulation"
   ClientHeight    =   9795
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9795
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Run_Parallel 
      Caption         =   "Run Parallel"
      Height          =   435
      Left            =   2760
      TabIndex        =   113
      Top             =   8040
      Width           =   1575
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   39
      Left            =   9360
      TabIndex        =   112
      Text            =   "20"
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   38
      Left            =   9360
      TabIndex        =   111
      Text            =   "100"
      Top             =   4320
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   37
      Left            =   9360
      TabIndex        =   110
      Text            =   "100"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   36
      Left            =   9360
      TabIndex        =   109
      Text            =   "6000"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   9
      Left            =   3960
      TabIndex        =   108
      Top             =   4680
      Width           =   375
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   9
      Left            =   2160
      TabIndex        =   107
      Text            =   "5"
      Top             =   4680
      Width           =   1485
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   35
      Left            =   9360
      TabIndex        =   106
      Text            =   "200"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   34
      Left            =   9360
      TabIndex        =   105
      Text            =   "5.0e6"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   33
      Left            =   9360
      TabIndex        =   104
      Text            =   "6000"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   8
      Left            =   3960
      TabIndex        =   103
      Top             =   4320
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   8
      Left            =   2160
      TabIndex        =   102
      Text            =   "1"
      Top             =   4320
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   0
      Left            =   2190
      TabIndex        =   101
      Text            =   "0.3"
      Top             =   1080
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   1
      Left            =   2160
      TabIndex        =   100
      Text            =   "100"
      Top             =   1440
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   2
      Left            =   2190
      TabIndex        =   99
      Text            =   "0.013"
      Top             =   1920
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   3
      Left            =   2160
      TabIndex        =   98
      Text            =   "3000"
      Top             =   2280
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   4
      Left            =   2190
      TabIndex        =   97
      Text            =   "2.0e6"
      Top             =   2760
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   5
      Left            =   2160
      TabIndex        =   96
      Text            =   "10"
      Top             =   3120
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   6
      Left            =   2190
      TabIndex        =   95
      Text            =   "1000"
      Top             =   3600
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   7
      Left            =   2160
      TabIndex        =   94
      Text            =   "5"
      Top             =   3960
      Width           =   1485
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   32
      Left            =   9360
      TabIndex        =   88
      Text            =   "0.5"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   4680
      TabIndex        =   87
      Text            =   "80"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   4680
      TabIndex        =   86
      Text            =   "0.02"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   4680
      TabIndex        =   85
      Text            =   "2500"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   4
      Left            =   4680
      TabIndex        =   84
      Text            =   "1.5e6"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   5
      Left            =   4680
      TabIndex        =   83
      Text            =   "10"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   6
      Left            =   4680
      TabIndex        =   82
      Text            =   "1000"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   7
      Left            =   4680
      TabIndex        =   81
      Text            =   "10"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   8
      Left            =   4680
      TabIndex        =   80
      Text            =   "6"
      Top             =   4320
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   9
      Left            =   4680
      TabIndex        =   79
      Text            =   "3"
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   10
      Left            =   6240
      TabIndex        =   78
      Text            =   "10"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   11
      Left            =   6240
      TabIndex        =   77
      Text            =   "150"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   12
      Left            =   6240
      TabIndex        =   76
      Text            =   "0.03"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   13
      Left            =   6240
      TabIndex        =   75
      Text            =   "3500"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   14
      Left            =   6240
      TabIndex        =   74
      Text            =   "3.0e6"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   15
      Left            =   6240
      TabIndex        =   73
      Text            =   "50"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   16
      Left            =   6240
      TabIndex        =   72
      Text            =   "4000"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   17
      Left            =   6240
      TabIndex        =   71
      Text            =   "30"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   18
      Left            =   6240
      TabIndex        =   70
      Text            =   "10"
      Top             =   4320
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   19
      Left            =   6240
      TabIndex        =   69
      Text            =   "8"
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   20
      Left            =   7800
      TabIndex        =   68
      Text            =   "1.0e-5"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   21
      Left            =   7800
      TabIndex        =   67
      Text            =   "50"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   22
      Left            =   7800
      TabIndex        =   66
      Text            =   "1.0e-5"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   23
      Left            =   7800
      TabIndex        =   65
      Text            =   "1000"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   24
      Left            =   7800
      TabIndex        =   64
      Text            =   "1.0e4"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   25
      Left            =   7800
      TabIndex        =   63
      Text            =   "1"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   26
      Left            =   7800
      TabIndex        =   62
      Text            =   "500"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   27
      Left            =   7800
      TabIndex        =   61
      Text            =   "5"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   28
      Left            =   7800
      TabIndex        =   60
      Text            =   "1"
      Top             =   4320
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   29
      Left            =   7800
      TabIndex        =   59
      Text            =   "2"
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   30
      Left            =   9360
      TabIndex        =   58
      Text            =   "200"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   31
      Left            =   9360
      TabIndex        =   57
      Text            =   "1000"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   0
      Left            =   3960
      TabIndex        =   56
      Top             =   1080
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   1
      Left            =   3960
      TabIndex        =   55
      Top             =   1440
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   2
      Left            =   3960
      TabIndex        =   54
      Top             =   1920
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   3
      Left            =   3960
      TabIndex        =   53
      Top             =   2280
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   4
      Left            =   3960
      TabIndex        =   52
      Top             =   2760
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   5
      Left            =   3960
      TabIndex        =   51
      Top             =   3120
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   6
      Left            =   3960
      TabIndex        =   50
      Top             =   3600
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   7
      Left            =   3960
      TabIndex        =   49
      Top             =   3960
      Width           =   375
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   360
      Top             =   7440
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton Command5 
      Caption         =   "..........."
      Height          =   375
      Left            =   10440
      TabIndex        =   48
      Top             =   7800
      Width           =   735
   End
   Begin VB.CommandButton Command4 
      Caption         =   "........."
      Height          =   375
      Left            =   10440
      TabIndex        =   47
      Top             =   7320
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Caption         =   "......."
      Height          =   375
      Left            =   10440
      TabIndex        =   46
      Top             =   6840
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Caption         =   "........"
      Height          =   375
      Left            =   10440
      TabIndex        =   45
      Top             =   6360
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Caption         =   "........"
      Height          =   375
      Left            =   10440
      TabIndex        =   44
      Top             =   5760
      Width           =   735
   End
   Begin VB.TextBox Text39 
      Height          =   375
      Left            =   6000
      TabIndex        =   42
      Text            =   "Text39"
      Top             =   7800
      Width           =   4335
   End
   Begin VB.TextBox Text38 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   4800
      TabIndex        =   40
      Text            =   "100"
      Top             =   5400
      Width           =   975
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   4680
      TabIndex        =   36
      Text            =   "6"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   2160
      TabIndex        =   35
      Text            =   "100"
      Top             =   5880
      Width           =   1575
   End
   Begin VB.CommandButton Display 
      Caption         =   "Show Results"
      Height          =   435
      Left            =   2760
      TabIndex        =   33
      Top             =   8760
      Width           =   1575
   End
   Begin VB.TextBox Text2 
      Height          =   300
      Left            =   6000
      TabIndex        =   29
      Text            =   "eee_4b.dat"
      Top             =   6840
      Width           =   4365
   End
   Begin VB.TextBox Text1 
      Height          =   300
      Left            =   6000
      TabIndex        =   28
      Text            =   "rrr_4b.dat"
      Top             =   7320
      Width           =   4365
   End
   Begin VB.TextBox Text30 
      Height          =   300
      Left            =   6000
      TabIndex        =   23
      Text            =   "a_t.txt"
      Top             =   5880
      Width           =   4365
   End
   Begin VB.TextBox Text29 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6840
      TabIndex        =   22
      Text            =   "0.15"
      Top             =   5400
      Width           =   915
   End
   Begin VB.TextBox Text20 
      Height          =   300
      Left            =   6000
      TabIndex        =   21
      Text            =   "kkk_4b.dat"
      Top             =   6360
      Width           =   4365
   End
   Begin VB.TextBox Text19 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   8880
      TabIndex        =   20
      Text            =   "10"
      Top             =   5400
      Width           =   1125
   End
   Begin VB.CommandButton Ret_comm 
      Caption         =   "Return to Main"
      Height          =   435
      Left            =   840
      TabIndex        =   6
      Top             =   8760
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type of Simulation"
      Height          =   645
      Left            =   360
      TabIndex        =   17
      Top             =   6360
      Width           =   3270
      Begin VB.OptionButton Titration_Option 
         Caption         =   "Titration"
         Height          =   225
         Left            =   1995
         TabIndex        =   3
         Top             =   315
         Width           =   960
      End
      Begin VB.OptionButton Time_Option 
         Caption         =   "Time Course"
         Height          =   225
         Left            =   525
         TabIndex        =   2
         Top             =   315
         Value           =   -1  'True
         Width           =   1275
      End
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run Serial"
      Height          =   435
      Left            =   840
      TabIndex        =   7
      Top             =   8040
      Width           =   1485
   End
   Begin VB.TextBox Text15 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   300
      Left            =   2760
      TabIndex        =   5
      Text            =   "0.25"
      Top             =   7440
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2760
      TabIndex        =   4
      Text            =   "5.0"
      Top             =   7050
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2190
      TabIndex        =   1
      Text            =   "0.5"
      Top             =   5400
      Width           =   1485
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2190
      TabIndex        =   0
      Text            =   "2"
      Top             =   660
      Width           =   1485
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Fitting"
      Height          =   255
      Left            =   3720
      TabIndex        =   93
      Top             =   720
      Width           =   615
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Guess1 of Param"
      Height          =   255
      Left            =   4560
      TabIndex        =   92
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Caption         =   "Guess2 of Param"
      Height          =   255
      Left            =   6120
      TabIndex        =   91
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Caption         =   "Lower Bound"
      Height          =   255
      Left            =   7680
      TabIndex        =   90
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Caption         =   "Upper Bound"
      Height          =   255
      Left            =   9360
      TabIndex        =   89
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label40 
      Alignment       =   1  'Right Justify
      Caption         =   "Parameter File:"
      Height          =   375
      Left            =   4680
      TabIndex        =   43
      Top             =   7800
      Width           =   1215
   End
   Begin VB.Label Label39 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of TmTn Units:"
      Height          =   615
      Left            =   3960
      TabIndex        =   41
      Top             =   5280
      Width           =   615
   End
   Begin VB.Label Label38 
      Alignment       =   1  'Right Justify
      Caption         =   "Cooperativity Factor:"
      Height          =   255
      Left            =   360
      TabIndex        =   39
      Top             =   4320
      Width           =   1695
   End
   Begin VB.Label Label37 
      Alignment       =   1  'Right Justify
      Caption         =   "Negative Coop Factor:"
      Height          =   255
      Left            =   240
      TabIndex        =   38
      Top             =   4680
      Width           =   1815
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Filaments:"
      Height          =   255
      Left            =   240
      TabIndex        =   37
      Top             =   5880
      Width           =   1815
   End
   Begin VB.Label Label16 
      Caption         =   "Parameter Estimation - MG Monte Carlo"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   480
      TabIndex        =   34
      Top             =   0
      Width           =   7575
   End
   Begin VB.Label Label36 
      Alignment       =   1  'Right Justify
      Caption         =   "[Myosin] in uM:"
      Height          =   285
      Left            =   1080
      TabIndex        =   32
      Top             =   7440
      Width           =   1635
   End
   Begin VB.Label Label35 
      Alignment       =   1  'Right Justify
      Caption         =   "Sensitivity Matrix File:"
      Height          =   285
      Left            =   4320
      TabIndex        =   31
      Top             =   7320
      Width           =   1575
   End
   Begin VB.Label Label34 
      Alignment       =   1  'Right Justify
      Caption         =   "Error File:"
      Height          =   285
      Left            =   4800
      TabIndex        =   30
      Top             =   6840
      Width           =   1035
   End
   Begin VB.Label Label26 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   285
      Left            =   4560
      TabIndex        =   27
      Top             =   6360
      Width           =   1395
   End
   Begin VB.Label Label25 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Iteration:"
      Height          =   525
      Left            =   7800
      TabIndex        =   26
      Top             =   5280
      Width           =   1035
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Experimentical Data File:"
      Height          =   525
      Left            =   4560
      TabIndex        =   25
      Top             =   5880
      Width           =   1395
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Epsilon:"
      Height          =   285
      Left            =   5760
      TabIndex        =   24
      Top             =   5400
      Width           =   1035
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "k1-:"
      Height          =   285
      Left            =   360
      TabIndex        =   19
      Top             =   3165
      Width           =   1635
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "k2- :"
      Height          =   285
      Left            =   360
      TabIndex        =   18
      Top             =   4005
      Width           =   1755
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "[Myosin] in uM:"
      Height          =   285
      Left            =   960
      TabIndex        =   16
      Top             =   7080
      Width           =   1635
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "[Actin] in uM:"
      Height          =   285
      Left            =   240
      TabIndex        =   15
      Top             =   5400
      Width           =   1875
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "k2+:"
      Height          =   285
      Left            =   360
      TabIndex        =   14
      Top             =   3585
      Width           =   1755
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "k1+    [1/(M*s)]:"
      Height          =   285
      Left            =   360
      TabIndex        =   13
      Top             =   2745
      Width           =   1635
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "kt-:"
      Height          =   285
      Left            =   360
      TabIndex        =   12
      Top             =   2400
      Width           =   1635
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Kt:"
      Height          =   285
      Left            =   360
      TabIndex        =   11
      Top             =   1920
      Width           =   1635
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "kb-:"
      Height          =   285
      Left            =   360
      TabIndex        =   10
      Top             =   1560
      Width           =   1635
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Kb:"
      Height          =   285
      Left            =   240
      TabIndex        =   9
      Top             =   1080
      Width           =   1755
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   360
      TabIndex        =   8
      Top             =   720
      Width           =   1635
   End
End
Attribute VB_Name = "Para_MGSTO_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False


Private Sub Check1_Click(Index As Integer)
    If (Index Mod 2) = 0 Then
        If Check1(Index).Value = vbChecked Then
            For i = 0 To 3
                Text16(i * 10 + Index).Visible = True
            Next i
            Check1(Index + 1).Value = vbUnchecked
        Else
            For i = 0 To 3
                Text16(i * 10 + Index).Visible = False
            Next i
        End If
    Else
        If Check1(Index).Value = vbChecked Then
            For i = 0 To 3
                Text16(i * 10 + Index).Visible = True
            Next i
            Check1(Index - 1).Value = vbUnchecked
        Else
            For i = 0 To 3
                Text16(i * 10 + Index).Visible = False
            Next i
        End If
    End If
    
    
End Sub

Private Sub Command1_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text30.Text = CommonDialog1.FileName
    
End Sub

Private Sub Command2_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text20.Text = CommonDialog1.FileName
End Sub

Private Sub Command3_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text2.Text = CommonDialog1.FileName
End Sub

Private Sub Command4_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text1.Text = CommonDialog1.FileName
End Sub

Private Sub Command5_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text39.Text = CommonDialog1.FileName
End Sub
Private Sub Display_Click()
    Para_MGSTO_Form.Hide
    MC_Display_Form.Show
    'Graphs_temp.Show
End Sub

Private Sub Form_Load()
    Label36.Visible = False
    Text30.Text = App.path & "\a_t.txt"
    Text20.Text = App.path & "\kkk.dat"
    Text2.Text = App.path & "\eee.dat"
    Text1.Text = App.path & "\rrr.dat"
    Text39.Text = App.path & "\param_est.dat"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub



Private Sub Ret_comm_Click()
    Main_Form.Show
    Para_MGSTO_Form.Hide
End Sub

Private Sub Run_Button_Click()

  RunT (1)

End Sub



Private Sub Run_Parallel_Click()

    RunT (2)
    
End Sub

Private Sub Time_Option_Click()
    If Time_Option.Value = True Then
        Text4.Text = 2
        Text14.Text = 0.5
        Text15.Text = Text14.Text
        Text15.Enabled = False
        Text15.Visible = False
        Label13.Caption = "[Myosin] in uM:"
        Label36.Visible = False
    End If
End Sub



Private Sub Titration_Option_Click()
    If Titration_Option.Value = True Then
        Text4.Text = 250
        Text14.Text = 0
        Text15.Enabled = True
        Text15.Visible = True
        Label36.Visible = True
        Label13.Caption = "Initial [Myosin] in uM:"
        Label36.Caption = "Final [Myosin] in uM:"
    End If
End Sub


Private Function RunT(b)
 Dim Input_File_Name As String
    Dim FileNumber As Integer
    Dim path As String
    Dim File_name As String
    Dim Paras(10), derror As Single
    Dim kavg(11), klast(11), kde(11) As Double
    Dim kkk(100, 10) As Double
    Dim r(100) As Double
    Dim sm(100, 10, 10) As Double
    Dim int_max, I_init, ip, jp, kp As Integer
    Dim position(10), real_vs, paramN As Integer
        
    ChDrive App.path
    ChDir App.path
    
    int_max = CInt(Text19.Text)
    paramN = 10   'number of params
    
    If (int_max > 100) Then
        int_max = 100
        Text19.Text = 100
    Else
       If (int_max < 0) Then
            int_max = 10
            Text19.Text = 10
        End If
    End If
        
    For i = 1 To 6
        position(i) = 1
    Next i
        
    FileNumber = 2
    On Error GoTo ErrHandler

        If Time_Option.Value = True Then
            Text15.Text = Text14.Text
            Text15.Enabled = False
            Text15.Visible = False
        End If

    kp = 0
    For i = 0 To paramN - 1
        If Check1(i).Value = vbUnchecked Then
            For j = 0 To 3
                Text16(j * 10 + i).Text = Text5(i).Text
            Next j
        Else
            kp = kp + 1
            position(kp) = i + 1
        End If
    
    Next i
    real_vs = kp

        Input_File_Name = App.path & "\tmtn-ca.dat"
        'Input_File_Name = "D:\newgui\tmtn+ca.dat"
        'Export data to a file
        Open Input_File_Name For Output As #FileNumber
        'Print #FileNumber, "#Output File Name:"
        'Print #FileNumber, "model"
        'Print #FileNumber, "#Total Time [s]:"
        Print #FileNumber, Text38.Text 'tmtn units
        Print #FileNumber, Text5(9).Text 'nmpt
        Print #FileNumber, Text3.Text '#filaments
        Print #FileNumber, Text4.Text 'total time
        'write out kb,kb-,kt,kt-,k1+,k1-,k2+,k2-,coopfactor
        For i = 0 To paramN - 2
            Print #FileNumber, Text5(i).Text
        Next i
        
        'Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text13.Text
        'Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text15.Text
        Print #FileNumber, "tmtn-ca.dat finished"
        Close #FileNumber

        Para_MGSTO_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        Run_Parallel.Enabled = False
        
        FileNumber = 3
        Input_File_Name = App.path & "\input_wrap"
        Open Input_File_Name For Output As #FileNumber
            Print #FileNumber, "10"
            For i = 0 To paramN - 1
                For j = 0 To 3
                    Print #FileNumber, Text16(j * 10 + i).Text
                Next j
            Next i
            
            
            Print #FileNumber, Text29.Text
            Print #FileNumber, Text19.Text
            
            Print #FileNumber, "'" & Text30.Text & "'"
            Print #FileNumber, "'" & Text20.Text & "'"
            Print #FileNumber, "'" & Text2.Text & "'"
            Print #FileNumber, "'" & Text1.Text & "'"
            If b = 1 Then Print #FileNumber, "'rigidchain'"
            If b = 2 Then Print #FileNumber, "'mpiexec -n 4 ParallelMGStochastic'"
            'JC's version num_rigid, my version is num_geeve
        Close #FileNumber
        
        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        'Call ExecCmd(App.path & "\ESTIMRE.exe input_estim" & Chr(34))
        Call ExecCmd("mpiexec -n 4 " & App.path & "\mc_pmestim.exe " & Chr(34))
         'Call ExecCmd(App.path & "\mc_pmestim.exe " & Chr(34))
        'Call ExecCmd("D:\newgui\paramestim.exe " & Chr(34))
        'Para_MGNUM_Form.Show
        
        
        
        FileNumber = FreeFile
        File_name = Text20.Text
        Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            For jp = 1 To paramN
                Input #FileNumber, kkk(ip, jp)
            Next jp
        Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = Text2.Text
        Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            Input #FileNumber, r(ip)
        Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = Text1.Text
        Open File_name For Input As #FileNumber
        For kp = 1 To int_max
            For ip = 1 To paramN
                For jp = 1 To paramN
                    Input #FileNumber, sm(kp, ip, jp)
                Next jp
            Next ip
        Next kp
        Close #FileNumber
        
        For ip = 1 To real_vs + 1
            kavg(ip) = 0
            kde(ip) = 0
        Next ip
    
        For ip = 1 To real_vs
            klast(ip) = kkk(int_max, position(ip))
        Next ip
        klast(real_vs + 1) = r(int_max)
        
        If (int_max > 10) Then
            I_init = int_max - 9
        Else
            I_init = 1
        End If
    
        For jp = 1 To real_vs
            For ip = I_init To int_max
                kavg(jp) = kavg(jp) + kkk(ip, position(jp))
            Next ip
        Next jp
    
        For ip = I_init To int_max
            kavg(real_vs + 1) = kavg(real_vs + 1) + r(ip)
        Next ip
        
        For ip = 1 To real_vs + 1
            kavg(ip) = kavg(ip) / (int_max - I_init + 1)
        Next ip
        
          
        For jp = 1 To real_vs
            For ip = I_init To int_max
                kde(jp) = kde(jp) + (kkk(ip, position(jp)) - kavg(jp)) _
                            * (kkk(ip, position(jp)) - kavg(jp))
            Next ip
        Next jp
        
        For ip = I_init To int_max
            kde(real_vs + 1) = kde(real_vs + 1) + (r(ip) - kavg(real_vs + 1)) _
                              * (r(ip) - kavg(real_vs + 1))
        Next ip
        
        For ip = 1 To real_vs + 1
            kde(ip) = Sqr(kde(ip) / (int_max - I_init + 1))
        Next ip
        
        FileNumber = 4
        Input_File_Name = Text39.Text
        Open Input_File_Name For Output As #FileNumber
        For ip = 1 To real_vs + 1
            Print #FileNumber, Format(klast(ip), "0.0000E+00"), _
            Format(kavg(ip), "0.0000E+00"), Format(kde(ip), "0.0000E+00")
        Next ip
        
        'sm only lists active (real) variables, here real_vs
        For jp = 1 To real_vs
            For ip = 1 To real_vs
                Print #FileNumber, Format(sm(int_max, ip, jp), "#0.########"),
            Next ip
            Print #FileNumber, " "
        Next jp
        
        Close #FileNumber
        
        
        FileNumber = FreeFile
        File_name = App.path & "\kb.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(1))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\kt.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(2))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\k1.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(3))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\k2.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(4))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\cf.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(5))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\er.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, r(ip)
            Next ip
        Close #FileNumber
       
        FileNumber = FreeFile
        File_name = App.path & "\ps.dat"
        Open File_name For Output As #FileNumber
            Print #FileNumber, real_vs
            For ip = 1 To real_vs
                Print #FileNumber, position(ip)
            Next ip
        Close #FileNumber
        
        Para_MGSTO_Form.MousePointer = vbArrow
        Run_Button.Enabled = True
        Run_Parallel.Enabled = True

    Exit Function

ErrHandler:
    'User pressed the Cancel button
Exit Function
End Function
