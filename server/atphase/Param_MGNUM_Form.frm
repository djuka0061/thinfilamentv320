VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Para_MGNUM_Form 
   Caption         =   "Para_Estim MG Probabilistic Thin Filament Regulation"
   ClientHeight    =   9390
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9390
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   7
      Left            =   1800
      TabIndex        =   94
      Text            =   "5"
      Top             =   3960
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   6
      Left            =   1830
      TabIndex        =   93
      Text            =   "1000"
      Top             =   3600
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   5
      Left            =   1800
      TabIndex        =   92
      Text            =   "10"
      Top             =   3120
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   4
      Left            =   1830
      TabIndex        =   91
      Text            =   "2.0e6"
      Top             =   2760
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   3
      Left            =   1800
      TabIndex        =   90
      Text            =   "3000"
      Top             =   2280
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   2
      Left            =   1830
      TabIndex        =   89
      Text            =   "0.013"
      Top             =   1920
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   1
      Left            =   1800
      TabIndex        =   88
      Text            =   "100"
      Top             =   1440
      Width           =   1485
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   7
      Left            =   3720
      TabIndex        =   87
      Top             =   3960
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   6
      Left            =   3720
      TabIndex        =   86
      Top             =   3600
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   5
      Left            =   3720
      TabIndex        =   85
      Top             =   3120
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   4
      Left            =   3720
      TabIndex        =   84
      Top             =   2760
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   3
      Left            =   3720
      TabIndex        =   83
      Top             =   2280
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   2
      Left            =   3720
      TabIndex        =   82
      Top             =   1920
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   1
      Left            =   3720
      TabIndex        =   81
      Top             =   1440
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   0
      Left            =   3720
      TabIndex        =   80
      Top             =   1080
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   31
      Left            =   9240
      TabIndex        =   79
      Text            =   "200"
      Top             =   3600
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   30
      Left            =   9120
      TabIndex        =   78
      Text            =   "5000"
      Top             =   4080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   29
      Left            =   9240
      TabIndex        =   77
      Text            =   "200"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   28
      Left            =   9240
      TabIndex        =   76
      Text            =   "5.0e6"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   27
      Left            =   9240
      TabIndex        =   75
      Text            =   "6000"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   26
      Left            =   9240
      TabIndex        =   74
      Text            =   "0.5"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   25
      Left            =   9240
      TabIndex        =   73
      Text            =   "1000"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   24
      Left            =   9240
      TabIndex        =   72
      Text            =   "200"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   23
      Left            =   7560
      TabIndex        =   71
      Text            =   "1"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   22
      Left            =   7560
      TabIndex        =   70
      Text            =   "800"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   21
      Left            =   7560
      TabIndex        =   69
      Text            =   "1"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   20
      Left            =   7560
      TabIndex        =   68
      Text            =   "1.0e4"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   19
      Left            =   7560
      TabIndex        =   67
      Text            =   "800"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   18
      Left            =   7560
      TabIndex        =   66
      Text            =   "0"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   17
      Left            =   7560
      TabIndex        =   65
      Text            =   "10"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   16
      Left            =   7560
      TabIndex        =   64
      Text            =   "0"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   15
      Left            =   6000
      TabIndex        =   63
      Text            =   "5"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   14
      Left            =   6000
      TabIndex        =   62
      Text            =   "4000"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   13
      Left            =   6000
      TabIndex        =   61
      Text            =   "5"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   12
      Left            =   6000
      TabIndex        =   60
      Text            =   "3.5e6"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   11
      Left            =   6000
      TabIndex        =   59
      Text            =   "1000"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   10
      Left            =   6000
      TabIndex        =   58
      Text            =   "0.2"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   9
      Left            =   6000
      TabIndex        =   57
      Text            =   "150"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   8
      Left            =   6000
      TabIndex        =   56
      Text            =   "6"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   7
      Left            =   4440
      TabIndex        =   55
      Text            =   "10"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   6
      Left            =   4440
      TabIndex        =   54
      Text            =   "1000"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   5
      Left            =   4440
      TabIndex        =   53
      Text            =   "10"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   4
      Left            =   4440
      TabIndex        =   52
      Text            =   "1.5e6"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   4440
      TabIndex        =   51
      Text            =   "2500"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   4440
      TabIndex        =   50
      Text            =   "0.3"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   4440
      TabIndex        =   49
      Text            =   "80"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   4440
      TabIndex        =   48
      Text            =   "10"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.CommandButton Command5 
      Caption         =   "..........."
      Height          =   375
      Left            =   10200
      TabIndex        =   42
      Top             =   6960
      Width           =   735
   End
   Begin VB.CommandButton Command4 
      Caption         =   "........."
      Height          =   375
      Left            =   10200
      TabIndex        =   41
      Top             =   6480
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Caption         =   "......."
      Height          =   375
      Left            =   10200
      TabIndex        =   40
      Top             =   6000
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Caption         =   "........"
      Height          =   375
      Left            =   10200
      TabIndex        =   39
      Top             =   5520
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Caption         =   "........"
      Height          =   375
      Left            =   10200
      TabIndex        =   38
      Top             =   5040
      Width           =   735
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1320
      Top             =   7920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Text3 
      Height          =   375
      Left            =   5880
      TabIndex        =   36
      Text            =   "para_est.dat"
      Top             =   6960
      Width           =   4215
   End
   Begin VB.CommandButton Display 
      Caption         =   "Show Results"
      Height          =   495
      Left            =   6240
      TabIndex        =   34
      Top             =   7560
      Width           =   1575
   End
   Begin VB.TextBox Text2 
      Height          =   300
      Left            =   5880
      TabIndex        =   30
      Text            =   "eee_4b.dat"
      Top             =   6000
      Width           =   4245
   End
   Begin VB.TextBox Text1 
      Height          =   300
      Left            =   5880
      TabIndex        =   29
      Text            =   "rrr_4b.dat"
      Top             =   6480
      Width           =   4245
   End
   Begin VB.TextBox Text30 
      Height          =   300
      Left            =   5880
      TabIndex        =   24
      Text            =   "a_t.txt"
      Top             =   5040
      Width           =   4245
   End
   Begin VB.TextBox Text29 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4440
      TabIndex        =   23
      Text            =   "0.5"
      Top             =   4560
      Width           =   1485
   End
   Begin VB.TextBox Text20 
      Height          =   300
      Left            =   5880
      TabIndex        =   22
      Text            =   "kkk_4b.dat"
      Top             =   5520
      Width           =   4245
   End
   Begin VB.TextBox Text19 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   7800
      TabIndex        =   21
      Text            =   "10"
      Top             =   4560
      Width           =   1485
   End
   Begin VB.CommandButton Ret_comm 
      Caption         =   "Return to Main"
      Height          =   435
      Left            =   5160
      TabIndex        =   7
      Top             =   8280
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type of Simulation"
      Height          =   645
      Left            =   120
      TabIndex        =   18
      Top             =   4920
      Width           =   3270
      Begin VB.OptionButton Titration_Option 
         Caption         =   "Titration"
         Height          =   225
         Left            =   1995
         TabIndex        =   4
         Top             =   315
         Width           =   960
      End
      Begin VB.OptionButton Time_Option 
         Caption         =   "Time Course"
         Height          =   225
         Left            =   525
         TabIndex        =   3
         Top             =   315
         Value           =   -1  'True
         Width           =   1275
      End
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run"
      Height          =   495
      Left            =   4320
      TabIndex        =   8
      Top             =   7560
      Width           =   1485
   End
   Begin VB.TextBox Text15 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   300
      Left            =   2640
      TabIndex        =   6
      Text            =   "0.25"
      Top             =   6000
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2640
      TabIndex        =   5
      Text            =   "5.0"
      Top             =   5610
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   1830
      TabIndex        =   2
      Text            =   "0.5"
      Top             =   4560
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   0
      Left            =   1830
      TabIndex        =   1
      Text            =   "0.3"
      Top             =   1080
      Width           =   1485
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   1830
      TabIndex        =   0
      Text            =   "2.5"
      Top             =   660
      Width           =   1485
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Caption         =   "Upper Bound"
      Height          =   255
      Left            =   9120
      TabIndex        =   47
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Caption         =   "Lower Bound"
      Height          =   255
      Left            =   7440
      TabIndex        =   46
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Caption         =   "Guess2 of Param"
      Height          =   255
      Left            =   5880
      TabIndex        =   45
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Guess1 of Param"
      Height          =   255
      Left            =   4320
      TabIndex        =   44
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Fitting"
      Height          =   255
      Left            =   3480
      TabIndex        =   43
      Top             =   720
      Width           =   615
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "Output Parameter File:"
      Height          =   255
      Left            =   4200
      TabIndex        =   37
      Top             =   7080
      Width           =   1575
   End
   Begin VB.Label Label16 
      Caption         =   "Parameter Estimation - MG Prob"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   480
      TabIndex        =   35
      Top             =   0
      Width           =   7575
   End
   Begin VB.Label Label36 
      Alignment       =   1  'Right Justify
      Caption         =   "[Myosin] in uM:"
      Height          =   285
      Left            =   960
      TabIndex        =   33
      Top             =   6000
      Width           =   1635
   End
   Begin VB.Label Label35 
      Alignment       =   1  'Right Justify
      Caption         =   "Sensitivity Matrix File:"
      Height          =   285
      Left            =   4200
      TabIndex        =   32
      Top             =   6480
      Width           =   1575
   End
   Begin VB.Label Label34 
      Alignment       =   1  'Right Justify
      Caption         =   "Error File:"
      Height          =   285
      Left            =   4680
      TabIndex        =   31
      Top             =   6000
      Width           =   1035
   End
   Begin VB.Label Label26 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   285
      Left            =   4200
      TabIndex        =   28
      Top             =   5520
      Width           =   1515
   End
   Begin VB.Label Label25 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Iteration:"
      Height          =   285
      Left            =   6120
      TabIndex        =   27
      Top             =   4560
      Width           =   1515
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Experimentical Data File:"
      Height          =   285
      Left            =   3840
      TabIndex        =   26
      Top             =   5040
      Width           =   1875
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Epsilon:"
      Height          =   285
      Left            =   3360
      TabIndex        =   25
      Top             =   4560
      Width           =   915
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "k1-:"
      Height          =   285
      Left            =   240
      TabIndex        =   20
      Top             =   3165
      Width           =   1395
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "k2- :"
      Height          =   285
      Left            =   240
      TabIndex        =   19
      Top             =   4005
      Width           =   1515
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "[Myosin] in uM:"
      Height          =   285
      Left            =   840
      TabIndex        =   17
      Top             =   5640
      Width           =   1635
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "[Actin] in uM:"
      Height          =   285
      Left            =   120
      TabIndex        =   16
      Top             =   4545
      Width           =   1635
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "k2+:"
      Height          =   285
      Left            =   240
      TabIndex        =   15
      Top             =   3585
      Width           =   1515
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "k1+    [1/(M*s)]:"
      Height          =   285
      Left            =   240
      TabIndex        =   14
      Top             =   2745
      Width           =   1395
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "kt-:"
      Height          =   285
      Left            =   240
      TabIndex        =   13
      Top             =   2280
      Width           =   1395
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Kt:"
      Height          =   285
      Left            =   360
      TabIndex        =   12
      Top             =   1920
      Width           =   1275
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "kb-:"
      Height          =   285
      Left            =   360
      TabIndex        =   11
      Top             =   1440
      Width           =   1275
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Kb:"
      Height          =   285
      Left            =   360
      TabIndex        =   10
      Top             =   1080
      Width           =   1275
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   360
      TabIndex        =   9
      Top             =   720
      Width           =   1275
   End
End
Attribute VB_Name = "Para_MGNUM_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False





Private Sub Check1_Click(Index As Integer)
    If (Index Mod 2) = 0 Then
        If Check1(Index).Value = vbChecked Then
            For i = 0 To 3
                Text16(i * 8 + Index).Visible = True
            Next i
            Check1(Index + 1).Value = vbUnchecked
        Else
            For i = 0 To 3
                Text16(i * 8 + Index).Visible = False
            Next i
        End If
    Else
        If Check1(Index).Value = vbChecked Then
            For i = 0 To 3
                Text16(i * 8 + Index).Visible = True
            Next i
            Check1(Index - 1).Value = vbUnchecked
        Else
            For i = 0 To 3
                Text16(i * 8 + Index).Visible = False
            Next i
        End If
    End If
    
    
End Sub

Private Sub Command1_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text30.Text = CommonDialog1.FileName
    Text30.Refresh
End Sub

Private Sub Command2_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text20.Text = CommonDialog1.FileName
    Text20.Refresh
End Sub

Private Sub Command3_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text2.Text = CommonDialog1.FileName
    Text2.Refresh
End Sub

Private Sub Command4_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text1.Text = CommonDialog1.FileName
    Text1.Refresh
End Sub

Private Sub Command5_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text3.Text = CommonDialog1.FileName
    Text3.Refresh
End Sub

Private Sub Display_Click()
    Para_MGNUM_Form.Hide
    Display_Form.Show
    'Graphs_temp.Show
End Sub

Private Sub Form_Load()
    Label36.Visible = False
    Text30.Text = App.path & "\a_t.txt"
    Text20.Text = App.path & "\kkk.dat"
    Text2.Text = App.path & "\eee.dat"
    Text1.Text = App.path & "\rrr.dat"
    Text3.Text = App.path & "\param_est.dat"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub

Private Sub Ret_comm_Click()
    Main_Form.Show
    Para_MGNUM_Form.Hide
End Sub

Private Sub Run_Button_Click()

    Dim Input_File_Name As String
    Dim FileNumber As Integer
    Dim path As String
    Dim File_name As String
    Dim Paras(4), derror As Single
    Dim kavg(5), klast(5), kde(5) As Double
    Dim kkk(100, 8) As Double
    Dim r(100) As Double
    Dim sm(100, 8, 8) As Double
    Dim int_max, I_init, ip, jp, kp As Integer
    Dim position(4), real_vs As Integer
    
   
    ChDrive App.path
    ChDir App.path
    
    int_max = CInt(Text19.Text)
    If (int_max > 100) Then
        int_max = 100
        Text19.Text = 100
    Else
       If (int_max < 0) Then
            int_max = 10
            Text19.Text = 10
        End If
    End If
    
    For i = 1 To 4
        position(i) = 1
    Next i
        
    FileNumber = 2
    On Error GoTo ErrHandler

        If Time_Option.Value = True Then
            Text15.Text = Text14.Text
            Text15.Enabled = False
            Text15.Visible = False
        End If

    'prepare for parameters
    kp = 0
    For i = 0 To 7
        If Check1(i).Value = vbUnchecked Then
            For j = 0 To 3
                Text16(j * 8 + i).Text = Text5(i).Text
            Next j
        Else
            kp = kp + 1
            position(kp) = i + 1
        End If
    
    Next i
    real_vs = kp


        Input_File_Name = App.path & "\tmtn-ca.dat"
        'Input_File_Name = "D:\newgui\tmtn+ca.dat"
        'Export data to a file
        Open Input_File_Name For Output As #FileNumber
        'Print #FileNumber, "#Output File Name:"
        'Print #FileNumber, "model"
        'Print #FileNumber, "#Total Time [s]:"
        Print #FileNumber, Text4.Text
        'Print #FileNumber, "#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):"
        For i = 0 To 7
            Print #FileNumber, Text5(i).Text
        Next i
        'Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text13.Text
        'Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text15.Text
        Close #FileNumber

        Para_MGNUM_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        
        FileNumber = 3
        Input_File_Name = App.path & "\input_wrap"
        Open Input_File_Name For Output As #FileNumber
            Print #FileNumber, "8"
            For i = 0 To 7
                For j = 0 To 3
                    Print #FileNumber, Text16(j * 8 + i).Text
                Next j
            Next i
                        
            Print #FileNumber, Text29.Text 'epsilon
            Print #FileNumber, Text19.Text 'iteration
            Print #FileNumber, "'" & Text30.Text & "'"
            Print #FileNumber, "'" & Text20.Text & "'"
            Print #FileNumber, "'" & Text2.Text & "'"
            Print #FileNumber, "'" & Text1.Text & "'"
            Print #FileNumber, "'num_rigid'"
            'JC's version num_rigid, my version is num_geeve
        Close #FileNumber
        
        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        'Call ExecCmd(App.path & "\ESTIMRE.exe input_estim" & Chr(34))
        Call ExecCmd(App.path & "\paramestim.exe " & Chr(34))
        'Call ExecCmd("D:\newgui\paramestim.exe " & Chr(34))
        'Para_MGNUM_Form.Show
        
        FileNumber = FreeFile
        File_name = Text20.Text
        Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            For jp = 1 To 8
                Input #FileNumber, kkk(ip, jp)
            Next jp
        Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = Text2.Text
        Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            Input #FileNumber, r(ip)
        Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = Text1.Text
        Open File_name For Input As #FileNumber
        For kp = 1 To int_max
            For ip = 1 To 8
                For jp = 1 To 8
                    Input #FileNumber, sm(kp, ip, jp)
                Next jp
            Next ip
        Next kp
        Close #FileNumber
        
        For ip = 1 To real_vs + 1
            kavg(ip) = 0
            kde(ip) = 0
        Next ip
    
        For ip = 1 To real_vs
            klast(ip) = kkk(int_max, position(ip))
        Next ip
        klast(real_vs + 1) = r(int_max)
        
        If (int_max > 10) Then
            I_init = int_max - 9
        Else
            I_init = 1
        End If
    
        For jp = 1 To real_vs
            For ip = I_init To int_max
                kavg(jp) = kavg(jp) + kkk(ip, position(jp))
            Next ip
        Next jp
    
        For ip = I_init To int_max
            kavg(real_vs) = kavg(real_vs) + r(ip)
        Next ip
        
        For ip = 1 To real_vs + 1
            kavg(ip) = kavg(ip) / (int_max - I_init + 1)
        Next ip
        
          
        For jp = 1 To real_vs
            For ip = I_init To int_max
                kde(jp) = kde(jp) + (kkk(ip, position(jp)) - kavg(jp)) _
                            * (kkk(ip, position(jp)) - kavg(jp))
            Next ip
        Next jp
        
        For ip = I_init To int_max
            kde(real_vs + 1) = kde(real_vs + 1) + (r(ip) - kavg(real_vs + 1)) _
                              * (r(ip) - kavg(real_vs + 1))
        Next ip
        
        For ip = 1 To real_vs + 1
            kde(ip) = Sqr(kde(ip) / (int_max - I_init + 1))
        Next ip
        
        FileNumber = 4
        Input_File_Name = Text3.Text
        Open Input_File_Name For Output As #FileNumber
        For ip = 1 To real_vs + 1
            Print #FileNumber, Format(klast(ip), "0.0000E+00"), _
            Format(kavg(ip), "0.0000E+00"), Format(kde(ip), "0.0000E+00")
        Next ip
        
        'sm only lists active (real) variables, here real_vs
        For jp = 1 To real_vs
            For ip = 1 To real_vs
                Print #FileNumber, Format(sm(int_max, ip, jp), "#0.########"),
            Next ip
            Print #FileNumber, " "
        Next jp
        
        Close #FileNumber
        
        
        FileNumber = FreeFile
        File_name = App.path & "\kb.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(1))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\kt.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(2))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\k1.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(3))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\k2.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(4))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\er.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, r(ip)
            Next ip
        Close #FileNumber
       
        FileNumber = FreeFile
        File_name = App.path & "\ps.dat"
        Open File_name For Output As #FileNumber
            Print #FileNumber, real_vs
            For ip = 1 To 4
                Print #FileNumber, position(ip)
            Next ip
        Close #FileNumber
        
        Para_MGNUM_Form.MousePointer = vbArrow
        Run_Button.Enabled = True
           
        
    
    
    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub



Private Sub Time_Option_Click()
    If Time_Option.Value = True Then
        Text4.Text = 2
        Text14.Text = 0.5
        Text15.Text = Text14.Text
        Text15.Enabled = False
        Text15.Visible = False
        Label13.Caption = "[Myosin] in uM:"
        Label36.Visible = False
    End If
End Sub



Private Sub Titration_Option_Click()
    If Titration_Option.Value = True Then
        Text4.Text = 250
        Text14.Text = 0
        Text15.Enabled = True
        Text15.Visible = True
        Label36.Visible = True
        Label13.Caption = "Initial [Myosin] in uM:"
        Label36.Caption = "Final [Myosin] in uM:"
    End If
End Sub
