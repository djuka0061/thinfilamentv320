
import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod'

const host = environment.server.host;

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded'
    })
};
const header = {
    headers: new HttpHeaders({
        'Accept': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class ProbabilisticService {
    constructor(private httpClient: HttpClient) {
    }

    getConfig(): Observable<any> {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get<any>('');

    }

    public getData(json: Object): Observable<any> {
        console.log(json)
        return this.httpClient.post<any>(host + 'probabilistics/users/test21/runSimulation', json, header)
    }

    public getInputData(userID): Observable<any> {
        return this.httpClient.get<any>(host + 'probabilistics/users/test21/returnInputData')
    }

    public download(userID): Observable<any> {
        return this.httpClient.get<any>(host + 'probabilistics/users/test21/download')
    }

    public saveSimulation(body): Observable<any> {
        return this.httpClient.post<any>(host + 'probabilistics/users/test21/saveData', body, header)
    }

    public openSaveSimulation(userID): Observable<any> {
        return this.httpClient.get<any>(host + 'probabilistics/users/test21/returnSaveData')
    }
}
