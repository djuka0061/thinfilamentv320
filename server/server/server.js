'use strict';

var loopback = require('loopback');
var boot = require('loopback-boot');
const fs = require('fs');
const configuration = require('../config/config').configuration();

var app = module.exports = loopback();

app.start = function () {
  // start the web server
  return app.listen(function () {
    app.emit('started');
    var baseUrl = app.get('url').replace(/\/$/, '');
    console.log('Web server listening at: %s', baseUrl);
    if (app.get('loopback-component-explorer')) {
      var explorerPath = app.get('loopback-component-explorer').mountPath;
      console.log('Browse your REST API at %s%s', baseUrl, explorerPath);
    }
  });
};

app.setUpConfigurationDataBase = () => {
  fs.writeFileSync('./server/datasources.json', JSON.stringify(configuration.loopback.datasources, null, 2));
}

app.setUpConfiguration = () => {
  fs.writeFileSync('./server/config.json', JSON.stringify(configuration.loopback.config, null, 2));
}

// Bootstrap the application, configure models, datasources and middleware.
// Sub-apps like REST API are mounted via boot scripts.
boot(app, __dirname, function (err) {
  if (err) throw err;

  // start the server if `$ node server.js`
  if (require.main === module) {
    app.setUpConfigurationDataBase();
    app.setUpConfiguration();
    app.start();
  }
});
