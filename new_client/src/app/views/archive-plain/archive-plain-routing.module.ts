import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ArchivePlainComponent } from './archive-plain.component';

const routes: Routes = [
  {
    path: '',
    component: ArchivePlainComponent,
    data: {
      title: 'Archive'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchivePlainRoutingModule {}
