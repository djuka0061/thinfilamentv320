import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StochasticalComponent } from './stochastical.component';

describe('StochasticalComponent', () => {
  let component: StochasticalComponent;
  let fixture: ComponentFixture<StochasticalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StochasticalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StochasticalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
