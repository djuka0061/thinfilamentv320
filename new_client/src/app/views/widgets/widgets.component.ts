import { Component, OnInit, QueryList, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { ATPhaseService } from '../../service/atphase.service';
import { timer } from 'rxjs';
import { environment } from '../../../environments/environment.prod';
import { switchMap } from 'rxjs/operators';
import { Chart } from 'chart.js'
import { ActivatedRoute } from '@angular/router';
import { ConfirmationDialogService } from '../confirmation-dialog/confirmation-dialog.service'

const host = environment.server.host;

export interface PeriodicElement {
  last: string;
  average: string;
  std_dev: string;
}

export interface Graph {
  datasets: Array<any>;
  options: any;
  chartType: string;
  colors: Array<any>;
  legend: boolean;
  labels: Array<any>;
  visibility: Boolean;
}

@Component({
  templateUrl: 'widgets.component.html',
  styleUrls: ['./widgets.component.scss'],
})
export class WidgetsComponent implements OnInit, AfterViewInit {

  angForm: FormGroup;
  profileForm = new FormGroup({
    ka: new FormControl(''),
    ka_fiting: new FormControl(''),
    ka_guess_of_params1: new FormControl(''),
    ka_guess_of_params2: new FormControl(''),
    ka_lower_bound: new FormControl(''),
    ka_upper_bound: new FormControl(''),
    ka_minus: new FormControl(''),
    ka_minus_fiting: new FormControl(''),
    ka_minus_guess_of_params1: new FormControl(''),
    ka_minus_guess_of_params2: new FormControl(''),
    ka_minus_lower_bound: new FormControl(''),
    ka_minus_upper_bound: new FormControl(''),
    kpi: new FormControl(''),
    kpi_fiting: new FormControl(''),
    kpi_guess_of_params1: new FormControl(''),
    kpi_guess_of_params2: new FormControl(''),
    kpi_lower_bound: new FormControl(''),
    kpi_upper_bound: new FormControl(''),
    kpi_minus: new FormControl(''),
    kpi_minus_fiting: new FormControl(''),
    kpi_minus_guess_of_params1: new FormControl(''),
    kpi_minus_guess_of_params2: new FormControl(''),
    kpi_minus_lower_bound: new FormControl(''),
    kpi_minus_upper_bound: new FormControl(''),
    kt: new FormControl(''),
    kt_fiting: new FormControl(''),
    kt_guess_of_params1: new FormControl(''),
    kt_guess_of_params2: new FormControl(''),
    kt_lower_bound: new FormControl(''),
    kt_upper_bound: new FormControl(''),
    kt_minus: new FormControl(''),
    kt_minus_fiting: new FormControl(''),
    kt_minus_guess_of_params1: new FormControl(''),
    kt_minus_guess_of_params2: new FormControl(''),
    kt_minus_lower_bound: new FormControl(''),
    kt_minus_upper_bound: new FormControl(''),
    kt_star: new FormControl(''),
    kt_star_fiting: new FormControl(''),
    kt_star_guess_of_params1: new FormControl(''),
    kt_star_guess_of_params2: new FormControl(''),
    kt_star_lower_bound: new FormControl(''),
    kt_star_upper_bound: new FormControl(''),
    kt_star_minus: new FormControl(''),
    kt_star_minus_fiting: new FormControl(''),
    kt_star_minus_guess_of_params1: new FormControl(''),
    kt_star_minus_guess_of_params2: new FormControl(''),
    kt_star_minus_lower_bound: new FormControl(''),
    kt_star_minus_upper_bound: new FormControl(''),
    kt_star_star: new FormControl(''),
    kt_star_star_fiting: new FormControl(''),
    kt_star_star_guess_of_params1: new FormControl(''),
    kt_star_star_guess_of_params2: new FormControl(''),
    kt_star_star_lower_bound: new FormControl(''),
    kt_star_star_upper_bound: new FormControl(''),
    kt_star_star_minus: new FormControl(''),
    kt_star_star_minus_fiting: new FormControl(''),
    kt_star_star_minus_guess_of_params1: new FormControl(''),
    kt_star_star_minus_guess_of_params2: new FormControl(''),
    kt_star_star_minus_lower_bound: new FormControl(''),
    kt_star_star_minus_upper_bound: new FormControl(''),
    kh: new FormControl(''),
    kh_fiting: new FormControl(''),
    kh_guess_of_params1: new FormControl(''),
    kh_guess_of_params2: new FormControl(''),
    kh_lower_bound: new FormControl(''),
    kh_upper_bound: new FormControl(''),
    kh_minus: new FormControl(''),
    kh_minus_fiting: new FormControl(''),
    kh_minus_guess_of_params1: new FormControl(''),
    kh_minus_guess_of_params2: new FormControl(''),
    kh_minus_lower_bound: new FormControl(''),
    kh_minus_upper_bound: new FormControl(''),
    kah: new FormControl(''),
    kah_fiting: new FormControl(''),
    kah_guess_of_params1: new FormControl(''),
    kah_guess_of_params2: new FormControl(''),
    kah_lower_bound: new FormControl(''),
    kah_upper_bound: new FormControl(''),
    kah_minus: new FormControl(''),
    kah_minus_fiting: new FormControl(''),
    kah_minus_guess_of_params1: new FormControl(''),
    kah_minus_guess_of_params2: new FormControl(''),
    kah_minus_lower_bound: new FormControl(''),
    kah_minus_upper_bound: new FormControl(''),
    kd_star: new FormControl(''),
    kd: new FormControl(''),
    kd_star_minus: new FormControl(''),
    kd_minus: new FormControl(''),
    epsilon: new FormControl(''),
    number_of_iteration: new FormControl(''),
    actin: new FormControl(''),
    myosin: new FormControl(''),
    pi: new FormControl(''),
    dpi: new FormControl(''),
    tpi: new FormControl(''),
    file: new FormControl('')
  });

  simulationId: string

  // displayedColumns: string[] = ['col1', 'col2', 'col3', 'col4', 'col5', 'col6', 'col7', 'col8'];
  constructor(private fb: FormBuilder, private atPhaseService: ATPhaseService, private route: ActivatedRoute, private confirmationDialogService: ConfirmationDialogService) {
    this.route.queryParams.subscribe(params => {
      this.simulationId = params['simulationId'];
      if (this.simulationId) {
        this.openArchiveSimulation(this.simulationId)
      }
    });
  }
  public atPhase: any;
  public dataSource;
  public sensitivityMatrix;
  public inputData = this.profileForm;
  public dugmeRun: Boolean = false;
  public displayedColumns: string[];
  public isVisible = true;
  public run_button = "RUN";
  fileToUpload: File = null;
  public experimentalModelGraph
  public parametersConvergance

  private colors = [
    { borderColor: 'blue', backgroundColor: 'transparent' },
    { borderColor: 'purple', backgroundColor: 'transparent' },
    { borderColor: 'yellow', backgroundColor: 'transparent' },
    { borderColor: 'orange', backgroundColor: 'transparent' },
    { borderColor: 'green', backgroundColor: 'transparent' },
    { borderColor: 'black', backgroundColor: 'transparent' },
    { borderColor: 'brown', backgroundColor: 'transparent' }
  ]

  ngOnInit(): void {
    this.atPhaseService.getInputData('test').subscribe(data => {
      if (data) {
        this.inputData = data;
      }
    });
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {})
    this.parametersConvergance = new Chart('parametersConvergance', {})
  }
  ngAfterViewInit(): void {

  }

  public openConfirmationDialog() {
    this.confirmationDialogService.confirm('Please confirm..', 'Do you start simulation in background ?')
      .then((confirmed) => {
        console.log(confirmed)
        if (confirmed.answer) {
          this.startSimulationInBackGround(confirmed.description)
        }
        else {
          this.onSubmit(confirmed.description)
        }
      })
      .catch(() => console.log('User dismissed the dialog (e.g., by using ESC, clicking the cross icon, or clicking outside the dialog)'));
  }

  returnData(simulationID) {
    let subrscription =
      timer(0, 10000).pipe(
        switchMap(() => this.atPhaseService.returnData(simulationID))
      ).subscribe(data => {
        if (data !== null && data.estimatedParams !== null && data.statusCode < 400) {
          this.dugmeRun = false;
          this.run_button = "RUN";
          const a_t = data.estimatedParams.a_t;
          const models = data.estimatedParams.model;
          const vidljivo = [
            Boolean(data.parameters.ka_fiting),
            Boolean(data.parameters.ka_minus_fiting),
            Boolean(data.parameters.kpi_fiting),
            Boolean(data.parameters.kpi_minus_fiting),
            Boolean(data.parameters.kt_fiting),
            Boolean(data.parameters.kt_minus_fiting),
            Boolean(data.parameters.kt_star_fiting),
            Boolean(data.parameters.kt_star_minus_fiting),
            Boolean(data.parameters.kt_star_star_fiting),
            Boolean(data.parameters.kt_star_star_minus_fiting),
            Boolean(data.parameters.kh_fiting),
            Boolean(data.parameters.kh_minus_fiting),
            Boolean(data.parameters.kah_fiting),
            Boolean(data.parameters.kah_minus_fiting),
          ];

          const podaci = [
            { data: data.estimatedParams.ka, label: 'KA' },
            { data: data.estimatedParams.ka_minus, label: 'kA-' },
            { data: data.estimatedParams.kpi, label: 'KPi' },
            { data: data.estimatedParams.kpi_minus, label: 'kPi-' },
            { data: data.estimatedParams.kt, label: 'KT' },
            { data: data.estimatedParams.kt_minus, label: 'kT-' },
            { data: data.estimatedParams.kt_star, label: 'KT*' },
            { data: data.estimatedParams.kt_star_minus, label: 'kT*-' },
            { data: data.estimatedParams.kt_star_2, label: 'KT**' },
            { data: data.estimatedParams.kt_star_minus_2, label: 'kT**-' },
            { data: data.estimatedParams.kh, label: 'KH' },
            { data: data.estimatedParams.kh_minus, label: 'kH-' },
            { data: data.estimatedParams.kah, label: 'KAh' },
            { data: data.estimatedParams.kah_minus, label: 'kAh-' }
          ];

          let dataset = []
          this.experimentAndModelGraph(a_t, models)
          dataset.push({
            label: 'Error',
            data: data.estimatedParams.error,
            fill: false,
            lineTension: 0.2,
            borderColor: 'red',
            borderWidth: 1,
          })
          let j = 0;
          for (let i = 0; i < 14; i++) {
            if (vidljivo[i]) {
              dataset.push({
                label: podaci[i].label,
                data: podaci[i].data,
                fill: false,
                lineTension: 0.2,
                borderColor: this.colors[j].borderColor,
                borderWidth: 1,
              })
              j++;
            }
          }
          this.parametersConvergancePrepare(dataset)

          this.isVisible = true;

          for (let i = 1; i < data.estimatedParams.estimated_values.length; i++) {
            data.estimatedParams.estimated_values[i][1] = data.estimatedParams.estimated_values[i][1].toFixed(6)
            data.estimatedParams.estimated_values[i][2] = data.estimatedParams.estimated_values[i][2].toFixed(6)
            data.estimatedParams.estimated_values[i][3] = data.estimatedParams.estimated_values[i][3].toFixed(6)
          }
          this.dataSource = data.estimatedParams.estimated_values;
          this.sensitivityMatrix = data.estimatedParams.sensitivity;
          subrscription.unsubscribe()
        }
      });
  }

  experimentAndModelGraph(experiment: any, model: any) {
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {
      type: 'line',
      data: {
        datasets: [{
          label: 'Model',
          data: model,
          fill: false,
          lineTension: 0.2,
          borderColor: 'blue',
          borderWidth: 1,
          showLine: false
        }, {
          label: 'Experiment',
          data: experiment,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Actin concentration [microM]'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Fractional Fluorescence'
            }
          }]
        }
      }
    })
  }

  parametersConvergancePrepare(datasets: any) {
    this.parametersConvergance = new Chart('parametersConvergance', {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Iteration'
            }
          }], yAxes: [{
            ticks: {
              callback: function (value) {
                return value.toExponential(2)
              }
            }
          }]
        }
      }
    })
  }


  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onSubmit(description: string) {
    this.dugmeRun = true;
    this.run_button = "RUNNING";
    this.isVisible = false;
    const body = this.profileForm.value;
    body.description = description
    const atData = [];
    const file = this.profileForm.value.file
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    this.atPhaseService.upload(this.fileToUpload).subscribe(upload => {
      this.atPhaseService.getData(body).subscribe(data => {
        this.returnData(data.simulationId)
      });
    });
  }

  startSimulationInBackGround(description: string) {
    this.dugmeRun = true;
    this.run_button = "RUNNING";
    this.isVisible = false;
    const body = this.profileForm.value;
    body.description = description
    const atData = [];
    const file = this.profileForm.value.file
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    this.atPhaseService.upload(this.fileToUpload).subscribe(upload => {
      this.atPhaseService.getData(body).subscribe(data => {
        this.dugmeRun = false;
        this.run_button = "RUN";
      });
    });
  }

  onCheckboxChange(box1: string, box2: string) {
    if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
      if (this.inputData[box1])
        this.inputData[box2] = false;
    }
  }

  download() {
    window.open(host + 'atphases/users/test21/download');
  }

  saveSimulation() {
    const body = this.profileForm.value;
    this.atPhaseService.saveSimulation(body).subscribe(data => {

    })
  }

  openSaveSimulation(file) {
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    const fileData = file.target.files
    var reader = new FileReader();
    reader.readAsText(fileData[0]);
    reader.onload = (_event) => {
      const data = JSON.parse(reader.result.toString());
      const inputDatas = data.parameters;
      this.inputData = data.parameters;
      const vidljivo = [
        Boolean(inputDatas.ka_fiting),
        Boolean(inputDatas.ka_minus_fiting),
        Boolean(inputDatas.kpi_fiting),
        Boolean(inputDatas.kpi_minus_fiting),
        Boolean(inputDatas.kt_fiting),
        Boolean(inputDatas.kt_minus_fiting),
        Boolean(inputDatas.kt_star_fiting),
        Boolean(inputDatas.kt_star_minus_fiting),
        Boolean(inputDatas.kt_star_star_fiting),
        Boolean(inputDatas.kt_star_star_minus_fiting),
        Boolean(inputDatas.kh_fiting),
        Boolean(inputDatas.kh_minus_fiting),
        Boolean(inputDatas.kah_fiting),
        Boolean(inputDatas.kah_minus_fiting),
      ];
      if (data.estimatedParams) {
        const params = data.estimatedParams;

        const podaci = [
          { data: params.ka, label: 'KA' },
          { data: params.ka_minus, label: 'kA-' },
          { data: params.kpi, label: 'KPi' },
          { data: params.kpi_minus, label: 'kPi-' },
          { data: params.kt, label: 'KT' },
          { data: params.kt_minus, label: 'kT-' },
          { data: params.kt_star, label: 'KT*' },
          { data: params.kt_star_minus, label: 'kT*-' },
          { data: params.kt_star_2, label: 'KT**' },
          { data: params.kt_star_minus_2, label: 'kT**-' },
          { data: params.kh, label: 'KH' },
          { data: params.kh_minus, label: 'kH-' },
          { data: params.kah, label: 'KAh' },
          { data: params.kah_minus, label: 'kAh-' }
        ];

        let dataset = []
        this.experimentAndModelGraph(params.a_t, params.model)
        dataset.push({
          label: 'Error',
          data: params.error,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        })
        let j = 0;
        for (let i = 0; i < 14; i++) {
          if (vidljivo[i]) {
            dataset.push({
              label: podaci[i].label,
              data: podaci[i].data,
              fill: false,
              lineTension: 0.2,
              borderColor: this.colors[j].borderColor,
              borderWidth: 1,
            })
            j++;
          }
        }
        this.parametersConvergancePrepare(dataset)

        this.isVisible = true;

        for (let i = 1; i < params.estimated_values.length; i++) {
          params.estimated_values[i][1] = params.estimated_values[i][1].toFixed(6)
          params.estimated_values[i][2] = params.estimated_values[i][2].toFixed(6)
          params.estimated_values[i][3] = params.estimated_values[i][3].toFixed(6)
        }

        this.dataSource = params.estimated_values;
        this.sensitivityMatrix = params.sensitivity;
      }
      this.dugmeRun = false;
      this.run_button = "RUN";
    }
  }
  openArchiveSimulation(simulationId) {
    this.atPhaseService.returnData(simulationId).subscribe(data => {
      const inputDatas = data.parameters;
      this.inputData = data.parameters;
      const vidljivo = [
        Boolean(inputDatas.ka_fiting),
        Boolean(inputDatas.ka_minus_fiting),
        Boolean(inputDatas.kpi_fiting),
        Boolean(inputDatas.kpi_minus_fiting),
        Boolean(inputDatas.kt_fiting),
        Boolean(inputDatas.kt_minus_fiting),
        Boolean(inputDatas.kt_star_fiting),
        Boolean(inputDatas.kt_star_minus_fiting),
        Boolean(inputDatas.kt_star_star_fiting),
        Boolean(inputDatas.kt_star_star_minus_fiting),
        Boolean(inputDatas.kh_fiting),
        Boolean(inputDatas.kh_minus_fiting),
        Boolean(inputDatas.kah_fiting),
        Boolean(inputDatas.kah_minus_fiting),
      ];
      const params = data.estimatedParams;

      const podaci = [
        { data: params.ka, label: 'KA' },
        { data: params.ka_minus, label: 'kA-' },
        { data: params.kpi, label: 'KPi' },
        { data: params.kpi_minus, label: 'kPi-' },
        { data: params.kt, label: 'KT' },
        { data: params.kt_minus, label: 'kT-' },
        { data: params.kt_star, label: 'KT*' },
        { data: params.kt_star_minus, label: 'kT*-' },
        { data: params.kt_star_2, label: 'KT**' },
        { data: params.kt_star_minus_2, label: 'kT**-' },
        { data: params.kh, label: 'KH' },
        { data: params.kh_minus, label: 'kH-' },
        { data: params.kah, label: 'KAh' },
        { data: params.kah_minus, label: 'kAh-' }
      ];

      let dataset = []
      this.experimentAndModelGraph(params.a_t, params.model)
      dataset.push({
        label: 'Error',
        data: params.error,
        fill: false,
        lineTension: 0.2,
        borderColor: 'red',
        borderWidth: 1,
      })
      let j = 0;
      for (let i = 0; i < 14; i++) {
        if (vidljivo[i]) {
          dataset.push({
            label: podaci[i].label,
            data: podaci[i].data,
            fill: false,
            lineTension: 0.2,
            borderColor: this.colors[j].borderColor,
            borderWidth: 1,
          })
          j++;
        }
      }
      this.parametersConvergancePrepare(dataset)

      this.isVisible = true;

      for (let i = 1; i < params.estimated_values.length; i++) {
        params.estimated_values[i][1] = params.estimated_values[i][1].toFixed(6)
        params.estimated_values[i][2] = params.estimated_values[i][2].toFixed(6)
        params.estimated_values[i][3] = params.estimated_values[i][3].toFixed(6)
      }

      this.dataSource = params.estimated_values;
      this.sensitivityMatrix = params.sensitivity;

    })
  }

}
