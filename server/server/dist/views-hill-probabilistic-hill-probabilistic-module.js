(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-hill-probabilistic-hill-probabilistic-module"],{

/***/ "./src/app/service/hill_probabilistic.service.ts":
/*!*******************************************************!*\
  !*** ./src/app/service/hill_probabilistic.service.ts ***!
  \*******************************************************/
/*! exports provided: HillProbabilisticService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HillProbabilisticService", function() { return HillProbabilisticService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");




var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].server.host;
var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var HillProbabilisticService = /** @class */ (function () {
    function HillProbabilisticService(httpClient) {
        this.httpClient = httpClient;
    }
    HillProbabilisticService.prototype.getConfig = function () {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get('');
    };
    HillProbabilisticService.prototype.getData = function (json) {
        return this.httpClient.post(host + 'hill_probabilistics/users/test21/runSimulation', json, httpOptions);
    };
    HillProbabilisticService.prototype.getInputData = function (userID) {
        return this.httpClient.get(host + 'hill_probabilistics/users/test21/returnInputData');
    };
    HillProbabilisticService.prototype.download = function (userID) {
        return this.httpClient.get(host + 'hill_probabilistics/users/test21/download');
    };
    HillProbabilisticService.prototype.saveSimulation = function (body) {
        return this.httpClient.post(host + 'hill_probabilistics/users/test21/saveData', body, httpOptions);
    };
    HillProbabilisticService.prototype.openSaveSimulation = function (userID) {
        return this.httpClient.get(host + 'hill_probabilistics/users/test21/returnSaveData');
    };
    HillProbabilisticService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], HillProbabilisticService);
    return HillProbabilisticService;
}());



/***/ }),

/***/ "./src/app/views/hill-probabilistic/hill-probabilistic-routing.module.ts":
/*!*******************************************************************************!*\
  !*** ./src/app/views/hill-probabilistic/hill-probabilistic-routing.module.ts ***!
  \*******************************************************************************/
/*! exports provided: HillProbabilisticRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HillProbabilisticRoutingModule", function() { return HillProbabilisticRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _hill_probabilistic_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./hill-probabilistic.component */ "./src/app/views/hill-probabilistic/hill-probabilistic.component.ts");




var routes = [
    {
        path: '',
        component: _hill_probabilistic_component__WEBPACK_IMPORTED_MODULE_3__["HillProbabilisticComponent"],
        data: {
            title: 'Hill Probabilistic'
        }
    }
];
var HillProbabilisticRoutingModule = /** @class */ (function () {
    function HillProbabilisticRoutingModule() {
    }
    HillProbabilisticRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], HillProbabilisticRoutingModule);
    return HillProbabilisticRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/hill-probabilistic/hill-probabilistic.component.html":
/*!****************************************************************************!*\
  !*** ./src/app/views/hill-probabilistic/hill-probabilistic.component.html ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn col-sm-12\">\r\n  <div class=\"d-flex w-100 flex-wrap\">\r\n    <!--Parameters-->\r\n    <div class=\"d-flex w-30 p-2 align-self-start flex-min-width\">\r\n      <div class=\"d-flex w-100 card card-accent-primary\">\r\n        <div class=\"card-header\">\r\n          SIMULATION AND MODEL DATA\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex flex-column w-100\">\r\n            <div class=\"d-flex w-100 pt-1 align-content-end\">\r\n              <div class=\"d-flex w-60\"> &nbsp; </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"saveSimulation()\">SAVE</button>\r\n              </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"openSaveSimulation()\">OPEN</button>\r\n              </div>\r\n            </div>\r\n            <form [formGroup]=\"profileForm\" (ngSubmit)=\"onSubmit()\">\r\n              <div class=\"d-flex title-div pb-1\">\r\n                Simulation paramaters\r\n              </div>\r\n              <div class=\"d-flex flex-column w-45\">\r\n                <div class=\"d-flex w-100\">\r\n                  <div class=\"w-35 p-1\">\r\n                    <span style=\"vertical-align: middle\">Total time:</span>\r\n                  </div>\r\n                  <div class=\"d-flex w-45 p-1\">\r\n                    <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.s}} formControlName=\"s\">\r\n                  </div>\r\n                  <div class=\"w-25 p-1\">\r\n                    <span style=\"vertical-align: middle\">[s]</span>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"d-flex title-div pb-1 pt-3\">\r\n                Fixed model parameters\r\n              </div>\r\n\r\n              <div class=\"d-flex w-100 pt-2\">\r\n                <!-- LEVA KOLONA-->\r\n                <div class=\"d-flex flex-column w-50\">\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Rate Constant Alpha 0\">\r\n                      <span style=\"vertical-align: middle\">Alpha 0</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.alpha_zero}}\r\n                        formControlName=\"alpha_zero\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Constant Y1\">\r\n                      <span style=\"vertical-align: middle\">Y1</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_y1}}\r\n                        formControlName=\"constant_y1\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Forward Rate Constant K1\">\r\n                      <span style=\"vertical-align: middle\">K1</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_k1}}\r\n                        formControlName=\"constant_k1\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Forward Rate Constant K2\">\r\n                      <span style=\"vertical-align: middle\">K2</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_k2}}\r\n                        formControlName=\"constant_k2\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Parameter Gama\">\r\n                      <span style=\"vertical-align: middle\">Gama</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.parameter_gama}}\r\n                        formControlName=\"parameter_gama\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Molar Concentration of Actin\">\r\n                      <span style=\"vertical-align: middle\">Actin</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.actin}}\r\n                        formControlName=\"actin\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                </div>\r\n                <!-- DESNA KOLONA-->\r\n                <div class=\"d-flex flex-column w-50\">\r\n                  <!--div class=\"d-flex w-100\"> &nbsp;</div-->\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Rate Constant Beta 0\">\r\n                      <span style=\"vertical-align: middle\">Beta 0</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.beta_zero}}\r\n                        formControlName=\"beta_zero\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Constant Y2\">\r\n                      <span style=\"vertical-align: middle\">Y2</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_y2}}\r\n                        formControlName=\"constant_y2\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Backward Rate Constant K1'\">\r\n                      <span style=\"vertical-align: middle\">K1'</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_k1_prim}}\r\n                        formControlName=\"constant_k1_prim\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Backward Rate Constant K2'\">\r\n                      <span style=\"vertical-align: middle\">K2'</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.constant_k2_prim}}\r\n                        formControlName=\"constant_k2_prim\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Cooperativity Parameter Delta\">\r\n                      <span style=\"vertical-align: middle\">Delta</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.parameter_delta}}\r\n                        formControlName=\"parameter_delta\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\" tooltip=\"Molar Concentration of Myosin\">\r\n                      <span style=\"vertical-align: middle\">Myosin</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                        formControlName=\"myosin\">\r\n                    </div>\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"d-flex w-100 pt-3 \">\r\n                <div class=\"d-flex w-80\"> &nbsp; </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"!profileForm.valid\">RUN </button>\r\n                </div>\r\n              </div>\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n\r\n    <div class=\"d-flex w-70 p-2\">\r\n      <div class=\"d-flex card card-accent-primary\">\r\n        <div class=\"card-header\">\r\n          RESULTS\r\n          <span class=\"float-right\" tooltip=\"Download graphic data\" placement=\"left\">\r\n            <i (click)=\"download()\" class=\"fa fa-cloud-download\" style=\"font-size:16px\"></i>\r\n          </span>\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex w-100 flex-wrap justify-content-around align-content-start\">\r\n            <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n            <div *ngFor=\"let graph of graphs\" class=\"w-100 p-2 mw-350\">\r\n              <div class=\"chart-wrapper\">\r\n                <!--style=\"height:300px;margin-top:40px;\"-->\r\n                <canvas baseChart class=\"chart\" [datasets]=\"graph.datasets\" [labels]=\"graph.labels\"\r\n                  [options]=\"graph.options\" [colors]=\"graph.colors\" [legend]=\"graph.legend\"\r\n                  [chartType]=\"graph.chartType\">\r\n                </canvas>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/hill-probabilistic/hill-probabilistic.component.scss":
/*!****************************************************************************!*\
  !*** ./src/app/views/hill-probabilistic/hill-probabilistic.component.scss ***!
  \****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column; }\n\ninput[type=number] {\n  -moz-appearance: textfield !important;\n       appearance: textfield !important;\n  -webkit-appearance: textfield !important;\n  margin-right: 2px !important;\n  text-align: right; }\n\ninput[_ngcontent-c1]::-webkit-outer-spin-button {\n  -webkit-appearance: texfield !important;\n  -moz-appearance: textfield !important;\n  appearance: none;\n  margin: 0; }\n\n.example-container > * {\n  width: 100%; }\n\n.example-container form {\n  margin-bottom: 20px; }\n\n.example-container form > * {\n  margin: 5px 0; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.table_form {\n  min-width: 550px;\n  padding: 0px; }\n\n.table_form p {\n  font-size: 10px;\n  text-align: right; }\n\n.table_form input {\n  height: 25px;\n  text-align: center;\n  max-width: 100px; }\n\n/* Customize the label (the container) */\n\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n/* Hide the browser's default checkbox */\n\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0; }\n\n/* Create a custom checkbox */\n\n.checkmark {\n  position: absolute;\n  margin-top: 5px;\n  top: 0;\n  left: 0;\n  height: 20 px;\n  width: 20px;\n  background-color: #eee; }\n\n/* On mouse-over, add a grey background color */\n\n.container:hover input ~ .checkmark {\n  background-color: #ccc; }\n\n/* When the checkbox is checked, add a blue background */\n\n.container input:checked ~ .checkmark {\n  background-color: #2196F3; }\n\n.btn-align {\n  text-align: center !important; }\n\n/* Create the checkmark/indicator (hidden when not checked) */\n\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none; }\n\n/* Show the checkmark when checked */\n\n.container input:checked ~ .checkmark:after {\n  display: block; }\n\n/* Style the checkmark/indicator */\n\n.container .checkmark:after {\n  left: 9px;\n  top: 5px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg); }\n\n.title-div-centar {\n  font-size: 1rem;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\n.title-div {\n  color: cadetblue;\n  font-style: 1rem;\n  border-bottom: 1px dashed lightblue; }\n\n.title-div-table {\n  font-size: 1rem;\n  color: cadetblue;\n  text-align: center !important; }\n\n.title-div-2 {\n  font-size: 1.2rem;\n  color: cadetblue; }\n\ninput {\n  background-color: #c3e4e5;\n  border: 1px solid \t#c3e4e5 !important;\n  padding-right: 2px !important; }\n\n.text-align-right {\n  text-align: right !important; }\n\n.text-align-center {\n  text-align: center; }\n\n.flex-min-width {\n  min-width: 470px; }\n\n.max-width-70posto {\n  max-width: 70%; }\n\n.over-auto {\n  overflow: visible; }\n\n.chart-wrapper {\n  border: 1px solid lightblue !important; }\n\n.mw-350 {\n  min-width: 700px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-30posto {\n  min-width: 370px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-400 {\n  min-width: 280px; }\n\n.tbl-10 {\n  min-width: 300px !important; }\n\n.tbl-8kolona {\n  min-width: 300px !important; }\n\n.td-1 {\n  padding: 0.4rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvaGlsbC1wcm9iYWJpbGlzdGljL0Y6XFx0aGluZmlsYW1lbnR2MzIwXFxuZXdfY2xpZW50L3NyY1xcYXBwXFx2aWV3c1xcaGlsbC1wcm9iYWJpbGlzdGljXFxoaWxsLXByb2JhYmlsaXN0aWMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCLEVBQUE7O0FBSXhCO0VBR0UscUNBQWdDO09BQWhDLGdDQUFnQztFQUNoQyx3Q0FBd0M7RUFDeEMsNEJBQTRCO0VBQzVCLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLHVDQUF1QztFQUN2QyxxQ0FBcUM7RUFDckMsZ0JBQWdCO0VBQ2hCLFNBQVMsRUFBQTs7QUFFWDtFQUNFLFdBQVcsRUFBQTs7QUFHYjtFQUNFLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsV0FBVyxFQUFBOztBQUdiO0VBQ0UsV0FBVyxFQUFBOztBQUdiO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQTs7QUFHZDtFQUNFLGVBQWU7RUFDZixpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUlsQix3Q0FBQTs7QUFDQTtFQUNBLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIscUJBQXFCO0VBQ3JCLGlCQUFpQixFQUFBOztBQUdqQix3Q0FBQTs7QUFDQTtFQUNBLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsZUFBZTtFQUNmLFNBQVM7RUFDVCxRQUFRLEVBQUE7O0FBR1IsNkJBQUE7O0FBQ0E7RUFDQSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLE1BQU07RUFDTixPQUFPO0VBQ1AsYUFBYTtFQUNiLFdBQVc7RUFDWCxzQkFBc0IsRUFBQTs7QUFHdEIsK0NBQUE7O0FBQ0E7RUFDQSxzQkFBc0IsRUFBQTs7QUFHdEIsd0RBQUE7O0FBQ0E7RUFDQSx5QkFBeUIsRUFBQTs7QUFHekI7RUFDRSw2QkFBOEIsRUFBQTs7QUFHaEMsNkRBQUE7O0FBQ0E7RUFDQSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGFBQWEsRUFBQTs7QUFHYixvQ0FBQTs7QUFDQTtFQUNBLGNBQWMsRUFBQTs7QUFHZCxrQ0FBQTs7QUFDQTtFQUNBLFNBQVM7RUFDVCxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLGdDQUFnQztFQUVoQyx3QkFBd0IsRUFBQTs7QUFHeEI7RUFFQSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUlwQjtFQUNBLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsbUNBQW1DLEVBQUE7O0FBR25DO0VBQ0EsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiw2QkFBNkIsRUFBQTs7QUFHN0I7RUFDQSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBSWhCO0VBRUEseUJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyw2QkFBNkIsRUFBQTs7QUFHN0I7RUFDQSw0QkFBNEIsRUFBQTs7QUFJNUI7RUFDQSxrQkFBa0IsRUFBQTs7QUFJbEI7RUFDQSxnQkFBZ0IsRUFBQTs7QUFTaEI7RUFDQSxjQUFjLEVBQUE7O0FBRWQ7RUFDQSxpQkFBaUIsRUFBQTs7QUFHakI7RUFFQSxzQ0FBc0MsRUFBQTs7QUFHdEM7RUFDQSxnQkFBZ0I7RUFDaEIsMkJBQW1CO0VBQW5CLHdCQUFtQjtFQUFuQixtQkFBbUIsRUFBQTs7QUFHbkI7RUFDQSxnQkFBZ0I7RUFDaEIsMkJBQW1CO0VBQW5CLHdCQUFtQjtFQUFuQixtQkFBbUIsRUFBQTs7QUFLbkI7RUFDQSxnQkFBZ0IsRUFBQTs7QUFHaEI7RUFFQSwyQkFBMkIsRUFBQTs7QUFFM0I7RUFFQSwyQkFBMkIsRUFBQTs7QUFJM0I7RUFDQSwwQkFBMEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL2hpbGwtcHJvYmFiaWxpc3RpYy9oaWxsLXByb2JhYmlsaXN0aWMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuXHJcbmlucHV0W3R5cGU9bnVtYmVyXSB7IFxyXG4gIC8vLXdlYmtpdC1hcHBlYXJhbmNlOiB0ZXhmaWVsZCAhaW1wb3J0YW50O1xyXG4gLy8gLW1vei1hcHBlYXJhbmNlOiB0ZXh0ZmllbGQgIWltcG9ydGFudDtcclxuICBhcHBlYXJhbmNlOiB0ZXh0ZmllbGQgIWltcG9ydGFudDtcclxuICAtd2Via2l0LWFwcGVhcmFuY2U6IHRleHRmaWVsZCAhaW1wb3J0YW50O1xyXG4gIG1hcmdpbi1yaWdodDogMnB4ICFpbXBvcnRhbnQ7IFxyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG5pbnB1dFtfbmdjb250ZW50LWMxXTo6LXdlYmtpdC1vdXRlci1zcGluLWJ1dHRvbiB7IFxyXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogdGV4ZmllbGQgIWltcG9ydGFudDtcclxuICAtbW96LWFwcGVhcmFuY2U6IHRleHRmaWVsZCAhaW1wb3J0YW50O1xyXG4gIGFwcGVhcmFuY2U6IG5vbmU7XHJcbiAgbWFyZ2luOiAwOyBcclxufVxyXG4uZXhhbXBsZS1jb250YWluZXIgPiAqIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyIGZvcm0ge1xyXG4gIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbn1cclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciBmb3JtID4gKiB7XHJcbiAgbWFyZ2luOiA1cHggMDtcclxufVxyXG5cclxuLmV4YW1wbGUtZm9ybSB7XHJcbiAgbWluLXdpZHRoOiAxNTBweDtcclxuICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZXhhbXBsZS1mdWxsLXdpZHRoIHtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLnRhYmxlX2Zvcm0ge1xyXG4gIG1pbi13aWR0aDogNTUwcHg7XHJcbiAgcGFkZGluZzogMHB4O1xyXG59XHJcblxyXG4udGFibGVfZm9ybSBwIHtcclxuICBmb250LXNpemU6IDEwcHg7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuXHJcbi50YWJsZV9mb3JtIGlucHV0IHtcclxuICBoZWlnaHQ6IDI1cHg7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gIG1heC13aWR0aDogMTAwcHg7XHJcbn1cclxuXHJcblxyXG4vKiBDdXN0b21pemUgdGhlIGxhYmVsICh0aGUgY29udGFpbmVyKSAqL1xyXG4uY29udGFpbmVyIHtcclxuZGlzcGxheTogYmxvY2s7XHJcbnBvc2l0aW9uOiByZWxhdGl2ZTtcclxucGFkZGluZy1sZWZ0OiAzNXB4O1xyXG5tYXJnaW4tYm90dG9tOiAxMnB4O1xyXG5jdXJzb3I6IHBvaW50ZXI7XHJcbmZvbnQtc2l6ZTogMjJweDtcclxuLXdlYmtpdC11c2VyLXNlbGVjdDogbm9uZTtcclxuLW1vei11c2VyLXNlbGVjdDogbm9uZTtcclxuLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG51c2VyLXNlbGVjdDogbm9uZTtcclxufVxyXG5cclxuLyogSGlkZSB0aGUgYnJvd3NlcidzIGRlZmF1bHQgY2hlY2tib3ggKi9cclxuLmNvbnRhaW5lciBpbnB1dCB7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxub3BhY2l0eTogMDtcclxuY3Vyc29yOiBwb2ludGVyO1xyXG5oZWlnaHQ6IDA7XHJcbndpZHRoOiAwO1xyXG59XHJcblxyXG4vKiBDcmVhdGUgYSBjdXN0b20gY2hlY2tib3ggKi9cclxuLmNoZWNrbWFyayB7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxubWFyZ2luLXRvcDogNXB4O1xyXG50b3A6IDA7XHJcbmxlZnQ6IDA7XHJcbmhlaWdodDogMjAgcHg7XHJcbndpZHRoOiAyMHB4O1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjZWVlO1xyXG59XHJcblxyXG4vKiBPbiBtb3VzZS1vdmVyLCBhZGQgYSBncmV5IGJhY2tncm91bmQgY29sb3IgKi9cclxuLmNvbnRhaW5lcjpob3ZlciBpbnB1dCB+IC5jaGVja21hcmsge1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjY2NjO1xyXG59XHJcblxyXG4vKiBXaGVuIHRoZSBjaGVja2JveCBpcyBjaGVja2VkLCBhZGQgYSBibHVlIGJhY2tncm91bmQgKi9cclxuLmNvbnRhaW5lciBpbnB1dDpjaGVja2VkIH4gLmNoZWNrbWFyayB7XHJcbmJhY2tncm91bmQtY29sb3I6ICMyMTk2RjM7XHJcbn1cclxuXHJcbi5idG4tYWxpZ24ge1xyXG4gIHRleHQtYWxpZ246ICBjZW50ZXIgIWltcG9ydGFudDtcclxufVxyXG5cclxuLyogQ3JlYXRlIHRoZSBjaGVja21hcmsvaW5kaWNhdG9yIChoaWRkZW4gd2hlbiBub3QgY2hlY2tlZCkgKi9cclxuLmNoZWNrbWFyazphZnRlciB7XHJcbmNvbnRlbnQ6IFwiXCI7XHJcbnBvc2l0aW9uOiBhYnNvbHV0ZTtcclxuZGlzcGxheTogbm9uZTtcclxufVxyXG5cclxuLyogU2hvdyB0aGUgY2hlY2ttYXJrIHdoZW4gY2hlY2tlZCAqL1xyXG4uY29udGFpbmVyIGlucHV0OmNoZWNrZWQgfiAuY2hlY2ttYXJrOmFmdGVyIHtcclxuZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi8qIFN0eWxlIHRoZSBjaGVja21hcmsvaW5kaWNhdG9yICovXHJcbi5jb250YWluZXIgLmNoZWNrbWFyazphZnRlciB7XHJcbmxlZnQ6IDlweDtcclxudG9wOiA1cHg7XHJcbndpZHRoOiA1cHg7XHJcbmhlaWdodDogMTBweDtcclxuYm9yZGVyOiBzb2xpZCB3aGl0ZTtcclxuYm9yZGVyLXdpZHRoOiAwIDNweCAzcHggMDtcclxuLXdlYmtpdC10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XHJcbi1tcy10cmFuc2Zvcm06IHJvdGF0ZSg0NWRlZyk7XHJcbnRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxufVxyXG5cclxuLnRpdGxlLWRpdi1jZW50YXIge1xyXG4vL2NvbG9yOiBjYWRldGJsdWU7XHJcbmZvbnQtc2l6ZTogMXJlbTtcclxucGFkZGluZy10b3A6IDFyZW07XHJcbnBhZGRpbmctYm90dG9tOiAxcmVtO1xyXG4vL2JvcmRlci1ib3R0b206IDFweCBkYXNoZWQgbGlnaHRibHVlO1xyXG59XHJcblxyXG4udGl0bGUtZGl2IHtcclxuY29sb3I6IGNhZGV0Ymx1ZTtcclxuZm9udC1zdHlsZTogMXJlbTtcclxuYm9yZGVyLWJvdHRvbTogMXB4IGRhc2hlZCBsaWdodGJsdWU7XHJcbn1cclxuXHJcbi50aXRsZS1kaXYtdGFibGUge1xyXG5mb250LXNpemU6IDFyZW07IFxyXG5jb2xvcjogY2FkZXRibHVlO1xyXG50ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRpdGxlLWRpdi0yIHtcclxuZm9udC1zaXplOiAxLjJyZW07IFxyXG5jb2xvcjogY2FkZXRibHVlO1xyXG4vLyB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW5wdXQge1xyXG4vL2JvcmRlcjogbm9uZTtcclxuYmFja2dyb3VuZC1jb2xvcjogXHQjYzNlNGU1O1xyXG5ib3JkZXI6IDFweCBzb2xpZCBcdCNjM2U0ZTUgIWltcG9ydGFudDtcclxucGFkZGluZy1yaWdodDogMnB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50ZXh0LWFsaWduLXJpZ2h0IHtcclxudGV4dC1hbGlnbjogcmlnaHQgIWltcG9ydGFudDtcclxuLy9wYWRkaW5nLXJpZ2h0OiAycHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtYWxpZ24tY2VudGVyIHtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxufVxyXG5cclxuLmZsZXgtbWluLXdpZHRoIHtcclxubWluLXdpZHRoOiA0NzBweDtcclxuLy8gIG1heC13aWR0aDogNjAwcHg7XHJcbi8vei1pbmRleDogMTAwMDtcclxufVxyXG5cclxuLmZsZXgtbWluLXdpZHRoICoge1xyXG4vL3otaW5kZXg6IDEwMDA7XHJcbn1cclxuXHJcbi5tYXgtd2lkdGgtNzBwb3N0byB7XHJcbm1heC13aWR0aDogNzAlO1xyXG59XHJcbi5vdmVyLWF1dG8ge1xyXG5vdmVyZmxvdzogdmlzaWJsZTtcclxufVxyXG5cclxuLmNoYXJ0LXdyYXBwZXIge1xyXG4vL2JvcmRlci10b3Atc3R5bGU6IGdyb292ZSAhaW1wb3J0YW50O1xyXG5ib3JkZXI6IDFweCBzb2xpZCBsaWdodGJsdWUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm13LTM1MCB7XHJcbm1pbi13aWR0aDogNzAwcHg7XHJcbmhlaWdodDogZml0LWNvbnRlbnQ7XHJcbn1cclxuXHJcbi5tdy0zMHBvc3RvIHtcclxubWluLXdpZHRoOiAzNzBweDtcclxuaGVpZ2h0OiBmaXQtY29udGVudDtcclxuLy9mb250LXNpemU6IDAuNXJlbSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuLm13LTQwMCB7XHJcbm1pbi13aWR0aDogMjgwcHg7XHJcbn1cclxuXHJcbi50YmwtMTAge1xyXG4vLyBtYXJnaW46IDEwcHggIWltcG9ydGFudDtcclxubWluLXdpZHRoOiAzMDBweCAhaW1wb3J0YW50O1xyXG59XHJcbi50YmwtOGtvbG9uYSB7XHJcbi8vIG1hcmdpbjogMTBweCAhaW1wb3J0YW50O1xyXG5taW4td2lkdGg6IDMwMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG4udGQtMXtcclxucGFkZGluZzogMC40cmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/views/hill-probabilistic/hill-probabilistic.component.ts":
/*!**************************************************************************!*\
  !*** ./src/app/views/hill-probabilistic/hill-probabilistic.component.ts ***!
  \**************************************************************************/
/*! exports provided: HillProbabilisticComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HillProbabilisticComponent", function() { return HillProbabilisticComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @coreui/coreui-plugin-chartjs-custom-tooltips */ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_hill_probabilistic_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/hill_probabilistic.service */ "./src/app/service/hill_probabilistic.service.ts");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");







var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].server.host;
var HillProbabilisticComponent = /** @class */ (function () {
    function HillProbabilisticComponent(fb, HillProbabilisticService) {
        this.fb = fb;
        this.HillProbabilisticService = HillProbabilisticService;
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            s: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            alpha_zero: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            beta_zero: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_y1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_y2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_k1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_k1_prim: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_k2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            constant_k2_prim: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            parameter_gama: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            parameter_delta: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            actin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.numberOfGraphs = 1;
        this.graphs = [];
        this.inputData = this.profileForm;
        this.typeOfSimulation = 1;
        this.test = [
            [1, 2, 3, 4],
            [1, 2, 3, 4],
            [1, 6, 3, 4],
            [5, 2, 3, 4],
            [1, 2, 3, 4],
        ];
    }
    HillProbabilisticComponent.prototype.testFja = function () {
        this.test = [
            [1, 2, 3, 4, 4, 5],
            [1, 2, 3, 4, 5, 3]
        ];
    };
    HillProbabilisticComponent.prototype.ngOnInit = function () {
        var _this = this;
        var titles = [
            'Fractional Fluorescence',
            'Fraction Saturation',
            'Fraction of Unbound Actin',
            'Free Myosin Concentration'
        ];
        var labels = [
            { xAxes: 'Time [s]', yAxes: 'Unit X1' },
            { xAxes: 'Time [s]', yAxes: 'Unit X2' },
            { xAxes: 'Time [s]', yAxes: 'Unit X3' },
            { xAxes: 'Time [s]', yAxes: 'Unit X4' }
        ];
        var colors = [
            [{ borderColor: 'blue', backgroundColor: 'transparent' }],
            [{ borderColor: 'red', backgroundColor: 'transparent' }],
            [{ borderColor: 'green', backgroundColor: 'transparent' }],
            [{ borderColor: 'purple', backgroundColor: 'transparent' }]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i]));
        }
        console.log(this.graphs);
        this.HillProbabilisticService.getInputData('test21').subscribe(function (data) {
            if (data) {
                _this.inputData = data;
            }
        });
    };
    HillProbabilisticComponent.prototype.prepareGraphs = function (graphLines) {
        var titles = [
            'Fractional Fluorescence',
            'Fraction Saturation',
            'Fraction of Unbound Actin',
            'Free Myosin Concentration'
        ];
        var labels = [
            { xAxes: 'Time [s]', yAxes: 'Unit X1' },
            { xAxes: 'Time [s]', yAxes: 'Unit X2' },
            { xAxes: 'Time [s]', yAxes: 'Unit X3' },
            { xAxes: 'Time [s]', yAxes: 'Unit X4' }
        ];
        var colors = [
            [{ borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' }]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i]));
        }
    };
    HillProbabilisticComponent.prototype.initGraph = function (newTitle, newLabels, newColors) {
        var basicGraph = {};
        basicGraph.options = {
            title: {
                display: true,
                text: 'Error',
                // fontColor: 'blue',
                fontStyle: 'small-caps',
            },
            tooltips: {
                enabled: false,
                custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                intersect: true,
                mode: 'index',
                position: 'nearest',
                callbacks: {
                    labelColor: function (tooltipItem, chart) {
                        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                    }
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 15,
                    top: 0,
                    bottom: 5
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        type: 'linear',
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            padding: 5,
                            beginAtZero: true,
                            stepSize: 0.5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC1 (%)',
                            fontSize: 11
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            beginAtZero: true,
                            padding: 5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC2 (%)',
                            fontSize: 11
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: false
            }
        };
        var newOptions = basicGraph.options;
        newOptions.title.text = newTitle;
        newOptions.scales.xAxes[0].scaleLabel.labelString = newLabels.xAxes;
        newOptions.scales.yAxes[0].scaleLabel.labelString = newLabels.yAxes;
        basicGraph.options = newOptions;
        basicGraph.colors = newColors;
        basicGraph.datasets = [{
                data: []
            }];
        basicGraph.chartType = 'line';
        basicGraph.labels = [];
        return basicGraph;
    };
    HillProbabilisticComponent.prototype.onSubmit = function () {
        var _this = this;
        var body = this.profileForm.value;
        body.type_of_simulation = 'titration';
        this.HillProbabilisticService.getData(body).subscribe(function (data) {
            console.log('podaci', data);
            _this.porbabilistic = data;
            _this.histogramData = data.histogram;
            var datasets = [
                [{ data: data.fractional_fluorescence },
                    { data: data.fraction_saturation },
                    { data: data.fraction_of_unbound_actin },
                    { data: data.free_myosin_concentration }]
            ];
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
            }
            _this.graphs[0].options = {
                title: {
                    display: true,
                    text: 'Error',
                    // fontColor: 'blue',
                    fontStyle: 'small-caps',
                },
                tooltips: {
                    enabled: false,
                    custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                    intersect: true,
                    mode: 'index',
                    position: 'nearest',
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                        }
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 15,
                        top: 0,
                        bottom: 5
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                            type: 'linear',
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                padding: 5,
                                beginAtZero: true,
                                stepSize: 0.5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC1 (%)',
                                fontSize: 11
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                beginAtZero: true,
                                padding: 5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC555 (%)',
                                fontSize: 11
                            }
                        }]
                },
                elements: {
                    line: {
                        borderWidth: 2
                    },
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4,
                        hoverBorderWidth: 3,
                    }
                },
                legend: {
                    display: false
                }
            };
        });
    };
    HillProbabilisticComponent.prototype.download = function () {
        window.open(host + 'hill_probabilistics/users/test21/download');
    };
    HillProbabilisticComponent.prototype.saveSimulation = function () {
        var body = this.profileForm.value;
        this.HillProbabilisticService.saveSimulation(body).subscribe(function (data) {
            console.log(data);
        });
    };
    HillProbabilisticComponent.prototype.openSaveSimulation = function () {
        var _this = this;
        this.HillProbabilisticService.openSaveSimulation('test21').subscribe(function (data) {
            console.log('podaci', data);
            _this.porbabilistic = data;
            _this.histogramData = data.inputData.histogram;
            _this.inputData = data.parameters;
            var datasets = [
                [{ data: data.inputData.fractional_fluorescence },
                    { data: data.inputData.fraction_saturation },
                    { data: data.inputData.fraction_of_unbound_actin },
                    { data: data.inputData.free_myosin_concentration }]
            ];
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
            }
            _this.graphs[0].options = {
                title: {
                    display: true,
                    text: 'Error',
                    // fontColor: 'blue',
                    fontStyle: 'small-caps',
                },
                tooltips: {
                    enabled: false,
                    custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                    intersect: true,
                    mode: 'index',
                    position: 'nearest',
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                        }
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 15,
                        top: 0,
                        bottom: 5
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                            type: 'linear',
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                padding: 5,
                                beginAtZero: true,
                                stepSize: 0.5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC1 (%)',
                                fontSize: 11
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                beginAtZero: true,
                                padding: 5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC555 (%)',
                                fontSize: 11
                            }
                        }]
                },
                elements: {
                    line: {
                        borderWidth: 2
                    },
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4,
                        hoverBorderWidth: 3,
                    }
                },
                legend: {
                    display: false
                }
            };
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__["BaseChartDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
    ], HillProbabilisticComponent.prototype, "charts", void 0);
    HillProbabilisticComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-hill-probabilistic',
            template: __webpack_require__(/*! ./hill-probabilistic.component.html */ "./src/app/views/hill-probabilistic/hill-probabilistic.component.html"),
            styles: [__webpack_require__(/*! ./hill-probabilistic.component.scss */ "./src/app/views/hill-probabilistic/hill-probabilistic.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _service_hill_probabilistic_service__WEBPACK_IMPORTED_MODULE_4__["HillProbabilisticService"]])
    ], HillProbabilisticComponent);
    return HillProbabilisticComponent;
}());



/***/ }),

/***/ "./src/app/views/hill-probabilistic/hill-probabilistic.module.ts":
/*!***********************************************************************!*\
  !*** ./src/app/views/hill-probabilistic/hill-probabilistic.module.ts ***!
  \***********************************************************************/
/*! exports provided: HillProbabilisticModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HillProbabilisticModule", function() { return HillProbabilisticModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _hill_probabilistic_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./hill-probabilistic.component */ "./src/app/views/hill-probabilistic/hill-probabilistic.component.ts");
/* harmony import */ var _hill_probabilistic_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./hill-probabilistic-routing.module */ "./src/app/views/hill-probabilistic/hill-probabilistic-routing.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");













var HillProbabilisticModule = /** @class */ (function () {
    function HillProbabilisticModule() {
    }
    HillProbabilisticModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _hill_probabilistic_routing_module__WEBPACK_IMPORTED_MODULE_8__["HillProbabilisticRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(),
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"],
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_11__["TooltipModule"].forRoot()
            ],
            declarations: [_hill_probabilistic_component__WEBPACK_IMPORTED_MODULE_7__["HillProbabilisticComponent"]],
            exports: [
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ]
        })
    ], HillProbabilisticModule);
    return HillProbabilisticModule;
}());



/***/ })

}]);
//# sourceMappingURL=views-hill-probabilistic-hill-probabilistic-module.js.map