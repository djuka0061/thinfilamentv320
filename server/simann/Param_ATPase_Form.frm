VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Para_ATPase_Form 
   Caption         =   "Para_Estim ATPase Thin Filament Regulation"
   ClientHeight    =   10950
   ClientLeft      =   225
   ClientTop       =   555
   ClientWidth     =   11580
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   10950
   ScaleWidth      =   11580
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   9000
      TabIndex        =   138
      Text            =   "27.8"
      Top             =   6840
      Width           =   1455
   End
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   6600
      TabIndex        =   137
      Text            =   "36.0"
      Top             =   6840
      Width           =   1455
   End
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   4200
      TabIndex        =   136
      Text            =   "354.0"
      Top             =   6840
      Width           =   1455
   End
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   1920
      TabIndex        =   135
      Text            =   "0.1667"
      Top             =   6840
      Width           =   1455
   End
   Begin VB.TextBox Text18 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   2160
      TabIndex        =   134
      Text            =   "5000"
      Top             =   9720
      Width           =   1455
   End
   Begin VB.TextBox Text17 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   2160
      TabIndex        =   133
      Text            =   "0.0"
      Top             =   9240
      Width           =   1455
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   8
      Left            =   4440
      TabIndex        =   124
      Text            =   "100"
      Top             =   4320
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   9
      Left            =   4440
      TabIndex        =   123
      Text            =   "5"
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   10
      Left            =   4440
      TabIndex        =   122
      Text            =   "7"
      Top             =   5160
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   11
      Left            =   4440
      TabIndex        =   121
      Text            =   "5"
      Top             =   5520
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   12
      Left            =   4440
      TabIndex        =   120
      Text            =   "40"
      Top             =   6000
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   13
      Left            =   4440
      TabIndex        =   119
      Text            =   "0.3"
      Top             =   6360
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   22
      Left            =   6000
      TabIndex        =   118
      Text            =   "500"
      Top             =   4320
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   23
      Left            =   6000
      TabIndex        =   117
      Text            =   "7"
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   24
      Left            =   6000
      TabIndex        =   116
      Text            =   "12"
      Top             =   5160
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   25
      Left            =   6000
      TabIndex        =   115
      Text            =   "18"
      Top             =   5520
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   26
      Left            =   6000
      TabIndex        =   114
      Text            =   "100"
      Top             =   6000
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   27
      Left            =   6000
      TabIndex        =   113
      Text            =   "0.7"
      Top             =   6360
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   36
      Left            =   7560
      TabIndex        =   112
      Text            =   "0"
      Top             =   4320
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   37
      Left            =   7560
      TabIndex        =   111
      Text            =   "0"
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   38
      Left            =   7560
      TabIndex        =   110
      Text            =   "0"
      Top             =   5160
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   39
      Left            =   7560
      TabIndex        =   109
      Text            =   "0"
      Top             =   5520
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   40
      Left            =   7560
      TabIndex        =   108
      Text            =   "0"
      Top             =   6000
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   41
      Left            =   7560
      TabIndex        =   107
      Text            =   "0"
      Top             =   6360
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   50
      Left            =   9240
      TabIndex        =   106
      Text            =   "10000"
      Top             =   4320
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   51
      Left            =   9240
      TabIndex        =   105
      Text            =   "200"
      Top             =   4680
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   52
      Left            =   9240
      TabIndex        =   104
      Text            =   "300"
      Top             =   5160
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   53
      Left            =   9240
      TabIndex        =   103
      Text            =   "200"
      Top             =   5520
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   54
      Left            =   9240
      TabIndex        =   102
      Text            =   "300"
      Top             =   6000
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   55
      Left            =   9240
      TabIndex        =   101
      Text            =   "100"
      Top             =   6360
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   8
      Left            =   3720
      TabIndex        =   100
      Top             =   4320
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   9
      Left            =   3720
      TabIndex        =   99
      Top             =   4680
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   10
      Left            =   3720
      TabIndex        =   98
      Top             =   5160
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   11
      Left            =   3720
      TabIndex        =   97
      Top             =   5520
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   12
      Left            =   3720
      TabIndex        =   96
      Top             =   6000
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   13
      Left            =   3720
      TabIndex        =   95
      Top             =   6360
      Width           =   375
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   8
      Left            =   1800
      TabIndex        =   94
      Text            =   "1200"
      Top             =   4320
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   9
      Left            =   1800
      TabIndex        =   93
      Text            =   "1.0"
      Top             =   4680
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   10
      Left            =   1830
      TabIndex        =   92
      Text            =   "10.66"
      Top             =   5160
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   11
      Left            =   1800
      TabIndex        =   91
      Text            =   "1.4"
      Top             =   5520
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   12
      Left            =   1830
      TabIndex        =   90
      Text            =   "75"
      Top             =   6000
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   13
      Left            =   1800
      TabIndex        =   89
      Text            =   "0.2"
      Top             =   6360
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   7
      Left            =   1800
      TabIndex        =   88
      Text            =   "10"
      Top             =   3960
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   6
      Left            =   1800
      TabIndex        =   87
      Text            =   "154.30"
      Top             =   3600
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   5
      Left            =   1800
      TabIndex        =   86
      Text            =   "3349.37"
      Top             =   3120
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   4
      Left            =   1830
      TabIndex        =   85
      Text            =   "0.003"
      Top             =   2760
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   3
      Left            =   1800
      TabIndex        =   84
      Text            =   "0.00013"
      Top             =   2280
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   2
      Left            =   1800
      TabIndex        =   83
      Text            =   "100000"
      Top             =   1920
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   1
      Left            =   1800
      TabIndex        =   82
      Text            =   "1000.0000000"
      Top             =   1440
      Width           =   1485
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   7
      Left            =   3720
      TabIndex        =   81
      Top             =   3960
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   6
      Left            =   3720
      TabIndex        =   80
      Top             =   3600
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   5
      Left            =   3720
      TabIndex        =   79
      Top             =   3120
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   4
      Left            =   3720
      TabIndex        =   78
      Top             =   2760
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   3
      Left            =   3720
      TabIndex        =   77
      Top             =   2280
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   2
      Left            =   3720
      TabIndex        =   76
      Top             =   1920
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   1
      Left            =   3720
      TabIndex        =   75
      Top             =   1440
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   0
      Left            =   3720
      TabIndex        =   74
      Top             =   1080
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   49
      Left            =   9240
      TabIndex        =   73
      Text            =   "200"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   48
      Left            =   9240
      TabIndex        =   72
      Text            =   "5000"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   47
      Left            =   9240
      TabIndex        =   71
      Text            =   "10000"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   46
      Left            =   9240
      TabIndex        =   70
      Text            =   "100"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   45
      Left            =   9240
      TabIndex        =   69
      Text            =   "100"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   44
      Left            =   9240
      TabIndex        =   68
      Text            =   "1.0e6"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   43
      Left            =   9240
      TabIndex        =   67
      Text            =   "10000"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   42
      Left            =   9240
      TabIndex        =   66
      Text            =   "200"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   35
      Left            =   7560
      TabIndex        =   65
      Text            =   "1"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   34
      Left            =   7560
      TabIndex        =   64
      Text            =   "10"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   33
      Left            =   7560
      TabIndex        =   63
      Text            =   "50"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   32
      Left            =   7560
      TabIndex        =   62
      Text            =   "0"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   31
      Left            =   7560
      TabIndex        =   61
      Text            =   "0"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   30
      Left            =   7560
      TabIndex        =   60
      Text            =   "1.0e3"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   29
      Left            =   7560
      TabIndex        =   59
      Text            =   "10"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   28
      Left            =   7560
      TabIndex        =   58
      Text            =   "0"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   21
      Left            =   6000
      TabIndex        =   57
      Text            =   "5"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   20
      Left            =   6000
      TabIndex        =   56
      Text            =   "160"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   19
      Left            =   6000
      TabIndex        =   55
      Text            =   "3350.0"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   18
      Left            =   6000
      TabIndex        =   54
      Text            =   "0.0005"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   17
      Left            =   6000
      TabIndex        =   53
      Text            =   "0.0002"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   16
      Left            =   6000
      TabIndex        =   52
      Text            =   "280000"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   15
      Left            =   6000
      TabIndex        =   51
      Text            =   "170"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   14
      Left            =   6000
      TabIndex        =   50
      Text            =   "0.02"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   7
      Left            =   4440
      TabIndex        =   49
      Text            =   "10"
      Top             =   3960
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   6
      Left            =   4440
      TabIndex        =   48
      Text            =   "200"
      Top             =   3600
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   5
      Left            =   4440
      TabIndex        =   47
      Text            =   "3200.0"
      Top             =   3120
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   4
      Left            =   4440
      TabIndex        =   46
      Text            =   "0.01"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   3
      Left            =   4440
      TabIndex        =   45
      Text            =   "0.0001"
      Top             =   2280
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   2
      Left            =   4440
      TabIndex        =   44
      Text            =   "200000"
      Top             =   1920
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   4440
      TabIndex        =   43
      Text            =   "200"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   4440
      TabIndex        =   42
      Text            =   "0.01"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.CommandButton Command5 
      Caption         =   "..........."
      Height          =   375
      Left            =   10080
      TabIndex        =   36
      Top             =   9840
      Width           =   735
   End
   Begin VB.CommandButton Command4 
      Caption         =   "........."
      Height          =   375
      Left            =   10080
      TabIndex        =   35
      Top             =   9360
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Caption         =   "......."
      Height          =   375
      Left            =   10080
      TabIndex        =   34
      Top             =   8880
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Caption         =   "........"
      Height          =   375
      Left            =   10080
      TabIndex        =   33
      Top             =   8400
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Caption         =   "........"
      Height          =   375
      Left            =   10080
      TabIndex        =   32
      Top             =   7920
      Width           =   735
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   240
      Top             =   10800
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Text3 
      Height          =   375
      Left            =   5760
      TabIndex        =   30
      Text            =   "para_est.dat"
      Top             =   9840
      Width           =   4215
   End
   Begin VB.CommandButton Display 
      Caption         =   "Show Results"
      Height          =   495
      Left            =   6360
      TabIndex        =   28
      Top             =   10440
      Width           =   1575
   End
   Begin VB.TextBox Text2 
      Height          =   300
      Left            =   5760
      TabIndex        =   25
      Text            =   "eee.dat"
      Top             =   8880
      Width           =   4245
   End
   Begin VB.TextBox Text1 
      Height          =   300
      Left            =   5760
      TabIndex        =   24
      Text            =   "rrr.dat"
      Top             =   9360
      Width           =   4245
   End
   Begin VB.TextBox Text30 
      Height          =   300
      Left            =   5760
      TabIndex        =   19
      Text            =   "a_t.txt"
      Top             =   7920
      Width           =   4245
   End
   Begin VB.TextBox Text29 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4080
      TabIndex        =   18
      Text            =   "0.15"
      Top             =   7320
      Width           =   1485
   End
   Begin VB.TextBox Text20 
      Height          =   300
      Left            =   5760
      TabIndex        =   17
      Text            =   "kkk.dat"
      Top             =   8400
      Width           =   4245
   End
   Begin VB.TextBox Text19 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   7680
      TabIndex        =   16
      Text            =   "100"
      Top             =   7320
      Width           =   1485
   End
   Begin VB.CommandButton Ret_comm 
      Caption         =   "Return to Main"
      Height          =   435
      Left            =   5400
      TabIndex        =   4
      Top             =   11160
      Width           =   1485
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run"
      Height          =   495
      Left            =   4320
      TabIndex        =   5
      Top             =   10440
      Width           =   1485
   End
   Begin VB.TextBox Text15 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2160
      TabIndex        =   3
      Text            =   "0.0"
      Top             =   8760
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2160
      TabIndex        =   2
      Text            =   "1"
      Top             =   8280
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   2160
      TabIndex        =   1
      Text            =   "3"
      Top             =   7800
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   0
      Left            =   1830
      TabIndex        =   0
      Text            =   "0.01127"
      Top             =   1080
      Width           =   1485
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Concentration of Myosin:"
      Height          =   285
      Index           =   1
      Left            =   240
      TabIndex        =   143
      Top             =   8280
      Width           =   1875
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "kD-:"
      Height          =   285
      Index           =   5
      Left            =   7920
      TabIndex        =   142
      Top             =   6840
      Width           =   1035
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "kD:"
      Height          =   285
      Index           =   4
      Left            =   5520
      TabIndex        =   141
      Top             =   6840
      Width           =   1035
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "kD*-:"
      Height          =   285
      Index           =   3
      Left            =   3120
      TabIndex        =   140
      Top             =   6840
      Width           =   1035
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "kD*:"
      Height          =   285
      Index           =   2
      Left            =   840
      TabIndex        =   139
      Top             =   6840
      Width           =   1035
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Concentration of TPi:"
      Height          =   285
      Index           =   2
      Left            =   480
      TabIndex        =   132
      Top             =   9720
      Width           =   1635
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Concentration of DPi:"
      Height          =   285
      Index           =   1
      Left            =   480
      TabIndex        =   131
      Top             =   9240
      Width           =   1635
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "KT**:"
      Height          =   285
      Index           =   1
      Left            =   360
      TabIndex        =   130
      Top             =   4320
      Width           =   1275
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "kT**-:"
      Height          =   285
      Index           =   1
      Left            =   240
      TabIndex        =   129
      Top             =   4680
      Width           =   1395
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "kH:"
      Height          =   285
      Index           =   1
      Left            =   240
      TabIndex        =   128
      Top             =   5145
      Width           =   1395
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "kAh:"
      Height          =   285
      Index           =   1
      Left            =   240
      TabIndex        =   127
      Top             =   5985
      Width           =   1515
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "kAh- :"
      Height          =   285
      Index           =   1
      Left            =   240
      TabIndex        =   126
      Top             =   6405
      Width           =   1515
   End
   Begin VB.Label Label21 
      Alignment       =   1  'Right Justify
      Caption         =   "kH-:"
      Height          =   285
      Left            =   240
      TabIndex        =   125
      Top             =   5565
      Width           =   1395
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Caption         =   "Upper Bound"
      Height          =   255
      Left            =   9120
      TabIndex        =   41
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Caption         =   "Lower Bound"
      Height          =   255
      Left            =   7440
      TabIndex        =   40
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Caption         =   "Guess2 of Param"
      Height          =   255
      Left            =   5880
      TabIndex        =   39
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Guess1 of Param"
      Height          =   255
      Left            =   4320
      TabIndex        =   38
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Fitting"
      Height          =   255
      Left            =   3480
      TabIndex        =   37
      Top             =   720
      Width           =   615
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "Output Parameter File:"
      Height          =   255
      Left            =   3960
      TabIndex        =   31
      Top             =   9960
      Width           =   1575
   End
   Begin VB.Label Label16 
      Caption         =   "Parameter Estimation - ATPase"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   480
      TabIndex        =   29
      Top             =   0
      Width           =   7575
   End
   Begin VB.Label Label35 
      Alignment       =   1  'Right Justify
      Caption         =   "Sensitivity Matrix File:"
      Height          =   285
      Left            =   3960
      TabIndex        =   27
      Top             =   9480
      Width           =   1575
   End
   Begin VB.Label Label34 
      Alignment       =   1  'Right Justify
      Caption         =   "Error File:"
      Height          =   285
      Left            =   4440
      TabIndex        =   26
      Top             =   9000
      Width           =   1035
   End
   Begin VB.Label Label26 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   285
      Left            =   3960
      TabIndex        =   23
      Top             =   8520
      Width           =   1515
   End
   Begin VB.Label Label25 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Iteration:"
      Height          =   285
      Left            =   5880
      TabIndex        =   22
      Top             =   7320
      Width           =   1515
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Experimentical Data File:"
      Height          =   285
      Left            =   3600
      TabIndex        =   21
      Top             =   8040
      Width           =   1875
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Epsilon:"
      Height          =   285
      Left            =   2640
      TabIndex        =   20
      Top             =   7320
      Width           =   915
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "kT-:"
      Height          =   285
      Left            =   240
      TabIndex        =   15
      Top             =   3165
      Width           =   1395
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "kT*- :"
      Height          =   285
      Index           =   0
      Left            =   240
      TabIndex        =   14
      Top             =   4005
      Width           =   1515
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Concentration of Pi:"
      Height          =   285
      Index           =   0
      Left            =   480
      TabIndex        =   13
      Top             =   8760
      Width           =   1635
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Concentration of Actin:"
      Height          =   285
      Index           =   0
      Left            =   480
      TabIndex        =   12
      Top             =   7800
      Width           =   1635
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "kT*:"
      Height          =   285
      Index           =   0
      Left            =   240
      TabIndex        =   11
      Top             =   3585
      Width           =   1515
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "kT:"
      Height          =   285
      Index           =   0
      Left            =   240
      TabIndex        =   10
      Top             =   2745
      Width           =   1395
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "kPi-:"
      Height          =   285
      Index           =   0
      Left            =   240
      TabIndex        =   9
      Top             =   2280
      Width           =   1395
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "KPi:"
      Height          =   285
      Index           =   0
      Left            =   360
      TabIndex        =   8
      Top             =   1920
      Width           =   1275
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "kA-:"
      Height          =   285
      Left            =   360
      TabIndex        =   7
      Top             =   1440
      Width           =   1275
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "KA:"
      Height          =   285
      Left            =   360
      TabIndex        =   6
      Top             =   1080
      Width           =   1275
   End
End
Attribute VB_Name = "Para_ATPase_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False





Private Sub Check1_Click(Index As Integer)
    If (Index Mod 2) = 0 Then
        If Check1(Index).Value = vbChecked Then
            For i = 0 To 3
                Text16(i * 14 + Index).Visible = True
            Next i
            Check1(Index + 1).Value = vbUnchecked
        Else
            For i = 0 To 3
                Text16(i * 14 + Index).Visible = False
            Next i
        End If
    Else
        If Check1(Index).Value = vbChecked Then
            For i = 0 To 3
                Text16(i * 14 + Index).Visible = True
            Next i
            Check1(Index - 1).Value = vbUnchecked
        Else
            For i = 0 To 3
                Text16(i * 14 + Index).Visible = False
            Next i
        End If
    End If
    
    
End Sub

Private Sub Command1_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text30.Text = CommonDialog1.FileName
    Text30.Refresh
End Sub

Private Sub Command2_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text20.Text = CommonDialog1.FileName
    Text20.Refresh
End Sub

Private Sub Command3_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text2.Text = CommonDialog1.FileName
    Text2.Refresh
End Sub

Private Sub Command4_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text1.Text = CommonDialog1.FileName
    Text1.Refresh
End Sub

Private Sub Command5_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text3.Text = CommonDialog1.FileName
    Text3.Refresh
End Sub

Private Sub Display_Click()
    Para_ATPase_Form.Hide
    Display_Form.Show
    'Graphs_temp.Show
End Sub

Private Sub Form_Load()

    Text30.Text = App.path & "\a_t.txt"
    Text20.Text = App.path & "\kkk.dat"
    Text2.Text = App.path & "\eee.dat"
    Text1.Text = App.path & "\rrr.dat"
    Text3.Text = App.path & "\param_est.dat"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub

Private Sub Ret_comm_Click()
    Main_Form.Show
    Para_ATPase_Form.Hide
End Sub

Private Sub Run_Button_Click()

    Dim Input_File_Name As String
    Dim FileNumber As Integer
    Dim path As String
    Dim File_name As String
    Dim Paras(4), derror As Single
    Dim kavg(8), klast(8), kde(8) As Double
    Dim kkk(100, 14) As Double
    Dim r(100) As Double
    Dim sm(100, 14, 14) As Double
    Dim int_max, I_init, ip, jp, kp As Integer
    Dim position(7), real_vs As Integer
    
   
    ChDrive App.path
    ChDir App.path
    
    int_max = CInt(Text19.Text)
    If (int_max > 100) Then
        int_max = 100
        Text19.Text = 100
    Else
       If (int_max < 0) Then
            int_max = 10
            Text19.Text = 10
        End If
    End If
    
    For i = 1 To 7
        position(i) = 1
    Next i
        
    FileNumber = 2
    On Error GoTo ErrHandler

        

    'prepare for parameters
    kp = 0
    For i = 0 To 13
        If Check1(i).Value = vbUnchecked Then
            For j = 0 To 3
                Text16(j * 14 + i).Text = Text5(i).Text
            Next j
        Else
            kp = kp + 1
            position(kp) = i + 1
        End If
    
    Next i
    real_vs = kp


        Input_File_Name = App.path & "\tmtn-ca.dat"
        'Input_File_Name = "D:\newgui\tmtn+ca.dat"
        'Export data to a file
        Open Input_File_Name For Output As #FileNumber
        'Print #FileNumber, "#Output File Name:"
        'Print #FileNumber, "model"
        'Print #FileNumber, "#Total Time [s]:"
        'Print #FileNumber, Text4.Text
        'Print #FileNumber, "#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):"
        For i = 0 To 3
            Print #FileNumber, Text5(i).Text
        Next i
        
        For i = 0 To 3
            Print #FileNumber, Text6(i).Text
        Next i
        
        For i = 4 To 13
            Print #FileNumber, Text5(i).Text
        Next i
        
        'Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text15.Text
        'Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text17.Text
        Print #FileNumber, Text18.Text
        Print #FileNumber, Text13.Text
        Print #FileNumber, Text14.Text
        Close #FileNumber

        Para_MGNUM_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        
        FileNumber = 3
        Input_File_Name = App.path & "\input_wrap"
        Open Input_File_Name For Output As #FileNumber
            Print #FileNumber, "14"
            For i = 0 To 13
                For j = 0 To 3
                    Print #FileNumber, Text16(j * 14 + i).Text
                Next j
            Next i
                        
            Print #FileNumber, Text29.Text 'epsilon
            Print #FileNumber, Text19.Text 'iteration
            Print #FileNumber, "'" & Text30.Text & "'"
            Print #FileNumber, "'" & Text20.Text & "'"
            Print #FileNumber, "'" & Text2.Text & "'"
            Print #FileNumber, "'" & Text1.Text & "'"
            Print #FileNumber, "'ATPaseMultiSimA'"
            'JC's version num_rigid, my version is num_geeve
        Close #FileNumber
        
        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        'Call ExecCmd(App.path & "\ESTIMRE.exe input_estim" & Chr(34))
        Call ExecCmd(App.path & "\ParaEstimATPase.exe " & Chr(34))
        'Call ExecCmd("D:\newgui\paramestim.exe " & Chr(34))
        'Para_MGNUM_Form.Show
        
        FileNumber = FreeFile
        File_name = Text20.Text
        Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            For jp = 1 To 14
                Input #FileNumber, kkk(ip, jp)
            Next jp
        Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = Text2.Text
        Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            Input #FileNumber, r(ip)
        Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = Text1.Text
        Open File_name For Input As #FileNumber
        For kp = 1 To int_max
            For ip = 1 To 14
                For jp = 1 To 14
                    Input #FileNumber, sm(kp, ip, jp)
                Next jp
            Next ip
        Next kp
        Close #FileNumber
        
        For ip = 1 To real_vs + 1
            kavg(ip) = 0
            kde(ip) = 0
        Next ip
    
        For ip = 1 To real_vs
            klast(ip) = kkk(int_max, position(ip))
        Next ip
        klast(real_vs + 1) = r(int_max)
        
        If (int_max > 10) Then
            I_init = int_max - 9
        Else
            I_init = 1
        End If
    
        For jp = 1 To real_vs
            For ip = I_init To int_max
                kavg(jp) = kavg(jp) + kkk(ip, position(jp))
            Next ip
        Next jp
    
        For ip = I_init To int_max
            kavg(real_vs) = kavg(real_vs) + r(ip)
        Next ip
        
        For ip = 1 To real_vs + 1
            kavg(ip) = kavg(ip) / (int_max - I_init + 1)
        Next ip
        
          
        For jp = 1 To real_vs
            For ip = I_init To int_max
                kde(jp) = kde(jp) + (kkk(ip, position(jp)) - kavg(jp)) _
                            * (kkk(ip, position(jp)) - kavg(jp))
            Next ip
        Next jp
        
        For ip = I_init To int_max
            kde(real_vs + 1) = kde(real_vs + 1) + (r(ip) - kavg(real_vs + 1)) _
                              * (r(ip) - kavg(real_vs + 1))
        Next ip
        
        For ip = 1 To real_vs + 1
            kde(ip) = Sqr(kde(ip) / (int_max - I_init + 1))
        Next ip
        
        FileNumber = 4
        Input_File_Name = Text3.Text
        Open Input_File_Name For Output As #FileNumber
        For ip = 1 To real_vs + 1
            Print #FileNumber, Format(klast(ip), "0.0000E+00"), _
            Format(kavg(ip), "0.0000E+00"), Format(kde(ip), "0.0000E+00")
        Next ip
        
        'sm only lists active (real) variables, here real_vs
        For jp = 1 To real_vs
            For ip = 1 To real_vs
                Print #FileNumber, Format(sm(int_max, ip, jp), "#0.########"),
            Next ip
            Print #FileNumber, " "
        Next jp
        
        Close #FileNumber
        
        
        FileNumber = FreeFile
        File_name = App.path & "\kb.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(1))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\kt.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(2))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\k1.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(3))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\k2.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(4))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\er.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, r(ip)
            Next ip
        Close #FileNumber
       
        FileNumber = FreeFile
        File_name = App.path & "\ps.dat"
        Open File_name For Output As #FileNumber
            Print #FileNumber, real_vs
            For ip = 1 To 7
                Print #FileNumber, position(ip)
            Next ip
        Close #FileNumber
        
        Para_MGNUM_Form.MousePointer = vbArrow
        Run_Button.Enabled = True
           
        
    
    
    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub







