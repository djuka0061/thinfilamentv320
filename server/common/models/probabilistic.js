'use strict';

// Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require('fs');
const promise = require('bluebird');
const returnOutputData = require('../../middleware/return_data');
const zip = require('../../middleware/zipping');
const winston = require('winston')

// Local include

module.exports = function (probabilistic) {
  // function for run simulation
  probabilistic.runSimulation = (id, parameters) => {
    let writeStream = fs.createWriteStream('./probabilistic/pain.dat');
    writeStream.write('#Output File Name:\n');
    writeStream.write('./Simu_output\n');
    writeStream.write('#Total Time [s]:\n');
    writeStream.write(parameters.s + '\n');
    writeStream.write('#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):\n');
    writeStream.write(parameters.kb_plus + '\n');
    writeStream.write(parameters.kb_minus + '\n');
    writeStream.write('#The Rate Constants Beetwen the Closed and Open States (Kt / kt-):\n');
    writeStream.write(parameters.kt_plus + '\n');
    writeStream.write(parameters.kt_minus + '\n');
    writeStream.write('#The Rate Constants of Myosin Weak Binding (k1+ / k1-):\n');
    writeStream.write(parameters.k1_plus + '\n');
    writeStream.write(parameters.k1_minus + '\n');
    writeStream.write('#Isomerization Rate Constants (k2+ / k2-):\n');
    writeStream.write(parameters.k2_plus + '\n');
    writeStream.write(parameters.k2_minus + '\n');
    writeStream.write('#Molar Concentration of Actin [uM]:\n');
    writeStream.write(parameters.molar_concentration_of_actin + '\n');
    writeStream.write('#Molar Concentration of Myosin Initial / Final [uM / uM]:\n');
    writeStream.write(parameters.molar_concentration_of_myosin + '\n');
    writeStream.end((parameters.final_myosin ? parameters.final_myosin : parameters.molar_concentration_of_myosin) + '\n');
    const options = {
      userID: id,
      ...parameters
    };
    // setup data for start simulation
    const fileName = './wraper.exe';
    const formater = './formater.exe';
    const params = [];
    const path = './probabilistic/';
    const formaterParams = [];
    let promise = new Promise((resolve, reject) => {
      exec(fileName, params, { cwd: path }, (err, data) => {
        winston.info('Simulation probabilistic successfully start')
        if (err) {
          winston.info(err)
          const response = {
            statusCode: 400,
            message: 'Simulation failed'
          }
          return response
        }
        else {
          winston.info('Simulation probabilistic successfully finish')
          exec(formater, formaterParams, { cwd: path }, (err, format) => {
            if (err) reject(err);
            else {
              return loopback.getModel('probabilistic').create(options)
                .then(data => {
                  return returnOutputData.returnDataForFirstFourSimulation('probabilistic', 'Simu_output_destiled.dat', 'Simu_output-histogram.DAT')
                    .then(data => {
                      zip.zippingFile('probabilistic', 'Simu_output_destiled', 'Simu_output-histogram', data);
                      return resolve(data);
                    });
                });
            }
          });
        }
      });
    });
    return promise;
  };

  probabilistic.saveData = (id, parameters) => {
    return returnOutputData.returnDataForFirstFourSimulation('probabilistic', 'Simu_output_destiled.dat', 'Simu_output-histogram.DAT')
      .then(data => {
        if (!data && data !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for probabilistic simulation'
          }
          winston.info('Not found output data for probabilistic simulation')
          return response
        }
        const inputParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel('probabilistic').create(inputParams)
          .then(inputData => {
            const options = {
              userID: id,
              input_id: inputData._id,
              ...data
            };
            return loopback.getModel('probabilistic_out').create(options)
              .then(saveData => {
                winston.info('Simulation probabilistic successfully save data')
                return saveData;
              })
          })
      });
  }

  probabilistic.returnSaveData = (id) => {
    return loopback.getModel('probabilistic_out').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found probabilistic simulation'
          }
          winston.info('Not found return data for probabilistic simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id;
        return loopback.getModel('probabilistic').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found probabilistic simulation'
              }
              winston.info('Not found input data for probabilistic simulation')
              return response
            }
            const response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response;
          });
      });
  }

  // Save input data for simulation
  probabilistic.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    };
    return loopback.getModel('probabilistic').create(options)
      .then(data => {
        winston.info('Simulation probabilistic successfully save input data')
        return data;
      });
  };

  probabilistic.returnInputData = (id) => {
    return loopback.getModel('probabilistic').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found probabilistic input data`
          }
          winston.info('Not found probabilistic input data')
          return response
        }
        return data[data.length - 1];
      });
  };


  probabilistic.returnOutPutData = (id) => {
    return loopback.getModel('probabilistic_out').findOne({ where: { userID: id } })
      .then(data => {
        if (!outputData && outputData !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for probabilistic simulation'
          }
          winston.info('Not found output data for probabilistic simulation')
          return response
        }
        return data;
      });
  };

  probabilistic.download = (id, res) => {
    winston.info('Simulation probabilistic successfully download data')
    res.download('./probabilistic/output_files.zip');
  };

  /** API for run simulation */
  probabilistic.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  probabilistic.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  /** API for return input data for simulation */
  probabilistic.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });


  probabilistic.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  probabilistic.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  /** API for return output data for simulation */
  probabilistic.remoteMethod('returnOutPutData', {
    http: { verb: 'GET', path: '/users/:id/returnOutPutData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  probabilistic.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  probabilistic.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'returnInputData', 'saveData', 'returnSaveData', 'returnOutPutData', 'download'], method.name)) {
      probabilistic.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
};
