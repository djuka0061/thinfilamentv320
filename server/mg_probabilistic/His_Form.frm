VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{D940E4E4-6079-11CE-88CB-0020AF6845F6}#1.6#0"; "cwui.ocx"
Begin VB.Form His_Form 
   Caption         =   "Histogram"
   ClientHeight    =   8910
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9030
   LinkTopic       =   "Form1"
   ScaleHeight     =   8910
   ScaleWidth      =   9030
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
   Begin VB.CommandButton Stop_Command 
      Caption         =   "Stop Animation"
      Enabled         =   0   'False
      Height          =   435
      Left            =   6825
      TabIndex        =   16
      Top             =   7770
      Width           =   1800
   End
   Begin VB.CommandButton Animation_Command 
      Caption         =   "Start Animation"
      Height          =   435
      Left            =   6825
      TabIndex        =   15
      Top             =   7140
      Width           =   1800
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   1
      Left            =   8505
      Top             =   8400
   End
   Begin MSComctlLib.Slider Slider1 
      Height          =   330
      Left            =   210
      TabIndex        =   14
      Top             =   8295
      Width           =   6315
      _ExtentX        =   11139
      _ExtentY        =   582
      _Version        =   393216
      Min             =   1
      Max             =   300
      SelStart        =   1
      Value           =   1
   End
   Begin VB.CommandButton Print_Button 
      Caption         =   "Print Graph"
      Height          =   435
      Left            =   6825
      TabIndex        =   13
      Top             =   5565
      Visible         =   0   'False
      Width           =   1800
   End
   Begin VB.Frame Frame1 
      Caption         =   "Axes"
      Height          =   3270
      Left            =   6615
      TabIndex        =   1
      Top             =   105
      Width           =   2220
      Begin VB.CommandButton Auto_Scale_Button 
         Caption         =   "Auto Scale"
         Height          =   435
         Left            =   210
         TabIndex        =   8
         Top             =   1440
         Width           =   1800
      End
      Begin VB.Frame Frame3 
         Caption         =   "Y_Axis_1"
         ForeColor       =   &H00000000&
         Height          =   1065
         Left            =   105
         TabIndex        =   5
         Top             =   315
         Width           =   2010
         Begin CWUIControlsLib.CWNumEdit Y1_Max_Text 
            Height          =   285
            Left            =   525
            TabIndex        =   12
            Top             =   630
            Width           =   1380
            _Version        =   458752
            _ExtentX        =   2434
            _ExtentY        =   503
            _StockProps     =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Reset_0         =   0   'False
            CompatibleVers_0=   458752
            NumEdit_0       =   1
            ClassName_1     =   "CCWNumEdit"
            opts_1          =   393278
            C[1]_1          =   0
            BorderStyle_1   =   1
            ButtonPosition_1=   1
            TextAlignment_1 =   2
            format_1        =   2
            ClassName_2     =   "CCWFormat"
            scale_1         =   3
            ClassName_3     =   "CCWScale"
            opts_3          =   65536
            dMax_3          =   10
            discInterval_3  =   1
            ValueVarType_1  =   5
            Value_Val_1     =   10
            IncValueVarType_1=   5
            IncValue_Val_1  =   1
            AccelIncVarType_1=   5
            AccelInc_Val_1  =   5
            RangeMinVarType_1=   5
            RangeMaxVarType_1=   5
            RangeMax_Val_1  =   100
            ButtonStyle_1   =   0
            Bindings_1      =   4
            ClassName_4     =   "CCWBindingHolderArray"
            Editor_4        =   5
            ClassName_5     =   "CCWBindingHolderArrayEditor"
            Owner_5         =   1
         End
         Begin CWUIControlsLib.CWNumEdit Y1_Min_Text 
            Height          =   285
            Left            =   525
            TabIndex        =   11
            Top             =   315
            Width           =   1380
            _Version        =   458752
            _ExtentX        =   2434
            _ExtentY        =   503
            _StockProps     =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Reset_0         =   0   'False
            CompatibleVers_0=   458752
            NumEdit_0       =   1
            ClassName_1     =   "CCWNumEdit"
            opts_1          =   393278
            C[1]_1          =   0
            BorderStyle_1   =   1
            ButtonPosition_1=   1
            TextAlignment_1 =   2
            format_1        =   2
            ClassName_2     =   "CCWFormat"
            scale_1         =   3
            ClassName_3     =   "CCWScale"
            opts_3          =   65536
            dMax_3          =   10
            discInterval_3  =   1
            ValueVarType_1  =   5
            IncValueVarType_1=   5
            IncValue_Val_1  =   1
            AccelIncVarType_1=   5
            AccelInc_Val_1  =   5
            RangeMinVarType_1=   5
            RangeMaxVarType_1=   5
            RangeMax_Val_1  =   100
            ButtonStyle_1   =   0
            Bindings_1      =   4
            ClassName_4     =   "CCWBindingHolderArray"
            Editor_4        =   5
            ClassName_5     =   "CCWBindingHolderArrayEditor"
            Owner_5         =   1
         End
         Begin VB.Label Label5 
            Alignment       =   1  'Right Justify
            Caption         =   "Max"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   105
            TabIndex        =   7
            Top             =   630
            Width           =   330
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            Caption         =   "Min"
            ForeColor       =   &H00000000&
            Height          =   225
            Left            =   105
            TabIndex        =   6
            Top             =   315
            Width           =   330
         End
      End
      Begin VB.Frame Frame2 
         Caption         =   "X-Axis"
         Height          =   1065
         Left            =   105
         TabIndex        =   2
         Top             =   1995
         Visible         =   0   'False
         Width           =   2010
         Begin CWUIControlsLib.CWNumEdit X_Max_Text 
            Height          =   285
            Left            =   525
            TabIndex        =   10
            Top             =   630
            Width           =   1380
            _Version        =   458752
            _ExtentX        =   2434
            _ExtentY        =   503
            _StockProps     =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Reset_0         =   0   'False
            CompatibleVers_0=   458752
            NumEdit_0       =   1
            ClassName_1     =   "CCWNumEdit"
            opts_1          =   393278
            BorderStyle_1   =   1
            ButtonPosition_1=   1
            TextAlignment_1 =   2
            format_1        =   2
            ClassName_2     =   "CCWFormat"
            scale_1         =   3
            ClassName_3     =   "CCWScale"
            opts_3          =   65536
            dMax_3          =   10
            discInterval_3  =   1
            ValueVarType_1  =   5
            Value_Val_1     =   10
            IncValueVarType_1=   5
            IncValue_Val_1  =   1
            AccelIncVarType_1=   5
            AccelInc_Val_1  =   5
            RangeMinVarType_1=   5
            RangeMaxVarType_1=   5
            RangeMax_Val_1  =   100
            ButtonStyle_1   =   0
            Bindings_1      =   4
            ClassName_4     =   "CCWBindingHolderArray"
            Editor_4        =   5
            ClassName_5     =   "CCWBindingHolderArrayEditor"
            Owner_5         =   1
         End
         Begin CWUIControlsLib.CWNumEdit X_Min_Text 
            Height          =   285
            Left            =   525
            TabIndex        =   9
            Top             =   315
            Width           =   1380
            _Version        =   458752
            _ExtentX        =   2434
            _ExtentY        =   503
            _StockProps     =   4
            BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Reset_0         =   0   'False
            CompatibleVers_0=   458752
            NumEdit_0       =   1
            ClassName_1     =   "CCWNumEdit"
            opts_1          =   393278
            BorderStyle_1   =   1
            ButtonPosition_1=   1
            TextAlignment_1 =   2
            format_1        =   2
            ClassName_2     =   "CCWFormat"
            scale_1         =   3
            ClassName_3     =   "CCWScale"
            opts_3          =   65536
            dMax_3          =   10
            discInterval_3  =   1
            ValueVarType_1  =   5
            IncValueVarType_1=   5
            IncValue_Val_1  =   1
            AccelIncVarType_1=   5
            AccelInc_Val_1  =   5
            RangeMinVarType_1=   5
            RangeMaxVarType_1=   5
            RangeMax_Val_1  =   100
            ButtonStyle_1   =   0
            Bindings_1      =   4
            ClassName_4     =   "CCWBindingHolderArray"
            Editor_4        =   5
            ClassName_5     =   "CCWBindingHolderArrayEditor"
            Owner_5         =   1
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Max"
            Height          =   225
            Left            =   105
            TabIndex        =   4
            Top             =   630
            Width           =   330
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Min"
            Height          =   225
            Left            =   105
            TabIndex        =   3
            Top             =   315
            Width           =   330
         End
      End
   End
   Begin CWUIControlsLib.CWGraph CWGraph1 
      Height          =   8100
      Left            =   240
      TabIndex        =   0
      Top             =   105
      Width           =   6315
      _Version        =   458752
      _ExtentX        =   11139
      _ExtentY        =   14287
      _StockProps     =   71
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Reset_0         =   0   'False
      CompatibleVers_0=   458752
      Graph_0         =   1
      ClassName_1     =   "CCWGraphFrame"
      opts_1          =   62
      C[0]_1          =   16777215
      C[1]_1          =   -2147483643
      Event_1         =   2
      ClassName_2     =   "CCWGFPlotEvent"
      Owner_2         =   1
      Plots_1         =   3
      ClassName_3     =   "CCWDataPlots"
      Array_3         =   2
      Editor_3        =   4
      ClassName_4     =   "CCWGFPlotArrayEditor"
      Owner_4         =   1
      Array[0]_3      =   5
      ClassName_5     =   "CCWDataPlot"
      opts_5          =   7340095
      Name_5          =   "Plot-1"
      C[0]_5          =   16711680
      C[1]_5          =   16711680
      C[2]_5          =   16711680
      C[3]_5          =   16711680
      Event_5         =   2
      X_5             =   6
      ClassName_6     =   "CCWAxis"
      opts_6          =   2111
      Name_6          =   "XAxis"
      Orientation_6   =   134016
      format_6        =   7
      ClassName_7     =   "CCWFormat"
      Scale_6         =   8
      ClassName_8     =   "CCWScale"
      opts_8          =   24576
      rMin_8          =   38
      rMax_8          =   413
      dMin_8          =   1
      dMax_8          =   15
      discInterval_8  =   1
      Radial_6        =   0
      Enum_6          =   9
      ClassName_9     =   "CCWEnum"
      Array_9         =   15
      Editor_9        =   10
      ClassName_10    =   "CCWEnumArrayEditor"
      Owner_10        =   6
      Array[0]_9      =   11
      ClassName_11    =   "CCWEnumElt"
      DrawList_11     =   0
      varVarType_11   =   5
      Array[1]_9      =   12
      ClassName_12    =   "CCWEnumElt"
      opts_12         =   1
      Name_12         =   "1"
      DrawList_12     =   0
      varVarType_12   =   5
      Array[2]_9      =   13
      ClassName_13    =   "CCWEnumElt"
      DrawList_13     =   0
      varVarType_13   =   5
      Array[3]_9      =   14
      ClassName_14    =   "CCWEnumElt"
      opts_14         =   1
      Name_14         =   "2"
      DrawList_14     =   0
      varVarType_14   =   5
      Array[4]_9      =   15
      ClassName_15    =   "CCWEnumElt"
      DrawList_15     =   0
      varVarType_15   =   5
      Array[5]_9      =   16
      ClassName_16    =   "CCWEnumElt"
      opts_16         =   1
      Name_16         =   "3"
      DrawList_16     =   0
      varVarType_16   =   5
      Array[6]_9      =   17
      ClassName_17    =   "CCWEnumElt"
      DrawList_17     =   0
      varVarType_17   =   5
      Array[7]_9      =   18
      ClassName_18    =   "CCWEnumElt"
      opts_18         =   1
      Name_18         =   "4"
      DrawList_18     =   0
      varVarType_18   =   5
      Array[8]_9      =   19
      ClassName_19    =   "CCWEnumElt"
      DrawList_19     =   0
      varVarType_19   =   5
      Array[9]_9      =   20
      ClassName_20    =   "CCWEnumElt"
      opts_20         =   1
      Name_20         =   "5"
      DrawList_20     =   0
      varVarType_20   =   5
      Array[10]_9     =   21
      ClassName_21    =   "CCWEnumElt"
      DrawList_21     =   0
      varVarType_21   =   5
      Array[11]_9     =   22
      ClassName_22    =   "CCWEnumElt"
      opts_22         =   1
      Name_22         =   "6"
      DrawList_22     =   0
      varVarType_22   =   5
      Array[12]_9     =   23
      ClassName_23    =   "CCWEnumElt"
      DrawList_23     =   0
      varVarType_23   =   5
      Array[13]_9     =   24
      ClassName_24    =   "CCWEnumElt"
      opts_24         =   1
      Name_24         =   "7"
      DrawList_24     =   0
      varVarType_24   =   5
      Array[14]_9     =   25
      ClassName_25    =   "CCWEnumElt"
      DrawList_25     =   0
      varVarType_25   =   5
      Font_6          =   0
      tickopts_6      =   2711
      Caption_6       =   26
      ClassName_26    =   "CCWDrawObj"
      opts_26         =   62
      C[0]_26         =   -2147483640
      Image_26        =   27
      ClassName_27    =   "CCWTextImage"
      szText_27       =   "Number of Myosin Bound per Structural Unit (Actin7.TmTn)"
      style_27        =   16777217
      font_27         =   0
      Animator_26     =   0
      Blinker_26      =   0
      Y_5             =   28
      ClassName_28    =   "CCWAxis"
      opts_28         =   575
      Name_28         =   "YAxis-1"
      C[2]_28         =   0
      Orientation_28  =   2067
      format_28       =   29
      ClassName_29    =   "CCWFormat"
      Scale_28        =   30
      ClassName_30    =   "CCWScale"
      opts_30         =   122880
      rMin_30         =   11
      rMax_30         =   500
      dMax_30         =   10
      discInterval_30 =   1
      Radial_28       =   0
      Enum_28         =   31
      ClassName_31    =   "CCWEnum"
      Editor_31       =   32
      ClassName_32    =   "CCWEnumArrayEditor"
      Owner_32        =   28
      Font_28         =   0
      tickopts_28     =   2711
      major_28        =   1
      minor_28        =   0.5
      Caption_28      =   33
      ClassName_33    =   "CCWDrawObj"
      opts_33         =   62
      C[0]_33         =   0
      Image_33        =   34
      ClassName_34    =   "CCWTextImage"
      szText_34       =   "Fraction of Actin7.TmTn Units"
      style_34        =   16777217
      font_34         =   0
      Animator_33     =   0
      Blinker_33      =   0
      LineStyle_5     =   2
      LineWidth_5     =   1
      BasePlot_5      =   0
      DefaultXInc_5   =   1
      DefaultPlotPerRow_5=   -1  'True
      Array[1]_3      =   35
      ClassName_35    =   "CCWDataPlot"
      opts_35         =   7340095
      Name_35         =   "Plot-2"
      C[0]_35         =   255
      C[1]_35         =   255
      C[2]_35         =   255
      C[3]_35         =   255
      Event_35        =   2
      X_35            =   6
      Y_35            =   28
      LineStyle_35    =   2
      LineWidth_35    =   1
      BasePlot_35     =   0
      DefaultXInc_35  =   1
      DefaultPlotPerRow_35=   -1  'True
      Axes_1          =   36
      ClassName_36    =   "CCWAxes"
      Array_36        =   2
      Editor_36       =   37
      ClassName_37    =   "CCWGFAxisArrayEditor"
      Owner_37        =   1
      Array[0]_36     =   6
      Array[1]_36     =   28
      DefaultPlot_1   =   38
      ClassName_38    =   "CCWDataPlot"
      opts_38         =   7340095
      Name_38         =   "[Template]"
      C[0]_38         =   16776960
      C[1]_38         =   255
      C[2]_38         =   16711680
      C[3]_38         =   16776960
      Event_38        =   2
      X_38            =   6
      Y_38            =   28
      LineStyle_38    =   2
      LineWidth_38    =   1
      BasePlot_38     =   0
      DefaultXInc_38  =   1
      DefaultPlotPerRow_38=   -1  'True
      Cursors_1       =   39
      ClassName_39    =   "CCWCursors"
      Editor_39       =   40
      ClassName_40    =   "CCWGFCursorArrayEditor"
      Owner_40        =   1
      TrackMode_1     =   2
      GraphBackground_1=   0
      GraphFrame_1    =   41
      ClassName_41    =   "CCWDrawObj"
      opts_41         =   62
      C[0]_41         =   -2147483643
      C[1]_41         =   -2147483643
      Image_41        =   42
      ClassName_42    =   "CCWPictImage"
      opts_42         =   1280
      Rows_42         =   1
      Cols_42         =   1
      F_42            =   -2147483643
      ColorReplaceWith_42=   8421504
      ColorReplace_42 =   8421504
      Tolerance_42    =   2
      Animator_41     =   0
      Blinker_41      =   0
      PlotFrame_1     =   43
      ClassName_43    =   "CCWDrawObj"
      opts_43         =   62
      C[0]_43         =   -2147483643
      C[1]_43         =   16777215
      Image_43        =   44
      ClassName_44    =   "CCWPictImage"
      opts_44         =   1280
      Rows_44         =   1
      Cols_44         =   1
      Pict_44         =   1
      F_44            =   -2147483643
      B_44            =   16777215
      ColorReplaceWith_44=   8421504
      ColorReplace_44 =   8421504
      Tolerance_44    =   2
      Animator_43     =   0
      Blinker_43      =   0
      Caption_1       =   45
      ClassName_45    =   "CCWDrawObj"
      opts_45         =   62
      C[0]_45         =   -2147483640
      Image_45        =   46
      ClassName_46    =   "CCWTextImage"
      font_46         =   0
      Animator_45     =   0
      Blinker_45      =   0
      DefaultXInc_1   =   1
      DefaultXFirst_1 =   -1
      DefaultPlotPerRow_1=   -1  'True
      Bindings_1      =   47
      ClassName_47    =   "CCWBindingHolderArray"
      Editor_47       =   48
      ClassName_48    =   "CCWBindingHolderArrayEditor"
      Owner_48        =   1
      Annotations_1   =   49
      ClassName_49    =   "CCWAnnotations"
      Array_49        =   3
      Editor_49       =   50
      ClassName_50    =   "CCWAnnotationArrayEditor"
      Owner_50        =   1
      Array[0]_49     =   51
      ClassName_51    =   "CCWAnnotation"
      opts_51         =   63
      Name_51         =   "Annotation-1"
      CoordinateType_51=   2
      Plot_51         =   52
      ClassName_52    =   "CCWDataPlot"
      opts_52         =   4194367
      Name_52         =   "[Template]"
      C[0]_52         =   65280
      C[1]_52         =   255
      C[2]_52         =   16711680
      C[3]_52         =   16776960
      Event_52        =   2
      X_52            =   6
      Y_52            =   28
      LineStyle_52    =   1
      LineWidth_52    =   1
      BasePlot_52     =   0
      DefaultXInc_52  =   1
      DefaultPlotPerRow_52=   -1  'True
      Text_51         =   "S1 in A state"
      TextXPoint_51   =   250
      TextYPoint_51   =   40
      TextColor_51    =   16711680
      TextFont_51     =   53
      ClassName_53    =   "CCWFont"
      bFont_53        =   -1  'True
      BeginProperty Font_53 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_51 =   54
      ClassName_54    =   "CDataBuffer"
      Type_54         =   5
      m_cDims;_54     =   1
      ShapeYPoints_51 =   55
      ClassName_55    =   "CDataBuffer"
      Type_55         =   5
      m_cDims;_55     =   1
      ShapeFillColor_51=   16777215
      ShapeLineColor_51=   16777215
      ShapeLineWidth_51=   1
      ShapeLineStyle_51=   1
      ShapePointStyle_51=   10
      ShapeImage_51   =   56
      ClassName_56    =   "CCWDrawObj"
      opts_56         =   62
      Image_56        =   57
      ClassName_57    =   "CCWPictImage"
      opts_57         =   1280
      Rows_57         =   1
      Cols_57         =   1
      Pict_57         =   7
      F_57            =   -2147483633
      B_57            =   -2147483633
      ColorReplaceWith_57=   8421504
      ColorReplace_57 =   8421504
      Tolerance_57    =   2
      Animator_56     =   0
      Blinker_56      =   0
      ArrowColor_51   =   16777215
      ArrowWidth_51   =   1
      ArrowLineStyle_51=   1
      ArrowHeadStyle_51=   1
      Array[1]_49     =   58
      ClassName_58    =   "CCWAnnotation"
      opts_58         =   63
      Name_58         =   "Annotation-2"
      CoordinateType_58=   2
      Plot_58         =   52
      Text_58         =   "S1 in R state"
      TextXPoint_58   =   250
      TextYPoint_58   =   60
      TextColor_58    =   255
      TextFont_58     =   59
      ClassName_59    =   "CCWFont"
      bFont_59        =   -1  'True
      BeginProperty Font_59 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_58 =   60
      ClassName_60    =   "CDataBuffer"
      Type_60         =   5
      m_cDims;_60     =   1
      ShapeYPoints_58 =   61
      ClassName_61    =   "CDataBuffer"
      Type_61         =   5
      m_cDims;_61     =   1
      ShapeFillColor_58=   16777215
      ShapeLineColor_58=   16777215
      ShapeLineWidth_58=   1
      ShapeLineStyle_58=   1
      ShapePointStyle_58=   10
      ShapeImage_58   =   62
      ClassName_62    =   "CCWDrawObj"
      opts_62         =   62
      Image_62        =   63
      ClassName_63    =   "CCWPictImage"
      opts_63         =   1280
      Rows_63         =   1
      Cols_63         =   1
      Pict_63         =   7
      F_63            =   -2147483633
      B_63            =   -2147483633
      ColorReplaceWith_63=   8421504
      ColorReplace_63 =   8421504
      Tolerance_63    =   2
      Animator_62     =   0
      Blinker_62      =   0
      ArrowColor_58   =   16777215
      ArrowWidth_58   =   1
      ArrowLineStyle_58=   1
      ArrowHeadStyle_58=   1
      Array[2]_49     =   64
      ClassName_64    =   "CCWAnnotation"
      opts_64         =   63
      Name_64         =   "Annotation-3"
      CoordinateType_64=   2
      Plot_64         =   52
      Text_64         =   "Time: 0.000000 s"
      TextXPoint_64   =   250
      TextYPoint_64   =   80
      TextColor_64    =   0
      TextFont_64     =   65
      ClassName_65    =   "CCWFont"
      bFont_65        =   -1  'True
      BeginProperty Font_65 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_64 =   66
      ClassName_66    =   "CDataBuffer"
      Type_66         =   5
      m_cDims;_66     =   1
      ShapeYPoints_64 =   67
      ClassName_67    =   "CDataBuffer"
      Type_67         =   5
      m_cDims;_67     =   1
      ShapeFillColor_64=   16777215
      ShapeLineColor_64=   16777215
      ShapeLineWidth_64=   1
      ShapeLineStyle_64=   1
      ShapePointStyle_64=   10
      ShapeImage_64   =   68
      ClassName_68    =   "CCWDrawObj"
      opts_68         =   62
      Image_68        =   69
      ClassName_69    =   "CCWPictImage"
      opts_69         =   1280
      Rows_69         =   1
      Cols_69         =   1
      Pict_69         =   7
      F_69            =   -2147483633
      B_69            =   -2147483633
      ColorReplaceWith_69=   8421504
      ColorReplace_69 =   8421504
      Tolerance_69    =   2
      Animator_68     =   0
      Blinker_68      =   0
      ArrowColor_64   =   16777215
      ArrowWidth_64   =   1
      ArrowLineStyle_64=   1
      ArrowHeadStyle_64=   1
      AnnotationTemplate_1=   70
      ClassName_70    =   "CCWAnnotation"
      opts_70         =   63
      Name_70         =   "[Template]"
      Plot_70         =   52
      Text_70         =   "[Template]"
      TextXPoint_70   =   6.7
      TextYPoint_70   =   6.7
      TextColor_70    =   16777215
      TextFont_70     =   71
      ClassName_71    =   "CCWFont"
      bFont_71        =   -1  'True
      BeginProperty Font_71 {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ShapeXPoints_70 =   72
      ClassName_72    =   "CDataBuffer"
      Type_72         =   5
      m_cDims;_72     =   1
      ShapeYPoints_70 =   73
      ClassName_73    =   "CDataBuffer"
      Type_73         =   5
      m_cDims;_73     =   1
      ShapeFillColor_70=   16777215
      ShapeLineColor_70=   16777215
      ShapeLineWidth_70=   1
      ShapeLineStyle_70=   1
      ShapePointStyle_70=   10
      ShapeImage_70   =   74
      ClassName_74    =   "CCWDrawObj"
      opts_74         =   62
      Image_74        =   75
      ClassName_75    =   "CCWPictImage"
      opts_75         =   1280
      Rows_75         =   1
      Cols_75         =   1
      Pict_75         =   7
      F_75            =   -2147483633
      B_75            =   -2147483633
      ColorReplaceWith_75=   8421504
      ColorReplace_75 =   8421504
      Tolerance_75    =   2
      Animator_74     =   0
      Blinker_74      =   0
      ArrowColor_70   =   16777215
      ArrowWidth_70   =   1
      ArrowLineStyle_70=   1
      ArrowHeadStyle_70=   1
   End
End
Attribute VB_Name = "His_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim num_Rows As Long
Dim Counter As Long

Dim Time() As Double
Dim Col_0() As Double
Dim Col_R_1() As Double
Dim Col_R_2() As Double
Dim Col_R_3() As Double
Dim Col_R_4() As Double
Dim Col_R_5() As Double
Dim Col_R_6() As Double
Dim Col_R_7() As Double
Dim For_Plot_R(-1 To 14) As Double

Dim Maxim As Double
    
Private Sub Animation_Command_Click()
    Counter = Slider1.Value
    Timer1.Enabled = True
    Stop_Command.Enabled = True
    Animation_Command.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Graphs_Form.Histogram_Button.Enabled = True
End Sub

Private Sub Stop_Command_Click()
    Timer1.Enabled = False
    Stop_Command.Enabled = False
    Animation_Command.Enabled = True
End Sub

Private Sub Auto_Scale_Button_Click()
    CWGraph1.Axes.Item(1).AutoScaleNow
    CWGraph1.Axes.Item(2).AutoScaleNow

    X_Min_Text.Value = CWGraph1.Axes.Item(1).Minimum
    X_Max_Text.Value = CWGraph1.Axes.Item(1).Maximum

    Y1_Min_Text.Value = CWGraph1.Axes.Item(2).Minimum
    Y1_Max_Text.Value = CWGraph1.Axes.Item(2).Maximum
End Sub

Private Sub Form_Load()
    Dim Msg As String

    On Error GoTo ErrorHandler                                      ' Enable error-handling routine.

    Dim i As Long, j As Long
    Dim Filenumber As Integer                                   ' Import data to a file
    Dim SearchText As String

    num_Rows = 0
    Filenumber = FreeFile
    
        
    Open MGNUM_Form.Label16.Caption For Input As #Filenumber          ' Open file

    While Not EOF(Filenumber)
        Line Input #Filenumber, SearchText
        num_Rows = num_Rows + 1
    Wend
    
    If MGNUM_Form.Time_Option.Value = True Then

        ReDim Time(1 To num_Rows)
        ReDim Col_0(1 To num_Rows)
        ReDim Col_R_1(1 To num_Rows)
        ReDim Col_R_2(1 To num_Rows)
        ReDim Col_R_3(1 To num_Rows)
        ReDim Col_R_4(1 To num_Rows)
        ReDim Col_R_5(1 To num_Rows)
        ReDim Col_R_6(1 To num_Rows)
        ReDim Col_R_7(1 To num_Rows)

        Seek #Filenumber, 1
        'Line Input #FileNumber, SearchText          ' First Line
        i = 0
        While Not EOF(Filenumber)
            i = i + 1
            Input #Filenumber, Time(i), Col_0(i), Col_R_1(i), Col_R_2(i), _
                            Col_R_3(i), Col_R_4(i), Col_R_5(i), _
                            Col_R_6(i), Col_R_7(i)
        Wend
        Close #Filenumber

        Slider1.Min = 0
        Slider1.Max = num_Rows
        Slider1.SmallChange = 1
        Slider1.LargeChange = 25
        
        Slider1.Value = 1

        'For_Plot_R(-1) = 0
        'For_Plot_R(0) = Col_0(Slider1.Value)
        
        For_Plot_R(1) = 0
        For_Plot_R(2) = Col_R_1(Slider1.Value)
        For_Plot_R(3) = 0
        For_Plot_R(4) = Col_R_2(Slider1.Value)
        For_Plot_R(5) = 0
        For_Plot_R(6) = Col_R_3(Slider1.Value)
        For_Plot_R(7) = 0
        For_Plot_R(8) = Col_R_4(Slider1.Value)
        For_Plot_R(9) = 0
        For_Plot_R(10) = Col_R_5(Slider1.Value)
        For_Plot_R(11) = 0
        For_Plot_R(12) = Col_R_6(Slider1.Value)
        For_Plot_R(13) = 0
        For_Plot_R(14) = Col_R_7(Slider1.Value)

        Maxim = 0

        For i = 1 To num_Rows
            'If Col_0(i) > Maxim Then Maxim = Col_0(i)
            If Col_R_1(i) > Maxim Then Maxim = Col_R_1(i)
            If Col_R_2(i) > Maxim Then Maxim = Col_R_2(i)
            If Col_R_3(i) > Maxim Then Maxim = Col_R_3(i)
            If Col_R_4(i) > Maxim Then Maxim = Col_R_4(i)
            If Col_R_5(i) > Maxim Then Maxim = Col_R_5(i)
            If Col_R_6(i) > Maxim Then Maxim = Col_R_6(i)
            If Col_R_7(i) > Maxim Then Maxim = Col_R_7(i)
        Next i

        CWGraph1.ClearData
        CWGraph1.Plots.Item(1).PlotY For_Plot_R
        
        CWGraph1.Annotations.Item(1).Caption.Text = ""
        CWGraph1.Annotations.Item(2).Caption.Text = ""
        CWGraph1.Annotations.Item(3).Caption.Text = "Time: " & Format(Time(Slider1.Value), "0.#####0") & " s"

        CWGraph1.Axes.Item(1).AutoScaleNow
        'CWGraph1.Axes.Item(1).Minimum = 0
        CWGraph1.Axes.Item(2).Minimum = 0
        CWGraph1.Axes.Item(2).Maximum = Maxim

        X_Min_Text.IncDecValue = (CWGraph1.Axes.Item(1).Maximum - CWGraph1.Axes.Item(1).Minimum) / 50
        X_Max_Text.IncDecValue = (CWGraph1.Axes.Item(1).Maximum - CWGraph1.Axes.Item(1).Minimum) / 50

        X_Min_Text.AccelTime = 5
        X_Min_Text.AccelInc = X_Min_Text.IncDecValue * 5
        X_Max_Text.AccelTime = 5
        X_Max_Text.AccelInc = X_Max_Text.IncDecValue * 5

        X_Min_Text.Value = CWGraph1.Axes.Item(1).Minimum
        X_Max_Text.Value = CWGraph1.Axes.Item(1).Maximum

        Y1_Min_Text.IncDecValue = (CWGraph1.Axes.Item(2).Maximum - CWGraph1.Axes.Item(2).Minimum) / 50
        Y1_Max_Text.IncDecValue = (CWGraph1.Axes.Item(2).Maximum - CWGraph1.Axes.Item(2).Minimum) / 50

        Y1_Min_Text.AccelTime = 5
        Y1_Min_Text.AccelInc = Y1_Min_Text.IncDecValue * 5
        Y1_Max_Text.AccelTime = 5
        Y1_Max_Text.AccelInc = Y1_Max_Text.IncDecValue * 5

        Y1_Min_Text.Value = CWGraph1.Axes.Item(2).Minimum
        Y1_Max_Text.Value = CWGraph1.Axes.Item(2).Maximum

    End If

    Exit Sub                                                        ' Exit to avoid handler.

ErrorHandler:                                                       ' Error-handling routine.

    'Display unanticipated error message.
    Msg = "Error " & Err.Number & ": " & Err.Description
    MsgBox Msg, vbExclamation

    'Resume Next                                                    ' Resume execution at same line
                                                                    ' that caused the error.

End Sub

Private Sub Slider1_Scroll()
    If Timer1.Enabled = False Then
        'For_Plot_R(-1) = 0
        'For_Plot_R(0) = Col_0(Slider1.Value)
        For_Plot_R(1) = 0
        For_Plot_R(2) = Col_R_1(Slider1.Value)
        For_Plot_R(3) = 0
        For_Plot_R(4) = Col_R_2(Slider1.Value)
        For_Plot_R(5) = 0
        For_Plot_R(6) = Col_R_3(Slider1.Value)
        For_Plot_R(7) = 0
        For_Plot_R(8) = Col_R_4(Slider1.Value)
        For_Plot_R(9) = 0
        For_Plot_R(10) = Col_R_5(Slider1.Value)
        For_Plot_R(11) = 0
        For_Plot_R(12) = Col_R_6(Slider1.Value)
        For_Plot_R(13) = 0
        For_Plot_R(14) = Col_R_7(Slider1.Value)

        CWGraph1.ClearData
        CWGraph1.Plots.Item(1).PlotY For_Plot_R
        
        CWGraph1.Axes.Item(1).AutoScaleNow
        'CWGraph1.Axes.Item(1).Minimum = 0
        CWGraph1.Axes.Item(2).Minimum = 0
        CWGraph1.Axes.Item(2).Maximum = Maxim

        X_Min_Text.Value = CWGraph1.Axes.Item(1).Minimum
        X_Max_Text.Value = CWGraph1.Axes.Item(1).Maximum

        Y1_Min_Text.Value = CWGraph1.Axes.Item(2).Minimum
        Y1_Max_Text.Value = CWGraph1.Axes.Item(2).Maximum

        Counter = Slider1.Value
        CWGraph1.Annotations.Item(1).Caption.Text = ""
        CWGraph1.Annotations.Item(2).Caption.Text = ""
        CWGraph1.Annotations.Item(3).Caption.Text = "Time: " & Format(Time(Slider1.Value), "0.#####0") & " s"
    End If
End Sub

Private Sub Timer1_Timer()

    If Counter = 1 Then
        CWGraph1.Axes.Item(1).AutoScaleNow
        CWGraph1.Axes.Item(2).Minimum = 0
        CWGraph1.Axes.Item(2).Maximum = Maxim
        'CWGraph1.Axes.Item(1).Minimum = 0
        
        X_Min_Text.Value = CWGraph1.Axes.Item(1).Minimum
        X_Max_Text.Value = CWGraph1.Axes.Item(1).Maximum

        Y1_Min_Text.Value = CWGraph1.Axes.Item(2).Minimum
        Y1_Max_Text.Value = CWGraph1.Axes.Item(2).Maximum
    End If

    If Counter <> num_Rows Then

        Counter = Counter + 1

        Slider1.Value = Counter
        'For_Plot_R(-1) = 0
        'For_Plot_R(0) = Col_0(Counter)
        For_Plot_R(1) = 0
        For_Plot_R(2) = Col_R_1(Counter)
        For_Plot_R(3) = 0
        For_Plot_R(4) = Col_R_2(Counter)
        For_Plot_R(5) = 0
        For_Plot_R(6) = Col_R_3(Counter)
        For_Plot_R(7) = 0
        For_Plot_R(8) = Col_R_4(Counter)
        For_Plot_R(9) = 0
        For_Plot_R(10) = Col_R_5(Counter)
        For_Plot_R(11) = 0
        For_Plot_R(12) = Col_R_6(Counter)
        For_Plot_R(13) = 0
        For_Plot_R(14) = Col_R_7(Counter)

        CWGraph1.ClearData
        CWGraph1.Plots.Item(1).PlotY For_Plot_R
        
        CWGraph1.Annotations.Item(1).Caption.Text = ""
        CWGraph1.Annotations.Item(2).Caption.Text = ""
        CWGraph1.Annotations.Item(3).Caption.Text = "Time: " & Format(Time(Slider1.Value), "0.#####0") & " s"

    Else

        Timer1.Enabled = False
        Stop_Command.Enabled = False
        Animation_Command.Enabled = True
        Slider1.Value = 1

    End If

End Sub

Private Sub X_Max_Text_ValueChanged(Value As Variant, PreviousValue As Variant, ByVal OutOfRange As Boolean)
    CWGraph1.Axes.Item(1).Maximum = Value
    X_Min_Text.Value = CWGraph1.Axes.Item(1).Minimum
End Sub

Private Sub X_Min_Text_ValueChanged(Value As Variant, PreviousValue As Variant, ByVal OutOfRange As Boolean)
    CWGraph1.Axes.Item(1).Minimum = Value
    X_Max_Text.Value = CWGraph1.Axes.Item(1).Maximum
End Sub

Private Sub Y1_Max_Text_ValueChanged(Value As Variant, PreviousValue As Variant, ByVal OutOfRange As Boolean)
    CWGraph1.Axes.Item(2).Maximum = Value
    Y1_Min_Text.Value = CWGraph1.Axes.Item(2).Minimum
End Sub

Private Sub Y1_Min_Text_ValueChanged(Value As Variant, PreviousValue As Variant, ByVal OutOfRange As Boolean)
    CWGraph1.Axes.Item(2).Minimum = Value
    Y1_Max_Text.Value = CWGraph1.Axes.Item(2).Maximum
End Sub

