import { Component, OnInit, QueryList, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { DavidService } from '../../service/david.service';
import { environment } from '../../../environments/environment.prod';
import { switchMap } from 'rxjs/operators';
import { timer } from 'rxjs';
import { Chart } from 'chart.js'

const host = environment.server.host;

export interface PeriodicElement {
  last: string;
  average: string;
  std_dev: string;
}

export interface Graph {
  datasets: Array<any>;
  options: any;
  chartType: string;
  colors: Array<any>;
  legend: boolean;
  labels: Array<any>;
  visibility: Boolean;
}

@Component({
  selector: 'app-david',
  templateUrl: './david.component.html',
  styleUrls: ['./david.component.scss']
})
export class DavidComponent implements OnInit, AfterViewInit {

  angForm: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    tni: new FormControl(''),
    tni_fiting: new FormControl(''),
    tni_guess_of_params1: new FormControl(''),
    tni_guess_of_params2: new FormControl(''),
    tni_lower_bound: new FormControl(''),
    tni_upper_bound: new FormControl(''),
    gama: new FormControl(''),
    gama_fiting: new FormControl(''),
    gama_guess_of_params1: new FormControl(''),
    gama_guess_of_params2: new FormControl(''),
    gama_lower_bound: new FormControl(''),
    gama_upper_bound: new FormControl(''),
    phm1: new FormControl(''),
    phm2: new FormControl(''),
    cpl: new FormControl(''),
    kkm1: new FormControl(''),
    kkm2: new FormControl(''),
    km1p: new FormControl(''),
    id1: new FormControl(''),
    id2: new FormControl(''),
    id3: new FormControl(''),
    dm1: new FormControl(''),
    alpha: new FormControl(''),
    epsilon: new FormControl(''),
    number_of_iteration: new FormControl(''),
    dt: new FormControl(''),
    no_of_repeats: new FormControl('')
  });

  constructor(private fb: FormBuilder, private DavidService: DavidService) { }

  public david: any;
  public dataSource;
  public sensitivityMatrix;
  public inputData = this.profileForm;;
  public dugmeRun: Boolean = false;
  public displayedColumns: string[];
  public isVisible = true;
  public run_button = "RUN";
  fileToUpload: File = null;

  public experimentalModelGraph
  public parametersConvergance

  private colors = [
    { borderColor: 'blue', backgroundColor: 'transparent' },
    { borderColor: 'purple', backgroundColor: 'transparent' },
    { borderColor: 'yellow', backgroundColor: 'transparent' },
    { borderColor: 'orange', backgroundColor: 'transparent' },
    { borderColor: 'green', backgroundColor: 'transparent' },
    { borderColor: 'black', backgroundColor: 'transparent' },
    { borderColor: 'brown', backgroundColor: 'transparent' }
  ]

  ngOnInit() {
    this.DavidService.getInputData('test21').subscribe(data => {
      if (data) {
        this.inputData = data;
      }
    });
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {})
    this.parametersConvergance = new Chart('parametersConvergance', {})
  }

  ngAfterViewInit(): void {

  }

  returnData(simulationID) {
    let subrscription =
      timer(0, 10000).pipe(
        switchMap(() => this.DavidService.returnData(simulationID))
      ).subscribe(data => {
        if (data !== null && data.estimatedParams !== null && data.statusCode < 400) {
          const a_t = data.a_t;
          const models = data.model;

          const vidljivo = [
            Boolean(data.parameters.tni_fiting),
            Boolean(data.parameters.gama_fiting)
          ];

          const podaci = [
            { data: data.estimatedParams.kb, label: 'Tni' },
            { data: data.estimatedParams.kb_minus, label: 'Gama' }
          ];

          let dataset = []
          this.experimentAndModelGraph(a_t, models)
          dataset.push({
            label: 'Error',
            data: data.estimatedParams.error,
            fill: false,
            lineTension: 0.2,
            borderColor: 'red',
            borderWidth: 1,
          })
          let j = 0;
          for (let i = 0; i < 2; i++) {
            if (vidljivo[i]) {
              dataset.push({
                label: podaci[i].label,
                data: podaci[i].data,
                fill: false,
                lineTension: 0.2,
                borderColor: this.colors[j].borderColor,
                borderWidth: 1,
              })
              j++;
            }
          }
          this.parametersConvergancePrepare(dataset)

          this.isVisible = true;

          for (let i = 1; i < data.estimatedParams.estimated_values.length; i++) {
            data.estimatedParadata.estimatedParamsstimated_values[i][1] = data.estimatedParams.estimated_values[i][1].toFixed(6)
            data.estimatedParams.estimated_values[i][2] = data.estimatedParams.estimated_values[i][2].toFixed(6)
            data.estimatedParams.estimated_values[i][3] = data.estimatedParams.estimated_values[i][3].toFixed(6)
          }

          this.dataSource = data.estimatedParams.estimated_values;
          this.sensitivityMatrix = data.estimatedParams.sensitivity;
          this.dugmeRun = false;
          this.run_button = "RUN";
          subrscription.unsubscribe()
        }
      })
  }

  onSubmit() {
    this.dugmeRun = true;
    this.run_button = "RUNNING";
    this.isVisible = false;
    const body = this.profileForm.value;
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    this.DavidService.upload(this.fileToUpload).subscribe(upload => {
      this.DavidService.getData(body).subscribe(data => {
        this.returnData(data.simulationId)
      })
    })
  }

  download() {
    window.open(host + 'davids/users/test21/download');
  }

  saveSimulation() {
    const body = this.profileForm.value;
    this.DavidService.saveSimulation(body).subscribe(data => {
      console.log(data);
    })
  }

  openSaveSimulation(file) {
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    const fileData = file.target.files
    var reader = new FileReader();
    reader.readAsText(fileData[0]);
    reader.onload = (_event) => {
      const data = JSON.parse(reader.result.toString());
      const a_t = data.a_t;
      const models = data.model;
      if (data.estimatedParams) {
        const vidljivo = [
          Boolean(data.parameters.tni_fiting),
          Boolean(data.parameters.gama_fiting)
        ];

        const podaci = [
          { data: data.estimatedParams.kb, label: 'Tni' },
          { data: data.estimatedParams.kb_minus, label: 'Gama' }
        ];

        let dataset = []
        this.experimentAndModelGraph(a_t, models)
        dataset.push({
          label: 'Error',
          data: data.estimatedParams.error,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        })
        let j = 0;
        for (let i = 0; i < 2; i++) {
          if (vidljivo[i]) {
            dataset.push({
              label: podaci[i].label,
              data: podaci[i].data,
              fill: false,
              lineTension: 0.2,
              borderColor: this.colors[j].borderColor,
              borderWidth: 1,
            })
            j++;
          }
        }
        this.parametersConvergancePrepare(dataset)

        this.isVisible = true;

        for (let i = 1; i < data.estimatedParams.estimated_values.length; i++) {
          data.estimatedParadata.estimatedParamsstimated_values[i][1] = data.estimatedParams.estimated_values[i][1].toFixed(6)
          data.estimatedParams.estimated_values[i][2] = data.estimatedParams.estimated_values[i][2].toFixed(6)
          data.estimatedParams.estimated_values[i][3] = data.estimatedParams.estimated_values[i][3].toFixed(6)
        }

        this.dataSource = data.estimatedParams.estimated_values;
        this.sensitivityMatrix = data.estimatedParams.sensitivity;
      }
    };
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onCheckboxChange(box1: string, box2: string) {
    if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
      if (this.inputData[box1])
        this.inputData[box2] = false;
    }
  }

  experimentAndModelGraph(experiment: any, model: any) {
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {
      type: 'line',
      data: {
        datasets: [{
          label: 'Model',
          data: model,
          fill: false,
          lineTension: 0.2,
          borderColor: 'blue',
          borderWidth: 1,
          showLine: false
        }, {
          label: 'Experiment',
          data: experiment,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Actin concentration [microM]'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Fractional Fluorescence'
            }
          }]
        }
      }
    })
  }

  parametersConvergancePrepare(datasets: any) {
    this.parametersConvergance = new Chart('parametersConvergance', {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Iteration'
            }
          }], yAxes: [{
            ticks: {
              callback: function (value) {
                return value.toExponential(2)
              }
            }
          }]
        }
      }
    })
  }
}

