import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StochasticalComponent } from './stochastical.component';

const routes: Routes = [
  {
    path: '',
    component: StochasticalComponent,
    data: {
      title: 'Stochastical'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StochasticalRoutingModule {}
