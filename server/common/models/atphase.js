'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const multer = require('multer');
const pravljenjeSensitivityMatrice = require('../../middleware/sensirivity_matrix');
const zippingFile = require('../../middleware/zipping');
const returnOutputData = require('../../middleware/return_data');
const winston = require('winston')
const fsExtra = require('fs-extra')

//Local include

module.exports = function (atphase) {

  const exeSimulation = (id, runSimulationId, parameters) => {
    const path = `./${runSimulationId}`

    if (!fs.existsSync(path)) {
      fs.mkdirSync(path);
    }
    const archive = {
      simulationId: runSimulationId,
      simulationType: 'ATPase DLS',
      description: parameters.description,
      status: 'In Progress',
      runDate: new Date(),
      simulation: 'widgets'
    }
    return loopback.getModel('arhive').create(archive)
      .then(arhiveData => {
        return fsExtra.copy('./atphase/', path, (err) => {
          if (err) {
            winston.error(err)
            const response = {
              statusCode: 400,
              message: 'Simulation failed'
            }
            return arhiveData.updateAttributes({ status: 'Failed' })
              .then(() => {
                return response
              })
          }
          //setup data for start simulation
          const fileName = 'ParaEstimATPase.exe';
          const params = [];
          return exec(fileName, params, { cwd: path }, (err, data, error) => {
            winston.info(data)
            if (err) {
              winston.error(err)
              winston.error(error)
              const response = {
                statusCode: 400,
                message: 'Simulation failed'
              }
              return arhiveData.updateAttributes({ status: 'Failed' })
                .then(() => {
                  return response
                })
            }
            else {
              winston.info('Simulation atphase successfully finish')
              //checked estimation params
              const cekiranaPolja = [Boolean(parameters.ka_fiting), Boolean(parameters.ka_minus_fiting), Boolean(parameters.kpi_fiting), Boolean(parameters.kpi_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting),
              Boolean(parameters.kt_star_fiting), Boolean(parameters.kt_star_minus_fiting), Boolean(parameters.kt_star_star_fiting), Boolean(parameters.kt_star_star_minus_fiting), Boolean(parameters.kh_fiting), Boolean(parameters.kh_minus_fiting),
              Boolean(parameters.kah_fiting), Boolean(parameters.kah_minus_fiting)];
              //save input data
              returnOutputData.removeData('atphase')
              //call function for create output data
              return returnOutputData.returnData('atphase', cekiranaPolja, parameters.number_of_iteration)
                .then(outputData => {
                  //call function for zip output data
                  zippingFile.zipFile('atphase', cekiranaPolja, parameters, outputData);
                  //return output data
                  const options = {
                    userID: id,
                    input_id: runSimulationId,
                    ...outputData
                  }

                  return loopback.getModel('atphase_eee').create(options)
                    .then(datas => {
                      return arhiveData.updateAttributes({ status: 'Complete' })
                        .then(() => {
                          return fsExtra.remove(path)
                            .then(() => {
                              return datas
                            })
                        })
                    })
                })
            }
          })
        })
      })

  }

  //function for run simulation
  atphase.runSimulation = (id, parameters) => {
    //create input file
    let writeStream = fs.createWriteStream('./atphase/tmtn-ca.dat');
    writeStream.write(parameters.ka + '\r\n');
    writeStream.write(parameters.ka_minus + '\r\n');
    writeStream.write(parameters.kpi + '\r\n');
    writeStream.write(parameters.kpi_minus + '\r\n');
    writeStream.write(parameters.kd_star + '\r\n');
    writeStream.write(parameters.kd_star_minus + '\r\n');
    writeStream.write(parameters.kd + '\r\n');
    writeStream.write(parameters.kd_minus + '\r\n');
    writeStream.write(parameters.kt + '\r\n');
    writeStream.write(parameters.kt_minus + '\r\n');
    writeStream.write(parameters.kt_star + '\r\n');
    writeStream.write(parameters.kt_star_minus + '\r\n');
    writeStream.write(parameters.kt_star_star + '\r\n');
    writeStream.write(parameters.kt_star_star_minus + '\r\n');
    writeStream.write(parameters.kh + '\r\n');
    writeStream.write(parameters.kh_minus + '\r\n');
    writeStream.write(parameters.kah + '\r\n');
    writeStream.write(parameters.kah_minus + '\r\n');
    writeStream.write(parameters.pi + '\r\n');
    writeStream.write(parameters.dpi + '\r\n');
    writeStream.write(parameters.tpi + '\r\n');
    writeStream.write(parameters.actin + '\r\n');
    writeStream.write(parameters.myosin + '\r\n');

    let writeInputWrap = fs.createWriteStream('./atphase/input_wrap')
    writeInputWrap.write('14\r\n');
    writeInputWrap.write((parameters.ka_fiting && parameters.ka_fiting !== undefined ? parameters.ka_guess_of_params1 : parameters.ka) + '\r\n');
    writeInputWrap.write((parameters.ka_fiting && parameters.ka_fiting !== undefined ? parameters.ka_guess_of_params2 : parameters.ka) + '\r\n');
    writeInputWrap.write((parameters.ka_fiting && parameters.ka_fiting !== undefined ? parameters.ka_lower_bound : parameters.ka) + '\r\n');
    writeInputWrap.write((parameters.ka_fiting && parameters.ka_fiting !== undefined ? parameters.ka_upper_bound : parameters.ka) + '\r\n');
    writeInputWrap.write((parameters.ka_minus_fiting && parameters.ka_minus_fiting !== undefined ? parameters.ka_minus_guess_of_params1 : parameters.ka_minus) + '\r\n');
    writeInputWrap.write((parameters.ka_minus_fiting && parameters.ka_minus_fiting !== undefined ? parameters.ka_minus_guess_of_params2 : parameters.ka_minus) + '\r\n');
    writeInputWrap.write((parameters.ka_minus_fiting && parameters.ka_minus_fiting !== undefined ? parameters.ka_minus_lower_bound : parameters.ka_minus) + '\r\n');
    writeInputWrap.write((parameters.ka_minus_fiting && parameters.ka_minus_fiting !== undefined ? parameters.ka_minus_upper_bound : parameters.ka_minus) + '\r\n');
    writeInputWrap.write((parameters.kpi_fiting && parameters.kpi_fiting !== undefined ? parameters.kpi_guess_of_params1 : parameters.kpi) + '\r\n');
    writeInputWrap.write((parameters.kpi_fiting && parameters.kpi_fiting !== undefined ? parameters.kpi_guess_of_params2 : parameters.kpi) + '\r\n');
    writeInputWrap.write((parameters.kpi_fiting && parameters.kpi_fiting !== undefined ? parameters.kpi_lower_bound : parameters.kpi) + '\r\n');
    writeInputWrap.write((parameters.kpi_fiting && parameters.kpi_fiting !== undefined ? parameters.kpi_upper_bound : parameters.kpi) + '\r\n');
    writeInputWrap.write((parameters.kpi_minus_fiting && parameters.kpi_minus_fiting !== undefined ? parameters.kpi_minus_guess_of_params1 : parameters.kpi_minus) + '\r\n');
    writeInputWrap.write((parameters.kpi_minus_fiting && parameters.kpi_minus_fiting !== undefined ? parameters.kpi_minus_guess_of_params2 : parameters.kpi_minus) + '\r\n');
    writeInputWrap.write((parameters.kpi_minus_fiting && parameters.kpi_minus_fiting !== undefined ? parameters.kpi_minus_lower_bound : parameters.kpi_minus) + '\r\n');
    writeInputWrap.write((parameters.kpi_minus_fiting && parameters.kpi_minus_fiting !== undefined ? parameters.kpi_minus_upper_bound : parameters.kpi_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_fiting && parameters.kt_fiting !== undefined ? parameters.kt_guess_of_params1 : parameters.kt) + '\r\n');
    writeInputWrap.write((parameters.kt_fiting && parameters.kt_fiting !== undefined ? parameters.kt_guess_of_params2 : parameters.kt) + '\r\n');
    writeInputWrap.write((parameters.kt_fiting && parameters.kt_fiting !== undefined ? parameters.kt_lower_bound : parameters.kt) + '\r\n');
    writeInputWrap.write((parameters.kt_fiting && parameters.kt_fiting !== undefined ? parameters.kt_upper_bound : parameters.kt) + '\r\n');
    writeInputWrap.write((parameters.kt_minus_fiting && parameters.kt_minus_fiting !== undefined ? parameters.kt_minus_guess_of_params1 : parameters.kt_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_minus_fiting && parameters.kt_minus_fiting !== undefined ? parameters.kt_minus_guess_of_params2 : parameters.kt_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_minus_fiting && parameters.kt_minus_fiting !== undefined ? parameters.kt_minus_lower_bound : parameters.kt_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_minus_fiting && parameters.kt_minus_fiting !== undefined ? parameters.kt_minus_upper_bound : parameters.kt_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_star_fiting && parameters.kt_star_fiting !== undefined ? parameters.kt_star_guess_of_params1 : parameters.kt_star) + '\r\n');
    writeInputWrap.write((parameters.kt_star_fiting && parameters.kt_star_fiting !== undefined ? parameters.kt_star_guess_of_params2 : parameters.kt_star) + '\r\n');
    writeInputWrap.write((parameters.kt_star_fiting && parameters.kt_star_fiting !== undefined ? parameters.kt_star_lower_bound : parameters.kt_star) + '\r\n');
    writeInputWrap.write((parameters.kt_star_fiting && parameters.kt_star_fiting !== undefined ? parameters.kt_star_upper_bound : parameters.kt_star) + '\r\n');
    writeInputWrap.write((parameters.kt_star_minus_fiting && parameters.kt_star_minus_fiting !== undefined ? parameters.kt_star_minus_guess_of_params1 : parameters.kt_star_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_star_minus_fiting && parameters.kt_star_minus_fiting !== undefined ? parameters.kt_star_minus_guess_of_params2 : parameters.kt_star_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_star_minus_fiting && parameters.kt_star_minus_fiting !== undefined ? parameters.kt_star_minus_lower_bound : parameters.kt_star_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_star_minus_fiting && parameters.kt_star_minus_fiting !== undefined ? parameters.kt_star_minus_upper_bound : parameters.kt_star_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_star_star_fiting && parameters.kt_star_star_fiting !== undefined ? parameters.kt_star_star_guess_of_params1 : parameters.kt_star_star) + '\r\n');
    writeInputWrap.write((parameters.kt_star_star_fiting && parameters.kt_star_star_fiting !== undefined ? parameters.kt_star_star_guess_of_params2 : parameters.kt_star_star) + '\r\n');
    writeInputWrap.write((parameters.kt_star_star_fiting && parameters.kt_star_star_fiting !== undefined ? parameters.kt_star_star_lower_bound : parameters.kt_star_star) + '\r\n');
    writeInputWrap.write((parameters.kt_star_star_fiting && parameters.kt_star_star_fiting !== undefined ? parameters.kt_star_star_upper_bound : parameters.kt_star_star) + '\r\n');
    writeInputWrap.write((parameters.kt_star_star_minus_fiting && parameters.kt_star_star_minus_fiting !== undefined ? parameters.kt_star_star_minus_guess_of_params1 : parameters.kt_star_star_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_star_star_minus_fiting && parameters.kt_star_star_minus_fiting !== undefined ? parameters.kt_star_star_minus_guess_of_params2 : parameters.kt_star_star_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_star_star_minus_fiting && parameters.kt_star_star_minus_fiting !== undefined ? parameters.kt_star_star_minus_lower_bound : parameters.kt_star_star_minus) + '\r\n');
    writeInputWrap.write((parameters.kt_star_star_minus_fiting && parameters.kt_star_star_minus_fiting !== undefined ? parameters.kt_star_star_minus_upper_bound : parameters.kt_star_star_minus) + '\r\n');
    writeInputWrap.write((parameters.kh_fiting && parameters.kh_fiting !== undefined ? parameters.kh_guess_of_params1 : parameters.kh) + '\r\n');
    writeInputWrap.write((parameters.kh_fiting && parameters.kh_fiting !== undefined ? parameters.kh_guess_of_params2 : parameters.kh) + '\r\n');
    writeInputWrap.write((parameters.kh_fiting && parameters.kh_fiting !== undefined ? parameters.kh_lower_bound : parameters.kh) + '\r\n');
    writeInputWrap.write((parameters.kh_fiting && parameters.kh_fiting !== undefined ? parameters.kh_upper_bound : parameters.kh) + '\r\n');
    writeInputWrap.write((parameters.kh_minus_fiting && parameters.kh_minus_fiting !== undefined ? parameters.kh_minus_guess_of_params1 : parameters.kh_minus) + '\r\n');
    writeInputWrap.write((parameters.kh_minus_fiting && parameters.kh_minus_fiting !== undefined ? parameters.kh_minus_guess_of_params2 : parameters.kh_minus) + '\r\n');
    writeInputWrap.write((parameters.kh_minus_fiting && parameters.kh_minus_fiting !== undefined ? parameters.kh_minus_lower_bound : parameters.kh_minus) + '\r\n');
    writeInputWrap.write((parameters.kh_minus_fiting && parameters.kh_minus_fiting !== undefined ? parameters.kh_minus_upper_bound : parameters.kh_minus) + '\r\n');
    writeInputWrap.write((parameters.kah_fiting && parameters.kah_fiting !== undefined ? parameters.kah_guess_of_params1 : parameters.kah) + '\r\n');
    writeInputWrap.write((parameters.kah_fiting && parameters.kah_fiting !== undefined ? parameters.kah_guess_of_params2 : parameters.kah) + '\r\n');
    writeInputWrap.write((parameters.kah_fiting && parameters.kah_fiting !== undefined ? parameters.kah_lower_bound : parameters.kah) + '\r\n');
    writeInputWrap.write((parameters.kah_fiting && parameters.kah_fiting !== undefined ? parameters.kah_upper_bound : parameters.kah) + '\r\n');
    writeInputWrap.write((parameters.kah_minus_fiting && parameters.kah_minus_fiting !== undefined ? parameters.kah_minus_guess_of_params1 : parameters.kah_minus) + '\r\n');
    writeInputWrap.write((parameters.kah_minus_fiting && parameters.kah_minus_fiting !== undefined ? parameters.kah_minus_guess_of_params2 : parameters.kah_minus) + '\r\n');
    writeInputWrap.write((parameters.kah_minus_fiting && parameters.kah_minus_fiting !== undefined ? parameters.kah_minus_lower_bound : parameters.kah_minus) + '\r\n');
    writeInputWrap.write((parameters.kah_minus_fiting && parameters.kah_minus_fiting !== undefined ? parameters.kah_minus_upper_bound : parameters.kah_minus) + '\r\n');
    writeInputWrap.write(parameters.epsilon + '\r\n');
    writeInputWrap.write(parameters.number_of_iteration + '\r\n');
    writeInputWrap.write('\'a_t.txt\'' + '\r\n');
    writeInputWrap.write('\'kkk.dat\'' + '\r\n');
    writeInputWrap.write('\'eee.dat\'' + '\r\n');
    writeInputWrap.write('\'rrr.dat\'' + '\r\n');
    writeInputWrap.end('\'ATPaseMultiSimA\'' + '\r\n');

    const options = {
      userID: id,
      ...parameters
    }
    return loopback.getModel('atphase').create(options)
      .then(data => {
        exeSimulation(id, data._id, parameters)
        winston.info('Simulation atphase successfully start')
        const response = {
          statusCode: 200,
          message: "Successfull run simulation",
          simulationId: data._id
        }

        return response
      });
  }

  atphase.saveData = (id, parameters) => {
    //checked estimation params
    const cekiranaPolja = [Boolean(parameters.ka_fiting), Boolean(parameters.ka_minus_fiting), Boolean(parameters.kpi_fiting), Boolean(parameters.kpi_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting),
    Boolean(parameters.kt_star_fiting), Boolean(parameters.kt_star_minus_fiting), Boolean(parameters.kt_star_star_fiting), Boolean(parameters.kt_star_star_minus_fiting), Boolean(parameters.kh_fiting), Boolean(parameters.kh_minus_fiting),
    Boolean(parameters.kah_fiting), Boolean(parameters.kah_minus_fiting)];

    //call function for create sensitivity matrix
    pravljenjeSensitivityMatrice.createMatrix('./atphase/new_eee.dat', './atphase/new_kkk.dat', './atphase/new_rrr.dat', parameters.number_of_iteration, cekiranaPolja);

    return returnOutputData.returnData('atphase', cekiranaPolja, parameters.number_of_iteration)
      .then(outputData => {
        if (!outputData && outputData !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for atphase simulation'
          }
          winston.info('Not found output data for atphase simulation')
          return response
        }
        const atphaseParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel(atphase).create(atphaseParams)
          .then(atp => {
            const options = {
              userID: id,
              input_id: atp._id,
              ...outputData
            }
            return loopback.getModel('atphase_eee').create(options)
              .then(data => {
                winston.info('Simulation atphase successfully save data')
                return data;
              })
          })
      })
  }

  atphase.returnSaveData = (id) => {
    let response;
    return loopback.getModel('atphase_eee').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found atphase simulation'
          }
          winston.info('Not found return data for atphase simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id
        return loopback.getModel('atphase').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found atphase simulation'
              }
              winston.info('Not found input data for atphase simulation')
              return response
            }
            response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response
          })
      })
  }

  atphase.returnData = (simulationId) => {
    let response;
    return loopback.getModel('atphase').findOne({ where: { _id: simulationId } })
      .then(data => {
        if (!data) {
          response = {
            statusCode: 404,
            message: `Not found atphase simulation with simulation id: ${simulationId}`
          }
          winston.info(`Not found input data for atphase simulation with simulation id: ${simulationId}`)
          return response
        }
        return loopback.getModel('atphase_eee').findOne({ where: { input_id: simulationId } })
          .then(estimatedParams => {
            if (!estimatedParams) {
              response = {
                statusCode: 404,
                message: `Not found atphase simulation with simulation id: ${simulationId}`
              }
              winston.info(`Not found estimated data for atphase simulation with simulation id: ${simulationId}`)
              return response
            }
            response = {
              statusCode: 200,
              message: 'Successfull find simulation',
              parameters: data,
              estimatedParams: estimatedParams
            }
            return response
          })
      })
  }


  atphase.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    }
    return loopback.getModel('atphase').create(options)
      .then(data => {
        winston.info('Simulation atphase successfully save input data')
        return data;
      });
  }

  atphase.returnInputData = (id) => {
    return loopback.getModel('atphase').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found atphase input data`
          }
          winston.info('Not found atphase input data')
          return response
        }
        return data[data.length - 1];
      })
  }

  atphase.saveDefaulValue = (id, parameters) => {
    const options = {
      userID: id,
      default_values: true,
      ...parameters
    }
    return loopback.getModel('atphase').create(options)
      .then(data => {
        winston.info('Simulation atphase successfully save default values')
        return data;
      })
  }

  atphase.returnSaveDefaulValue = (id) => {
    return loopback.getModel('atphase').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found atphase default values`
          }
          winston.info('Not found atphase default values')
          return response
        }
        return data[data.length - 1];
      })
  }

  atphase.download = (id, res) => {
    winston.info('Simulation atphase successfully download data')
    res.download('./atphase/output_files.zip');
  };

  atphase.upload = (id, req, res) => {
    var storage = multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './atphase');
      },
      filename: function (req, file, callback) {
        callback(null, 'a_t.txt');
      }
    });

    let upload = multer({ storage: storage }).single('file');

    upload(req, res, function (err) {
      if (err) {
        winston.info(err)
        return res.end({ message: "Error uploading file." });
      }
      winston.info('Simulation atphase successfully update a_t')
      res.json({ message: "File is uploaded" });
    });
  }

  /** API for run simulation */
  atphase.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  atphase.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  atphase.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  /** API for return input data for simulation */
  atphase.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  atphase.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  atphase.remoteMethod('returnData', {
    http: { verb: 'GET', path: '/users/:simulationId/returnData' },
    accepts: [
      { arg: 'simulationId', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  atphase.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  atphase.remoteMethod('upload',
    {
      http: { verb: 'POST', path: '/users/:id/upload' },
      accepts: [
        { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
        { arg: 'req', type: 'object', http: { source: 'req' } },
        { arg: 'res', type: 'object', http: { source: 'res' } }
      ],
      returns: { arg: 'data', type: 'string', root: true }
    }
  );

  atphase.remoteMethod('saveDefaulValue', {
    http: { verb: 'POST', path: '/users/:id/saveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  atphase.remoteMethod('returnSaveDefaulValue', {
    http: { verb: 'GET', path: '/users/:id/returnSaveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  atphase.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download', 'upload', 'returnSaveDefaulValue', 'saveDefaulValue', 'returnData'], method.name)) {
      atphase.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}