import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HillMonteCarloComponent } from './hill-monte-carlo.component';

const routes: Routes = [
  {
    path: '',
    component: HillMonteCarloComponent,
    data: {
      title: 'Hill Monte Carlo'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HillMonteCarloRoutingModule {}
