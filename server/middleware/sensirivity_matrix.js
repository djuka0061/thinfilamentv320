const fs = require('fs');
const EOL = require('os').EOL;
module.exports.createMatrix = (eee_path, kkk_path, rrr_path, int_max, checkboxes, simulation) => {
  let estim_params;
  if (simulation === 'mg_probabilistic' || simulation === 'hill_monte_carlo') {
    estim_params = 7;
  } else if (simulation === 'monte_carlo') {
    estim_params = 9;
  } else {
    estim_params = 13;
  }

  const kavg = [];
  const kde = [];
  const kkk = readKKKMatrix(kkk_path, estim_params);
  const position = [];
  const klast = [];
  const r = readRArray(eee_path);
  let kp = 0;
  const sm = readSMMatrix(rrr_path, int_max, estim_params);
  let real_vs;
  let i_init;
  let params_estim;
  let sensitivity;

  for (let i = 0; i <= 13; i++) {
    if (checkboxes[i] != null && checkboxes[i] != undefined) {
      if (checkboxes[i] === false) {
        //Ovde treba promeniti neke labele
        //For j = 0 To 3
        //Text16(j * 14 + i).Text = Text5(i).Text
      } else {
        kp += 1;
        position[kp] = i + 1;
      }
    }
  }

  real_vs = kp;

  if (int_max > 100) {
    int_max = 100
    //Text19.Text = 100 ---------> Ovde se menja neka labela na frontend-u
  } else if (int_max < 0) {
    int_max = 10
    //Text19.Text = 10  ---------> Ovde se menja neka labela na frontend-u
  }

  //Inicijalizacija nizova kavg i kde
  for (let i = 0; i <= real_vs + 1; i++) {
    kavg.push(0.0);
    kde.push(0.0);
  }

  //Inicijalizacija niza position
  for (let i = 0; i <= 7; i++) {
    position.push(1);
  }

  klast.push(0)
  for (let i = 1; i <= real_vs; i++) {
    klast.push(kkk[int_max][position[i]]);
  }
  klast.push(r[int_max]);

  if (int_max > 10)
    i_init = int_max - 9
  else
    i_init = 1

  //Sredjivanje niza kavg
  for (let j = 1; j <= real_vs; j++) {
    for (let i = i_init; i <= int_max; i++) {
      kavg[j] = kavg[j] + kkk[i][position[j]];
    }
  }

  for (let i = i_init; i <= int_max; i++) {
    kavg[real_vs] += r[i];
  }

  for (let i = 1; i <= real_vs + 1; i++) {
    kavg[i] = kavg[i] / (int_max - i_init + 1);
  }

  //Sredjivanje niza kde
  for (let j = 1; j <= real_vs; j++) {
    for (let i = i_init; i <= int_max; i++) {
      kde[j] += (kkk[i][position[j]] - kavg[j]) * (kkk[i][position[j]] - kavg[j]);
    }
  }

  for (let i = i_init; i <= int_max; i++) {
    kde[real_vs + 1] += (r[i] - kavg[real_vs + 1]) * (r[i] - kavg[real_vs + 1]);
  }

  for (let i = 1; i <= real_vs + 1; i++) {
    kde[i] = Math.sqrt(kde[i] / (int_max - i_init + 1));
  }
  printArrays(klast, kavg, kde, real_vs, simulation);
  printSMMatrix(sm, int_max, real_vs, simulation);
}

function printArrays(klast, kavg, kde, real_vs, simulation) {
  let params_estim = [];
  const save = fs.createWriteStream('./' + simulation + '/new_param_est.dat');
  for (let i = 1; i <= real_vs + 1; i++) {
    params_estim.push(klast[i], kavg[i], kde[i]);
    save.write(klast[i] + ',' + kavg[i] + ',' + kde[i] + '\n');
  }
  save.end('\n');
  return params_estim;
}

//Indeksi u ovoj funkciji krecu od 0!
function printSMMatrix(sm, int_max, real_vs, simulation) {
  let row = [], sensitivity = [];
  const save = fs.createWriteStream('./' + simulation +'/new_sensitivity.dat')
  for (let j = 0; j < real_vs; j++) {
    row = [];
    for (let i = 0; i < real_vs; i++) {
      save.write(sm[int_max - 1][i][j] + ',');
    }
    save.write('\n');
  }
}

function readKKKMatrix(path, estim_params) {
  let content = fs.readFileSync(path).toString();


  content = content.trim().split(EOL).map(x => x.trim().split(','));

  for (let i = 0; i < content.length; i++) {
    for (let j = 0; j < estim_params; j++) {
      content[i][j] = parseFloat(content[i][j]);
    }
  }

  let row = []
  for (let i = 0; i < estim_params + 2; i++)
    row.push(0);

  content.unshift(row);

  for (let i = 1; i < content.length; i++) {
    content[i].unshift(0);
  }

  return content;
}

function readRArray(path) {
  let content = fs.readFileSync(path).toString();


  content = content.trim().split(',');

  for (let i = 0; i < content.length; i++) {
    content[i] = parseFloat(content[i]);
  }

  content.unshift(0);

  return content;
}

function readSMMatrix(path, int_max, estim_params) {
  let content = fs.readFileSync(path).toString();

  content = content.trim().split(EOL).map(x => x.trim().split(','));
  for (let i = 0; i < content.length; i++) {
    for (let j = 0; j < estim_params+1; j++) {
      content[i][j] = parseFloat(content[i][j]);
    }
  }

  console.log(content)
  let sm = [];
  let mat = [];
  for (let k = 0; k < content.length; k++) {

    if (k % (estim_params+1) === 0 && k !== 0) {
      mat = [content[k]];
      sm.push(mat);
    } else {
      mat.push(content[k]);
    }
  }

  sm.push(mat);
  return sm;
}