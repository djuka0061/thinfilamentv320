VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Hill_NUM_Form 
   Caption         =   "Hill Model Simulation Numerical "
   ClientHeight    =   9390
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9390
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton browse 
      Caption         =   "Browse"
      Height          =   375
      Left            =   8520
      TabIndex        =   33
      Top             =   2040
      Width           =   855
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5040
      TabIndex        =   30
      Text            =   "0.8"
      Top             =   5880
      Width           =   1485
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5040
      TabIndex        =   29
      Text            =   "0.5"
      Top             =   6360
      Width           =   1485
   End
   Begin VB.CommandButton Histogram 
      Caption         =   "Histogram"
      Height          =   435
      Left            =   5040
      TabIndex        =   28
      Top             =   8640
      Width           =   1485
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   5040
      TabIndex        =   27
      Text            =   "Text1"
      Top             =   1560
      Width           =   4335
   End
   Begin VB.CommandButton Return_comm 
      Caption         =   "Return Main"
      Height          =   435
      Left            =   6720
      TabIndex        =   12
      Top             =   8640
      Width           =   1485
   End
   Begin VB.TextBox Text10 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   6
      Text            =   "500"
      Top             =   4620
      Width           =   1485
   End
   Begin VB.TextBox Text12 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   8
      Text            =   "0.09"
      Top             =   5460
      Width           =   1485
   End
   Begin VB.CommandButton Graphs_Button 
      Caption         =   "Graphs"
      Enabled         =   0   'False
      Height          =   435
      Left            =   6720
      TabIndex        =   11
      Top             =   8040
      Width           =   1485
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run"
      Enabled         =   0   'False
      Height          =   435
      Left            =   5040
      TabIndex        =   13
      Top             =   8040
      Width           =   1485
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   10
      Text            =   "0.25"
      Top             =   7320
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   9
      Text            =   "0.05"
      Top             =   6840
      Width           =   1485
   End
   Begin VB.TextBox Text11 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   7
      Text            =   "2.831"
      Top             =   5040
      Width           =   1485
   End
   Begin VB.TextBox Text9 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   5
      Text            =   "5.0"
      Top             =   4200
      Width           =   1485
   End
   Begin VB.TextBox Text8 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   4
      Text            =   "0.208"
      Top             =   3780
      Width           =   1485
   End
   Begin VB.TextBox Text7 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   3
      Text            =   "4.682"
      Top             =   3360
      Width           =   1485
   End
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   2
      Text            =   "300"
      Top             =   2940
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   1
      Text            =   "200"
      Top             =   2520
      Width           =   1485
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   5070
      TabIndex        =   0
      Text            =   "5"
      Top             =   2100
      Width           =   1485
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Parameter Gama:"
      Height          =   285
      Left            =   480
      TabIndex        =   32
      Top             =   5880
      Width           =   4515
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Cooperativity Parameter Delta:"
      Height          =   285
      Left            =   840
      TabIndex        =   31
      Top             =   6360
      Width           =   4155
   End
   Begin VB.Label Label2 
      Caption         =   "Numerical Hill Model Simulation"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   960
      TabIndex        =   26
      Top             =   720
      Width           =   7575
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant K1':"
      Height          =   285
      Left            =   600
      TabIndex        =   25
      Top             =   4725
      Width           =   4395
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant K2':"
      Height          =   285
      Left            =   840
      TabIndex        =   24
      Top             =   5565
      Width           =   4155
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Myosin [uM]:"
      Height          =   285
      Left            =   720
      TabIndex        =   23
      Top             =   7320
      Width           =   4275
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Actin [uM]:"
      Height          =   285
      Left            =   720
      TabIndex        =   22
      Top             =   6840
      Width           =   4275
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Rate Constant K2:"
      Height          =   285
      Left            =   480
      TabIndex        =   21
      Top             =   5145
      Width           =   4515
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Rate Constant K1:"
      Height          =   285
      Left            =   600
      TabIndex        =   20
      Top             =   4305
      Width           =   4395
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "Constant Y2:"
      Height          =   285
      Left            =   720
      TabIndex        =   19
      Top             =   3885
      Width           =   4275
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Constant Y1:"
      Height          =   285
      Left            =   720
      TabIndex        =   18
      Top             =   3465
      Width           =   4275
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Rate Constant Beta0:"
      Height          =   285
      Left            =   840
      TabIndex        =   17
      Top             =   3045
      Width           =   4155
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Rate Constant Alpha0:"
      Height          =   285
      Left            =   960
      TabIndex        =   16
      Top             =   2625
      Width           =   4035
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   1080
      TabIndex        =   15
      Top             =   2205
      Width           =   3915
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   285
      Left            =   2400
      TabIndex        =   14
      Top             =   1560
      Width           =   2595
   End
End
Attribute VB_Name = "Hill_NUM_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub browse_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text1.Text = CommonDialog1.FileName
    ' Display name of selected file (without the path)
    'OutputFiletitle = CommonDialog1.FileTitle
End Sub

Private Sub Form_Load()
    Text1.Text = App.path & "\Hillnum_output"
    Run_Button.Enabled = True
    Graphs_Button.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub

Private Sub Graphs_Button_Click()
    

    'Graphs_Button.Enabled = False

    Graphs_Form.Show
    Hill_NUM_Form.Hide

ErrHandler:
    'User pressed the Cancel button

End Sub


Private Sub Histogram_Click()
    Histogram_Form.Show
    Hill_NUM_Form.Hide

End Sub



Private Sub Return_comm_Click()
    Main_Form.Show
    Hill_NUM_Form.Hide
End Sub

Private Sub Run_Button_Click()
    
    Dim FileNumber As Integer
    Dim Input_File_Name As String

    On Error GoTo ErrHandler
    
    ChDrive App.path
    ChDir App.path

    If Text1.Text <> "" Then

               
        Input_File_Name = "Input_params.dat"
        
        'Export data to a file
        FileNumber = FreeFile
        Open Input_File_Name For Output As #FileNumber
        Print #FileNumber, "#Output File Name:"
        Print #FileNumber, Text1.Text
        Print #FileNumber, "#Total Time [s]:"
        Print #FileNumber, Text4.Text
        Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text13.Text
        Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text14.Text
        Print #FileNumber, "#The Rate Constants Beetwen the ON and OFF States (Alpha0 / Beta0):"
        Print #FileNumber, Text5.Text
        Print #FileNumber, Text6.Text
        Print #FileNumber, "#The Constants Y1 and Y2:"
        Print #FileNumber, Text7.Text
        Print #FileNumber, Text8.Text
        Print #FileNumber, "#The Forward/Backward Rate Constants of Off States (K1 / K1'):"
        Print #FileNumber, Text9.Text
        Print #FileNumber, Text10.Text
        Print #FileNumber, "#The Forward/Backward Rate Constants of ON States (K2+ / K2'):"
        Print #FileNumber, Text11.Text
        Print #FileNumber, Text12.Text
        Print #FileNumber, "#The Parameter Gama:"
        Print #FileNumber, Text3.Text
        Print #FileNumber, "#The Cooperativity Parameter Delta:"
        Print #FileNumber, Text2.Text
        
        Close #FileNumber

        Hill_NUM_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        Graphs_Button.Enabled = False
        

        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        'Call ExecCmd(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34))
        Call ExecCmd(App.path & "\num_hill.exe " & Chr(34) & Input_File_Name & Chr(34))
        
        Hill_NUM_Form.MousePointer = vbArrow
        
        Run_Button.Enabled = True
        Graphs_Button.Enabled = True
       
    End If

    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub




