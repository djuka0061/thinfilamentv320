import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ATPhaseComponent } from './atphase.component';

const routes: Routes = [
  {
    path: '/atphase',
    component: ATPhaseComponent,
    data: {
      title: 'ATPhaseComponent'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ATPhaseRoutingModule {}
