import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from "@angular/common";

import { SimulatedAnnealingComponent } from './simulated-annealing.component';
import { SimulatedAnnealingRoutingModule } from './simulated-annealing-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    SimulatedAnnealingRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    MatTableModule,
    MatPaginatorModule
  ],
  declarations: [ SimulatedAnnealingComponent ],
  exports: [
    MatTableModule,
    MatPaginatorModule
  ]
})
export class SimulatedAnnealingModule { }
