VERSION 5.00
Begin VB.Form About_Form 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "About"
   ClientHeight    =   5100
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5685
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5100
   ScaleWidth      =   5685
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton Close_Command 
      Caption         =   "Close"
      Height          =   435
      Left            =   1995
      TabIndex        =   6
      Top             =   4515
      Width           =   1590
   End
   Begin VB.Label Label8 
      Alignment       =   2  'Center
      Caption         =   "Edward Xiaochuan Li"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   360
      TabIndex        =   8
      Top             =   1200
      Width           =   4845
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Caption         =   "Juan Del Alamo De Pedro"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   360
      TabIndex        =   7
      Top             =   1560
      Width           =   4845
   End
   Begin VB.Label Label6 
      Alignment       =   2  'Center
      Caption         =   "Boston MA, August 16, 2006"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   330
      Left            =   315
      TabIndex        =   5
      Top             =   3780
      Width           =   4845
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "Harvard School of Public Health"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00008000&
      Height          =   330
      Left            =   315
      TabIndex        =   4
      Top             =   3465
      Width           =   4845
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Srba Mijailovich"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   420
      TabIndex        =   3
      Top             =   2625
      Width           =   4845
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "Adriano Alencar"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   420
      TabIndex        =   2
      Top             =   2280
      Width           =   4845
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Aleksandar Marinkovic"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   330
      Left            =   360
      TabIndex        =   1
      Top             =   1920
      Width           =   4845
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Thin Filament Regulation v3.0"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF0000&
      Height          =   540
      Left            =   420
      TabIndex        =   0
      Top             =   525
      Width           =   4845
   End
End
Attribute VB_Name = "About_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Close_Command_Click()
    About_Form.Hide
End Sub

