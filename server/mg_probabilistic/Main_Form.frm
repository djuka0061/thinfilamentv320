VERSION 5.00
Begin VB.Form Main_Form 
   Caption         =   "Main Form Thin Filament Regulation"
   ClientHeight    =   7815
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9675
   LinkTopic       =   "Form1"
   ScaleHeight     =   7815
   ScaleWidth      =   9675
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Graph 
      Caption         =   "Draw Graph"
      Height          =   375
      Left            =   6840
      TabIndex        =   10
      Top             =   5280
      Width           =   1335
   End
   Begin VB.TextBox Text1 
      BackColor       =   &H0000FFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2535
      Left            =   1080
      MultiLine       =   -1  'True
      TabIndex        =   9
      Text            =   "Main_Form.frx":0000
      Top             =   3720
      Visible         =   0   'False
      Width           =   4935
   End
   Begin VB.CommandButton Exit 
      Caption         =   "Exit"
      Height          =   375
      Left            =   6840
      TabIndex        =   7
      Top             =   5880
      Width           =   1335
   End
   Begin VB.CommandButton go 
      Caption         =   "Go"
      Height          =   495
      Left            =   6480
      TabIndex        =   6
      Top             =   2400
      Width           =   855
   End
   Begin VB.CommandButton Scheme 
      Caption         =   "Scheme"
      Height          =   375
      Left            =   6840
      TabIndex        =   5
      Top             =   4680
      Width           =   1335
   End
   Begin VB.CommandButton About 
      Caption         =   "About"
      Height          =   375
      Left            =   6840
      TabIndex        =   4
      Top             =   4080
      Width           =   1335
   End
   Begin VB.CommandButton Readme 
      Caption         =   "Read Me"
      Height          =   375
      Left            =   6840
      TabIndex        =   3
      Top             =   3480
      Width           =   1335
   End
   Begin VB.ComboBox Task_Comb 
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   480
      ItemData        =   "Main_Form.frx":000C
      Left            =   1080
      List            =   "Main_Form.frx":000E
      TabIndex        =   1
      Text            =   "Please choose one task and click go"
      Top             =   2400
      Width           =   5295
   End
   Begin VB.Label Label3 
      Caption         =   "Harvard School of Public Health"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   1320
      TabIndex        =   8
      Top             =   6960
      Width           =   6135
   End
   Begin VB.Label Label2 
      Caption         =   "Task:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   13.5
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   240
      TabIndex        =   2
      Top             =   2400
      Width           =   855
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Main Menu"
      BeginProperty Font 
         Name            =   "Georgia"
         Size            =   24
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   855
      Left            =   2760
      TabIndex        =   0
      Top             =   600
      Width           =   4215
   End
End
Attribute VB_Name = "Main_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim sdescp(11) As String



Private Sub About_Click()
    About_Form.Show
End Sub

Private Sub Exit_Click()
    End
End Sub

Private Sub Form_Load()
    ChDrive App.path
    ChDir App.path
        
    Task_Comb.AddItem "MG Model - Probabilistic"
    Task_Comb.AddItem "MG Model - Stochastical"
    Task_Comb.AddItem "Hill Model - Stochastical"
    Task_Comb.AddItem "Hill Model - Probabilistic"
    Task_Comb.AddItem "Param. Est. - MG Prob DLS"
    Task_Comb.AddItem "Param. Est. - MG Sto DLS"
    Task_Comb.AddItem "Param. Est. - Hill Sto DLS"
    Task_Comb.AddItem "Parameter Optimization - SIMANN"
    Task_Comb.AddItem "Parameter Optimization - Quasi-Newton"
    Task_Comb.AddItem "Param. Est. - David DLS"
    Task_Comb.AddItem "Param. Est. - ATPase DLS"
    
End Sub

Private Sub go_Click()

    Main_Form.Hide
    Select Case Task_Comb.ListIndex
    Case 0
        MGNUM_Form.Show
    Case 4
        Para_MGNUM_Form.Show
    Case 3
        Hill_NUM_Form.Show
    Case 2
        Hill_STO_Form.Show
    Case 1
        STO_MG_Form.Show
    Case 8
        QNewton_Form.Show
    Case 5
        Para_MGSTO_Form.Show
    Case 6
        Para_HILLSTO_Form.Show
    Case 9
        Para_DAVID_Form.Show
    Case 10
        Para_ATPase_Form.Show
    Case 7
        SIMANN_Form.Show
    Case Else
        MGNUM_Form.Show
    End Select
    
End Sub

Private Sub Graph_Click()
    Graphs_Form.Show
    Main_Form.Hide
End Sub

Private Sub Readme_Click()
    Readme_Form.Show
End Sub

Private Sub Scheme_Click()
    Scheme_Form.Show
End Sub

Private Sub Task_Comb_Click()
     Dim iselect As Integer
     Wrap$ = Chr$(13) + Chr$(10)   'create wrap characters
     
    sdescp(1) = "The three-state MG model of myosin S1 " _
          & "binding with regulated Actin7.TmTn is simulated. " _
          & "The kinetic equations is solved by numerical method. "
    sdescp(2) = "The three-state MG model of myosin S1 " _
          & "binding with regulated Actin7.TmTn is simulated. " _
          & "The kinetic equations is solved by Monte Carlo approach. "
    sdescp(3) = "The two-state Hill model of myosin S1 " _
          & "binding with regulated Actin7.TmTn is simulated. " _
          & "The kinetic equations is solved by Monte Carlo approach. " _
          & "The cooperativity is implemented. "
    sdescp(4) = "The two-state Hill model of myosin S1 " _
          & "binding with regulated Actin7.TmTn is simulated. " _
          & "The kinetic equations is solved by numerical method. "
    sdescp(5) = "Kinetic Parameters, kb, kt, k1+, k2+ of MG model " _
          & "are matched with the experimental data. " _
          & "The resolution matrix of those parameters are calculated. "
    sdescp(9) = "Kinetic Parameters, kb, kt, k1+, k2+ of MG model " _
          & "are optimazed by Quasi-Newton method against the " _
          & "experimental data. "
    sdescp(6) = "Kinetic Parameters, kb, kt, k1+, etc. based on MG model " _
          & "are matched with the experimental data. " _
          & "The resolution matrix of those parameters are calculated through the " _
          & "Monte Carlo simulation. "
    sdescp(7) = "Hill Model Parameters K1, K2, Y, L' " _
          & "are estimated by DLS method against " _
          & "Experimental Data through Monte Carlo simulation."
    sdescp(8) = "Kinetic Parameters, kb, kt, k1+, k2+ of MG model " _
          & "are optimazed by simulated annealing (SIMANN) global method against the " _
          & "experimental data. "
    sdescp(10) = "Kinetic Parameters, Kti, GAMA of David model " _
          & "are matched with the experimental data. "
    sdescp(11) = "Kinetic Parameters of ATPase model " _
          & "are matched with the experimental data. "

    iselect = Task_Comb.ListIndex
    Text1.Text = Task_Comb.Text & Wrap$ & sdescp(iselect + 1)
    Text1.Visible = True
    
End Sub
