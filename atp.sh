if [[ $@ = *--dev* ]]; then
  export IMAGE_NAME=atphase-nodejs
  export BUILD_DIR=atphase-tools
  # just keep the container running
  export COMMAND="tail -F /none"
else
  # mount anything, but not /opt/app
  export MOUNT_VOLUME=./README.md:/tmp/README.md
fi

if [[ $@ = *--rebuild* ]]; then
  export REBUILD=true
fi

docker_compose_sh () {
   docker exec -it $2 sh
}

docker_compose_run() {
  if [[ $1 == up ]] && !(docker network ls | grep -q " atphase "); then
    echo -n "Creating new docker network 'atphase' ... "
    docker network create atphase && echo OK
  fi

  if [[ $2 == all ]]; then
    services=("atphase")
  fi

  if [[ $2 != all ]]; then
    services=("$2")
  fi

  for i in "${services[@]}"
  do
    docker-compose -f docker-compose.$i.yml $1 -d --build
  done
}

main () {
  case "$1" in
    up | down) docker_compose_run $@ ;;
    sh) docker_compose_sh $@ ;;
  esac
}

main $@
