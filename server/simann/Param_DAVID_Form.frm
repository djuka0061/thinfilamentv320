VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Para_DAVID_Form 
   Caption         =   "Para_Estim DAVID"
   ClientHeight    =   9390
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9390
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text10 
      Height          =   285
      Left            =   1800
      TabIndex        =   69
      Text            =   "40"
      Top             =   5760
      Width           =   1455
   End
   Begin VB.TextBox Text5 
      Height          =   285
      Index           =   10
      Left            =   8640
      TabIndex        =   67
      Text            =   "995"
      Top             =   3360
      Width           =   1335
   End
   Begin VB.TextBox Text5 
      Height          =   285
      Index           =   9
      Left            =   5280
      TabIndex        =   65
      Text            =   "993"
      Top             =   3360
      Width           =   1455
   End
   Begin VB.TextBox Text5 
      Height          =   285
      Index           =   8
      Left            =   1800
      TabIndex        =   63
      Text            =   "991"
      Top             =   3360
      Width           =   1455
   End
   Begin VB.TextBox Text5 
      Height          =   285
      Index           =   7
      Left            =   8640
      TabIndex        =   61
      Text            =   "18"
      Top             =   2760
      Width           =   1335
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   12
      Left            =   1800
      TabIndex        =   59
      Text            =   "2.0"
      Top             =   3960
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   6
      Left            =   5280
      TabIndex        =   58
      Text            =   "100"
      Top             =   2760
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   5
      Left            =   1800
      TabIndex        =   57
      Text            =   "1.0"
      Top             =   2760
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   4
      Left            =   1830
      TabIndex        =   56
      Text            =   "20"
      Top             =   2400
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   3
      Left            =   5280
      TabIndex        =   55
      Text            =   "0.4"
      Top             =   1920
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   2
      Left            =   1800
      TabIndex        =   54
      Text            =   "-1.0"
      Top             =   1920
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   1
      Left            =   1800
      TabIndex        =   53
      Text            =   "0.39"
      Top             =   1440
      Width           =   1485
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   1
      Left            =   3720
      TabIndex        =   52
      Top             =   1440
      Width           =   375
   End
   Begin VB.CheckBox Check1 
      Height          =   255
      Index           =   0
      Left            =   3720
      TabIndex        =   51
      Top             =   1080
      Value           =   1  'Checked
      Width           =   375
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   25
      Left            =   9240
      TabIndex        =   50
      Text            =   "10"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   24
      Left            =   9240
      TabIndex        =   49
      Text            =   "100"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   17
      Left            =   7560
      TabIndex        =   48
      Text            =   "0.1"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   16
      Left            =   7560
      TabIndex        =   47
      Text            =   "0.01"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   9
      Left            =   6000
      TabIndex        =   46
      Text            =   "1"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   8
      Left            =   6000
      TabIndex        =   45
      Text            =   "6"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   1
      Left            =   4440
      TabIndex        =   44
      Text            =   "0.6"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   285
      Index           =   0
      Left            =   4440
      TabIndex        =   43
      Text            =   "10"
      Top             =   1080
      Width           =   1335
   End
   Begin VB.CommandButton Command5 
      Caption         =   "..........."
      Height          =   375
      Left            =   10200
      TabIndex        =   37
      Top             =   6960
      Width           =   735
   End
   Begin VB.CommandButton Command4 
      Caption         =   "........."
      Height          =   375
      Left            =   10200
      TabIndex        =   36
      Top             =   6480
      Width           =   735
   End
   Begin VB.CommandButton Command3 
      Caption         =   "......."
      Height          =   375
      Left            =   10200
      TabIndex        =   35
      Top             =   6000
      Width           =   735
   End
   Begin VB.CommandButton Command2 
      Caption         =   "........"
      Height          =   375
      Left            =   10200
      TabIndex        =   34
      Top             =   5520
      Width           =   735
   End
   Begin VB.CommandButton Command1 
      Caption         =   "........"
      Height          =   375
      Left            =   10200
      TabIndex        =   33
      Top             =   5040
      Width           =   735
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   1320
      Top             =   7920
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Text3 
      Height          =   375
      Left            =   5880
      TabIndex        =   31
      Text            =   "para_est.dat"
      Top             =   6960
      Width           =   4215
   End
   Begin VB.CommandButton Display 
      Caption         =   "Show Results"
      Height          =   495
      Left            =   6240
      TabIndex        =   29
      Top             =   7560
      Width           =   1575
   End
   Begin VB.TextBox Text2 
      Height          =   300
      Left            =   5880
      TabIndex        =   26
      Text            =   "eee_4b.dat"
      Top             =   6000
      Width           =   4245
   End
   Begin VB.TextBox Text1 
      Height          =   300
      Left            =   5880
      TabIndex        =   25
      Text            =   "rrr_4b.dat"
      Top             =   6480
      Width           =   4245
   End
   Begin VB.TextBox Text30 
      Height          =   300
      Left            =   5880
      TabIndex        =   20
      Text            =   "a_t.txt"
      Top             =   5040
      Width           =   4245
   End
   Begin VB.TextBox Text29 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4440
      TabIndex        =   19
      Text            =   "0.5"
      Top             =   4560
      Width           =   1485
   End
   Begin VB.TextBox Text20 
      Height          =   300
      Left            =   5880
      TabIndex        =   18
      Text            =   "kkk_4b.dat"
      Top             =   5520
      Width           =   4245
   End
   Begin VB.TextBox Text19 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   7800
      TabIndex        =   17
      Text            =   "10"
      Top             =   4560
      Width           =   1485
   End
   Begin VB.CommandButton Ret_comm 
      Caption         =   "Return to Main"
      Height          =   435
      Left            =   5160
      TabIndex        =   4
      Top             =   8280
      Width           =   1485
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run"
      Height          =   495
      Left            =   4320
      TabIndex        =   5
      Top             =   7560
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   1800
      TabIndex        =   3
      Text            =   "1.0e-4"
      Top             =   5250
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   11
      Left            =   1830
      TabIndex        =   2
      Text            =   "0.1"
      Top             =   4560
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Index           =   0
      Left            =   1830
      TabIndex        =   1
      Text            =   "0.25"
      Top             =   1080
      Width           =   1485
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   1830
      TabIndex        =   0
      Text            =   "2.5"
      Top             =   660
      Width           =   1485
   End
   Begin VB.Label Label27 
      Caption         =   "no. of repeats"
      Height          =   375
      Left            =   720
      TabIndex        =   68
      Top             =   5760
      Width           =   1095
   End
   Begin VB.Label Label24 
      Caption         =   "ID3"
      Height          =   255
      Left            =   8040
      TabIndex        =   66
      Top             =   3360
      Width           =   495
   End
   Begin VB.Label Label23 
      Caption         =   "ID2"
      Height          =   255
      Left            =   4800
      TabIndex        =   64
      Top             =   3360
      Width           =   375
   End
   Begin VB.Label Label22 
      Caption         =   "ID1"
      Height          =   255
      Left            =   1320
      TabIndex        =   62
      Top             =   3360
      Width           =   375
   End
   Begin VB.Label Label21 
      Caption         =   "KM1P"
      Height          =   255
      Left            =   7920
      TabIndex        =   60
      Top             =   2760
      Width           =   615
   End
   Begin VB.Label Label20 
      Alignment       =   2  'Center
      Caption         =   "Upper Bound"
      Height          =   255
      Left            =   9120
      TabIndex        =   42
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label17 
      Alignment       =   2  'Center
      Caption         =   "Lower Bound"
      Height          =   255
      Left            =   7440
      TabIndex        =   41
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label14 
      Alignment       =   2  'Center
      Caption         =   "Guess2 of Param"
      Height          =   255
      Left            =   5880
      TabIndex        =   40
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   "Guess1 of Param"
      Height          =   255
      Left            =   4320
      TabIndex        =   39
      Top             =   720
      Width           =   1455
   End
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Fitting"
      Height          =   255
      Left            =   3480
      TabIndex        =   38
      Top             =   720
      Width           =   615
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "Output Parameter File:"
      Height          =   255
      Left            =   4200
      TabIndex        =   32
      Top             =   7080
      Width           =   1575
   End
   Begin VB.Label Label16 
      Caption         =   "Parameter Estimation - DAVID"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   480
      TabIndex        =   30
      Top             =   0
      Width           =   7575
   End
   Begin VB.Label Label35 
      Alignment       =   1  'Right Justify
      Caption         =   "Sensitivity Matrix File:"
      Height          =   285
      Left            =   4200
      TabIndex        =   28
      Top             =   6480
      Width           =   1575
   End
   Begin VB.Label Label34 
      Alignment       =   1  'Right Justify
      Caption         =   "Error File:"
      Height          =   285
      Left            =   4680
      TabIndex        =   27
      Top             =   6000
      Width           =   1035
   End
   Begin VB.Label Label26 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   285
      Left            =   4200
      TabIndex        =   24
      Top             =   5520
      Width           =   1515
   End
   Begin VB.Label Label25 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Iteration:"
      Height          =   285
      Left            =   6120
      TabIndex        =   23
      Top             =   4560
      Width           =   1515
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Experimentical Data File:"
      Height          =   285
      Left            =   3840
      TabIndex        =   22
      Top             =   5040
      Width           =   1875
   End
   Begin VB.Label Label2 
      Alignment       =   1  'Right Justify
      Caption         =   "Epsilon:"
      Height          =   285
      Left            =   3360
      TabIndex        =   21
      Top             =   4560
      Width           =   915
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "KKM1"
      Height          =   285
      Left            =   240
      TabIndex        =   16
      Top             =   2805
      Width           =   1395
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "DM1"
      Height          =   285
      Left            =   240
      TabIndex        =   15
      Top             =   4005
      Width           =   1515
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "dt:"
      Height          =   285
      Left            =   120
      TabIndex        =   14
      Top             =   5280
      Width           =   1635
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "ALPHA  [A]/[S]"
      Height          =   285
      Left            =   120
      TabIndex        =   13
      Top             =   4560
      Width           =   1635
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "KKM2"
      Height          =   285
      Index           =   0
      Left            =   3600
      TabIndex        =   12
      Top             =   2760
      Width           =   1515
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "CPL"
      Height          =   285
      Left            =   240
      TabIndex        =   11
      Top             =   2400
      Width           =   1395
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "PHM2"
      Height          =   285
      Left            =   3720
      TabIndex        =   10
      Top             =   1920
      Width           =   1395
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "PHM1"
      Height          =   285
      Left            =   360
      TabIndex        =   9
      Top             =   1920
      Width           =   1275
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Gama"
      Height          =   285
      Left            =   360
      TabIndex        =   8
      Top             =   1440
      Width           =   1275
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Tni"
      Height          =   285
      Left            =   360
      TabIndex        =   7
      Top             =   1080
      Width           =   1275
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   360
      TabIndex        =   6
      Top             =   720
      Width           =   1275
   End
End
Attribute VB_Name = "Para_DAVID_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False





Private Sub Check1_Click(Index As Integer)
   
        If Check1(Index).Value = vbChecked Then
            For i = 0 To 3
                Text16(i * 8 + Index).Visible = True
            Next i

        Else
            For i = 0 To 3
                Text16(i * 8 + Index).Visible = False
            Next i
        End If
   
    
    
End Sub

Private Sub Command1_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text30.Text = CommonDialog1.FileName
    Text30.Refresh
End Sub

Private Sub Command2_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text20.Text = CommonDialog1.FileName
    Text20.Refresh
End Sub

Private Sub Command3_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text2.Text = CommonDialog1.FileName
    Text2.Refresh
End Sub

Private Sub Command4_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text1.Text = CommonDialog1.FileName
    Text1.Refresh
End Sub

Private Sub Command5_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text3.Text = CommonDialog1.FileName
    Text3.Refresh
End Sub

Private Sub Display_Click()
    Para_DAVID_Form.Hide
    Display_Form.Show
    'Graphs_temp.Show
End Sub

Private Sub Form_Load()

    Text30.Text = App.path & "\a_t.txt"
    Text20.Text = App.path & "\kkk.dat"
    Text2.Text = App.path & "\eee.dat"
    Text1.Text = App.path & "\rrr.dat"
    Text3.Text = App.path & "\param_est.dat"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub

Private Sub Ret_comm_Click()
    Main_Form.Show
    Para_DAVID_Form.Hide
End Sub

Private Sub Run_Button_Click()

    Dim Input_File_Name As String
    Dim FileNumber As Integer
    Dim path As String
    Dim File_name As String
    Dim Paras(4), derror As Single
    Dim kavg(5), klast(5), kde(5) As Double
    Dim kkk(100, 8) As Double
    Dim r(100) As Double
    Dim sm(100, 8, 8) As Double
    Dim int_max, I_init, ip, jp, kp As Integer
    Dim position(4), real_vs As Integer
    
   
    ChDrive App.path
    ChDir App.path
    
    int_max = CInt(Text19.Text)
    If (int_max > 100) Then
        int_max = 100
        Text19.Text = 100
    Else
       If (int_max < 0) Then
            int_max = 10
            Text19.Text = 10
        End If
    End If
    
    For i = 1 To 2
        position(i) = 1
    Next i
        
    FileNumber = 2
    On Error GoTo ErrHandler

    'prepare for parameters
    kp = 0
    For i = 0 To 1
        If Check1(i).Value = vbUnchecked Then
            For j = 0 To 3
                Text16(j * 8 + i).Text = Text5(i).Text
            Next j
        Else
            kp = kp + 1
            position(kp) = i + 1
        End If
    
    Next i
    real_vs = kp


        Input_File_Name = App.path & "\tmtn-ca.dat"
        Open Input_File_Name For Output As #FileNumber
        

        For i = 2 To 6
            Print #FileNumber, Text5(i).Text
        Next i
        Print #FileNumber, Text5(0).Text
        
        For i = 7 To 12
            Print #FileNumber, Text5(i).Text
        Next i
        
        Print #FileNumber, Text5(1).Text
        
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text4.Text
        Print #FileNumber, Text10.Text
        Close #FileNumber

        Para_DAVID_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        
        FileNumber = 3
        Input_File_Name = App.path & "\input_wrap"
        Open Input_File_Name For Output As #FileNumber
            Print #FileNumber, "2"
            For i = 0 To 1
                For j = 0 To 3
                    Print #FileNumber, Text16(j * 8 + i).Text
                Next j
            Next i
                        
            Print #FileNumber, Text29.Text 'epsilon
            Print #FileNumber, Text19.Text 'iteration
            Print #FileNumber, "'" & Text30.Text & "'"
            Print #FileNumber, "'" & Text20.Text & "'"
            Print #FileNumber, "'" & Text2.Text & "'"
            Print #FileNumber, "'" & Text1.Text & "'"
            Print #FileNumber, "'DavidModel'"
            'JC's version num_rigid, my version is num_geeve
        Close #FileNumber
        
        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        'Call ExecCmd(App.path & "\ESTIMRE.exe input_estim" & Chr(34))
        Call ExecCmd(App.path & "\ParaEstimDavid.exe " & Chr(34))
        'Call ExecCmd("D:\newgui\paramestim.exe " & Chr(34))
        'Para_DAVID_Form.Show
        
        FileNumber = FreeFile
        File_name = Text20.Text
        Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            For jp = 1 To 2
                Input #FileNumber, kkk(ip, jp)
            Next jp
        Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = Text2.Text
        Open File_name For Input As #FileNumber
        For ip = 1 To int_max
            Input #FileNumber, r(ip)
        Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = Text1.Text
        Open File_name For Input As #FileNumber
        For kp = 1 To int_max
            For ip = 1 To 2
                For jp = 1 To 2
                    Input #FileNumber, sm(kp, ip, jp)
                Next jp
            Next ip
        Next kp
        Close #FileNumber
        
        For ip = 1 To real_vs + 1
            kavg(ip) = 0
            kde(ip) = 0
        Next ip
    
        For ip = 1 To real_vs
            klast(ip) = kkk(int_max, position(ip))
        Next ip
        klast(real_vs + 1) = r(int_max)
        
        If (int_max > 10) Then
            I_init = int_max - 9
        Else
            I_init = 1
        End If
    
        For jp = 1 To real_vs
            For ip = I_init To int_max
                kavg(jp) = kavg(jp) + kkk(ip, position(jp))
            Next ip
        Next jp
    
        For ip = I_init To int_max
            kavg(real_vs) = kavg(real_vs) + r(ip)
        Next ip
        
        For ip = 1 To real_vs + 1
            kavg(ip) = kavg(ip) / (int_max - I_init + 1)
        Next ip
        
          
        For jp = 1 To real_vs
            For ip = I_init To int_max
                kde(jp) = kde(jp) + (kkk(ip, position(jp)) - kavg(jp)) _
                            * (kkk(ip, position(jp)) - kavg(jp))
            Next ip
        Next jp
        
        For ip = I_init To int_max
            kde(real_vs + 1) = kde(real_vs + 1) + (r(ip) - kavg(real_vs + 1)) _
                              * (r(ip) - kavg(real_vs + 1))
        Next ip
        
        For ip = 1 To real_vs + 1
            kde(ip) = Sqr(kde(ip) / (int_max - I_init + 1))
        Next ip
        
        FileNumber = 4
        Input_File_Name = Text3.Text
        Open Input_File_Name For Output As #FileNumber
        For ip = 1 To real_vs + 1
            Print #FileNumber, Format(klast(ip), "0.0000E+00"), _
            Format(kavg(ip), "0.0000E+00"), Format(kde(ip), "0.0000E+00")
        Next ip
        
        'sm only lists active (real) variables, here real_vs
        For jp = 1 To real_vs
            For ip = 1 To real_vs
                Print #FileNumber, Format(sm(int_max, ip, jp), "#0.########"),
            Next ip
            Print #FileNumber, " "
        Next jp
        
        Close #FileNumber
        
        
        FileNumber = FreeFile
        File_name = App.path & "\kb.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(1))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\kt.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(2))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\k1.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(3))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\k2.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, kkk(ip, position(4))
            Next ip
        Close #FileNumber
        
        FileNumber = FreeFile
        File_name = App.path & "\er.dat"
        Open File_name For Output As #FileNumber
            For ip = 1 To int_max
                Print #FileNumber, r(ip)
            Next ip
        Close #FileNumber
       
        FileNumber = FreeFile
        File_name = App.path & "\ps.dat"
        Open File_name For Output As #FileNumber
            Print #FileNumber, real_vs
            For ip = 1 To 4
                Print #FileNumber, position(ip)
            Next ip
        Close #FileNumber
        
        Para_DAVID_Form.MousePointer = vbArrow
        Run_Button.Enabled = True
           
        
    
    
    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub



Private Sub Time_Option_Click()
    If Time_Option.Value = True Then
        Text4.Text = 2
        Text14.Text = 0.5
        Text15.Text = Text14.Text
        Text15.Enabled = False
        Text15.Visible = False
        Label13.Caption = "[Myosin] in uM:"
        Label36.Visible = False
    End If
End Sub



Private Sub Titration_Option_Click()
    If Titration_Option.Value = True Then
        Text4.Text = 250
        Text14.Text = 0
        Text15.Enabled = True
        Text15.Visible = True
        Label36.Visible = True
        Label13.Caption = "Initial [Myosin] in uM:"
        Label36.Caption = "Final [Myosin] in uM:"
    End If
End Sub

Private Sub Text8_Change()

End Sub
