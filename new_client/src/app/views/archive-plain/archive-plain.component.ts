import { Component, OnInit, QueryList, ViewChild, AfterViewInit } from '@angular/core';
import { environment } from '../../../environments/environment.prod';
import { ArchiveService } from '../../service/archive.service';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';

const host = environment.server.host;

@Component({
  templateUrl: 'archive-plain.component.html',
  styleUrls: ['./archive-plain.component.scss'],
})
export class ArchivePlainComponent implements OnInit, AfterViewInit {
  archiveForm = new FormGroup({
    searchText: new FormControl('')
  })
  public archiveData
  private skip: number = 0
  private limit: number = 25
  public nextBtn: string = "Next"
  public backBtn: string = "Back"
  public nextVisible: boolean = false
  public backVisible: boolean = false
  public paginationVisible: boolean = true
  constructor(private archiveService: ArchiveService) { }

  ngOnInit(): void {

  }

  ngAfterViewInit(): void {
    this.archiveService.returnAllArchive(this.limit, this.skip).subscribe(data => {
      this.enableDisableBtn()
      this.archiveData = data
    })
  }

  enableDisableBtn() {
    this.archiveService.countSimulation().subscribe(data => {
      if (this.skip + this.limit < parseInt(data.count)) {
        this.nextVisible = false
      } else {
        this.nextVisible = true
      }

      if (this.skip - this.limit >= 0) {
        this.backVisible = false
      } else {
        this.backVisible = true
      }
    })
  }

  search() {
    this.archiveService.searchArchive(this.archiveForm.value.searchText).subscribe(data => {
      this.archiveData = data
      this.paginationVisible = false
    })
  }

  searchByType(type: string) {
    this.archiveService.searchArchiveByType(type).subscribe(data => {
      this.archiveData = data
      this.paginationVisible = false
    })
  }

  clearFilter() {
    this.archiveService.returnAllArchive(this.limit, this.skip).subscribe(data => {
      this.enableDisableBtn()
      this.archiveData = data
      this.paginationVisible = true
    })
  }

  onSubmit() {
  }

  openSimulation(simulation, simulationId) {
    var landingUrl = `#/models/${simulation}?simulationId=${simulationId}`;
    window.location.href = landingUrl;
  }

  next() {
    this.skip = this.skip + this.limit
    this.archiveService.returnAllArchive(this.limit, this.skip).subscribe(data => {
      this.enableDisableBtn()
      this.archiveData = data
    })
  }

  back() {
    this.skip = this.skip - this.limit
    this.archiveService.returnAllArchive(this.limit, this.skip).subscribe(data => {
      this.enableDisableBtn()
      this.archiveData = data
    })
  }
}
