import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProbabilisticComponent } from './probabilistic.component';

const routes: Routes = [
  {
    path: '',
    component: ProbabilisticComponent,
    data: {
      title: 'Probabilistic'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProbabilisticRoutingModule {}
