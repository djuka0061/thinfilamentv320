import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HillProbabilisticComponent } from './hill-probabilistic.component';

describe('HillProbabilisticComponent', () => {
  let component: HillProbabilisticComponent;
  let fixture: ComponentFixture<HillProbabilisticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HillProbabilisticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HillProbabilisticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
