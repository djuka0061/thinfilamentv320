'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const zippingFile = require('../../middleware/zipping');
const returnOutputData = require('../../middleware/return_data');
const winston = require('winston');

//Local include

module.exports = function (hill_monte_carlo) {

  const exeSimulation = (id, runSimulationId, parameters) => {
    //setup data for start simulation
    const fileName = 'hilmc_pe.exe';
    const params = [];
    const path = './hill_monte_carlo/'
    return exec(fileName, params, { cwd: path }, (err, data) => {
      if (err) {
        winston.info(err)
        const response = {
          statusCode: 400,
          message: 'Simulation failed'
        }
        return response
      }
      else {
        winston.info('Simulation hill monte carlo successfully finish')
        //checked estimation params
        const cekiranaPolja = [Boolean(parameters.alpha_fiting), Boolean(parameters.beta_fiting), Boolean(parameters.y1_fiting), Boolean(parameters.y2_fiting), Boolean(parameters.k1_fiting), Boolean(parameters.k1_prim_fiting),
        Boolean(parameters.k2_fiting), Boolean(parameters.k2_prim_fiting)];

        returnOutputData.removeData('hill_monte_carlo')
        //call function for create output data
        return returnOutputData.returnData('hill_monte_carlo', cekiranaPolja, parameters.number_of_iteration)
          .then(outputData => {
            //call function for zip output data
            zippingFile.zipFile('hill_monte_carlo', cekiranaPolja, parameters, outputData);

            const options = {
              userID: id,
              input_id: runSimulationId,
              ...outputData
            }

            return loopback.getModel('hill_monte_carlo_output').create(options)
              .then(datas => {
                return datas;
              })
          })
      }
    });
  }

  //function for run simulation
  hill_monte_carlo.runSimulation = (id, parameters) => {

    //create input file
    let writeStream = fs.createWriteStream('./hill_monte_carlo/pain.dat');
    writeStream.write(parameters.number_of_filaments + '\n');
    writeStream.write(parameters.number_of_tm_tn_units + '\n');
    writeStream.write(parameters.number_of_actin_per_unit + '\n');;
    writeStream.write(parameters.s + '\n');
    writeStream.write(parameters.actin + '\n');
    writeStream.write(parameters.myosin + '\n');
    writeStream.write((parameters.final_const_of_myosin ? parameters.final_const_of_myosin : parameters.myosin) + '\n');
    writeStream.write(parameters.alpha_zero + '\n');
    writeStream.write(parameters.beta_zero + '\n');
    writeStream.write(parameters.constant_y1 + '\n');
    writeStream.write(parameters.constant_y2 + '\n');
    writeStream.write(parameters.constant_k1 + '\n');
    writeStream.write(parameters.constant_k1_prim + '\n');
    writeStream.write(parameters.constant_k2 + '\n');
    writeStream.write(parameters.constant_k2_prim + '\n');
    writeStream.write(parameters.parameter_gama + '\n');
    writeStream.write(parameters.parameter_delta + '\n');
    writeStream.write(parameters.coopertivity_switch ? ('1' + '\n') : ('2' + '\n'));
    writeStream.end('pain.dat finished\n');

    //create input warp
    let inputWarp = fs.createWriteStream('./hill_monte_carlo/input_wrap');
    inputWarp.write('8\n');
    inputWarp.write((parameters.alpha_fiting ? parameters.alpha_guess_of_params1 : parameters.alpha_zero) + '\n');
    inputWarp.write((parameters.alpha_fiting ? parameters.alpha_guess_of_params2 : parameters.alpha_zero) + '\n');
    inputWarp.write((parameters.alpha_fiting ? parameters.alpha_lower_bound : parameters.alpha_zero) + '\n');
    inputWarp.write((parameters.alpha_fiting ? parameters.alpha_upper_bound : parameters.alpha_zero) + '\n');
    inputWarp.write((parameters.beta_fiting ? parameters.beta_guess_of_params1 : parameters.beta_zero) + '\n');
    inputWarp.write((parameters.beta_fiting ? parameters.beta_guess_of_params2 : parameters.beta_zero) + '\n');
    inputWarp.write((parameters.beta_fiting ? parameters.beta_lower_bound : parameters.beta_zero) + '\n');
    inputWarp.write((parameters.beta_fiting ? parameters.beta_upper_bound : parameters.beta_zero) + '\n');
    inputWarp.write((parameters.y1_fiting ? parameters.y1_guess_of_params1 : parameters.constant_y1) + '\n');
    inputWarp.write((parameters.y1_fiting ? parameters.y1_guess_of_params2 : parameters.constant_y1) + '\n');
    inputWarp.write((parameters.y1_fiting ? parameters.y1_lower_bound : parameters.constant_y1) + '\n');
    inputWarp.write((parameters.y1_fiting ? parameters.y1_upper_bound : parameters.constant_y1) + '\n');
    inputWarp.write((parameters.y2_fiting ? parameters.y2_guess_of_params1 : parameters.constant_y2) + '\n');
    inputWarp.write((parameters.y2_fiting ? parameters.y2_guess_of_params2 : parameters.constant_y2) + '\n');
    inputWarp.write((parameters.y2_fiting ? parameters.y2_lower_bound : parameters.constant_y2) + '\n');
    inputWarp.write((parameters.y2_fiting ? parameters.y2_upper_bound : parameters.constant_y2) + '\n');
    inputWarp.write((parameters.k1_fiting ? parameters.k1_guess_of_params1 : parameters.constant_k1) + '\n');
    inputWarp.write((parameters.k1_fiting ? parameters.k1_guess_of_params2 : parameters.constant_k1) + '\n');
    inputWarp.write((parameters.k1_fiting ? parameters.k1_lower_bound : parameters.constant_k1) + '\n');
    inputWarp.write((parameters.k1_fiting ? parameters.k1_upper_bound : parameters.constant_k1) + '\n');
    inputWarp.write((parameters.k1_prim_fiting ? parameters.k1_prim_guess_of_params1 : parameters.constant_k1_prim) + '\n');
    inputWarp.write((parameters.k1_prim_fiting ? parameters.k1_prim_guess_of_params2 : parameters.constant_k1_prim) + '\n');
    inputWarp.write((parameters.k1_prim_fiting ? parameters.k1_prim_lower_bound : parameters.constant_k1_prim) + '\n');
    inputWarp.write((parameters.k1_prim_fiting ? parameters.k1_prim_upper_bound : parameters.constant_k1_prim) + '\n');
    inputWarp.write((parameters.k2_fiting ? parameters.k2_guess_of_params1 : parameters.constant_k2) + '\n');
    inputWarp.write((parameters.k2_fiting ? parameters.k2_guess_of_params2 : parameters.constant_k2) + '\n');
    inputWarp.write((parameters.k2_fiting ? parameters.k2_lower_bound : parameters.constant_k2) + '\n');
    inputWarp.write((parameters.k2_fiting ? parameters.k2_upper_bound : parameters.constant_k2) + '\n');
    inputWarp.write((parameters.k2_prim_fiting ? parameters.k2_prim_guess_of_params1 : parameters.constant_k2_prim) + '\n');
    inputWarp.write((parameters.k2_prim_fiting ? parameters.k2_prim_guess_of_params2 : parameters.constant_k2_prim) + '\n');
    inputWarp.write((parameters.k2_prim_fiting ? parameters.k2_prim_lower_bound : parameters.constant_k2_prim) + '\n');
    inputWarp.write((parameters.k2_prim_fiting ? parameters.k2_prim_upper_bound : parameters.constant_k2_prim) + '\n');
    inputWarp.write(parameters.epsilon + '\n');
    inputWarp.write(parameters.number_of_iteration + '\n');
    inputWarp.write('\'a_t.txt\'' + '\n');
    inputWarp.write('\'kkk_4b.dat\'' + '\n');
    inputWarp.write('\'eee_4b.dat\'' + '\n');
    inputWarp.write('\'rrr_4b.dat\'' + '\n');
    inputWarp.end('\'hill_mc\'' + '\n');

    const options = {
      userID: id,
      ...parameters
    }
    return loopback.getModel('hill_monte_carlo').create(options)
      .then(data => {
        exeSimulation(id, data._id, parameters)
        winston.info('Simulation hill monte carlo successfully start')
        const response = {
          statusCode: 200,
          message: "Successfull run simulation",
          simulationId: data._id
        }

        return response
      });
  }

  hill_monte_carlo.saveData = (id, parameters) => {
    //checked estimation params
    const cekiranaPolja = [Boolean(parameters.alpha_fiting), Boolean(parameters.beta_fiting), Boolean(parameters.y1_fiting), Boolean(parameters.y2_fiting), Boolean(parameters.k1_fiting), Boolean(parameters.k1_prim_fiting),
    Boolean(parameters.k2_fiting), Boolean(parameters.k2_prim_fiting)];

    //call function for create sensitivity matrix
    pravljenjeSensitivityMatrice.createMatrix('./hill_monte_carlo/new_eee.dat', './hill_monte_carlo/new_kkk.dat', './hill_monte_carlo/new_rrr.dat', parameters.number_of_iteration, cekiranaPolja);

    return returnOutputData.returnData('hill_monte_carlo', cekiranaPolja, parameters.number_of_iteration)
      .then(outputData => {
        if (!outputData && outputData !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for hill monte carlo simulation'
          }
          winston.info('Not found output data for hill monte carlo simulation')
          return response
        }
        const hill_monte_carloParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel(hill_monte_carlo).create(hill_monte_carloParams)
          .then(atp => {
            const options = {
              userID: id,
              input_id: atp._id,
              ...outputData
            }
            return loopback.getModel('hill_monte_carlo_output').create(options)
              .then(data => {
                winston.info('Simulation hill monte carlo successfully save data')
                return data;
              })
          })
      })
  }

  hill_monte_carlo.returnSaveData = (id) => {
    let response
    return loopback.getModel('hill_monte_carlo_output').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found hill monte carlo simulation'
          }
          winston.info('Not found return data for hill monte carlo simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id
        return loopback.getModel('hill_monte_carlo').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found hill monte carlo simulation'
              }
              winston.info('Not found input data for hill monte carlo simulation')
              return response
            }
            response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response
          })
      })
  }

  hill_monte_carlo.returnData = (simulationId) => {
    let response
    return loopback.getModel('hill_monte_carlo').findOne({ where: { _id: simulationId } })
      .then(data => {
        if (!data) {
          response = {
            statusCode: 404,
            message: `Not found hill monte carlo simulation with simulation id: ${simulationId}`
          }
          winston.info(`Not found input data for hill monte carlo simulation with simulation id: ${simulationId}`)
          return response
        }
        return loopback.getModel('hill_monte_carlo_output').findOne({ where: { input_id: simulationId } })
          .then(estimatedParams => {
            if (!estimatedParams) {
              response = {
                statusCode: 404,
                message: `Not found hill monte carlo simulation with simulation id: ${simulationId}`
              }
              winston.info(`Not found estimated data for hill monte carlo simulation with simulation id: ${simulationId}`)
              return response
            }
            response = {
              parameters: data,
              estimatedParams: estimatedParams
            }
            return response
          })
      })
  }

  hill_monte_carlo.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    }
    return loopback.getModel('hill_monte_carlo').create(options)
      .then(data => {
        winston.info('Simulation hill monte carlo successfully save input data')
        return data;
      });
  }

  hill_monte_carlo.returnInputData = (id) => {
    return loopback.getModel('hill_monte_carlo').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found hill monte carlo input data`
          }
          winston.info('Not found hill monte carlo input data')
          return response
        }
        return data[data.length - 1];
      })
  }

  hill_monte_carlo.saveDefaulValue = (id, parameters) => {
    const options = {
      userID: id,
      default_values: true,
      ...parameters
    }
    return loopback.getModel('hill_monte_carlo').create(options)
      .then(data => {
        winston.info('Simulation hill monte carlo successfully save default values')
        return data;
      })
  }

  hill_monte_carlo.returnSaveDefaulValue = (id) => {
    return loopback.getModel('hill_monte_carlo').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found hill monte carlo default values`
          }
          winston.info('Not found hill monte carlo default values')
          return response
        }
        return data[data.length - 1];
      })
  }

  hill_monte_carlo.download = (id, res) => {
    winston.info('Simulation hill monte carlo successfully download data')
    res.download('./hill_monte_carlo/output_files.zip');
  };

  hill_monte_carlo.upload = (id, req, res) => {
    var storage = multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './hill_monte_carlo');
      },
      filename: function (req, file, callback) {
        callback(null, 'a_t.txt');
      }
    });

    let upload = multer({ storage: storage }).single('file');

    upload(req, res, function (err) {
      if (err) {
        winston.info(err)
        return res.end({ message: "Error uploading file." });
      }
      winston.info('Simulation hill monte carlo successfully update a_t')
      res.json({ message: "File is uploaded" });
    });
  }

  /** API for run simulation */
  hill_monte_carlo.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_monte_carlo.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_monte_carlo.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  /** API for return input data for simulation */
  hill_monte_carlo.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  hill_monte_carlo.remoteMethod('returnData', {
    http: { verb: 'GET', path: '/users/:simulationId/returnData' },
    accepts: [
      { arg: 'simulationId', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  hill_monte_carlo.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  hill_monte_carlo.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_monte_carlo.remoteMethod('upload',
    {
      http: { verb: 'POST', path: '/users/:id/upload' },
      accepts: [
        { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
        { arg: 'req', type: 'object', http: { source: 'req' } },
        { arg: 'res', type: 'object', http: { source: 'res' } }
      ],
      returns: { arg: 'data', type: 'string', root: true }
    }
  );

  hill_monte_carlo.remoteMethod('saveDefaulValue', {
    http: { verb: 'POST', path: '/users/:id/saveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  hill_monte_carlo.remoteMethod('returnSaveDefaulValue', {
    http: { verb: 'GET', path: '/users/:id/returnSaveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  hill_monte_carlo.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download', 'upload', 'returnSaveDefaulValue', 'saveDefaulValue', 'returnData'], method.name)) {
      hill_monte_carlo.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}