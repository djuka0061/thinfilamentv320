import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { QuasiNewtonComponent } from './quasi-newton.component';

const routes: Routes = [
  {
    path: '',
    component: QuasiNewtonComponent,
    data: {
      title: 'QuasiNewton'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class QuasiNewtonRoutingModule {}
