VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Hill_STO_Form 
   Caption         =   "Hill Model Simulation Stochastical "
   ClientHeight    =   9390
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9390
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.TextBox Text18 
      Alignment       =   2  'Center
      Height          =   285
      Left            =   7800
      TabIndex        =   44
      Text            =   "0.25"
      Top             =   7560
      Visible         =   0   'False
      Width           =   1335
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type of Simulation"
      Height          =   645
      Left            =   5760
      TabIndex        =   41
      Top             =   6120
      Width           =   3270
      Begin VB.OptionButton Time_Option 
         Caption         =   "Time Course"
         Height          =   225
         Left            =   525
         TabIndex        =   43
         Top             =   315
         Value           =   -1  'True
         Width           =   1275
      End
      Begin VB.OptionButton Titration_Option 
         Caption         =   "Titration"
         Height          =   225
         Left            =   1995
         TabIndex        =   42
         Top             =   315
         Width           =   960
      End
   End
   Begin VB.CommandButton browse 
      Caption         =   "Browse"
      Height          =   375
      Left            =   7200
      TabIndex        =   40
      Top             =   1680
      Width           =   1095
   End
   Begin VB.TextBox Text17 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   7320
      TabIndex        =   39
      Text            =   "7"
      Top             =   3480
      Width           =   735
   End
   Begin VB.CheckBox Check1 
      Caption         =   "Coopertivity On"
      Height          =   375
      Left            =   6720
      TabIndex        =   37
      Top             =   4080
      Value           =   1  'Checked
      Width           =   1455
   End
   Begin VB.TextBox Text16 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   7320
      TabIndex        =   36
      Text            =   "100"
      Top             =   2880
      Width           =   855
   End
   Begin VB.TextBox Text15 
      Alignment       =   2  'Center
      Height          =   375
      Left            =   7320
      TabIndex        =   34
      Text            =   "100"
      Top             =   2400
      Width           =   1095
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   18
      Text            =   "5"
      Top             =   2340
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   17
      Text            =   "200"
      Top             =   2760
      Width           =   1485
   End
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   16
      Text            =   "300"
      Top             =   3180
      Width           =   1485
   End
   Begin VB.TextBox Text7 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   15
      Text            =   "4.682"
      Top             =   3600
      Width           =   1485
   End
   Begin VB.TextBox Text8 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   14
      Text            =   "0.208"
      Top             =   4020
      Width           =   1485
   End
   Begin VB.TextBox Text9 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   13
      Text            =   "5.0"
      Top             =   4440
      Width           =   1485
   End
   Begin VB.TextBox Text11 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   12
      Text            =   "2.831"
      Top             =   5280
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   11
      Text            =   "0.05"
      Top             =   7080
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   10
      Text            =   "0.25"
      Top             =   7560
      Width           =   1485
   End
   Begin VB.TextBox Text12 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   9
      Text            =   "0.09"
      Top             =   5700
      Width           =   1485
   End
   Begin VB.TextBox Text10 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4110
      TabIndex        =   8
      Text            =   "500"
      Top             =   4860
      Width           =   1485
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   1920
      TabIndex        =   7
      Text            =   "Text1"
      Top             =   1680
      Width           =   5055
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4080
      TabIndex        =   6
      Text            =   "0.5"
      Top             =   6600
      Width           =   1485
   End
   Begin VB.TextBox Text3 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   4080
      TabIndex        =   5
      Text            =   "0.8"
      Top             =   6120
      Width           =   1485
   End
   Begin VB.CommandButton Histogram 
      Caption         =   "Histogram"
      Height          =   435
      Left            =   4440
      TabIndex        =   4
      Top             =   8640
      Width           =   1485
   End
   Begin VB.CommandButton Return_comm 
      Caption         =   "Return Main"
      Height          =   435
      Left            =   6120
      TabIndex        =   1
      Top             =   8640
      Width           =   1485
   End
   Begin VB.CommandButton Graphs_Button 
      Caption         =   "Graphs"
      Enabled         =   0   'False
      Height          =   435
      Left            =   6120
      TabIndex        =   0
      Top             =   8040
      Width           =   1485
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run"
      Enabled         =   0   'False
      Height          =   435
      Left            =   4440
      TabIndex        =   2
      Top             =   8040
      Width           =   1485
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label17 
      Caption         =   "Final Conc. of Myosin [uM]:"
      Height          =   255
      Left            =   5760
      TabIndex        =   45
      Top             =   7560
      Visible         =   0   'False
      Width           =   2055
   End
   Begin VB.Label Label16 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Actin Per Unit:"
      Height          =   375
      Left            =   6000
      TabIndex        =   38
      Top             =   3480
      Width           =   1215
   End
   Begin VB.Label Label15 
      Alignment       =   1  'Right Justify
      Caption         =   "Numer of TmTn Units:"
      Height          =   495
      Left            =   6120
      TabIndex        =   35
      Top             =   2880
      Width           =   1095
   End
   Begin VB.Label Label14 
      Alignment       =   1  'Right Justify
      Caption         =   "Number of Filaments:"
      Height          =   375
      Left            =   6120
      TabIndex        =   33
      Top             =   2400
      Width           =   1095
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   525
      Left            =   960
      TabIndex        =   32
      Top             =   1680
      Width           =   915
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   600
      TabIndex        =   31
      Top             =   2400
      Width           =   3315
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Rate Constant Alpha0:"
      Height          =   285
      Left            =   1320
      TabIndex        =   30
      Top             =   2760
      Width           =   2595
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Rate Constant Beta0:"
      Height          =   285
      Left            =   720
      TabIndex        =   29
      Top             =   3165
      Width           =   3195
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Constant Y1:"
      Height          =   285
      Left            =   840
      TabIndex        =   28
      Top             =   3600
      Width           =   3075
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "Constant Y2:"
      Height          =   285
      Left            =   480
      TabIndex        =   27
      Top             =   4080
      Width           =   3435
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Rate Constant K1:"
      Height          =   285
      Left            =   600
      TabIndex        =   26
      Top             =   4440
      Width           =   3315
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Rate Constant K2:"
      Height          =   285
      Left            =   600
      TabIndex        =   25
      Top             =   5280
      Width           =   3315
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Actin [uM]:"
      Height          =   285
      Left            =   1320
      TabIndex        =   24
      Top             =   7080
      Width           =   2595
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Myosin [uM]:"
      Height          =   285
      Left            =   720
      TabIndex        =   23
      Top             =   7560
      Width           =   3195
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant K2':"
      Height          =   285
      Left            =   1080
      TabIndex        =   22
      Top             =   5685
      Width           =   2835
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant K1':"
      Height          =   285
      Left            =   480
      TabIndex        =   21
      Top             =   4920
      Width           =   3435
   End
   Begin VB.Label Label3 
      Alignment       =   1  'Right Justify
      Caption         =   "Cooperativity Parameter Delta:"
      Height          =   285
      Left            =   600
      TabIndex        =   20
      Top             =   6600
      Width           =   3315
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      Caption         =   "Parameter Gama:"
      Height          =   285
      Left            =   720
      TabIndex        =   19
      Top             =   6120
      Width           =   3195
   End
   Begin VB.Label Label2 
      Caption         =   "Stochastical Hill Model Simulation"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   960
      TabIndex        =   3
      Top             =   720
      Width           =   7575
   End
End
Attribute VB_Name = "Hill_STO_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub browse_Click()
    CommonDialog1.CancelError = False
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    Text1.Text = CommonDialog1.FileName
    ' Display name of selected file (without the path)
    'OutputFiletitle = CommonDialog1.FileTitle
End Sub

Private Sub Form_Load()
    Text1.Text = App.path & "\hill_mcout"
    Run_Button.Enabled = True
    Graphs_Button.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub

Private Sub Graphs_Button_Click()
    

    'Graphs_Button.Enabled = False

    Graphs_Form.Show
    Hill_STO_Form.Hide

ErrHandler:
    'User pressed the Cancel button

End Sub


Private Sub Histogram_Click()
    Histogram_Form.Show
    Hill_STO_Form.Hide

End Sub

Private Sub Return_comm_Click()
    Main_Form.Show
    Hill_STO_Form.Hide
End Sub

Private Sub Run_Button_Click()
    Dim cswitch As Integer
    Dim FileNumber As Integer
    Dim Input_File_Name, ffname As String

    On Error GoTo ErrHandler

    ChDrive App.path
    ChDir App.path
    
    If Time_Option.Value = True Then
        Text18.Text = Text14.Text
    End If

    If Text1.Text <> "" Then

        If Check1.Value = vbChecked Then
            cswitch = 1
        Else
            cswitch = 0
        End If
        
        'looks like only 10 char allowed
        Input_File_Name = "pain.dat"
        
        
        'Export data to a file
        FileNumber = FreeFile
        Open Input_File_Name For Output As #FileNumber
        Print #FileNumber, "#Output File Name:"
        Print #FileNumber, Text1.Text
        Print #FileNumber, "#Number of Filaments:"
        Print #FileNumber, Text15.Text
        Print #FileNumber, "#Number of TmTn units:"
        Print #FileNumber, Text16.Text
        Print #FileNumber, "#Number of Actin per unit:"
        Print #FileNumber, Text17.Text
        Print #FileNumber, "#Total Time [s]:"
        Print #FileNumber, Text4.Text
        Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text13.Text
        Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text18.Text
        Print #FileNumber, "#The Rate Constants Beetwen ON and OFF States (Alpha0 / Beta0):"
        Print #FileNumber, Text5.Text
        Print #FileNumber, Text6.Text
        Print #FileNumber, "#Constants Y1 and Y2:"
        Print #FileNumber, Text7.Text
        Print #FileNumber, Text8.Text
        Print #FileNumber, "#The Foward/Backward Rate Constants of OFF State (K1 / K1'):"
        Print #FileNumber, Text9.Text
        Print #FileNumber, Text10.Text
        Print #FileNumber, "#The Foward/Backward Rate Constants of ON State (K2/ K2'):"
        Print #FileNumber, Text11.Text
        Print #FileNumber, Text12.Text
        Print #FileNumber, "#Parameter Gama:"
        Print #FileNumber, Text3.Text
        Print #FileNumber, "#Cooperativity Paramater Delta:"
        Print #FileNumber, Text2.Text
        Print #FileNumber, "#Cooperativity Switch (1 - ON /0 - OFF):"
        Print #FileNumber, cswitch
        
        Close #FileNumber
        
        
        ffname = "input_wrap"
        FileNumber = FreeFile
        Open ffname For Output As #FileNumber
            Print #FileNumber, "pain.dat"
            Print #FileNumber, "hill_mc.exe"
            Print #FileNumber, "1"
        Close #FileNumber

        Hill_STO_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        Graphs_Button.Enabled = False
        

        
        Call ExecCmd(App.path & "\wraper.exe " & Chr(34))
        'Call ExecCmd(App.path & "\hill_mc.exe " & Chr(34) & Input_File_Name & Chr(34))
        
        Hill_STO_Form.MousePointer = vbArrow
        
        Run_Button.Enabled = True
        Graphs_Button.Enabled = True
       
    End If

    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub




Private Sub Time_Option_Click()
    If Time_Option.Value = True Then
        Text14.Text = 0.5
        Text18.Text = Text14.Text
        Text4.Text = 5
        Label17.Visible = False
        Text18.Visible = False
        Label13.Caption = "Molar Concentration of Myosin [uM]:"
    End If
End Sub



Private Sub Titration_Option_Click()
    If Titration_Option.Value = True Then
        Text4.Text = 250
        Text14.Text = 0
        Label17.Visible = True
        Text18.Visible = True
        Label13.Caption = "Initial Molar Concentration of Myosin [uM]:"
    End If
End Sub


