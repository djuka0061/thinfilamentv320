'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const multer = require('multer');
const zippingFile = require('../../middleware/zipping');
const returnOutputData = require('../../middleware/return_data');
const winston = require('winston')

//Local include

module.exports = function (monte_carlo) {

	const exeSimulation = (id, runSimulationId, parameters) => {
		//setup data for start simulation
		const fileName = 'mc_pmestim.exe';
		const params = [];
		const path = './monte_carlo/'
		return exec(fileName, params, { cwd: path }, (err, data) => {
			if (err) {
				winston.info(err)
				const response = {
					statusCode: 400,
					message: 'Simulation failed'
				}
				return response
			}
			else {
				winston.info('Simulation monte carlo successfully finish')
				//checked estimation params
				const cekiranaPolja = [Boolean(parameters.ka_fiting), Boolean(parameters.ka_minus_fiting), Boolean(parameters.kpi_fiting), Boolean(parameters.kpi_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting),
				Boolean(parameters.kt_star_fiting), Boolean(parameters.kt_star_minus_fiting), Boolean(parameters.kt_star_star_fiting), Boolean(parameters.kt_star_star_minus_fiting), Boolean(parameters.kh_fiting), Boolean(parameters.kh_minus_fiting),
				Boolean(parameters.kah_fiting), Boolean(parameters.kah_minus_fiting)];

				//call function for create output data
				return returnOutputData.returnData('monte_carlo', cekiranaPolja, parameters.number_of_iteration)
					.then(outputData => {
						//call function for zip output data
						zippingFile.zipFile('monte_carlo', cekiranaPolja, parameters, outputData);

						//return output data
						const options = {
							userID: id,
							input_id: runSimulationId,
							...outputData
						}

						return loopback.getModel('monte_carlo_output').create(options)
							.then(datas => {
								return datas;
							})
					})
			}
		});
	}

	//function for run simulation
	monte_carlo.runSimulation = (id, parameters) => {

		//create input file
		let writeStream = fs.createWriteStream('./monte_carlo/tmtn-ca.dat');
		writeStream.write(parameters.number_of_tm_tn_units + '\n');
		writeStream.write(parameters.negative_cooperativity_factor + '\n');
		writeStream.write(parameters.filaments + '\n');
		writeStream.write(parameters.s + '\n');
		writeStream.write(parameters.kb + '\n');
		writeStream.write(parameters.kb_minus + '\n');
		writeStream.write(parameters.kt + '\n');
		writeStream.write(parameters.kt_minus + '\n');
		writeStream.write(parameters.k1 + '\n');
		writeStream.write(parameters.k1_minus + '\n');
		writeStream.write(parameters.k2 + '\n');
		writeStream.write(parameters.k2_minus + '\n');
		writeStream.write(parameters.cooperativity_factor + '\n');
		writeStream.write(parameters.actin + '\n');
		writeStream.write(parameters.myosin + '\n');
		writeStream.write((parameters.final_myosin ? parameters.final_myosin : parameters.myosin) + '\n');
		writeStream.end('tmtn-ca.dat finished\n');

		//create input warp
		let inputWarp = fs.createWriteStream('./monte_carlo/input_wrap');
		inputWarp.write('10\n');
		inputWarp.write((parameters.kb_fiting ? parameters.kb_guess_of_params1 : parameters.kb) + '\n');
		inputWarp.write((parameters.kb_fiting ? parameters.kb_guess_of_params2 : parameters.kb) + '\n');
		inputWarp.write((parameters.kb_fiting ? parameters.kb_lower_bound : parameters.kb) + '\n');
		inputWarp.write((parameters.kb_fiting ? parameters.kb_upper_bound : parameters.kb) + '\n');
		inputWarp.write((parameters.kb_minus_fiting ? parameters.kb_minus_guess_of_params1 : parameters.kb_minus) + '\n');
		inputWarp.write((parameters.kb_minus_fiting ? parameters.kb_minus_guess_of_params2 : parameters.kb_minus) + '\n');
		inputWarp.write((parameters.kb_minus_fiting ? parameters.kb_minus_lower_bound : parameters.kb_minus) + '\n');
		inputWarp.write((parameters.kb_minus_fiting ? parameters.kb_minus_upper_bound : parameters.kb_minus) + '\n');
		inputWarp.write((parameters.kt_fiting ? parameters.kt_guess_of_params1 : parameters.kt) + '\n');
		inputWarp.write((parameters.kt_fiting ? parameters.kt_guess_of_params2 : parameters.kt) + '\n');
		inputWarp.write((parameters.kt_fiting ? parameters.kt_lower_bound : parameters.kt) + '\n');
		inputWarp.write((parameters.kt_fiting ? parameters.kt_upper_bound : parameters.kt) + '\n');
		inputWarp.write((parameters.kt_minus_fiting ? parameters.kt_minus_guess_of_params1 : parameters.kt_minus) + '\n');
		inputWarp.write((parameters.kt_minus_fiting ? parameters.kt_minus_guess_of_params2 : parameters.kt_minus) + '\n');
		inputWarp.write((parameters.kt_minus_fiting ? parameters.kt_minus_lower_bound : parameters.kt_minus) + '\n');
		inputWarp.write((parameters.kt_minus_fiting ? parameters.kt_minus_upper_bound : parameters.kt_minus) + '\n');
		inputWarp.write((parameters.k1_fiting ? parameters.k1_guess_of_params1 : parameters.constant_k1) + '\n');
		inputWarp.write((parameters.k1_fiting ? parameters.k1_guess_of_params2 : parameters.constant_k1) + '\n');
		inputWarp.write((parameters.k1_fiting ? parameters.k1_lower_bound : parameters.constant_k1) + '\n');
		inputWarp.write((parameters.k1_fiting ? parameters.k1_upper_bound : parameters.constant_k1) + '\n');
		inputWarp.write((parameters.k1_minus_fiting ? parameters.k1_minus_guess_of_params1 : parameters.k1_minus) + '\n');
		inputWarp.write((parameters.k1_minus_fiting ? parameters.k1_minus_guess_of_params2 : parameters.k1_minus) + '\n');
		inputWarp.write((parameters.k1_minus_fiting ? parameters.k1_minus_lower_bound : parameters.k1_minus) + '\n');
		inputWarp.write((parameters.k1_minus_fiting ? parameters.k1_minus_upper_bound : parameters.k1_minus) + '\n');
		inputWarp.write((parameters.k2_fiting ? parameters.k2_guess_of_params1 : parameters.constant_k2) + '\n');
		inputWarp.write((parameters.k2_fiting ? parameters.k2_guess_of_params2 : parameters.constant_k2) + '\n');
		inputWarp.write((parameters.k2_fiting ? parameters.k2_lower_bound : parameters.constant_k2) + '\n');
		inputWarp.write((parameters.k2_fiting ? parameters.k2_upper_bound : parameters.constant_k2) + '\n');
		inputWarp.write((parameters.k2_minus_fiting ? parameters.k2_minus_guess_of_params1 : parameters.k2_minus) + '\n');
		inputWarp.write((parameters.k2_minus_fiting ? parameters.k2_minus_guess_of_params2 : parameters.k2_minus) + '\n');
		inputWarp.write((parameters.k2_minus_fiting ? parameters.k2_minus_lower_bound : parameters.k2_minus) + '\n');
		inputWarp.write((parameters.k2_minus_fiting ? parameters.k2_minus_upper_bound : parameters.k2_minus) + '\n');
		inputWarp.write((parameters.cooperativity_factor_fiting ? parameters.cooperativity_factor_guess_of_params1 : parameters.cooperativity_factor) + '\n');
		inputWarp.write((parameters.cooperativity_factor_fiting ? parameters.cooperativity_factor_guess_of_params2 : parameters.cooperativity_factor) + '\n');
		inputWarp.write((parameters.cooperativity_factor_fiting ? parameters.cooperativity_factor_lower_bound : parameters.cooperativity_factor) + '\n');
		inputWarp.write((parameters.cooperativity_factor_fiting ? parameters.cooperativity_factor_upper_bound : parameters.cooperativity_factor) + '\n');
		inputWarp.write((parameters.negative_cooperativity_factor_fiting ? parameters.negative_cooperativity_factor_guess_of_params1 : parameters.negative_cooperativity_factor) + '\n');
		inputWarp.write((parameters.negative_cooperativity_factor_fiting ? parameters.negative_cooperativity_factor_guess_of_params2 : parameters.negative_cooperativity_factor) + '\n');
		inputWarp.write((parameters.negative ? parameters.negative_cooperativity_factor_lower_bound : parameters.negative_cooperativity_factor) + '\n');
		inputWarp.write((parameters.negative_cooperativity_factor_fiting ? parameters.negative_cooperativity_factor_upper_bound : parameters.negative_cooperativity_factor) + '\n');
		inputWarp.write(parameters.epsilon + '\n');
		inputWarp.write(parameters.number_of_iteration + '\n');
		inputWarp.write('\'a_t.txt\'' + '\n');
		inputWarp.write('\'kkk_4b.dat\'' + '\n');
		inputWarp.write('\'eee_4b.dat\'' + '\n');
		inputWarp.write('\'rrr_4b.dat\'' + '\n');
		inputWarp.end('\'rigidchain\'' + '\n');

		//setup data for start simulation
		const options = {
			userID: id,
			...parameters
		}
		return loopback.getModel('monte_carlo').create(options)
			.then(data => {
				exeSimulation(id, data._id, parameters)
				winston.info('Simulation monte carlo successfully start')
				const response = {
					statusCode: 200,
					message: "Successfull run simulation",
					simulationId: data._id
				}

				return response
			});
	}

	monte_carlo.saveData = (id, parameters) => {
		//checked estimation params
		const cekiranaPolja = [Boolean(parameters.ka_fiting), Boolean(parameters.ka_minus_fiting), Boolean(parameters.kpi_fiting), Boolean(parameters.kpi_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting),
		Boolean(parameters.kt_star_fiting), Boolean(parameters.kt_star_minus_fiting), Boolean(parameters.kt_star_star_fiting), Boolean(parameters.kt_star_star_minus_fiting), Boolean(parameters.kh_fiting), Boolean(parameters.kh_minus_fiting),
		Boolean(parameters.kah_fiting), Boolean(parameters.kah_minus_fiting)];

		//call function for create sensitivity matrix
		pravljenjeSensitivityMatrice.createMatrix('./monte_carlo/new_eee.dat', './monte_carlo/new_kkk.dat', './monte_carlo/new_rrr.dat', parameters.number_of_iteration, cekiranaPolja);

		return returnOutputData.returnData('monte_carlo', cekiranaPolja, parameters.number_of_iteration)
			.then(outputData => {
				if (!outputData && outputData !== []) {
					response = {
						statusCode: 404,
						message: 'Not found output data for monte carlo simulation'
					}
					winston.info('Not found output data for monte carlo simulation')
					return response
				}
				const monte_carloParams = {
					userID: id,
					...parameters
				}
				return loopback.getModel(monte_carlo).create(monte_carloParams)
					.then(atp => {
						const options = {
							userID: id,
							input_id: atp._id,
							...outputData
						}
						return loopback.getModel('monte_carlo_output').create(options)
							.then(data => {
								winston.info('Simulation monte carlo successfully save data')
								return data;
							})
					})
			})
	}

	monte_carlo.returnSaveData = (id) => {
		return loopback.getModel('monte_carlo_output').find({ where: { userID: id } })
			.then(inputData => {
				if (!inputData) {
					response = {
						statusCode: 404,
						message: 'Not found monte carlo simulation'
					}
					winston.info('Not found return data for monte carlo simulation')
					return response
				}
				const inputId = inputData[inputData.length - 1].input_id
				return loopback.getModel('monte_carlo').findOne({ where: { _id: inputId } })
					.then(data => {
						if (!data) {
							response = {
								statusCode: 404,
								message: 'Not found monte carlo simulation'
							}
							winston.info('Not found input data for monte carlo simulation')
							return response
						}
						const response = {
							inputData: inputData[inputData.length - 1],
							parameters: data
						}
						return response
					})
			})
	}

	monte_carlo.saveInputData = (id, parameters) => {
		const options = {
			userID: id,
			...parameters
		}
		return loopback.getModel('monte_carlo').create(options)
			.then(data => {
				winston.info('Simulation monte carlo successfully save input data')
				return data;
			});
	}

	monte_carlo.returnInputData = (id) => {
		return loopback.getModel('monte_carlo').find({ where: { userID: id } })
			.then(data => {
				if (!data) {
					const response = {
						statusCode: 404,
						message: `Not found monte carlo input data`
					}
					winston.info('Not found monte carlo input data')
					return response
				}
				return data[data.length - 1];
			})
	}

	monte_carlo.saveDefaulValue = (id, parameters) => {
		const options = {
			userID: id,
			default_values: true,
			...parameters
		}
		return loopback.getModel('monte_carlo').create(options)
			.then(data => {
				winston.info('Simulation monte carlo successfully save default values')
				return data;
			})
	}

	monte_carlo.returnData = (simulationId) => {
		return loopback.getModel('monte_carlo').findOne({ where: { _id: simulationId } })
			.then(data => {
				return loopback.getModel('monte_carlo_output').findOne({ where: { input_id: simulationId } })
					.then(estimatedParams => {
						if (!estimatedParams) {
							const response = {
								statusCode: 404,
								message: `Not found monte carlo default values`
							}
							winston.info('Not found monte carlo default values')
							return response
						}
						const response = {
							statusCode: 200,
							parameters: data,
							estimatedParams: estimatedParams
						}
						return response
					})
			})
	}

	monte_carlo.returnSaveDefaulValue = (id) => {
		return loopback.getModel('monte_carlo').find({ where: { userID: id } })
			.then(data => {
				if (!data) {
					const response = {
						statusCode: 404,
						message: `Not found monte carlo default values`
					}
					winston.info('Not found monte carlo default values')
					return response
				}
				return data[data.length - 1];
			})
	}

	monte_carlo.download = (id, res) => {
		winston.info('Simulation monte carlo successfully download data')
		res.download('./monte_carlo/output_files.zip');
	};

	monte_carlo.upload = (id, req, res) => {
		var storage = multer.diskStorage({
			destination: function (req, file, callback) {
				callback(null, './monte_carlo');
			},
			filename: function (req, file, callback) {
				callback(null, 'a_t.txt');
			}
		});

		let upload = multer({ storage: storage }).single('file');

		upload(req, res, function (err) {
			if (err) {
				winston.info(err)
				return res.end({ message: "Error uploading file." });
			}
			winston.info('Simulation monte carlo successfully update a_t')
			res.json({ message: "File is uploaded" });
		});
	}

	/** API for run simulation */
	monte_carlo.remoteMethod('runSimulation', {
		http: { verb: 'POST', path: '/users/:id/runSimulation' },
		accepts: [
			{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
			{ arg: 'parameters', type: 'object', http: { source: 'body' } }
		],
		returns: { arg: 'data', type: 'object', root: true },
	});

	monte_carlo.remoteMethod('saveInputData', {
		http: { verb: 'POST', path: '/users/:id/saveInputData' },
		accepts: [
			{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
			{ arg: 'parameters', type: 'object', http: { source: 'body' } }
		],
		returns: { arg: 'data', type: 'object', root: true },
	});

	monte_carlo.remoteMethod('saveData', {
		http: { verb: 'POST', path: '/users/:id/saveData' },
		accepts: [
			{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
			{ arg: 'parameters', type: 'object', http: { source: 'body' } }
		],
		returns: { arg: 'data', type: 'object', root: true },
	});

	/** API for return input data for simulation */
	monte_carlo.remoteMethod('returnInputData', {
		http: { verb: 'GET', path: '/users/:id/returnInputData' },
		accepts: [
			{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
		],
		returns: { arg: 'data', type: 'object', root: true }
	});

	monte_carlo.remoteMethod('returnSaveData', {
		http: { verb: 'GET', path: '/users/:id/returnSaveData' },
		accepts: [
			{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
		],
		returns: { arg: 'data', type: 'object', root: true }
	});

	monte_carlo.remoteMethod('returnData', {
		http: { verb: 'GET', path: '/users/:simulationId/returnData' },
		accepts: [
			{ arg: 'simulationId', type: 'string', required: true, http: { source: 'path' } },
		],
		returns: { arg: 'data', type: 'object', root: true }
	});


	monte_carlo.remoteMethod('download', {
		http: { verb: 'GET', path: '/users/:id/download' },
		accepts: [
			{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
			{ arg: 'res', type: 'object', 'http': { source: 'res' } },
		],
		returns: { arg: 'data', type: 'object', root: true },
	});

	monte_carlo.remoteMethod('upload',
		{
			http: { verb: 'POST', path: '/users/:id/upload' },
			accepts: [
				{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
				{ arg: 'req', type: 'object', http: { source: 'req' } },
				{ arg: 'res', type: 'object', http: { source: 'res' } }
			],
			returns: { arg: 'data', type: 'string', root: true }
		}
	);

	monte_carlo.remoteMethod('saveDefaulValue', {
		http: { verb: 'POST', path: '/users/:id/saveDefaulValue' },
		accepts: [
			{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
			{ arg: 'parameters', type: 'object', http: { source: 'body' } }
		],
		returns: { arg: 'data', type: 'object', root: true },
	});

	monte_carlo.remoteMethod('returnSaveDefaulValue', {
		http: { verb: 'GET', path: '/users/:id/returnSaveDefaulValue' },
		accepts: [
			{ arg: 'id', type: 'string', required: true, http: { source: 'path' } },
		],
		returns: { arg: 'data', type: 'object', root: true }
	});

	monte_carlo.sharedClass.methods().forEach((method) => {
		if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download', 'upload', 'returnSaveDefaulValue', 'saveDefaulValue', 'returnData'], method.name)) {
			monte_carlo.disableRemoteMethodByName(method.name, method.isStatic);
		}
	});
}