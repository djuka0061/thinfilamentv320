VERSION 5.00
Begin VB.Form Display_Form 
   AutoRedraw      =   -1  'True
   Caption         =   "Results"
   ClientHeight    =   7740
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9915
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   12
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7740
   ScaleWidth      =   9915
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton PrintComm 
      Caption         =   "Print"
      Height          =   495
      Left            =   4440
      TabIndex        =   2
      Top             =   6240
      Width           =   1215
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Graph"
      Height          =   495
      Left            =   2880
      TabIndex        =   1
      Top             =   6240
      Width           =   1215
   End
   Begin VB.CommandButton CloseB 
      Caption         =   "Close"
      Height          =   495
      Left            =   6000
      TabIndex        =   0
      Top             =   6240
      Width           =   1215
   End
End
Attribute VB_Name = "Display_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Base 1



Private Sub CloseB_Click()
    Display_Form.Hide
    Para_MGNUM_Form.Show
End Sub

Private Sub Command1_Click()
    ParaGraphs_Form.Show
    Display_Form.Hide
End Sub

Private Sub Form_Activate()
    Call Form_Load
End Sub

Private Sub Form_Load()

    Dim File_name As String
    Dim FileNumber As Integer
    Dim Paras(4), derror As Single
    Dim kavg(5), klast(5), kde(5) As Double
    Dim ftitle(5), citem(9) As String
    Dim kkk(100, 4) As Double
    Dim r(100) As Double
    Dim sm(1, 4, 4) As Double
    Dim int_max, flen, jp, ip, kp As Integer
    Dim position(4) As Integer
    
    
    citem(1) = "Kb: "
    citem(2) = "kb-: "
    citem(3) = "Kt: "
    citem(4) = "kt-: "
    citem(5) = "k1+: "
    citem(6) = "k1-: "
    citem(7) = "k2+: "
    citem(8) = "k2-: "
    
    
    ChDrive App.path
    ChDir App.path
    
    Cls
    FileNumber = FreeFile
    
    int_max = CInt(Para_MGNUM_Form.Text19.Text)
    
    'ReDim Preserve kkk(int_max, 4)
    
    'File_name = App.Path & "\" & Para_MGNUM_Form.Text20.Text
    'File_name = Para_MGNUM_Form.Text20.Text
            
    'File_name = App.Path & "\kkk_4b.dat"
    'Open File_name For Binary As #FileNumber
    'Do While Not EOF(FileNumber)
       'Get #FileNumber, , Paras(ip)
       'ip = ip + 1
    'Loop
    'Close #FileNumber
          
        
    
    'flen = 8 * 4 * int_max
        
    'Open File_name For Random As #FileNumber Len = flen
    'Get #FileNumber, 1, kkk
    'Close #FileNumber
    
    
            
    'flen = 8 * int_max
        
    'Open File_name For Random As #FileNumber Len = flen
    'Get #FileNumber, 1, r
    'Close #FileNumber
    
    
            
    'flen = 8 * int_max * 4 * 4
        
    'Open File_name For Random As #FileNumber Len = flen
    'Get #FileNumber, 1, sm
    'Close #FileNumber
    
    File_name = "ps.dat"
    Open File_name For Input As #FileNumber
    Input #FileNumber, real_vs
    For ip = 1 To real_vs
            Input #FileNumber, position(ip)
    Next ip
    Close #FileNumber
    
    File_name = Para_MGNUM_Form.Text3.Text
    Open File_name For Input As #FileNumber
    For ip = 1 To real_vs + 1
            Input #FileNumber, klast(ip), kavg(ip), kde(ip)
    Next ip
        
    For jp = 1 To real_vs
         For ip = 1 To real_vs
                Input #FileNumber, sm(1, ip, jp)
         Next ip
    Next jp
    Close #FileNumber
    
    
        
    For i = 1 To real_vs
        ftitle(i) = citem(position(i))
    Next i
    ftitle(real_vs + 1) = "Error: "
    
    Print "Parameters Estimated: "
    'Print #FileNumber, "Parameters Estimated: "
    Print
    
    Print "                               Last            " _
            & "    Average         Standard Deviation"
    
    Print
    
    For ip = 1 To real_vs + 1
       Print ftitle(ip), Format(klast(ip), "0.0000E+00"), _
         Format(kavg(ip), "0.0000E+00"), Format(kde(ip), "0.0000E+00")
    Next ip
    
      
    Print
    Print "Sensitivity Matrix is: "
    Print
         
     For jp = 1 To real_vs
        For ip = 1 To real_vs
            Print Format(sm(1, ip, jp), "#0.########"),
        Next ip
        Print
     Next jp
    
       
    
     
    
    
 
    
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub

Private Sub PrintComm_Click()
    Display_Form.PrintForm
    Printer.EndDoc
End Sub
