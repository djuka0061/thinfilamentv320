'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const returnOutputData = require('../../middleware/return_data');
const zip = require('../../middleware/zipping');
const winston = require('winston')


//Local include

module.exports = function (stochastical) {

  //function for run simulation
  stochastical.runSimulation = (id, parameters) => {

    //create input file
    let writeStream = fs.createWriteStream('./stochastical/pain.dat');
    writeStream.write("#Output File Name:\n");
    writeStream.write('./Simu_output\n');
    writeStream.write("#Number of Tmtn units:\n");
    writeStream.write(parameters.number_of_tm_tn_units + '\n');
    writeStream.write("#Negative Cooperativity Factor:\n");
    writeStream.write(parameters.negative_cooperativity_factor + '\n');
    writeStream.write('#Numer of Filaments:\n');
    writeStream.write(parameters.number_of_filaments + '\n');
    writeStream.write('#Total Time [s]:\n');
    writeStream.write(parameters.s + '\n');
    writeStream.write('#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):\n');
    writeStream.write(parameters.kb_plus + '\n');
    writeStream.write(parameters.kb_minus + '\n');
    writeStream.write('#The Rate Constants Beetwen the Closed and Open States (Kt / kt-):\n');
    writeStream.write(parameters.kt_plus + '\n');
    writeStream.write(parameters.kt_minus + '\n');
    writeStream.write('#The Rate Constants of Myosin Weak Binding (k1+ / k1-):\n');
    writeStream.write(parameters.k1_plus + '\n');
    writeStream.write(parameters.k1_minus + '\n');
    writeStream.write('#Isomerization Rate Constants (k2+ / k2-):\n');
    writeStream.write(parameters.k2_plus + '\n');
    writeStream.write(parameters.k2_minus + '\n');
    writeStream.write('#Cooperativity Factor:\n')
    writeStream.write(parameters.cooperativity_factor + '\n');
    writeStream.write('#Molar Concentration of Actin [uM]:\n');
    writeStream.write(parameters.molar_concentration_of_actin + '\n');
    writeStream.write('#Molar Concentration of Myosin Initial / Final [uM / uM]:\n')
    writeStream.write(parameters.molar_concentration_of_myosin + '\n');
    writeStream.end(parameters.final_myosin ? parameters.final_myosin + '\n' : parameters.molar_concentration_of_myosin + '\n');

    const options = {
      userID: id,
      ...parameters
    };
    //Simu_output-histogram.DAT
    //Simu_output.DAT

    //setup data for start simulation
    const fileName = 'wraper.exe';
    const formater = 'formater.exe';
    const params = ['input_warp.dat'];
    const path = './stochastical/'
    const formaterParams = [];
    let promise = new Promise((resolve, reject) => {
      exec(fileName, params, { cwd: path }, (err, data) => {
        winston.info('Simulation stochastical successfully start')
        if (err) {
          winston.info(err)
          const response = {
            statusCode: 400,
            message: 'Simulation failed'
          }
          return response
        }
        else {
          winston.info('Simulation stochastical successfully finish')
          exec(formater, formaterParams, { cwd: path }, (err, format) => {
            if (err) reject(err);
            else {
              return loopback.getModel('stochastical').create(options)
                .then(data => {
                  return returnOutputData.returnDataForFirstFourSimulation('stochastical', 'Simu_output_destiled.DAT', 'Simu_output-histogram.DAT')
                    .then(data => {
                      zip.zippingFile('stochastical', 'Simu_output_destiled', 'Simu_output-histogram', data);
                      return resolve(data);
                    });
                })
            }
          })
        }
      });
    });
    return promise;
  }

  stochastical.saveData = (id, parameters) => {
    return returnOutputData.returnDataForFirstFourSimulation('stochastical', 'Simu_output_destiled.DAT', 'Simu_output-histogram.DAT')
      .then(data => {
        if (!data && data !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for stochastical simulation'
          }
          winston.info('Not found output data for stochastical simulation')
          return response
        }
        const inputParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel('stochastical').create(inputParams)
          .then(inputData => {
            const options = {
              userID: id,
              input_id: inputData._id,
              ...data
            };
            return loopback.getModel('stochastical_out').create(options)
              .then(saveData => {
                winston.info('Simulation stochastical successfully save data')
                saveData.statusCode = 200
                return saveData;
              })
          })
      });
  }

  stochastical.returnSaveData = (id) => {
    return loopback.getModel('stochastical_out').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found stochastical simulation'
          }
          winston.info('Not found return data for stochastical simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id;
        return loopback.getModel('stochastical').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found stochastical simulation'
              }
              winston.info('Not found input data for stochastical simulation')
              return response
            }
            const response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response;
          });
      });
  }

  //Save input data for simulation
  stochastical.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    };
    return loopback.getModel('stochastical').create(options)
      .then(data => {
        winston.info('Simulation stochastical successfully save input data')
        return data;
      });
  };

  stochastical.returnInputData = (id) => {
    return loopback.getModel('stochastical').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found stochastical input data`
          }
          winston.info('Not found stochastical input data')
          return response
        }
        return data[data.length - 1];
      })
  }

  stochastical.download = (id, res) => {
    winston.info('Simulation stochastical successfully download data')
    res.download('./stochastical/output_files.zip');
  };

  /** API for run simulation */
  stochastical.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  stochastical.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  stochastical.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  /** API for return input data for simulation */
  stochastical.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  stochastical.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  stochastical.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  stochastical.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download'], method.name)) {
      stochastical.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}