'use strict';

//Global include
const loopback = require('loopback');
const exec = require('child_process').execFile;
const _ = require('underscore');
const fs = require("fs");
const promise = require('bluebird');
const multer = require('multer');
const zippingFile = require('../../middleware/zipping');
const returnOutputData = require('../../middleware/return_data');
const winston = require('winston')

//Local include

module.exports = function (simualted_annealing) {

  //function for run simulation
  simualted_annealing.runSimulation = (id, parameters) => {
    //create input file
    let writeStream = fs.createWriteStream('./simann/Input_params.dat');
    writeStream.write('#Experimental and Output File Name:\n')
    writeStream.write('a_t.txt\n')
    writeStream.write('simann\n')
    writeStream.write('#Number of parameters:\n');
    writeStream.write('8\n');
    writeStream.write('#Total Time [s]:\n');
    writeStream.write(parameters.s + '\n');
    writeStream.write('#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):\n')
    writeStream.write(parameters.kb_plus + '\n');
    writeStream.write(parameters.kb_minus + '\n');
    writeStream.write(parameters.kt_plus + '\n');
    writeStream.write(parameters.kt_minus + '\n');
    writeStream.write(parameters.k1_plus + '\n');
    writeStream.write(parameters.k1_minus + '\n');
    writeStream.write(parameters.k2_plus + '\n');
    writeStream.write(parameters.k2_minus + '\n');
    writeStream.write('#Molar Concentration of Actin [uM]:\n');
    writeStream.write(parameters.actin + '\n');
    writeStream.write('#Molar Concentration of Myosin Initial / Final [uM / uM]:\n');
    writeStream.write(parameters.myosin + '\n');
    writeStream.write((parameters.final_myosin ? parameters.final_myosin : parameters.myosin) + '\n');
    writeStream.write('#Parameters for guess, low and upper bound:\n');
    writeStream.write((parameters.kb_fiting ? parameters.kb_guess : parameters.kb_plus) + '\n');
    writeStream.write((parameters.kb_fiting ? parameters.kb_lower_bound : parameters.kb_plus) + '\n');
    writeStream.write((parameters.kb_fiting ? parameters.kb_upper_bound : parameters.kb_plus) + '\n');
    writeStream.write((parameters.kb_minus_fiting ? parameters.kb_minus_guess : parameters.kb_minus) + '\n');
    writeStream.write((parameters.kb_minus_fiting ? parameters.kb_minus_lower_bound : parameters.kb_minus) + '\n');
    writeStream.write((parameters.kb_minus_fiting ? parameters.kb_minus_upper_bound : parameters.kb_minus) + '\n');
    writeStream.write((parameters.kt_fiting ? parameters.parameters.kt_guess : parameters.kt_plus) + '\n');
    writeStream.write((parameters.kt_fiting ? parameters.parameters.kt_lower_bound : parameters.kt_plus) + '\n');
    writeStream.write((parameters.kt_fiting ? parameters.parameters.kt_upper_bound : parameters.kt_plus) + '\n');
    writeStream.write((parameters.kt_minus_fiting ? parameters.kt_minus_guess : parameters.kt_minus) + '\n');
    writeStream.write((parameters.kt_minus_fiting ? parameters.kt_minus_lower_bound : parameters.kt_minus) + '\n');
    writeStream.write((parameters.kt_minus_fiting ? parameters.kt_minus_upper_bound : parameters.kt_minus) + '\n');
    writeStream.write((parameters.k1_fiting ? parameters.k1_guess : parameters.k1_plus) + '\n');
    writeStream.write((parameters.k1_fiting ? parameters.k1_lower_bound : parameters.k1_plus) + '\n');
    writeStream.write((parameters.k1_fiting ? parameters.k1_upper_bound : parameters.k1_plus) + '\n');
    writeStream.write((parameters.k1_minus_fiting ? parameters.k1_minus_guess : parameters.k1_minus) + '\n');
    writeStream.write((parameters.k1_minus_fiting ? parameters.k1_minus_lower_bound : parameters.k1_minus) + '\n');
    writeStream.write((parameters.k1_minus_fiting ? parameters.k1_minus_upper_bound : parameters.k1_minus) + '\n');
    writeStream.write((parameters.k2_fiting ? parameters.k2_guess : parameters.k2_plus) + '\n');
    writeStream.write((parameters.k2_fiting ? parameters.k2_lower_bound : parameters.k2_plus) + '\n');
    writeStream.write((parameters.k2_fiting ? parameters.k2_upper_bound : parameters.k2_plus) + '\n');
    writeStream.write((parameters.k2_minus_fiting ? parameters.k2_minus_guess : parameters.k2_minus) + '\n');
    writeStream.write((parameters.k2_minus_fiting ? parameters.k2_minus_lower_bound : parameters.k2_minus) + '\n');
    writeStream.end((parameters.k2_minus_fiting ? parameters.parameters.k2_minus_upper_bound : parameters.k2_minus) + '\n');


    const options = {
      userID: id,
      ...parameters
    }

    //setup data for start simulation
    const fileName = 'simann.exe';
    const params = ['Input_params.dat'];
    const path = './simann/'
    let promise = new Promise((resolve, reject) => {
      return exec(fileName, params, { cwd: path }, (err, data) => {
        winston.info('Simulation atphase successfully start')
        if (err) {
          winston.info(err)
          const response = {
            statusCode: 400,
            message: 'Simulation failed'
          }
          return response
        }
        else {
          winston.info('Simulation atphase successfully finish')
          //checked estimation params
          const cekiranaPolja = [Boolean(parameters.kb_fiting), Boolean(parameters.kb_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting),
          Boolean(parameters.k1_fiting), Boolean(parameters.k1_minus_fiting), Boolean(parameters.k2_fiting), Boolean(parameters.k2_minus_fiting), true];

          return loopback.getModel('simualted_annealing').create(options)
            .then(data => {
              //call function for create output data
              console.log("Cap");
              return returnOutputData.returnSimann(cekiranaPolja)
                .then(outputData => {
                  console.log(outputData);
                  //call function for zip output data
                  zippingFile.zipFileSimanQuasi('simann', cekiranaPolja, parameters, outputData);

                  //return output data
                  return resolve(outputData);
                })
            });
        }
      });
    });
    return promise
  }

  simualted_annealing.saveData = (id, parameters) => {
    //checked estimation params
    const cekiranaPolja = [Boolean(parameters.kb_fiting), Boolean(parameters.kb_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting), Boolean(parameters.kt_fiting), Boolean(parameters.kt_minus_fiting),
    Boolean(parameters.k1_fiting), Boolean(parameters.k1_minus_fiting), Boolean(parameters.k2_fiting), Boolean(parameters.k2_minus_fiting), true];

    //call function for create sensitivity matrix
    pravljenjeSensitivityMatrice.createMatrix('./simualted_annealing/new_eee.dat', './simualted_annealing/new_kkk.dat', './simualted_annealing/new_rrr.dat', parameters.number_of_iteration, cekiranaPolja);

    return returnOutputData.returnSimann(cekiranaPolja)
      .then(outputData => {
        if (!outputData && outputData !== []) {
          response = {
            statusCode: 404,
            message: 'Not found output data for atphase simulation'
          }
          winston.info('Not found output data for atphase simulation')
          return response
        }
        const simualted_annealingParams = {
          userID: id,
          ...parameters
        }
        return loopback.getModel(simualted_annealing).create(simualted_annealingParams)
          .then(atp => {
            const options = {
              userID: id,
              input_id: atp._id,
              ...outputData
            }
            return loopback.getModel('simualted_annealing_output').create(options)
              .then(data => {
                winston.info('Simulation atphase successfully save data')
                return data;
              })
          })
      })
  }

  simualted_annealing.returnSaveData = (id) => {
    return loopback.getModel('simualted_annealing_output').find({ where: { userID: id } })
      .then(inputData => {
        if (!inputData) {
          response = {
            statusCode: 404,
            message: 'Not found atphase simulation'
          }
          winston.info('Not found return data for atphase simulation')
          return response
        }
        const inputId = inputData[inputData.length - 1].input_id
        return loopback.getModel('simualted_annealing').findOne({ where: { _id: inputId } })
          .then(data => {
            if (!data) {
              response = {
                statusCode: 404,
                message: 'Not found atphase simulation'
              }
              winston.info('Not found input data for atphase simulation')
              return response
            }
            const response = {
              inputData: inputData[inputData.length - 1],
              parameters: data
            }
            return response
          })
      })
  }

  simualted_annealing.saveInputData = (id, parameters) => {
    const options = {
      userID: id,
      ...parameters
    }
    return loopback.getModel('simualted_annealing').create(options)
      .then(data => {
        winston.info('Simulation atphase successfully save input data')
        return data;
      });
  }

  simualted_annealing.returnInputData = (id) => {
    return loopback.getModel('simualted_annealing').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found atphase input data`
          }
          winston.info('Not found atphase input data')
          return response
        }
        return data[data.length - 1];
      })
  }

  simualted_annealing.saveDefaulValue = (id, parameters) => {
    const options = {
      userID: id,
      default_values: true,
      ...parameters
    }
    return loopback.getModel('simualted_annealing').create(options)
      .then(data => {
        winston.info('Simulation atphase successfully save default values')
        return data;
      })
  }

  simualted_annealing.returnSaveDefaulValue = (id) => {
    return loopback.getModel('simualted_annealing').find({ where: { userID: id } })
      .then(data => {
        if (!data) {
          const response = {
            statusCode: 404,
            message: `Not found atphase default values`
          }
          winston.info('Not found atphase default values')
          return response
        }
        return data[data.length - 1];
      })
  }

  simualted_annealing.download = (id, res) => {
    winston.info('Simulation atphase successfully download data')
    res.download('./simualted_annealing/output_files.zip');
  };

  simualted_annealing.upload = (id, req, res) => {
    console.log("tests");
    var storage = multer.diskStorage({
      destination: function (req, file, callback) {
        callback(null, './simann');
      },
      filename: function (req, file, callback) {
        callback(null, 'a_t.txt');
      }
    });

    console.log("test");
    let upload = multer({ storage: storage }).single('file');

    upload(req, res, function (err) {
      if (err) {
        winston.info(err)
        return res.end({ message: "Error uploading file." });
      }
      winston.info('Simulation atphase successfully update a_t')
      res.json({ message: "File is uploaded" });
    });
  }

  /** API for run simulation */
  simualted_annealing.remoteMethod('runSimulation', {
    http: { verb: 'POST', path: '/users/:id/runSimulation' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  simualted_annealing.remoteMethod('saveInputData', {
    http: { verb: 'POST', path: '/users/:id/saveInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  simualted_annealing.remoteMethod('saveData', {
    http: { verb: 'POST', path: '/users/:id/saveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  /** API for return input data for simulation */
  simualted_annealing.remoteMethod('returnInputData', {
    http: { verb: 'GET', path: '/users/:id/returnInputData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  simualted_annealing.remoteMethod('returnSaveData', {
    http: { verb: 'GET', path: '/users/:id/returnSaveData' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  simualted_annealing.remoteMethod('download', {
    http: { verb: 'GET', path: '/users/:id/download' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'res', type: 'object', 'http': { source: 'res' } },
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  simualted_annealing.remoteMethod('upload',
    {
      http: { verb: 'POST', path: '/users/:id/upload' },
      accepts: [
        { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
        { arg: 'req', type: 'object', http: { source: 'req' } },
        { arg: 'res', type: 'object', http: { source: 'res' } }
      ],
      returns: { arg: 'data', type: 'string', root: true }
    }
  );

  simualted_annealing.remoteMethod('saveDefaulValue', {
    http: { verb: 'POST', path: '/users/:id/saveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
      { arg: 'parameters', type: 'object', http: { source: 'body' } }
    ],
    returns: { arg: 'data', type: 'object', root: true },
  });

  simualted_annealing.remoteMethod('returnSaveDefaulValue', {
    http: { verb: 'GET', path: '/users/:id/returnSaveDefaulValue' },
    accepts: [
      { arg: 'id', type: 'string', required: true, http: { source: 'path' } },
    ],
    returns: { arg: 'data', type: 'object', root: true }
  });

  simualted_annealing.sharedClass.methods().forEach((method) => {
    if (!_.contains(['runSimulation', 'saveInputData', 'saveData', 'returnInputData', 'returnSaveData', 'download', 'upload', 'returnSaveDefaulValue', 'saveDefaulValue'], method.name)) {
      simualted_annealing.disableRemoteMethodByName(method.name, method.isStatic);
    }
  });
}