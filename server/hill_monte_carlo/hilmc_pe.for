!=======================================================================
      program hillmc_pe
!=======================================================================

!=======================================================================
!     Wrapper program that deals with:
!
!     1) Monte Carlo approach code for 
!        Hill's Two-state myosin-actin binding model
!     2) Parameter estimation code (with parameters in log space and
!     data compared in linear space)
!
!     to fit the model constants and obtain the sentsitivity matrix
!
!     The code can estimate any subset of the reaction constants
!       K1,  (parameter 1)
!       K2,  (parameter 2)
!       Y, (parameter 3)
!       L', (parameter 4)
!
!     Edward X. Li March 15, 2006
!       
! 
!      
!=======================================================================
	USE MSFLIB   !for file handler, path, etc.
	implicit none
      
      integer(4) nparam0 !!! total number of parameters
      integer(4), allocatable, dimension (:) :: vecparam, nsim
      integer(4) :: iter, maxiter, ip, ip0, nlen, nparam

      real(8), allocatable, dimension (:) :: knew, kold, minparam, 
     &                                       maxparam, delta, xparam0, 
     &                                       xparam, dum

      real(8), allocatable, dimension (:) :: err
      real(8), allocatable, dimension (:,:) :: kkk
      real(8), allocatable, dimension (:,:,:) :: rrr
      real(8) :: eps

      character(200) :: filexp, filkkk, filerr, filrrr, filexe

      ! gets input data

      open(10,file='input_wrap')
      write(*,*) 'Number of parameters to be estimated'
      read(10,*) nparam0
   
      ! does some allocation
      allocate(vecparam(nparam0+1))  !!! flag that controls if the parameter has to be estimated
      allocate(nsim(nparam0+1))      !!! length of the simulation data
      allocate(knew(nparam0))        !!! parameters at current iteration  
      allocate(kold(nparam0))        !!! parameters at previous iteration  
      allocate(minparam(nparam0))    !!! minimum bound for parameter
      allocate(maxparam(nparam0))    !!! maximum bound for parameter
      allocate(delta(nparam0))       !!! variation of the parameters with respect to previous iteration
      allocate(xparam0(nparam0))     !!! working array for parameters
      allocate(xparam(nparam0))      !!! working array for parameters
      allocate(dum(nparam0))         !!! dummy
      
      write(*,*) 'Determination of parameters to be estimated'
      do ip = 1,nparam0
         write(*,*) 'Guess 1 of param',ip,'(type 0d0 to estimate param)'
         read(10,*) kold(ip)
         write(*,*) 'Guess 2 of param',ip,'(type 0d0 to estimate param)'
         read(10,*) knew(ip)
         write(*,*) 'Minimum bound for param',ip
         read(10,*) minparam(ip)
         write(*,*) 'Maximum bound for param',ip
         read(10,*) maxparam(ip)
      enddo
      write(*,*) 'Introduce Epsilon'
      read(10,*) eps
      write(*,*) 'Introduce maximum number of iterations'
      read(10,*) maxiter
      write(*,*) 'Introduce file name of experimental data'
      read(10,*) filexp
      write(*,*) 'Introduce file name of constants file'
      read(10,*) filkkk
      write(*,*) 'Introduce file name of error file'
      read(10,*) filerr
      write(*,*) 'Introduce file name of sensitivity matrix file'
      read(10,*) filrrr
      write(*,*) 'Introduce file name of executable model code'
      read(10,*) filexe
      close(10)

      ! allocate some other variables
      allocate (err(maxiter))  !! error
      allocate (kkk(maxiter,nparam0))  !! vector of constants
      allocate (rrr(maxiter,nparam0,nparam0))  !! sensitivity matrix
      
      do iter = 1,maxiter  ! main iteration loop
      
		write(*,*) "Iteration ", iter, " / ", maxiter
		write(*,*) "  " 
   
         ! first simulation run
         call runsimulation(kold,1,nlen,filexe)
         nsim(1) = nlen
         
         nparam = 0  ! actual number of parameters estimated

         do ip = 1,nparam0  ! perturbed simulation runs
            
            vecparam(ip) = 0

            if (kold(ip).ne.knew(ip)) then ! checks if run needed for this param
               
               nparam = nparam + 1
               vecparam(ip) = 1

               delta(nparam) = knew(ip)/kold(ip) - 1d0
               xparam(nparam) = kold(ip)

               do ip0=1,nparam0
                  xparam0(ip0) = kold(ip0)
               enddo
               
               ! perturbs parameter and run
               xparam0(ip) = knew(ip)
               call runsimulation(xparam0,nparam+1,nlen,filexe)
               nsim(nparam+1) = nlen
                    
            endif

         enddo   ! end simulation runs

         ! runs estimation code
         call runestim(filexp,nsim,nparam,eps,xparam,delta)

         ! saves knew
         do ip=1,nparam0
            kold(ip) = knew(ip)
         enddo
         
         ! loads output from estimation code
         open(10,file='output.dat')
         do ip = 1,nparam
            read(10,*) dum(ip)
         enddo

         ! updates knew
         nparam = 0
         do ip=1,nparam0
            if (vecparam(ip) == 1) then
               nparam = nparam + 1
               knew(ip) = min( max( dum(nparam),minparam(ip)),
     &                              maxparam(ip))
            endif
         enddo
         
         ! postprocessing results
         do ip=1,nparam0
            kkk(iter,ip) = knew(ip)
         enddo
         
         ! error
         open(10,file='error.dat')
         read(10,*) err(iter)
         close(10)

         ! sensitivity matrix
         open(10,file='R.dat')
         do ip=1,nparam
            read(10,*) (rrr(iter,ip,ip0),ip0=1,nparam)
         enddo
         close(10)

      enddo  ! end main iteration loop

      ! writes results
	open(10,file='nIter.dat')
	write(10,*) maxiter
      write(10,*) nparam0
      close(10)

      open(10,file=filkkk)
	do iter = 1, maxiter
         write(10,*) (kkk(iter,ip),ip=1,nparam0)
	end do
      close(10)

      ! error
      open(10,file=filerr)
      write(10,*) (err(iter),iter=1,maxiter)
      close(10)

      ! sensitivity matrix
      open(10,file=filrrr)
	do iter = 1, maxiter
	   do ip = 1, nparam0
           write(10,*) (rrr(iter,ip, ip0),ip0=1,nparam0)
	   end do
	end do
      close(10)

C      ! constants
C      open(10,file=filkkk,form='binary',access='direct',
C     &     recl=8*maxiter*nparam0)
C      write(10,rec=1) ((kkk(iter,ip),iter=1,maxiter),ip=1,nparam0)
C      close(10)

      ! error
C      open(10,file=filerr,form='binary',access='direct',
C     &     recl=8*maxiter)
C      write(10,rec=1) (err(iter),iter=1,maxiter)
C      close(10)

      ! sensitivity matrix
C      open(10,file=filrrr,form='binary',access='direct',
C     &     recl=8*maxiter*nparam0*nparam0)
C      write(10,rec=1) (((rrr(iter,ip,ip0),iter=1,maxiter),ip=1,nparam0),
C     &                   ip0=1,nparam0)
C      close(10)

!=======================================================================
      end
!=======================================================================

!=======================================================================
      subroutine runsimulation(param0,icase,nlen,filexe)
!=======================================================================
!     writes parameters to input files and runs simulation
!     version that works for Edward's rigidchain_jc code, low calcium
! edward li Jan. 27, 2006
!
!     add a variable nmpt -- #actin per unit 
!       executable filename comes now from outside
!
!=======================================================================
      USE PORTLIB
	USE MSFLIB   !for file handler, path, etc.
      implicit none
      
      real(8), dimension (*) :: param0
      integer(4) icase, ierr, nlen
      character(200) :: filexe
	real(8) rtemp !temp variable
	character(4) fcase

!     writes parameters to simulation input files

      open(20,file='pain.dat',form='formatted')
!	 read in file
      open(10,file='tmtn+ca.dat',form='formatted')
!	 write file

      write(10,'(a18)') '#Output File Name:'
      write(10,'(a5)') 'model'
	read(20,*) rtemp  !readin #filaments
	write(10,'(a25)') '#The Number of Filaments:'
	write(10,*) rtemp
 	
      read(20,*) rtemp  !readin #tmtn
	write(10,'(a26)') '#The Number of TmTn units:'
	write(10,*) rtemp

	read(20,*) rtemp  !readin #Actin per unit
 	write(10,'(a30)') '#The Number of Actin per unit:'
	write(10,*) rtemp
	
	read(20,*) rtemp  !readin total time
      write(10,'(a16)') '#Total Time [s]:'
      write(10,*) rtemp                    
 	                            
      write(10,'(a35)')'#Molar Concentration of Actin [uM]:'
	read(20,*) rtemp  !readin ca
	write(10,*) rtemp

      write(10,'(a35)')'#Molar Concentration of Myosin[uM]:'
	read(20,*) rtemp  !readin cmi
	write(10,*) rtemp  
	read(20,*) rtemp  !readin cmf
	write(10,*) rtemp  
 
     	           
      write(10,'(a18)') '#Alpha0 and Beta0:'
      read(20,*) rtemp  !readin alphao
 	write(10,*) param0(1) !write alpha0 
	write(10,*) param0(2)  !write beta0
	read(20,*) rtemp  !readin beta0

	                  
      write(10,'(a22)')'#Constants Y1 and Y2: ' 
      write(10,*) param0(3)	!write y1
	read(20,*) rtemp  !readin y1
	read(20,*) rtemp  !readin y2
	write(10,*) param0(4)  !write y2                  

      write(10,'(a59)') '#The Foward/Backward Rate Constants '//
     &            'of OFF State (k1 / k1):'
      write(10,*) param0(5)
	read(20,*) rtemp  !readin k1
	read(20,*) rtemp  !readin k1'
	write(10,*) param0(6)  
	          
      write(10,'(a58)')'#The Foward/Backward Rate Constants '//
	&            'of ON State (k2 / k2):'
	write(10,*) param0(7)
      read(20,*) rtemp  !readin k2
	read(20,*) rtemp  !readin k2'
      write(10,*) param0(8)   
       
	write(10,'(a16)')'#Parameter Gama:' 
	read(20,*) rtemp  !readin gama
	write(10,*) rtemp  !write gama
	
   
	write(10,'(a35)') '#The Cooperativity Parameter Delta:'
	read(20,*) rtemp  !readin cooperativity factor
	write(10,*) rtemp  


	write(10,'(a44)') '#The Cooperativity Switch (1 - ON /0 - OFF):'
	read(20,*) rtemp  !readin cooperativity switch
	write(10,*) rtemp  


      close (10)
	close (20)

	 

      open(10,file='input')
      write(10,'(a11)') 'tmtn+ca.dat'
      write(10,*) icase
      close(10)

      ! runs simulation
      ierr = system(filexe //' < input')
      if( ierr==1) stop

	!get nlen
	write (fcase,'(I4.4)') icase
	open(20,file='nlen'//fcase//'.dat',form='formatted')
	read(20,*) nlen
	close(20)

!=======================================================================
      end
!=======================================================================

!=======================================================================
      subroutine runestim(filexp,nsim,nparam,eps,xparam,delta)
!=======================================================================
!     arranges input files for estimation code
! juanc, Dec 8 2005.
!=======================================================================
      USE PORTLIB
	USE MSFLIB   !for file handler, path, etc.
      implicit none 

      integer(4) :: iread, nobs, nparam, ierr, ip
      integer(4), dimension(*) :: nsim

      real(8) :: data1, data2, eps
      real(8), dimension(*) :: xparam, delta

      character(200) :: filexp

      ! experimental data
      open(10,file=filexp)
      open(20,file='obser_data.dat')

      nobs = 0   !! number of observations
      iread=1
      do while(iread==1)
         read (10,*,err=100) data1, data2
         write(20,*) data1,data2
         nobs = nobs + 1
      enddo
 100  continue
      
      close(10)
      close(20)

      open(10,file='input_estim')
      write(10,*) nparam
      write(10,*) nobs
      do ip=1,nparam+1
         write(10,*) nsim(ip)
      enddo
      write(10,*) eps
      close(10)

      open(10,file='param_data.dat')
      do ip=1,nparam
         write(10,*) xparam(ip)
      enddo
      close(10)

      open(10,file='delta_data.dat')
      do ip=1,nparam
         write(10,*) delta(ip)
      enddo
      close(10)
      
      ierr = system('estimre input_estim')
      if( ierr==1) stop

!=======================================================================
      end
!=======================================================================
