VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form MGNUM_Form 
   Caption         =   "MG Model Simulation Probabilistic "
   ClientHeight    =   9390
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11235
   LinkTopic       =   "Form1"
   MousePointer    =   1  'Arrow
   ScaleHeight     =   9390
   ScaleWidth      =   11235
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton browse 
      Caption         =   "Browse"
      Height          =   375
      Left            =   8040
      TabIndex        =   33
      Top             =   1560
      Width           =   735
   End
   Begin VB.CommandButton Histogram 
      Caption         =   "Histogram"
      Height          =   435
      Left            =   4920
      TabIndex        =   32
      Top             =   8400
      Width           =   1485
   End
   Begin VB.TextBox Text1 
      Height          =   375
      Left            =   2400
      TabIndex        =   31
      Text            =   "Text1"
      Top             =   1560
      Width           =   5415
   End
   Begin VB.CommandButton Return_comm 
      Caption         =   "Return Main"
      Height          =   435
      Left            =   6600
      TabIndex        =   15
      Top             =   8400
      Width           =   1485
   End
   Begin VB.TextBox Text10 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   6
      Text            =   "20"
      Top             =   4620
      Width           =   1485
   End
   Begin VB.TextBox Text12 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   8
      Text            =   "20"
      Top             =   5460
      Width           =   1485
   End
   Begin VB.Frame Frame1 
      Caption         =   "Type of Simulation"
      Height          =   645
      Left            =   3120
      TabIndex        =   27
      Top             =   6195
      Width           =   3270
      Begin VB.OptionButton Titration_Option 
         Caption         =   "Titration"
         Height          =   225
         Left            =   1995
         TabIndex        =   11
         Top             =   315
         Width           =   960
      End
      Begin VB.OptionButton Time_Option 
         Caption         =   "Time Course"
         Height          =   225
         Left            =   525
         TabIndex        =   10
         Top             =   315
         Value           =   -1  'True
         Width           =   1275
      End
   End
   Begin VB.CommandButton Graphs_Button 
      Caption         =   "Graphs"
      Enabled         =   0   'False
      Height          =   435
      Left            =   6600
      TabIndex        =   14
      Top             =   7800
      Width           =   1485
   End
   Begin VB.CommandButton Run_Button 
      Caption         =   "Run"
      Enabled         =   0   'False
      Height          =   435
      Left            =   4920
      TabIndex        =   16
      Top             =   7800
      Width           =   1485
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.TextBox Text15 
      Alignment       =   2  'Center
      Enabled         =   0   'False
      Height          =   300
      Left            =   8085
      TabIndex        =   13
      Text            =   "0.25"
      Top             =   6930
      Visible         =   0   'False
      Width           =   1485
   End
   Begin VB.TextBox Text14 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   12
      Text            =   "0.25"
      Top             =   6930
      Width           =   1485
   End
   Begin VB.TextBox Text13 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   9
      Text            =   "0.05"
      Top             =   5880
      Width           =   1485
   End
   Begin VB.TextBox Text11 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   7
      Text            =   "3760"
      Top             =   5040
      Width           =   1485
   End
   Begin VB.TextBox Text9 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   5
      Text            =   "2e6"
      Top             =   4200
      Width           =   1485
   End
   Begin VB.TextBox Text8 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   4
      Text            =   "3000"
      Top             =   3780
      Width           =   1485
   End
   Begin VB.TextBox Text7 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   3
      Text            =   "0.013"
      Top             =   3360
      Width           =   1485
   End
   Begin VB.TextBox Text6 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   2
      Text            =   "100"
      Top             =   2940
      Width           =   1485
   End
   Begin VB.TextBox Text5 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   1
      Text            =   "0.3"
      Top             =   2520
      Width           =   1485
   End
   Begin VB.TextBox Text4 
      Alignment       =   2  'Center
      Height          =   300
      Left            =   6510
      TabIndex        =   0
      Text            =   "5"
      Top             =   2100
      Width           =   1485
   End
   Begin VB.Label Label2 
      Caption         =   "Probabilistic MG Model Simulation"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000000FF&
      Height          =   495
      Left            =   960
      TabIndex        =   30
      Top             =   720
      Width           =   7575
   End
   Begin VB.Label Label19 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant of Myosin Weak Binding (k1- [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   29
      Top             =   4725
      Width           =   5595
   End
   Begin VB.Label Label18 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Isomerization Rate Constant (k2- [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   28
      Top             =   5565
      Width           =   5595
   End
   Begin VB.Label Label13 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Myosin [uM]:"
      Height          =   285
      Left            =   0
      TabIndex        =   26
      Top             =   7035
      Width           =   6435
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Molar Concentration of Actin [uM]:"
      Height          =   285
      Left            =   840
      TabIndex        =   25
      Top             =   5985
      Width           =   5595
   End
   Begin VB.Label Label11 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Isomerization Rate Constant (k2+ [1/s]):"
      Height          =   285
      Left            =   840
      TabIndex        =   24
      Top             =   5145
      Width           =   5595
   End
   Begin VB.Label Label10 
      Alignment       =   1  'Right Justify
      Caption         =   "Forward Rate Constant of Myosin Weak Binding (k1+ [1/(M*s)]):"
      Height          =   285
      Left            =   840
      TabIndex        =   23
      Top             =   4305
      Width           =   5595
   End
   Begin VB.Label Label9 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant Beetwen the Closed and Open States (kt-):"
      Height          =   285
      Left            =   840
      TabIndex        =   22
      Top             =   3885
      Width           =   5595
   End
   Begin VB.Label Label8 
      Alignment       =   1  'Right Justify
      Caption         =   "Equlibrium Constant Beetwen the Closed and Open States (Kt):"
      Height          =   285
      Left            =   840
      TabIndex        =   21
      Top             =   3465
      Width           =   5595
   End
   Begin VB.Label Label7 
      Alignment       =   1  'Right Justify
      Caption         =   "Backward Rate Constant Beetwen the Blocked and Closed States (kb-):"
      Height          =   285
      Left            =   840
      TabIndex        =   20
      Top             =   3045
      Width           =   5595
   End
   Begin VB.Label Label6 
      Alignment       =   1  'Right Justify
      Caption         =   "Equilibrium Constant Beetwen the Blocked and Closed States (Kb):"
      Height          =   285
      Left            =   840
      TabIndex        =   19
      Top             =   2625
      Width           =   5595
   End
   Begin VB.Label Label5 
      Alignment       =   1  'Right Justify
      Caption         =   "Total Time [s]:"
      Height          =   285
      Left            =   840
      TabIndex        =   18
      Top             =   2205
      Width           =   5595
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      Caption         =   "Output File Name:"
      Height          =   405
      Left            =   1440
      TabIndex        =   17
      Top             =   1560
      Width           =   915
   End
End
Attribute VB_Name = "MGNUM_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim OutputFileName As String

Private Sub browse_Click()

    CommonDialog1.CancelError = False
    
    ' Set flags
    CommonDialog1.Flags = cdlOFNHideReadOnly
    ' Set filters
    'CommonDialog1.Filter = " *.dat |*"
    ' Specify default filter
    CommonDialog1.FilterIndex = 1
    ' Suggest the file name
    'opencd1.FileName = "Simu_Output"
    ' Display the open dialog box
    CommonDialog1.ShowOpen
    ' Display name of selected file (with the path)
    OutputFileName = CommonDialog1.FileName
    Text1.Text = OutputFileName
    ' Display name of selected file (without the path)
    'OutputFiletitle = CommonDialog1.FileTitle
    
    
End Sub

Private Sub Form_Load()
    Text1.Text = App.path & "\Simu_output"
    OutputFileName = Text1.Text
    Run_Button.Enabled = True
    Graphs_Button.Enabled = True
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Main_Form.Show
End Sub

Private Sub Graphs_Button_Click()
    

    'Graphs_Button.Enabled = False

    Graphs_Form.Show
   MGNUM_Form.Hide

ErrHandler:
    'User pressed the Cancel button

End Sub


Private Sub Histogram_Click()
    Histogram_Form.Show
    MGNUM_Form.Hide

End Sub

Private Sub Return_comm_Click()
    Main_Form.Show
    MGNUM_Form.Hide
End Sub

Private Sub Run_Button_Click()
    
    Dim FileNumber As Integer
    Dim Input_File_Name, ffname As String

    On Error GoTo ErrHandler
    
    ChDrive App.path
    ChDir App.path

    If OutputFileName <> "" Then

        If Time_Option.Value = True Then
            Text15.Text = Text14.Text
            Text15.Enabled = False
            Text15.Visible = False
            Label13.Caption = "Molar Concentration of Myosin [uM]:"
        End If

        
        Input_File_Name = "pain.dat"
        
        'Export data to a file
        FileNumber = FreeFile
        Open Input_File_Name For Output As #FileNumber
        Print #FileNumber, "#Output File Name:"
        Print #FileNumber, OutputFileName
        Print #FileNumber, "#Total Time [s]:"
        Print #FileNumber, Text4.Text
        Print #FileNumber, "#The Rate Constants Beetwen the Blocked and Closed States (Kb / kb-):"
        Print #FileNumber, Text5.Text
        Print #FileNumber, Text6.Text
        Print #FileNumber, "#The Rate Constants Beetwen the Closed and Open States (Kt / kt-):"
        Print #FileNumber, Text7.Text
        Print #FileNumber, Text8.Text
        Print #FileNumber, "#The Rate Constants of Myosin Weak Binding (k1+ / k1-):"
        Print #FileNumber, Text9.Text
        Print #FileNumber, Text10.Text
        Print #FileNumber, "#Isomerization Rate Constants (k2+ / k2-):"
        Print #FileNumber, Text11.Text
        Print #FileNumber, Text12.Text
        Print #FileNumber, "#Molar Concentration of Actin [uM]:"
        Print #FileNumber, Text13.Text
        Print #FileNumber, "#Molar Concentration of Myosin Initial / Final [uM / uM]:"
        Print #FileNumber, Text14.Text
        Print #FileNumber, Text15.Text
        Close #FileNumber

        MGNUM_Form.MousePointer = vbHourglass
        Run_Button.Enabled = False
        Graphs_Button.Enabled = False
        
        ffname = "input_wrap"
        FileNumber = FreeFile
        Open ffname For Output As #FileNumber
            Print #FileNumber, "pain.dat"
            Print #FileNumber, "num_rigid.exe"
            Print #FileNumber, "1"
        Close #FileNumber

        'Call xShell(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34), 1, False)
        'Call ExecCmd(App.Path & "\acto-myosin-07.exe " & Chr(34) & Input_File_Name & Chr(34))
        Call ExecCmd(App.path & "\wraper.exe " & Chr(34))
        
        MGNUM_Form.MousePointer = vbArrow
        
        Run_Button.Enabled = True
        Graphs_Button.Enabled = True
       
    End If

    Exit Sub

ErrHandler:
    'User pressed the Cancel button
    Exit Sub

End Sub

Private Sub Text1_Change()
    OutputFileName = Text1.Text
End Sub

Private Sub Time_Option_Click()
    If Time_Option.Value = True Then
        Text14.Text = 0.5
        Text15.Text = Text14.Text
        Text4.Text = 5
        Text15.Enabled = False
        Text15.Visible = False
        Label13.Caption = "Molar Concentration of Myosin [uM]:"
    End If
End Sub



Private Sub Titration_Option_Click()
    If Titration_Option.Value = True Then
        Text4.Text = 250
        Text14.Text = 0
        Text15.Enabled = True
        Text15.Visible = True
        Label13.Caption = "(For Titration) Molar Concentration of Myosin Initial / Final [uM / uM]:"
    End If
End Sub
