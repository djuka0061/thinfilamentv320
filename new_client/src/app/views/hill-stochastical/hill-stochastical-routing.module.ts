import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HillStochasticalComponent } from './hill-stochastical.component';

const routes: Routes = [
  {
    path: '',
    component: HillStochasticalComponent,
    data: {
      title: 'HillStochastical'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HillStochasticalRoutingModule {}
