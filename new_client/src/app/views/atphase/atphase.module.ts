import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';

import { ATPhaseComponent } from './atphase.component';
import { ATPhaseRoutingModule } from './atphase-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    ATPhaseRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    MatTableModule,
    MatPaginatorModule
  ],
  declarations: [ ATPhaseComponent ],
  exports: [
    MatTableModule,
    MatPaginatorModule
  ]
})
export class ATPhaseModule { }
