#!/bin/sh

set -ex

if [[ $@ == run ]]; then
    echo -n "Running npm run start command..."
    exec npm run start
fi

exec "$@"
