import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { CommonModule } from "@angular/common";

import { ArchivePlainComponent} from './archive-plain.component';
import { ArchivePlainRoutingModule } from './archive-plain-routing.module';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    ArchivePlainRoutingModule,
    ChartsModule,
    BsDropdownModule,
    MatTableModule,
    MatPaginatorModule
  ],
  declarations: [ ArchivePlainComponent ],
  exports: [
    MatTableModule,
    MatPaginatorModule
  ]
})
export class ArchivePlainModule { }
