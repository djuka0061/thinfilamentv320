!=======================================================================
      program paramestim
!=======================================================================

!=======================================================================
!     Wrapper program that deals with:
!
!     1) Continuous approach code for 
!        Geeve's myosin-actin binding model
!     2) Parameter estimation code (with parameters in log space and
!     data compared in linear space)
!
!     to fit the model constants and obtain the sentsitivity matrix
!
!     The code can estimate any subset of the reaction constants
!       kb,  (parameter 1)
!       kt,  (parameter 2)
!       k1+, (parameter 3)
!       k2+, (parameter 4)
!
! juanc, Dec 8 2005.
!       
!       REVISION 1  (juanc, jan 17 2006)
!      
!       total number of parameters is now read from input file
!       some comments added
!       model executable filename is now read from input file
!      
!       REVISION 2  (juanc and eli, jan 25 2006)
!      
!       changed the way the estimation code is called to make it
!       compatible with edward's VB code. 
!
!	  REVISION 2.1 (edward li, Jan. 26, 2006)
!		correted the way read/write input file
!		because some parameters are not fixed. 
!
!	 REVISION 2.2 (edward li, Jan.27,2006)
!	   readin NLEN from File   
!      
!=======================================================================
	USE MSFLIB   !for file handler, path, etc.
      implicit none
      
      integer(4) nparam0 !!! total number of parameters
      integer(4), allocatable, dimension (:) :: vecparam, nsim
      integer(4) :: iter, maxiter, ip, ip0, nlen, nparam

      real(8), allocatable, dimension (:) :: knew, kold, minparam, 
     &                                       maxparam, delta, xparam0, 
     &                                       xparam, dum

      real(8), allocatable, dimension (:) :: err
      real(8), allocatable, dimension (:,:) :: kkk
      real(8), allocatable, dimension (:,:,:) :: rrr
      real(8) :: eps


      character(200) :: filexp, filkkk, filerr, filrrr, filexe

      ! gets input data

      open(10,file='input_wrap')
C      write(*,*) 'Number of parameters to be estimated'
      read(10,*) nparam0
   
      ! does some allocation
      allocate(vecparam(nparam0+1))  !!! flag that controls if the parameter has to be estimated
      allocate(nsim(nparam0+1))      !!! length of the simulation data
      allocate(knew(nparam0))        !!! parameters at current iteration  
      allocate(kold(nparam0))        !!! parameters at previous iteration  
      allocate(minparam(nparam0))    !!! minimum bound for parameter
      allocate(maxparam(nparam0))    !!! maximum bound for parameter
      allocate(delta(nparam0))       !!! variation of the parameters with respect to previous iteration
      allocate(xparam0(nparam0))     !!! working array for parameters
      allocate(xparam(nparam0))      !!! working array for parameters
      allocate(dum(nparam0))         !!! dummy
      
C      write(*,*) 'Determination of parameters to be estimated'
      do ip = 1,nparam0
C         write(*,*) 'Guess 1 of param',ip,'(type 0d0 to estimate param)'
         read(10,*) kold(ip)
C         write(*,*) 'Guess 2 of param',ip,'(type 0d0 to estimate param)'
         read(10,*) knew(ip)
C         write(*,*) 'Minimum bound for param',ip
         read(10,*) minparam(ip)
C         write(*,*) 'Maximum bound for param',ip
         read(10,*) maxparam(ip)
      enddo
C      write(*,*) 'Introduce Epsilon'
      read(10,*) eps
C      write(*,*) 'Introduce maximum number of iterations'
      read(10,*) maxiter
C      write(*,*) 'Introduce file name of experimental data'
      read(10,*) filexp
C      write(*,*) 'Introduce file name of constants file'
      read(10,*) filkkk
C      write(*,*) 'Introduce file name of error file'
      read(10,*) filerr
C      write(*,*) 'Introduce file name of sensitivity matrix file'
      read(10,*) filrrr
C      write(*,*) 'Introduce file name of executable model code'
      read(10,*) filexe
      close(10)

      ! allocate some other variables
      allocate (err(maxiter))  !! error
      allocate (kkk(maxiter,nparam0))  !! vector of constants
      allocate (rrr(maxiter,nparam0,nparam0))  !! sensitivity matrix
      

      do iter = 1,maxiter  ! main iteration loop
         
	   write(*,*) "Iteration ", iter, " / ", maxiter
	   write(*,*) "  " 
         ! first simulation run
         call runsimulation(kold,1,nlen,filexe)
         nsim(1) = nlen
         
         nparam = 0  ! actual number of parameters estimated

         do ip = 1,nparam0  ! perturbed simulation runs
            
            vecparam(ip) = 0

            if (kold(ip).ne.knew(ip)) then ! checks if run needed for this param
               
               nparam = nparam + 1
               vecparam(ip) = 1

               delta(nparam) = knew(ip)/kold(ip) - 1d0
               xparam(nparam) = kold(ip)

               do ip0=1,nparam0
                  xparam0(ip0) = kold(ip0)
               enddo
               
               ! perturbs parameter and run
               xparam0(ip) = knew(ip)
               call runsimulation(xparam0,nparam+1,nlen,filexe)
               nsim(nparam+1) = nlen
                    
            endif

         enddo   ! end simulation runs

	   !setting eps by connecting it with spread function
	   !sf:0~nparam,  eps:0.001~1.0	in log
	   !edward li Jan.03, 2007
	!matrix is good, but method fails sometimes.
C		if ( iter .gt. 1 ) then
C			sf=0.0
C			do ip=1,nparam
C				do ip0=1, nparam
C					if (ip .ne. ip0 ) then
C					  sf = sf + rrr(iter-1,ip,ip0)*rrr(iter-1,ip,ip0)
C					else
C					  sf = sf + (rrr(iter-1,ip,ip0)-1.0) *
C	&						  (rrr(iter-1,ip,ip0)-1.0)
C					endif
C				enddo
C			enddo
C			eps = 3.0*sf/(nparam*1.0)-3.0
C			eps = 10**eps
C	   	else
C			eps=0.050  	   !initial eps value
C		endif


         ! runs estimation code
         call runestim(filexp,nsim,nparam,eps,xparam,delta)

         ! saves knew
         do ip=1,nparam0
            kold(ip) = knew(ip)
         enddo
         
         ! loads output from estimation code
         open(10,file='output.dat')
         do ip = 1,nparam
            read(10,*) dum(ip)
         enddo

         ! updates knew
         nparam = 0
         do ip=1,nparam0
            if (vecparam(ip) == 1) then
               nparam = nparam + 1
               knew(ip) = min( max( dum(nparam),minparam(ip)),
     &                              maxparam(ip))
            endif
         enddo
         
         ! postprocessing results
         do ip=1,nparam0
            kkk(iter,ip) = knew(ip)
         enddo
         
         ! error
         open(10,file='error.dat')
         read(10,*) err(iter)
         close(10)

         ! sensitivity matrix
         open(10,file='R.dat')
         do ip=1,nparam
            read(10,*) (rrr(iter,ip,ip0),ip0=1,nparam)
         enddo
         close(10)

      enddo  ! end main iteration loop

      ! writes results
	open(10,file='nIter.dat')
      write(10,*) maxiter
      write(10,*) nparam0
      close(10)

      open(10,file=filkkk)
	do iter = 1, maxiter
         write(10,*) (kkk(iter,ip),ip=1,nparam0)
	end do
      close(10)

      ! error
      open(10,file=filerr)
      write(10,*) (err(iter),iter=1,maxiter)
      close(10)

      ! sensitivity matrix
      open(10,file=filrrr)
	do iter = 1, maxiter
	   do ip = 1, nparam0
           write(10,*) (rrr(iter,ip, ip0),ip0=1,nparam0)
	   end do
	end do
      close(10)


      ! constants
C      open(10,file=filkkk,form='binary',access='direct',
C     &     recl=8*maxiter*nparam0)
C      write(10,rec=1) ((kkk(iter,ip),iter=1,maxiter),ip=1,nparam0)
C      close(10)

      ! error
C      open(10,file=filerr,form='binary',access='direct',
C     &     recl=8*maxiter)
C      write(10,rec=1) (err(iter),iter=1,maxiter)
C      close(10)

      ! sensitivity matrix
C      open(10,file=filrrr,form='binary',access='direct',
C     &     recl=8*maxiter*nparam0*nparam0)
C      write(10,rec=1) (((rrr(iter,ip,ip0),iter=1,maxiter),ip=1,nparam0),
C     &                   ip0=1,nparam0)
C      close(10)

!=======================================================================
      end
!=======================================================================

!=======================================================================
      subroutine runsimulation(param0,icase,nlen,filexe)
!=======================================================================
!     writes parameters to input files and runs simulation
!     version that works for Edward's num_rigid code, low calcium
! juanc, Dec 8 2005.
!
!       REVISION 1  (juanc, jan 17 2006)
!      
!       executable filename comes now from outside
!
!=======================================================================
      USE PORTLIB
	USE MSFLIB   !for file handler, path, etc.
      implicit none
      
      real(8), dimension (*) :: param0
      integer(4) icase, ierr, nlen
      character(200) :: filexe
	real(8) rtemp !temp variable
	character(4) fcase

!     writes parameters to simulation input files

      open(20,file='tmtn-ca.dat',form='formatted')
!	 read in file
      open(10,file='tmtn+ca.dat',form='formatted')
!	 write file

      write(10,'(a18)') '#Output File Name:'
      write(10,'(a5)') 'model'
	read(20,*) rtemp  !readin total time
      write(10,'(a16)') '#Total Time [s]:'
      write(10,*) rtemp                    
 
      write(10,'(a69)') '#The Rate Constants Beetwen the Blocked and '//
     &            'Closed States (Kb / kb-):'
      write(10,*) param0(1)  !writing kb
	write(10,*) param0(2)  !writing kb-
	read(20,*) rtemp  !readin kb
	read(20,*) rtemp  !readin kb-
 
C      write(10,*) rtemp !write kb-                   
      write(10,'(a66)')'#The Rate Constants Beetwen the Closed and '// 
     &           'Open States (Kt / kt-):' 
      write(10,*) param0(3) !writing kt
	write(10,*) param0(4) !writing kt-
	read(20,*) rtemp  !readin kt
	read(20,*) rtemp  !readin kt-

C      write(10,*) rtemp                    

      write(10,'(a54)') '#The Rate Constants of Myosin Weak Binding'//
     &            '(k1+ / k1-):'
      write(10,*) param0(5)	!writing k1+
	write(10,*) param0(6)	!writing k1-
	read(20,*) rtemp  !readin k1+
	read(20,*) rtemp  !readin k1-

C      write(10,*) rtemp            
      write(10,'(a42)')'#Isomerization Rate Constants (k2+ / k2-):'
      write(10,*) param0(7)  !writing k2+
	write(10,*) param0(8)   !writing k2-
	read(20,*) rtemp  !readin k2+
	read(20,*) rtemp  !readin k2-

C      write(10,*) rtemp                       
      write(10,'(a35)')'#Molar Concentration of Actin [uM]:'
	read(20,*) rtemp  !readin ca
	
      write(10,*) rtemp
      write(10,'(a55)') '#Molar Concentration of Myosin Initial'//
     &            '/Final [uM / uM]:'
	read(20,*) rtemp  !readin cmi
	write(10,*) rtemp  
	read(20,*) rtemp  !readin cmf
      write(10,*) rtemp                    

      close (10)
	close (20)

	 

      open(10,file='input')
      write(10,'(a11)') 'tmtn+ca.dat'
      write(10,*) icase
      write(10,*) icase
      close(10)

      ! runs simulation
      ierr = system(filexe //' < input')
      if( ierr==1) stop

	!get nlen
	write (fcase,'(I4.4)') icase
	open(20,file='nlen'//fcase//'.dat',form='formatted')
	read(20,*) nlen
	close(20)

!=======================================================================
      end
!=======================================================================

!=======================================================================
      subroutine runestim(filexp,nsim,nparam,eps,xparam,delta)
!=======================================================================
!     arranges input files for estimation code
! juanc, Dec 8 2005.
!=======================================================================
      USE PORTLIB
	USE MSFLIB
      implicit none 

      integer(4) :: iread, nobs, nparam, ierr, ip
      integer(4), dimension(*) :: nsim

      real(8) :: data1, data2, eps
      real(8), dimension(*) :: xparam, delta

      character(200) :: filexp

      ! experimental data
      open(10,file=filexp)
      open(20,file='obser_data.dat')

      nobs = 0   !! number of observations
      iread=1
      do while(iread==1)
         read (10,*,err=100) data1, data2
         write(20,*) data1,data2
         nobs = nobs + 1
      enddo
 100  continue
      
      close(10)
      close(20)

      open(10,file='input_estim')
      write(10,*) nparam
      write(10,*) nobs
      do ip=1,nparam+1
         write(10,*) nsim(ip)
      enddo
      write(10,*) eps
      close(10)

      open(10,file='param_data.dat')
      do ip=1,nparam
         write(10,*) xparam(ip)
      enddo
      close(10)

      open(10,file='delta_data.dat')
      do ip=1,nparam
         write(10,*) delta(ip)
      enddo
      close(10)
      
      ierr = system('estimre input_estim')
      if( ierr==1) stop

!=======================================================================
      end
!=======================================================================
