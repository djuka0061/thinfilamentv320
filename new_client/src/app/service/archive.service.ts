
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.prod'

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json',
        timeout: '600000'
    })
}

const host = environment.server.host;

const headers: HttpHeaders = new HttpHeaders();
headers.set('enctype', 'multipart/form-data');

@Injectable({
    providedIn: 'root'
})
export class ArchiveService {
    constructor(private httpClient: HttpClient) {
    }

    getConfig(): Observable<any> {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get<any>('');

    }

    public returnAllArchive(limit: number, page: number): Observable<any> {
        return this.httpClient.get<any>(`${host}/arhive?filter[limit]=${limit}&filter[skip]=${page}&filter[order]=runDate DESC`)
    }

    public countSimulation(): Observable<any> {
        return this.httpClient.get<any>(`${host}/arhive/count`)
    }

    public searchArchive(search: string): Observable<any> {
        return this.httpClient.get<any>(`${host}/arhive?filter[where][or][0][simulationType][like]=${search}&filter[where][or][1][description][like]=${search}&filter[order]=runDate DESC`)
    }

    public searchArchiveByType(type: string): Observable<any> {
        return this.httpClient.get<any>(`${host}/arhive?filter[where][simulationType]=${type}&filter[order]=runDate DESC`)
    }
}