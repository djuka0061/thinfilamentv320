import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { ButtonsModule } from 'ngx-bootstrap/buttons';
import { CommonModule } from '@angular/common';

import { StochasticalComponent } from './stochastical.component';
import { StochasticalRoutingModule } from './stochastical-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

@NgModule({
  imports: [
    FormsModule,
    CommonModule,
    ReactiveFormsModule,
    StochasticalRoutingModule,
    ChartsModule,
    BsDropdownModule,
    ButtonsModule.forRoot(),
    MatTableModule,
    MatPaginatorModule,    
    TooltipModule.forRoot()
  ],
  declarations: [ StochasticalComponent ],
  exports: [
    MatTableModule,
    MatPaginatorModule
  ]
})
export class StochasticalModule { }
