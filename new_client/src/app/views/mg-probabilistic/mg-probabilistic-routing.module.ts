import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MgProbabilisticComponent } from './mg-probabilistic.component';

const routes: Routes = [
  {
    path: '',
    component: MgProbabilisticComponent,
    data: {
      title: 'MgProbabilistic'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MgProbabilisticRoutingModule {}
