import { Component, OnInit, QueryList, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { HillMonteCarloService } from '../../service/hill_monte_carlo.service';
import { environment } from '../../../environments/environment.prod';
import { switchMap } from 'rxjs/operators';
import { timer } from 'rxjs';
import { Chart } from 'chart.js'

const host = environment.server.host;

export interface PeriodicElement {
  last: string;
  average: string;
  std_dev: string;
}

export interface Graph {
  datasets: Array<any>;
  options: any;
  chartType: string;
  colors: Array<any>;
  legend: boolean;
  labels: Array<any>;
  visibility: Boolean;
}

@Component({
  selector: 'app-hill-monte-carlo',
  templateUrl: './hill-monte-carlo.component.html',
  styleUrls: ['./hill-monte-carlo.component.scss']
})
export class HillMonteCarloComponent implements OnInit, AfterViewInit {

  angForm: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    alpha_zero: new FormControl(''),
    alpha_fiting: new FormControl(''),
    alpha_guess_of_params1: new FormControl(''),
    alpha_guess_of_params2: new FormControl(''),
    alpha_lower_bound: new FormControl(''),
    alpha_upper_bound: new FormControl(''),
    beta_zero: new FormControl(''),
    beta_fiting: new FormControl(''),
    beta_guess_of_params1: new FormControl(''),
    beta_guess_of_params2: new FormControl(''),
    beta_lower_bound: new FormControl(''),
    beta_upper_bound: new FormControl(''),
    constant_y1: new FormControl(''),
    y1_fiting: new FormControl(''),
    y1_guess_of_params1: new FormControl(''),
    y1_guess_of_params2: new FormControl(''),
    y1_lower_bound: new FormControl(''),
    y1_upper_bound: new FormControl(''),
    constant_y2: new FormControl(''),
    y2_fiting: new FormControl(''),
    y2_guess_of_params1: new FormControl(''),
    y2_guess_of_params2: new FormControl(''),
    y2_lower_bound: new FormControl(''),
    y2_upper_bound: new FormControl(''),
    constant_k1: new FormControl(''),
    k1_fiting: new FormControl(''),
    k1_guess_of_params1: new FormControl(''),
    k1_guess_of_params2: new FormControl(''),
    k1_lower_bound: new FormControl(''),
    k1_upper_bound: new FormControl(''),
    constant_k1_prim: new FormControl(''),
    k1_prim_fiting: new FormControl(''),
    k1_prim_guess_of_params1: new FormControl(''),
    k1_prim_guess_of_params2: new FormControl(''),
    k1_prim_lower_bound: new FormControl(''),
    k1_prim_upper_bound: new FormControl(''),
    constant_k2: new FormControl(''),
    k2_fiting: new FormControl(''),
    k2_guess_of_params1: new FormControl(''),
    k2_guess_of_params2: new FormControl(''),
    k2_lower_bound: new FormControl(''),
    k2_upper_bound: new FormControl(''),
    constant_k2_prim: new FormControl(''),
    k2_prim_fiting: new FormControl(''),
    k2_prim_guess_of_params1: new FormControl(''),
    k2_prim_guess_of_params2: new FormControl(''),
    k2_prim_lower_bound: new FormControl(''),
    k2_prim_upper_bound: new FormControl(''),
    number_of_filaments: new FormControl(''),
    coopertivity_switch: new FormControl(''),
    parameter_delta: new FormControl(''),
    parameter_gama: new FormControl(''),
    epsilon: new FormControl(''),
    number_of_iteration: new FormControl(''),
    number_of_actin_per_unit: new FormControl(''),
    number_of_tm_tn_units: new FormControl(''),
    actin: new FormControl(''),
    myosin: new FormControl(''),
    final_myosin: new FormControl(''),
    type_of_simulation: new FormControl('')
  });

  constructor(private fb: FormBuilder, private HillMonteCarloService: HillMonteCarloService) { }

  public hillMonteCarlo: any;
  public dataSource;
  public sensitivityMatrix;
  public inputData = this.profileForm;;
  public dugmeRun: Boolean = false;
  public displayedColumns: string[];
  public isVisible = true;
  public run_button = "RUN";
  public typeOfSimulation: Number = 1;
  fileToUpload: File = null;

  public experimentalModelGraph
  public parametersConvergance

  private colors = [
    { borderColor: 'blue', backgroundColor: 'transparent' },
    { borderColor: 'purple', backgroundColor: 'transparent' },
    { borderColor: 'yellow', backgroundColor: 'transparent' },
    { borderColor: 'orange', backgroundColor: 'transparent' },
    { borderColor: 'green', backgroundColor: 'transparent' },
    { borderColor: 'black', backgroundColor: 'transparent' },
    { borderColor: 'brown', backgroundColor: 'transparent' }
  ]

  ngOnInit() {
    this.HillMonteCarloService.getInputData('test21').subscribe(data => {
      if (data) {
        this.inputData = data;
      }
    });

    this.experimentalModelGraph = new Chart('experimentalModelGraph', {})
    this.parametersConvergance = new Chart('parametersConvergance', {})
  }

  ngAfterViewInit(): void {

  }

  returnData(simulationID) {
    let subrscription =
      timer(0, 10000).pipe(
        switchMap(() => this.HillMonteCarloService.returnData(simulationID))
      ).subscribe(data => {
        if (data !== null && data.estimatedParams !== null && data.statusCode < 400) {
          const a_t = data.a_t;
          const models = data.model;

          const vidljivo = [
            Boolean(data.parameters.alpha_fiting),
            Boolean(data.parameters.beta_fiting),
            Boolean(data.parameters.y1_fiting),
            Boolean(data.parameters.y2_fiting),
            Boolean(data.parameters.y2_fiting),
            Boolean(data.parameters.k1_fiting),
            Boolean(data.parameters.k2_fiting),
            Boolean(data.parameters.k1_prim_fiting),
            Boolean(data.parameters.k2_prim_fiting)
          ];

          const podaci = [
            { data: data.estimatedParams.ka, label: 'Alpha0' },
            { data: data.estimatedParams.ka_minus, label: 'Beta0' },
            { data: data.estimatedParams.kpi, label: 'Y1' },
            { data: data.estimatedParams.kpi_minus, label: 'Y2' },
            { data: data.estimatedParams.kt, label: 'K1' },
            { data: data.estimatedParams.kt_minus, label: 'K2' },
            { data: data.estimatedParams.kt_star, label: 'K1\'' },
            { data: data.estimatedParams.kt_star_minus, label: 'K2\'' }
          ];

          let dataset = []
          this.experimentAndModelGraph(a_t, models)
          dataset.push({
            label: 'Error',
            data: data.estimatedParams.error,
            fill: false,
            lineTension: 0.2,
            borderColor: 'red',
            borderWidth: 1,
          })
          let j = 0;
          for (let i = 0; i < 8; i++) {
            if (vidljivo[i]) {
              dataset.push({
                label: podaci[i].label,
                data: podaci[i].data,
                fill: false,
                lineTension: 0.2,
                borderColor: this.colors[j].borderColor,
                borderWidth: 1,
              })
              j++;
            }
          }
          this.parametersConvergancePrepare(dataset)

          this.isVisible = true;

          for (let i = 1; i < data.estimatedParams.estimated_values.length; i++) {
            data.estimatedParams.estimated_values[i][1] = data.estimatedParams.estimated_values[i][1].toFixed(6)
            data.estimatedParams.estimated_values[i][2] = data.estimatedParams.estimated_values[i][2].toFixed(6)
            data.estimatedParams.estimated_values[i][3] = data.estimatedParams.estimated_values[i][3].toFixed(6)
          }
          this.dataSource = data.estimatedParams.estimated_values;
          this.sensitivityMatrix = data.estimatedParams.sensitivity;
          subrscription.unsubscribe()
          this.dugmeRun = false;
          this.run_button = "RUN";
        }
      })
  }

  onSubmit() {
    this.dugmeRun = true;
    this.run_button = "RUNNING";
    this.isVisible = false;
    const body = this.profileForm.value;
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    this.HillMonteCarloService.upload(this.fileToUpload).subscribe(upload => {
      this.HillMonteCarloService.getData(body).subscribe(data => {
        this.returnData(data.simulationID)
      })
    })
  }

  download() {
    window.open(host + 'hill_monte_carlos/users/test21/download');
  }

  saveSimulation() {
    const body = this.profileForm.value;
    this.hillMonteCarlo.saveSimulation(body).subscribe(data => {
      console.log(data);
    })
  }

  openSaveSimulation(file) {
    this.parametersConvergance.destroy()
    this.experimentalModelGraph.destroy()
    const fileData = file.target.files
    var reader = new FileReader();
    reader.readAsText(fileData[0]);
    reader.onload = (_event) => {
      const data = JSON.parse(reader.result.toString());
      const a_t = data.a_t;
      const models = data.model;

      const vidljivo = [
        Boolean(data.parameters.alpha_fiting),
        Boolean(data.parameters.beta_fiting),
        Boolean(data.parameters.y1_fiting),
        Boolean(data.parameters.y2_fiting),
        Boolean(data.parameters.y2_fiting),
        Boolean(data.parameters.k1_fiting),
        Boolean(data.parameters.k2_fiting),
        Boolean(data.parameters.k1_prim_fiting),
        Boolean(data.parameters.k2_prim_fiting)
      ];

      if (data.estimatedParams) {
        const podaci = [
          { data: data.estimatedParams.ka, label: 'Alpha0' },
          { data: data.estimatedParams.ka_minus, label: 'Beta0' },
          { data: data.estimatedParams.kpi, label: 'Y1' },
          { data: data.estimatedParams.kpi_minus, label: 'Y2' },
          { data: data.estimatedParams.kt, label: 'K1' },
          { data: data.estimatedParams.kt_minus, label: 'K2' },
          { data: data.estimatedParams.kt_star, label: 'K1\'' },
          { data: data.estimatedParams.kt_star_minus, label: 'K2\'' }
        ];

        let dataset = []
        this.experimentAndModelGraph(a_t, models)
        dataset.push({
          label: 'Error',
          data: data.estimatedParams.error,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        })
        let j = 0;
        for (let i = 0; i < 8; i++) {
          if (vidljivo[i]) {
            dataset.push({
              label: podaci[i].label,
              data: podaci[i].data,
              fill: false,
              lineTension: 0.2,
              borderColor: this.colors[j].borderColor,
              borderWidth: 1,
            })
            j++;
          }
        }
        this.parametersConvergancePrepare(dataset)

        this.isVisible = true;

        for (let i = 1; i < data.estimatedParams.estimated_values.length; i++) {
          data.estimatedParams.estimated_values[i][1] = data.estimatedParams.estimated_values[i][1].toFixed(6)
          data.estimatedParams.estimated_values[i][2] = data.estimatedParams.estimated_values[i][2].toFixed(6)
          data.estimatedParams.estimated_values[i][3] = data.estimatedParams.estimated_values[i][3].toFixed(6)
        }
        this.dataSource = data.estimatedParams.estimated_values;
        this.sensitivityMatrix = data.estimatedParams.sensitivity;
      }
    };
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  onCheckboxChange(box1: string, box2: string) {
    if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
      if (this.inputData[box1])
        this.inputData[box2] = false;
    }
  }

  experimentAndModelGraph(experiment: any, model: any) {
    this.experimentalModelGraph = new Chart('experimentalModelGraph', {
      type: 'line',
      data: {
        datasets: [{
          label: 'Model',
          data: model,
          fill: false,
          lineTension: 0.2,
          borderColor: 'blue',
          borderWidth: 1,
          showLine: false
        }, {
          label: 'Experiment',
          data: experiment,
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Actin concentration [microM]'
            }
          }],
          yAxes: [{
            scaleLabel: {
              display: true,
              labelString: 'Fractional Fluorescence'
            }
          }]
        }
      }
    })
  }

  parametersConvergancePrepare(datasets: any) {
    this.parametersConvergance = new Chart('parametersConvergance', {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Iteration'
            }
          }], yAxes: [{
            ticks: {
              callback: function (value) {
                return value.toExponential(2)
              }
            }
          }]
        }
      }
    })
  }
}
