(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-mg-probabilistic-mg-probabilistic-module"],{

/***/ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js":
/*!************************************************************************************************!*\
  !*** ./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
   true ? factory(exports) :
  undefined;
}(this, (function (exports) { 'use strict';

  /**
   * --------------------------------------------------------------------------
   * CoreUI Plugins - Custom Tooltips for Chart.js (v1.2.0): custom-tooltips.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */
  function CustomTooltips(tooltipModel) {
    var _this = this;

    // Add unique id if not exist
    var _setCanvasId = function _setCanvasId() {
      var _idMaker = function _idMaker() {
        var _hex = 16;
        var _multiplier = 0x10000;
        return ((1 + Math.random()) * _multiplier | 0).toString(_hex);
      };

      var _canvasId = "_canvas-" + (_idMaker() + _idMaker());

      _this._chart.canvas.id = _canvasId;
      return _canvasId;
    };

    var ClassName = {
      ABOVE: 'above',
      BELOW: 'below',
      CHARTJS_TOOLTIP: 'chartjs-tooltip',
      NO_TRANSFORM: 'no-transform',
      TOOLTIP_BODY: 'tooltip-body',
      TOOLTIP_BODY_ITEM: 'tooltip-body-item',
      TOOLTIP_BODY_ITEM_COLOR: 'tooltip-body-item-color',
      TOOLTIP_BODY_ITEM_LABEL: 'tooltip-body-item-label',
      TOOLTIP_BODY_ITEM_VALUE: 'tooltip-body-item-value',
      TOOLTIP_HEADER: 'tooltip-header',
      TOOLTIP_HEADER_ITEM: 'tooltip-header-item'
    };
    var Selector = {
      DIV: 'div',
      SPAN: 'span',
      TOOLTIP: (this._chart.canvas.id || _setCanvasId()) + "-tooltip"
    };
    var tooltip = document.getElementById(Selector.TOOLTIP);

    if (!tooltip) {
      tooltip = document.createElement('div');
      tooltip.id = Selector.TOOLTIP;
      tooltip.className = ClassName.CHARTJS_TOOLTIP;

      this._chart.canvas.parentNode.appendChild(tooltip);
    } // Hide if no tooltip


    if (tooltipModel.opacity === 0) {
      tooltip.style.opacity = 0;
      return;
    } // Set caret Position


    tooltip.classList.remove(ClassName.ABOVE, ClassName.BELOW, ClassName.NO_TRANSFORM);

    if (tooltipModel.yAlign) {
      tooltip.classList.add(tooltipModel.yAlign);
    } else {
      tooltip.classList.add(ClassName.NO_TRANSFORM);
    } // Set Text


    if (tooltipModel.body) {
      var titleLines = tooltipModel.title || [];
      var tooltipHeader = document.createElement(Selector.DIV);
      tooltipHeader.className = ClassName.TOOLTIP_HEADER;
      titleLines.forEach(function (title) {
        var tooltipHeaderTitle = document.createElement(Selector.DIV);
        tooltipHeaderTitle.className = ClassName.TOOLTIP_HEADER_ITEM;
        tooltipHeaderTitle.innerHTML = title;
        tooltipHeader.appendChild(tooltipHeaderTitle);
      });
      var tooltipBody = document.createElement(Selector.DIV);
      tooltipBody.className = ClassName.TOOLTIP_BODY;
      var tooltipBodyItems = tooltipModel.body.map(function (item) {
        return item.lines;
      });
      tooltipBodyItems.forEach(function (item, i) {
        var tooltipBodyItem = document.createElement(Selector.DIV);
        tooltipBodyItem.className = ClassName.TOOLTIP_BODY_ITEM;
        var colors = tooltipModel.labelColors[i];
        var tooltipBodyItemColor = document.createElement(Selector.SPAN);
        tooltipBodyItemColor.className = ClassName.TOOLTIP_BODY_ITEM_COLOR;
        tooltipBodyItemColor.style.backgroundColor = colors.backgroundColor;
        tooltipBodyItem.appendChild(tooltipBodyItemColor);

        if (item[0].split(':').length > 1) {
          var tooltipBodyItemLabel = document.createElement(Selector.SPAN);
          tooltipBodyItemLabel.className = ClassName.TOOLTIP_BODY_ITEM_LABEL;
          tooltipBodyItemLabel.innerHTML = item[0].split(': ')[0];
          tooltipBodyItem.appendChild(tooltipBodyItemLabel);
          var tooltipBodyItemValue = document.createElement(Selector.SPAN);
          tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          tooltipBodyItemValue.innerHTML = item[0].split(': ').pop();
          tooltipBodyItem.appendChild(tooltipBodyItemValue);
        } else {
          var _tooltipBodyItemValue = document.createElement(Selector.SPAN);

          _tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          _tooltipBodyItemValue.innerHTML = item[0];
          tooltipBodyItem.appendChild(_tooltipBodyItemValue);
        }

        tooltipBody.appendChild(tooltipBodyItem);
      });
      tooltip.innerHTML = '';
      tooltip.appendChild(tooltipHeader);
      tooltip.appendChild(tooltipBody);
    }

    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft; // Display, position, and set styles for font

    tooltip.style.opacity = 1;
    tooltip.style.left = positionX + tooltipModel.caretX + "px";
    tooltip.style.top = positionY + tooltipModel.caretY + "px";
  }

  exports.CustomTooltips = CustomTooltips;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=custom-tooltips.js.map


/***/ }),

/***/ "./src/app/service/mg_probabilistic.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/service/mg_probabilistic.service.ts ***!
  \*****************************************************/
/*! exports provided: MGProbabilisticService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MGProbabilisticService", function() { return MGProbabilisticService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");




var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].server.host;
var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
headers.set('enctype', 'multipart/form-data');
var MGProbabilisticService = /** @class */ (function () {
    function MGProbabilisticService(httpClient) {
        this.httpClient = httpClient;
    }
    MGProbabilisticService.prototype.getConfig = function () {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get('');
    };
    MGProbabilisticService.prototype.getData = function (json) {
        return this.httpClient.post(host + 'mg_probabilistic/users/test21/runSimulation', json, httpOptions);
    };
    MGProbabilisticService.prototype.getInputData = function (userID) {
        return this.httpClient.get(host + 'mg_probabilistic/users/test21/returnInputData');
    };
    MGProbabilisticService.prototype.download = function (userID) {
        return this.httpClient.get(host + 'mg_probabilistic/users/test21/download');
    };
    MGProbabilisticService.prototype.upload = function (fileToUpload) {
        console.log(fileToUpload);
        var formData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        console.log(formData.getAll('file'));
        return this.httpClient.post(host + 'mg_probabilistic/users/test21/upload', formData, { headers: headers });
    };
    MGProbabilisticService.prototype.saveSimulation = function (body) {
        return this.httpClient.post(host + 'mg_probabilistic/users/test21/saveData', body, httpOptions);
    };
    MGProbabilisticService.prototype.openSaveSimulation = function (userID) {
        return this.httpClient.get(host + 'mg_probabilistic/users/test21/returnSaveData');
    };
    MGProbabilisticService.prototype.resetSimulation = function (userID) {
        return this.httpClient.get(host + 'mg_probabilistic/users/test21/download');
    };
    MGProbabilisticService.prototype.saveDefaultValue = function (userID) {
        return this.httpClient.get(host + 'mg_probabilistic/users/test21/download');
    };
    MGProbabilisticService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], MGProbabilisticService);
    return MGProbabilisticService;
}());



/***/ }),

/***/ "./src/app/views/mg-probabilistic/mg-probabilistic-routing.module.ts":
/*!***************************************************************************!*\
  !*** ./src/app/views/mg-probabilistic/mg-probabilistic-routing.module.ts ***!
  \***************************************************************************/
/*! exports provided: MgProbabilisticRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MgProbabilisticRoutingModule", function() { return MgProbabilisticRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _mg_probabilistic_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./mg-probabilistic.component */ "./src/app/views/mg-probabilistic/mg-probabilistic.component.ts");




var routes = [
    {
        path: '',
        component: _mg_probabilistic_component__WEBPACK_IMPORTED_MODULE_3__["MgProbabilisticComponent"],
        data: {
            title: 'MgProbabilistic'
        }
    }
];
var MgProbabilisticRoutingModule = /** @class */ (function () {
    function MgProbabilisticRoutingModule() {
    }
    MgProbabilisticRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], MgProbabilisticRoutingModule);
    return MgProbabilisticRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/mg-probabilistic/mg-probabilistic.component.html":
/*!************************************************************************!*\
  !*** ./src/app/views/mg-probabilistic/mg-probabilistic.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn d-flex\">\r\n  <div class=\"d-flex w-100 flex-wrap \">\r\n    <!--Parameters-->\r\n    <div class=\"d-flex w-30 p-2 align-self-start flex-min-width\">\r\n      <div class=\"d-flex card card-accent-primary \">\r\n        <div class=\"card-header\">\r\n          SIMULATION AND MODEL DATA\r\n        </div>\r\n        <div class=\"card-body d-flex w-100  \">\r\n          <div class=\"d-flex flex-column w-100\">\r\n            <div class=\"d-flex w-100 pt-1 align-content-end\">\r\n              <div class=\"d-flex w-60\"> &nbsp; </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"saveSimulation()\">SAVE</button>\r\n              </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"openSaveSimulation()\">OPEN</button>\r\n              </div>\r\n            </div>\r\n            <form [formGroup]=\"profileForm\" (ngSubmit)=\"onSubmit()\">\r\n              <div class=\"d-flex flex-column w-100\">\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Simulation parameters and experimental data\r\n                </div>\r\n                <div class=\"d-flex flex-column w-100 pt-2\">\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Total time:</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.s}}\r\n                        formControlName=\"s\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[s]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Epsilon:</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.epsilon}}\r\n                        formControlName=\"epsilon\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Actin:</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.actin}}\r\n                        formControlName=\"actin\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Number of iterations:</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.number_of_iteration}}\r\n                        formControlName=\"number_of_iteration\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Experimental Data File:</span>\r\n                    </div>\r\n                    <div class=\"w-40 p-1\">\r\n                      <input class=\"w-100\" type=\"file\" id=\"a_t\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"d-flex w-100 pt-3\">\r\n                    <div class=\"d-flex w-50\">\r\n                      <div class=\"d-flex w-35\"><span>Type of Simulation</span></div>\r\n                      <div class=\"d-flex flex-column w-65\">\r\n                        <div class=\"form-check\">\r\n                          <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                            [(ngModel)]=\"this.inputData.type_of_simulation\" formControlName=\"type_of_simulation\"\r\n                            (ngModelChange)=\"onCheckboxChange('type_of_simulation', 'type_of_simulation_final_myosin')\">\r\n                          <label class=\"form-check-label\">Time Course</label>\r\n                        </div>\r\n                        <div class=\"form-check\">\r\n                          <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                            [(ngModel)]=\"this.inputData.type_of_simulation_final_myosin\" formControlName=\"type_of_simulation_final_myosin\"\r\n                            (ngModelChange)=\"onCheckboxChange('type_of_simulation_final_myosin', 'type_of_simulation')\">\r\n                          <label class=\"form-check-label\">Titration</label>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"d-flex flex-column w-50\">\r\n                      <div class=\"d-flex w-100\">\r\n                        <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Initial)\">\r\n                          <span style=\"vertical-align: middle\">Myosin Initial</span>\r\n                        </div>\r\n                        <div class=\"w-37 p-1\">\r\n                          <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                              formControlName=\"myosin\"-->\r\n                          <input class=\"w-100 text-align-right\" type=\"text\">\r\n                        </div>\r\n                        <div class=\"w-20 p-1\">\r\n                          <span style=\"vertical-align: middle\">[uM]</span>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"d-flex w-100\" *ngIf=\"this.inputData.type_of_simulation_final_myosin\">\r\n                        <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Final)\">\r\n                          <span style=\"vertical-align: middle\">Myosin Final</span>\r\n                        </div>\r\n                        <div class=\"w-37 p-1\">\r\n                          <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                              formControlName=\"myosin\"-->\r\n                          <input class=\"w-100 text-align-right\" type=\"text\">\r\n                        </div>\r\n                        <div class=\"w-20 p-1\">\r\n                          <span style=\"vertical-align: middle\">[uM]</span>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"d-flex flex-column w-100 pt-2\">\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Parameters for estimation\r\n                </div>\r\n                <!--<div class=\"d-flex text-center pt-2 text-bottom\"> -->\r\n                <div class=\"d-flex text-center pt-2 my-font-sm75\">\r\n                  <div class=\"w-40 p-1\"></div>\r\n                  <div class=\"w-10 p-1 align-self-end\">Fitting</div>\r\n                  <div class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1 align-self-end\">Guess 1</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Guess 2</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Lower Bound</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Upper Bound</div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">Kb:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kb}}\r\n                        formControlName=\"kb\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kb_fiting\"\r\n                      formControlName=\"kb_fiting\" (ngModelChange)=\"onCheckboxChange('kb_fiting', 'kb_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kb_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_guess_of_params1}}\r\n                        formControlName=\"kb_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_guess_of_params2}}\r\n                        formControlName=\"kb_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_lower_bound}}\r\n                        formControlName=\"kb_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_upper_bound}}\r\n                        formControlName=\"kb_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kb-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kb_minus}}\r\n                        formControlName=\"kb_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kb_minus_fiting\"\r\n                      formControlName=\"kb_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kb_minus_fiting', 'kb_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kb_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kb_minus_guess_of_params1}}\r\n                        formControlName=\"kb_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kb_minus_guess_of_params2}}\r\n                        formControlName=\"kb_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minus_lower_bound}}\r\n                        formControlName=\"kb_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minus_upper_bound}}\r\n                        formControlName=\"kb_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kt:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt}}\r\n                        formControlName=\"kt\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_fiting\"\r\n                      formControlName=\"kt_fiting\" (ngModelChange)=\"onCheckboxChange('kt_fiting', 'kt_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_guess_of_params1}}\r\n                        formControlName=\"kt_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_guess_of_params2}}\r\n                        formControlName=\"kt_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_lower_bound}}\r\n                        formControlName=\"kt_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_upper_bound}}\r\n                        formControlName=\"kt_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kt-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_minus}}\r\n                        formControlName=\"kt_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_minus_fiting\"\r\n                      formControlName=\"kt_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kt_minus_fiting', 'kt_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_minus_guess_of_params1}}\r\n                        formControlName=\"kt_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.kt_minus_guess_of_params2}}\r\n                        formControlName=\"kt_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_lower_bound}}\r\n                        formControlName=\"kt_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_upper_bound}}\r\n                        formControlName=\"kt_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k1:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k1}}\r\n                        formControlName=\"k1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k1_fiting\"\r\n                      formControlName=\"k1_fiting\" (ngModelChange)=\"onCheckboxChange('k1_fiting', 'k1_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k1_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_guess_of_params1}}\r\n                        formControlName=\"k1_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_guess_of_params2}}\r\n                        formControlName=\"k1_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_lower_bound}}\r\n                        formControlName=\"k1_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_upper_bound}}\r\n                        formControlName=\"k1_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k1-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k1_minus}}\r\n                        formControlName=\"k1_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center \">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k1_minus_fiting\"\r\n                      formControlName=\"k1_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('k1_minus_fiting', 'k1_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k1_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.k1_minus_guess_of_params1}}\r\n                        formControlName=\"k1_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.k1_minus_guess_of_params2}}\r\n                        formControlName=\"k1_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_lower_bound}}\r\n                        formControlName=\"k1_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_upper_bound}}\r\n                        formControlName=\"k1_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k2:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k2}}\r\n                        formControlName=\"k2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k2_fiting\"\r\n                      formControlName=\"k2_fiting\" (ngModelChange)=\"onCheckboxChange('k2_fiting', 'k2_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k2_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_guess_of_params1}}\r\n                        formControlName=\"k2_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_guess_of_params2}}\r\n                        formControlName=\"k2_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_lower_bound}}\r\n                        formControlName=\"k2_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_upper_bound}}\r\n                        formControlName=\"k2_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k2-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k2_minus}}\r\n                        formControlName=\"k2_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k2_minus_fiting\"\r\n                      formControlName=\"k2_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('k2_minus_fiting', 'k2_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k2_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.k2_minus_guess_of_params1}}\r\n                        formControlName=\"k2_minus_guess_of_params1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.k2_minus_guess_of_params2}}\r\n                        formControlName=\"k2_minus_guess_of_params2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_lower_bound}}\r\n                        formControlName=\"k2_minus_lower_bound\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_upper_bound}}\r\n                        formControlName=\"kt_star_minus_upper_bound\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"d-flex w-100 pt-3\">\r\n                <div class=\"d-flex w-80\"> &nbsp; </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button id=\"dugmeRun\" type=\"submit w-25\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"dugmeRun\">{{run_button}}</button>\r\n                </div>\r\n              </div>\r\n\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"d-flex w-70 p-2\">\r\n      <div class=\"d-flex card card-accent-primary w-100\">\r\n        <div class=\"card-header\">\r\n          RESULTS\r\n          <span class=\"float-right\" tooltip=\"Download graphic data\" placement=\"left\">\r\n            <i (click)=\"download()\" class=\"fa fa-cloud-download\" style=\"font-size:16px\"></i>\r\n          </span>\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex flex-column w-100 mv-400\">\r\n            <div class=\"d-flex w-100 h-5 justify-content-around\">\r\n            </div>\r\n            <div class=\"d-flex flex-column w-100 pt-2\">\r\n              <div class=\"d-flex w-100 flex-wrap justify-content-around\">\r\n\r\n                <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n                <div class=\"w-25 tbl-10\">\r\n                  <!--style=\"height:300px;margin-top:40px;\"-->\r\n                  <div class=\"d-flex pb-10 title-div-table\">\r\n                    Estimated values\r\n                  </div>\r\n                  <!--div class=\"table-responsive\">-->\r\n                  <div class=\"tbl-10\">\r\n                    <table class=\"table table-condensed \" *ngIf=\"isVisible\">\r\n                      <!--table-striped-->\r\n                      <tbody>\r\n                        <tr *ngFor=\"let row of dataSource\">\r\n                          <td *ngFor=\"let col of row\" class=\"td td-1\">{{col}}</td>\r\n                        </tr>\r\n                      </tbody>\r\n                    </table>\r\n                  </div>\r\n                </div>\r\n                <div class=\"w-60 tbl-10 \">\r\n                  <div class=\"d-flex pb-10 title-div-table\">\r\n                    Sensitivity matrix\r\n                  </div>\r\n                  <div class=\"tbl-8kolona\">\r\n                    <table class=\"table table-condensed\" *ngIf=\"isVisible\">\r\n                      <tbody>\r\n                        <tr *ngFor=\"let row of sensitivityMatrix\">\r\n                          <td *ngFor=\"let col of row\" class=\"td td-1\">{{col}}</td>\r\n                        </tr>\r\n                      </tbody>\r\n                    </table>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <!--div class=\"card-body d-flex w-100\"-->\r\n              <div class=\"d-flex w-100 flex-wrap justify-content-around align-content-start\">\r\n                <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n                <div class=\"w-100 p-2 mw-350\">\r\n                  <div class=\"chart-wrapper\">\r\n                    <!--style=\"height:300px;margin-top:40px;\"-->\r\n                    <canvas *ngIf=\"graphs[0]\" baseChart class=\"chart\" [datasets]=\"graphs[0].datasets\"\r\n                      [labels]=\"graphs[0].labels\" [options]=\"graphs[0].options\" [colors]=\"graphs[0].colors\"\r\n                      [legend]=\"graphs[0].legend\" [chartType]=\"graphs[0].chartType\">\r\n                    </canvas>\r\n                    <canvas *ngIf=\"!graphs[0]\" baseChart class=\"chart\"></canvas>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"d-flex title-div-2 w-100 h-20 p-3 justify-content-around\">\r\n              Estimated parameters convergation\r\n            </div>\r\n            <!--div class=\"d-flex flex-column w-100 pt-2\"></div-->\r\n            <div class=\"d-inline-flex p-2 w-100 flex-wrap justify-content-center\">\r\n              <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n              <div class=\"w-100\">\r\n                <div class=\"chart-wrapper\">\r\n                  <!--style=\"height:300px;margin-top:40px;\"-->\r\n                  <canvas *ngIf=\"graphs[1]\" baseChart class=\"chart\" [datasets]=\"graphs[1].datasets\"\r\n                    [labels]=\"graphs[1].labels\" [options]=\"graphs[1].options\" [colors]=\"graphs[1].colors\"\r\n                    [legend]=\"graphs[1].legend\" [chartType]=\"graphs[1].chartType\">\r\n                  </canvas>\r\n                  <canvas *ngIf=\"!graphs[1]\" baseChart class=\"chart\">\r\n                  </canvas>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/mg-probabilistic/mg-probabilistic.component.scss":
/*!************************************************************************!*\
  !*** ./src/app/views/mg-probabilistic/mg-probabilistic.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column; }\n\n.example-container > * {\n  width: 100%; }\n\n.example-container form {\n  margin-bottom: 20px; }\n\n.example-container form > * {\n  margin: 5px 0; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.table_form {\n  min-width: 550px;\n  padding: 0px; }\n\n.table_form p {\n  font-size: 10px;\n  text-align: right; }\n\n.table_form input {\n  height: 25px;\n  text-align: center;\n  max-width: 100px; }\n\n/* Customize the label (the container) */\n\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n/* Hide the browser's default checkbox */\n\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0; }\n\n/* Create a custom checkbox */\n\n.checkmark {\n  position: absolute;\n  margin-top: 5px;\n  top: 0;\n  left: 0;\n  height: 20 px;\n  width: 20px;\n  background-color: #eee; }\n\n/* On mouse-over, add a grey background color */\n\n.container:hover input ~ .checkmark {\n  background-color: #ccc; }\n\n/* When the checkbox is checked, add a blue background */\n\n.container input:checked ~ .checkmark {\n  background-color: #2196F3; }\n\n/* Create the checkmark/indicator (hidden when not checked) */\n\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none; }\n\n/* Show the checkmark when checked */\n\n.container input:checked ~ .checkmark:after {\n  display: block; }\n\n/* Style the checkmark/indicator */\n\n.container .checkmark:after {\n  left: 9px;\n  top: 5px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg); }\n\n.title-div-centar {\n  font-size: 1rem;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\n.title-div {\n  color: cadetblue;\n  font-style: 1rem;\n  border-bottom: 1px dashed lightblue; }\n\n.title-div-table {\n  font-size: 1rem;\n  color: cadetblue;\n  text-align: center !important; }\n\n.title-div-2 {\n  font-size: 1.2rem;\n  color: cadetblue; }\n\ninput {\n  background-color: #c3e4e5;\n  border: 1px solid \t#c3e4e5 !important; }\n\n.text-align-right {\n  text-align: right;\n  padding-right: 2px !important; }\n\n.text-align-center {\n  text-align: center; }\n\n.flex-min-width {\n  min-width: 470px; }\n\n.max-width-70posto {\n  max-width: 70%; }\n\n.over-auto {\n  overflow: visible; }\n\n.chart-wrapper {\n  border: 1px solid lightblue !important; }\n\n.mw-350 {\n  min-width: 280px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-30posto {\n  min-width: 370px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-400 {\n  min-width: 280px; }\n\n.tbl-10 {\n  min-width: 300px !important; }\n\n.tbl-8kolona {\n  min-width: 300px !important; }\n\n.td-1 {\n  padding: 0.4rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvbWctcHJvYmFiaWxpc3RpYy9GOlxcdGhpbmZpbGFtZW50djMyMFxcbmV3X2NsaWVudC9zcmNcXGFwcFxcdmlld3NcXG1nLXByb2JhYmlsaXN0aWNcXG1nLXByb2JhYmlsaXN0aWMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxhQUFhO0VBQ2Isc0JBQXNCLEVBQUE7O0FBR3hCO0VBQ0UsV0FBVyxFQUFBOztBQUdiO0VBQ0UsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsYUFBYSxFQUFBOztBQUdmO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixXQUFXLEVBQUE7O0FBR2I7RUFDRSxXQUFXLEVBQUE7O0FBR2I7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBSW5CLHdDQUFBOztBQUNEO0VBQ0UsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBR25CLHdDQUFBOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUztFQUNULFFBQVEsRUFBQTs7QUFHViw2QkFBQTs7QUFDQTtFQUNFLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsTUFBTTtFQUNOLE9BQU87RUFDUCxhQUFhO0VBQ2IsV0FBVztFQUNYLHNCQUFzQixFQUFBOztBQUd4QiwrQ0FBQTs7QUFDQTtFQUNFLHNCQUFzQixFQUFBOztBQUd4Qix3REFBQTs7QUFDQTtFQUNFLHlCQUF5QixFQUFBOztBQUczQiw2REFBQTs7QUFDQTtFQUNFLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsYUFBYSxFQUFBOztBQUdmLG9DQUFBOztBQUNBO0VBQ0UsY0FBYyxFQUFBOztBQUdoQixrQ0FBQTs7QUFDQTtFQUNFLFNBQVM7RUFDVCxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLGdDQUFnQztFQUVoQyx3QkFBd0IsRUFBQTs7QUFHMUI7RUFFRSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUl0QjtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsbUNBQW1DLEVBQUE7O0FBR3JDO0VBQ0UsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiw2QkFBNkIsRUFBQTs7QUFHL0I7RUFDRSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBSWxCO0VBRUUseUJBQTBCO0VBQzFCLHFDQUFxQyxFQUFBOztBQUd2QztFQUNFLGlCQUFpQjtFQUNqQiw2QkFBNkIsRUFBQTs7QUFHL0I7RUFDRSxrQkFBa0IsRUFBQTs7QUFJcEI7RUFDRSxnQkFBZ0IsRUFBQTs7QUFTbEI7RUFDRSxjQUFjLEVBQUE7O0FBRWhCO0VBQ0UsaUJBQWlCLEVBQUE7O0FBR25CO0VBRUUsc0NBQXNDLEVBQUE7O0FBR3hDO0VBQ0UsZ0JBQWdCO0VBQ2hCLDJCQUFtQjtFQUFuQix3QkFBbUI7RUFBbkIsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsZ0JBQWdCO0VBQ2hCLDJCQUFtQjtFQUFuQix3QkFBbUI7RUFBbkIsbUJBQW1CLEVBQUE7O0FBS3JCO0VBQ0UsZ0JBQWdCLEVBQUE7O0FBR2xCO0VBRUUsMkJBQTJCLEVBQUE7O0FBRTdCO0VBRUUsMkJBQTJCLEVBQUE7O0FBSTdCO0VBQ0UsMEJBQTBCLEVBQUEiLCJmaWxlIjoic3JjL2FwcC92aWV3cy9tZy1wcm9iYWJpbGlzdGljL21nLXByb2JhYmlsaXN0aWMuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciBmb3JtIHtcclxuICAgIG1hcmdpbi1ib3R0b206IDIwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWNvbnRhaW5lciBmb3JtID4gKiB7XHJcbiAgICBtYXJnaW46IDVweCAwO1xyXG4gIH1cclxuICBcclxuICAuZXhhbXBsZS1mb3JtIHtcclxuICAgIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgICBtYXgtd2lkdGg6IDUwMHB4O1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIC5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgfVxyXG4gIFxyXG4gIC50YWJsZV9mb3JtIHtcclxuICAgIG1pbi13aWR0aDogNTUwcHg7XHJcbiAgICBwYWRkaW5nOiAwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIC50YWJsZV9mb3JtIHAge1xyXG4gICAgZm9udC1zaXplOiAxMHB4O1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgfVxyXG4gIFxyXG4gIC50YWJsZV9mb3JtIGlucHV0IHtcclxuICAgIGhlaWdodDogMjVweDtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICAgIG1heC13aWR0aDogMTAwcHg7XHJcbiAgfVxyXG5cclxuXHJcbiAvKiBDdXN0b21pemUgdGhlIGxhYmVsICh0aGUgY29udGFpbmVyKSAqL1xyXG4uY29udGFpbmVyIHtcclxuICBkaXNwbGF5OiBibG9jaztcclxuICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgcGFkZGluZy1sZWZ0OiAzNXB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDEycHg7XHJcbiAgY3Vyc29yOiBwb2ludGVyO1xyXG4gIGZvbnQtc2l6ZTogMjJweDtcclxuICAtd2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4gIC1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbiAgLW1zLXVzZXItc2VsZWN0OiBub25lO1xyXG4gIHVzZXItc2VsZWN0OiBub25lO1xyXG59XHJcblxyXG4vKiBIaWRlIHRoZSBicm93c2VyJ3MgZGVmYXVsdCBjaGVja2JveCAqL1xyXG4uY29udGFpbmVyIGlucHV0IHtcclxuICBwb3NpdGlvbjogYWJzb2x1dGU7XHJcbiAgb3BhY2l0eTogMDtcclxuICBjdXJzb3I6IHBvaW50ZXI7XHJcbiAgaGVpZ2h0OiAwO1xyXG4gIHdpZHRoOiAwO1xyXG59XHJcblxyXG4vKiBDcmVhdGUgYSBjdXN0b20gY2hlY2tib3ggKi9cclxuLmNoZWNrbWFyayB7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIG1hcmdpbi10b3A6IDVweDtcclxuICB0b3A6IDA7XHJcbiAgbGVmdDogMDtcclxuICBoZWlnaHQ6IDIwIHB4O1xyXG4gIHdpZHRoOiAyMHB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZWU7XHJcbn1cclxuXHJcbi8qIE9uIG1vdXNlLW92ZXIsIGFkZCBhIGdyZXkgYmFja2dyb3VuZCBjb2xvciAqL1xyXG4uY29udGFpbmVyOmhvdmVyIGlucHV0IH4gLmNoZWNrbWFyayB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2NjYztcclxufVxyXG5cclxuLyogV2hlbiB0aGUgY2hlY2tib3ggaXMgY2hlY2tlZCwgYWRkIGEgYmx1ZSBiYWNrZ3JvdW5kICovXHJcbi5jb250YWluZXIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcmsge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICMyMTk2RjM7XHJcbn1cclxuXHJcbi8qIENyZWF0ZSB0aGUgY2hlY2ttYXJrL2luZGljYXRvciAoaGlkZGVuIHdoZW4gbm90IGNoZWNrZWQpICovXHJcbi5jaGVja21hcms6YWZ0ZXIge1xyXG4gIGNvbnRlbnQ6IFwiXCI7XHJcbiAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gIGRpc3BsYXk6IG5vbmU7XHJcbn1cclxuXHJcbi8qIFNob3cgdGhlIGNoZWNrbWFyayB3aGVuIGNoZWNrZWQgKi9cclxuLmNvbnRhaW5lciBpbnB1dDpjaGVja2VkIH4gLmNoZWNrbWFyazphZnRlciB7XHJcbiAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbi8qIFN0eWxlIHRoZSBjaGVja21hcmsvaW5kaWNhdG9yICovXHJcbi5jb250YWluZXIgLmNoZWNrbWFyazphZnRlciB7XHJcbiAgbGVmdDogOXB4O1xyXG4gIHRvcDogNXB4O1xyXG4gIHdpZHRoOiA1cHg7XHJcbiAgaGVpZ2h0OiAxMHB4O1xyXG4gIGJvcmRlcjogc29saWQgd2hpdGU7XHJcbiAgYm9yZGVyLXdpZHRoOiAwIDNweCAzcHggMDtcclxuICAtd2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxuICAtbXMtdHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG4gIHRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxufVxyXG5cclxuLnRpdGxlLWRpdi1jZW50YXIge1xyXG4gIC8vY29sb3I6IGNhZGV0Ymx1ZTtcclxuICBmb250LXNpemU6IDFyZW07XHJcbiAgcGFkZGluZy10b3A6IDFyZW07XHJcbiAgcGFkZGluZy1ib3R0b206IDFyZW07XHJcbiAgLy9ib3JkZXItYm90dG9tOiAxcHggZGFzaGVkIGxpZ2h0Ymx1ZTtcclxufVxyXG5cclxuLnRpdGxlLWRpdiB7XHJcbiAgY29sb3I6IGNhZGV0Ymx1ZTtcclxuICBmb250LXN0eWxlOiAxcmVtO1xyXG4gIGJvcmRlci1ib3R0b206IDFweCBkYXNoZWQgbGlnaHRibHVlO1xyXG59XHJcblxyXG4udGl0bGUtZGl2LXRhYmxlIHtcclxuICBmb250LXNpemU6IDFyZW07IFxyXG4gIGNvbG9yOiBjYWRldGJsdWU7XHJcbiAgdGV4dC1hbGlnbjogY2VudGVyICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi50aXRsZS1kaXYtMiB7XHJcbiAgZm9udC1zaXplOiAxLjJyZW07IFxyXG4gIGNvbG9yOiBjYWRldGJsdWU7XHJcbiAvLyB0ZXh0LWFsaWduOiBjZW50ZXIgIWltcG9ydGFudDtcclxufVxyXG5cclxuaW5wdXQge1xyXG4gIC8vYm9yZGVyOiBub25lO1xyXG4gIGJhY2tncm91bmQtY29sb3I6IFx0I2MzZTRlNTtcclxuICBib3JkZXI6IDFweCBzb2xpZCBcdCNjM2U0ZTUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtYWxpZ24tcmlnaHQge1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG4gIHBhZGRpbmctcmlnaHQ6IDJweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGV4dC1hbGlnbi1jZW50ZXIge1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuXHJcbn1cclxuXHJcbi5mbGV4LW1pbi13aWR0aCB7XHJcbiAgbWluLXdpZHRoOiA0NzBweDtcclxuLy8gIG1heC13aWR0aDogNjAwcHg7XHJcbiAgLy96LWluZGV4OiAxMDAwO1xyXG59XHJcblxyXG4uZmxleC1taW4td2lkdGggKiB7XHJcbiAgLy96LWluZGV4OiAxMDAwO1xyXG59XHJcblxyXG4ubWF4LXdpZHRoLTcwcG9zdG8ge1xyXG4gIG1heC13aWR0aDogNzAlO1xyXG59XHJcbi5vdmVyLWF1dG8ge1xyXG4gIG92ZXJmbG93OiB2aXNpYmxlO1xyXG59XHJcblxyXG4uY2hhcnQtd3JhcHBlciB7XHJcbiAgLy9ib3JkZXItdG9wLXN0eWxlOiBncm9vdmUgIWltcG9ydGFudDtcclxuICBib3JkZXI6IDFweCBzb2xpZCBsaWdodGJsdWUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm13LTM1MCB7XHJcbiAgbWluLXdpZHRoOiAyODBweDtcclxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xyXG59XHJcblxyXG4ubXctMzBwb3N0byB7XHJcbiAgbWluLXdpZHRoOiAzNzBweDtcclxuICBoZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4gIC8vZm9udC1zaXplOiAwLjVyZW0gIWltcG9ydGFudDtcclxufVxyXG5cclxuXHJcbi5tdy00MDAge1xyXG4gIG1pbi13aWR0aDogMjgwcHg7XHJcbn1cclxuXHJcbi50YmwtMTAge1xyXG4gLy8gbWFyZ2luOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbiAgbWluLXdpZHRoOiAzMDBweCAhaW1wb3J0YW50O1xyXG59XHJcbi50YmwtOGtvbG9uYSB7XHJcbiAvLyBtYXJnaW46IDEwcHggIWltcG9ydGFudDtcclxuICBtaW4td2lkdGg6IDMwMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG4udGQtMXtcclxuICBwYWRkaW5nOiAwLjRyZW0gIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/views/mg-probabilistic/mg-probabilistic.component.ts":
/*!**********************************************************************!*\
  !*** ./src/app/views/mg-probabilistic/mg-probabilistic.component.ts ***!
  \**********************************************************************/
/*! exports provided: MgProbabilisticComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MgProbabilisticComponent", function() { return MgProbabilisticComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @coreui/coreui-plugin-chartjs-custom-tooltips */ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_mg_probabilistic_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/mg_probabilistic.service */ "./src/app/service/mg_probabilistic.service.ts");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");







var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].server.host;
var MgProbabilisticComponent = /** @class */ (function () {
    function MgProbabilisticComponent(fb, MGProbabilisticService) {
        this.fb = fb;
        this.MGProbabilisticService = MGProbabilisticService;
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            s: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            actin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            epsilon: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            number_of_iteration: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_guess_of_params1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_guess_of_params2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_lower_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_upper_bound: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            final_myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            type_of_simulation: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            type_of_simulation_final_myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.inputData = this.profileForm;
        this.dugmeRun = false;
        this.isVisible = true;
        this.run_button = "RUN";
        this.fileToUpload = null;
        this.numberOfGraphs = 2;
        this.graphs = [];
        this.atModelGraph = [];
        this.errorGraph = [];
    }
    ;
    MgProbabilisticComponent.prototype.ngOnInit = function () { };
    MgProbabilisticComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.MGProbabilisticService.getInputData('test').subscribe(function (data) {
            if (data) {
                _this.inputData = data;
            }
        });
    };
    MgProbabilisticComponent.prototype.download = function () {
        window.open(host + 'mg_probabilistic/users/test21/download');
    };
    MgProbabilisticComponent.prototype.saveSimulation = function () {
        var body = this.profileForm.value;
        this.MGProbabilisticService.saveSimulation(body).subscribe(function (data) {
            console.log(data);
        });
    };
    MgProbabilisticComponent.prototype.openSaveSimulation = function () {
        var _this = this;
        this.graphs = [];
        this.MGProbabilisticService.openSaveSimulation('test21').subscribe(function (data) {
            var inputDatas = data.parameters;
            _this.inputData = data.parameters;
            var vidljivo = [
                Boolean(inputDatas.kb_fiting),
                Boolean(inputDatas.kb_minus_fiting),
                Boolean(inputDatas.kt_fiting),
                Boolean(inputDatas.kt_minus_fiting),
                Boolean(inputDatas.k1_fiting),
                Boolean(inputDatas.k1_minus_fiting),
                Boolean(inputDatas.k2_fiting),
                Boolean(inputDatas.k2_minus_fiting)
            ];
            var params = data.inputData;
            var podaci = [
                { data: data.ka, label: 'KB' },
                { data: data.ka_minus, label: 'KB-' },
                { data: data.kpi, label: 'KT' },
                { data: data.kpi_minus, label: 'kT-' },
                { data: data.kt, label: 'K1' },
                { data: data.kt_minus, label: 'K1-' },
                { data: data.kt_star, label: 'K2' },
                { data: data.kt_star_minus, label: 'K2-' }
            ];
            var datasets = [
                [
                    { data: params.a_t, label: 'Experiment' },
                    { data: params.models, label: 'Model' }
                ],
                []
            ];
            for (var i = 0; i < 14; i++) {
                if (vidljivo[i]) {
                    datasets[1].push(podaci[i]);
                }
            }
            datasets[1].push({ data: params.error, label: 'Error' });
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length, datasets[1].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
                _this.graphs[i].legend = true;
            }
            for (var i = 1; i < data.estimated_values.length; i++) {
                data.estimated_values[i][1] = data.estimated_values[i][1].toFixed(6);
                data.estimated_values[i][2] = data.estimated_values[i][2].toFixed(6);
                data.estimated_values[i][3] = data.estimated_values[i][3].toFixed(6);
            }
            _this.dataSource = params.estimated_values;
            _this.sensitivityMatrix = params.sensitivity;
            _this.dugmeRun = false;
            _this.run_button = "RUN";
        });
    };
    MgProbabilisticComponent.prototype.onSubmit = function () {
        var _this = this;
        this.dugmeRun = true;
        this.run_button = "RUNNING";
        this.isVisible = false;
        this.graphs = [];
        console.log('start simulation');
        var body = this.profileForm.value;
        var atData = [];
        console.log(this.profileForm.value.file);
        var file = this.profileForm.value.file;
        this.MGProbabilisticService.upload(this.fileToUpload).subscribe(function (upload) {
            console.log(upload);
            _this.MGProbabilisticService.getData(body).subscribe(function (data) {
                console.log('finish simulation');
                var a_t = data.a_t;
                var models = data.model;
                console.log(data);
                var vidljivo = [
                    Boolean(body.kb_fiting),
                    Boolean(body.kb_minus_fiting),
                    Boolean(body.kt_fiting),
                    Boolean(body.kt_minus_fiting),
                    Boolean(body.k1_fiting),
                    Boolean(body.k1_minus_fiting),
                    Boolean(body.k2_fiting),
                    Boolean(body.k2_minus_fiting)
                ];
                var podaci = [
                    { data: data.ka, label: 'KB' },
                    { data: data.ka_minus, label: 'KB-' },
                    { data: data.kpi, label: 'KT' },
                    { data: data.kpi_minus, label: 'kT-' },
                    { data: data.kt, label: 'K1' },
                    { data: data.kt_minus, label: 'K1-' },
                    { data: data.kt_star, label: 'K2' },
                    { data: data.kt_star_minus, label: 'K2-' }
                ];
                var datasets = [
                    [
                        { data: a_t, label: 'Experiment' },
                        { data: models, label: 'Model' }
                    ],
                    []
                ];
                for (var i = 0; i < 14; i++) {
                    if (vidljivo[i]) {
                        datasets[1].push(podaci[i]);
                    }
                }
                datasets[1].push({ data: data.error, label: 'Error' });
                _this.graphs = [];
                _this.prepareGraphs([datasets[0].length, datasets[1].length]);
                for (var i = 0; i < _this.numberOfGraphs; i++) {
                    _this.graphs[i].datasets = datasets[i];
                    _this.graphs[i].legend = true;
                }
                _this.isVisible = true;
                console.log(_this.graphs);
                for (var i = 1; i < data.estimated_values.length; i++) {
                    data.estimated_values[i][1] = data.estimated_values[i][1].toFixed(6);
                    data.estimated_values[i][2] = data.estimated_values[i][2].toFixed(6);
                    data.estimated_values[i][3] = data.estimated_values[i][3].toFixed(6);
                }
                _this.dataSource = data.estimated_values;
                _this.sensitivityMatrix = data.sensitivity;
                _this.dugmeRun = false;
                _this.run_button = "RUN";
            });
        });
    };
    //Graph setup
    MgProbabilisticComponent.prototype.initGraph = function (newTitle, newLabels, newColors, numberOfLines) {
        var basicGraph = {};
        basicGraph.options = {
            title: {
                display: true,
                text: 'Error',
                // fontColor: 'blue',
                fontStyle: 'small-caps',
            },
            tooltips: {
                enabled: false,
                custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                intersect: true,
                mode: 'index',
                position: 'nearest',
                callbacks: {
                    labelColor: function (tooltipItem, chart) {
                        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                    }
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 15,
                    top: 0,
                    bottom: 5
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        type: 'linear',
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            padding: 5,
                            beginAtZero: false,
                            stepSize: numberOfLines === 2 ? 1000 : 1,
                            fontSize: 12,
                            min: 1
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC1 (%)',
                            fontSize: 11
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            beginAtZero: true,
                            padding: 5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC2 (%)',
                            fontSize: 11
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: true,
                position: 'right'
            }
        };
        var newOptions = basicGraph.options;
        newOptions.title.text = newTitle;
        newOptions.scales.xAxes[0].type = newLabels.xAxes.type;
        newOptions.scales.xAxes[0].scaleLabel.labelString = newLabels.xAxes.title;
        newOptions.scales.yAxes[0].type = newLabels.yAxes.type;
        newOptions.scales.yAxes[0].scaleLabel.display = newLabels.yAxes.displayTitle;
        newOptions.scales.yAxes[0].scaleLabel.labelString = newLabels.yAxes.title;
        basicGraph.options = newOptions;
        basicGraph.colors = [];
        basicGraph.datasets = [];
        for (var i = 0; i < numberOfLines; i++) {
            basicGraph.datasets.push({ data: [] });
            basicGraph.colors.push(newColors[i]);
        }
        basicGraph.chartType = 'line';
        basicGraph.labels = [];
        basicGraph.legend = false;
        basicGraph.visibility = true;
        return basicGraph;
    };
    MgProbabilisticComponent.prototype.prepareGraphs = function (graphLines) {
        var titles = [
            'Experiment VS Model',
            'Error & Parameters convergence'
        ];
        var labels = [
            { xAxes: { type: 'linear', title: 'Time [s]' }, yAxes: { type: 'linear', displayTitle: true, title: 'Fractional Fluorescence' } },
            { xAxes: { type: 'linear', title: 'Iteration' }, yAxes: { type: 'logarithmic', displayTitle: false, title: '' } }
        ];
        var colors = [
            [
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'yellow', backgroundColor: 'transparent' }
            ],
            [
                { borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' },
                { borderColor: 'yellow', backgroundColor: 'transparent' },
                { borderColor: 'oragne', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'black', backgroundColor: 'transparent' },
                { borderColor: 'brown', backgroundColor: 'transparent' },
            ]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i], graphLines[i]));
        }
    };
    MgProbabilisticComponent.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
    };
    MgProbabilisticComponent.prototype.onCheckboxChange = function (box1, box2) {
        if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
            if (this.inputData[box1])
                this.inputData[box2] = false;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__["BaseChartDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
    ], MgProbabilisticComponent.prototype, "charts", void 0);
    MgProbabilisticComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-mg-probabilistic',
            template: __webpack_require__(/*! ./mg-probabilistic.component.html */ "./src/app/views/mg-probabilistic/mg-probabilistic.component.html"),
            styles: [__webpack_require__(/*! ./mg-probabilistic.component.scss */ "./src/app/views/mg-probabilistic/mg-probabilistic.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _service_mg_probabilistic_service__WEBPACK_IMPORTED_MODULE_4__["MGProbabilisticService"]])
    ], MgProbabilisticComponent);
    return MgProbabilisticComponent;
}());



/***/ }),

/***/ "./src/app/views/mg-probabilistic/mg-probabilistic.module.ts":
/*!*******************************************************************!*\
  !*** ./src/app/views/mg-probabilistic/mg-probabilistic.module.ts ***!
  \*******************************************************************/
/*! exports provided: MgProbabilisticModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MgProbabilisticModule", function() { return MgProbabilisticModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _mg_probabilistic_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./mg-probabilistic.component */ "./src/app/views/mg-probabilistic/mg-probabilistic.component.ts");
/* harmony import */ var _mg_probabilistic_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./mg-probabilistic-routing.module */ "./src/app/views/mg-probabilistic/mg-probabilistic-routing.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");












var MgProbabilisticModule = /** @class */ (function () {
    function MgProbabilisticModule() {
    }
    MgProbabilisticModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _mg_probabilistic_routing_module__WEBPACK_IMPORTED_MODULE_8__["MgProbabilisticRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(),
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ],
            declarations: [_mg_probabilistic_component__WEBPACK_IMPORTED_MODULE_7__["MgProbabilisticComponent"]],
            exports: [
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ]
        })
    ], MgProbabilisticModule);
    return MgProbabilisticModule;
}());



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    server: {
        host: 'http://localhost:3000/api/',
    }
};


/***/ })

}]);
//# sourceMappingURL=views-mg-probabilistic-mg-probabilistic-module.js.map