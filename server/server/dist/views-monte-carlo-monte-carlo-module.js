(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-monte-carlo-monte-carlo-module"],{

/***/ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js":
/*!************************************************************************************************!*\
  !*** ./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

(function (global, factory) {
   true ? factory(exports) :
  undefined;
}(this, (function (exports) { 'use strict';

  /**
   * --------------------------------------------------------------------------
   * CoreUI Plugins - Custom Tooltips for Chart.js (v1.2.0): custom-tooltips.js
   * Licensed under MIT (https://coreui.io/license)
   * --------------------------------------------------------------------------
   */
  function CustomTooltips(tooltipModel) {
    var _this = this;

    // Add unique id if not exist
    var _setCanvasId = function _setCanvasId() {
      var _idMaker = function _idMaker() {
        var _hex = 16;
        var _multiplier = 0x10000;
        return ((1 + Math.random()) * _multiplier | 0).toString(_hex);
      };

      var _canvasId = "_canvas-" + (_idMaker() + _idMaker());

      _this._chart.canvas.id = _canvasId;
      return _canvasId;
    };

    var ClassName = {
      ABOVE: 'above',
      BELOW: 'below',
      CHARTJS_TOOLTIP: 'chartjs-tooltip',
      NO_TRANSFORM: 'no-transform',
      TOOLTIP_BODY: 'tooltip-body',
      TOOLTIP_BODY_ITEM: 'tooltip-body-item',
      TOOLTIP_BODY_ITEM_COLOR: 'tooltip-body-item-color',
      TOOLTIP_BODY_ITEM_LABEL: 'tooltip-body-item-label',
      TOOLTIP_BODY_ITEM_VALUE: 'tooltip-body-item-value',
      TOOLTIP_HEADER: 'tooltip-header',
      TOOLTIP_HEADER_ITEM: 'tooltip-header-item'
    };
    var Selector = {
      DIV: 'div',
      SPAN: 'span',
      TOOLTIP: (this._chart.canvas.id || _setCanvasId()) + "-tooltip"
    };
    var tooltip = document.getElementById(Selector.TOOLTIP);

    if (!tooltip) {
      tooltip = document.createElement('div');
      tooltip.id = Selector.TOOLTIP;
      tooltip.className = ClassName.CHARTJS_TOOLTIP;

      this._chart.canvas.parentNode.appendChild(tooltip);
    } // Hide if no tooltip


    if (tooltipModel.opacity === 0) {
      tooltip.style.opacity = 0;
      return;
    } // Set caret Position


    tooltip.classList.remove(ClassName.ABOVE, ClassName.BELOW, ClassName.NO_TRANSFORM);

    if (tooltipModel.yAlign) {
      tooltip.classList.add(tooltipModel.yAlign);
    } else {
      tooltip.classList.add(ClassName.NO_TRANSFORM);
    } // Set Text


    if (tooltipModel.body) {
      var titleLines = tooltipModel.title || [];
      var tooltipHeader = document.createElement(Selector.DIV);
      tooltipHeader.className = ClassName.TOOLTIP_HEADER;
      titleLines.forEach(function (title) {
        var tooltipHeaderTitle = document.createElement(Selector.DIV);
        tooltipHeaderTitle.className = ClassName.TOOLTIP_HEADER_ITEM;
        tooltipHeaderTitle.innerHTML = title;
        tooltipHeader.appendChild(tooltipHeaderTitle);
      });
      var tooltipBody = document.createElement(Selector.DIV);
      tooltipBody.className = ClassName.TOOLTIP_BODY;
      var tooltipBodyItems = tooltipModel.body.map(function (item) {
        return item.lines;
      });
      tooltipBodyItems.forEach(function (item, i) {
        var tooltipBodyItem = document.createElement(Selector.DIV);
        tooltipBodyItem.className = ClassName.TOOLTIP_BODY_ITEM;
        var colors = tooltipModel.labelColors[i];
        var tooltipBodyItemColor = document.createElement(Selector.SPAN);
        tooltipBodyItemColor.className = ClassName.TOOLTIP_BODY_ITEM_COLOR;
        tooltipBodyItemColor.style.backgroundColor = colors.backgroundColor;
        tooltipBodyItem.appendChild(tooltipBodyItemColor);

        if (item[0].split(':').length > 1) {
          var tooltipBodyItemLabel = document.createElement(Selector.SPAN);
          tooltipBodyItemLabel.className = ClassName.TOOLTIP_BODY_ITEM_LABEL;
          tooltipBodyItemLabel.innerHTML = item[0].split(': ')[0];
          tooltipBodyItem.appendChild(tooltipBodyItemLabel);
          var tooltipBodyItemValue = document.createElement(Selector.SPAN);
          tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          tooltipBodyItemValue.innerHTML = item[0].split(': ').pop();
          tooltipBodyItem.appendChild(tooltipBodyItemValue);
        } else {
          var _tooltipBodyItemValue = document.createElement(Selector.SPAN);

          _tooltipBodyItemValue.className = ClassName.TOOLTIP_BODY_ITEM_VALUE;
          _tooltipBodyItemValue.innerHTML = item[0];
          tooltipBodyItem.appendChild(_tooltipBodyItemValue);
        }

        tooltipBody.appendChild(tooltipBodyItem);
      });
      tooltip.innerHTML = '';
      tooltip.appendChild(tooltipHeader);
      tooltip.appendChild(tooltipBody);
    }

    var positionY = this._chart.canvas.offsetTop;
    var positionX = this._chart.canvas.offsetLeft; // Display, position, and set styles for font

    tooltip.style.opacity = 1;
    tooltip.style.left = positionX + tooltipModel.caretX + "px";
    tooltip.style.top = positionY + tooltipModel.caretY + "px";
  }

  exports.CustomTooltips = CustomTooltips;

  Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=custom-tooltips.js.map


/***/ }),

/***/ "./src/app/service/monte_carlo.service.ts":
/*!************************************************!*\
  !*** ./src/app/service/monte_carlo.service.ts ***!
  \************************************************/
/*! exports provided: MonteCarloService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonteCarloService", function() { return MonteCarloService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");




var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/json'
    })
};
var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].server.host;
var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
headers.set('enctype', 'multipart/form-data');
var MonteCarloService = /** @class */ (function () {
    function MonteCarloService(httpClient) {
        this.httpClient = httpClient;
    }
    MonteCarloService.prototype.getConfig = function () {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get('');
    };
    MonteCarloService.prototype.getData = function (json) {
        return this.httpClient.post(host + 'monte_carlos/users/test21/runSimulation', json, httpOptions);
    };
    MonteCarloService.prototype.getInputData = function (userID) {
        return this.httpClient.get(host + 'monte_carlos/users/test21/returnInputData');
    };
    MonteCarloService.prototype.download = function (userID) {
        return this.httpClient.get(host + 'monte_carlos/users/test21/download');
    };
    MonteCarloService.prototype.upload = function (fileToUpload) {
        console.log(fileToUpload);
        var formData = new FormData();
        formData.append('file', fileToUpload, fileToUpload.name);
        console.log(formData.getAll('file'));
        return this.httpClient.post(host + 'monte_carlos/users/test21/upload', formData, { headers: headers });
    };
    MonteCarloService.prototype.saveSimulation = function (body) {
        return this.httpClient.post(host + 'monte_carlos/users/test21/saveData', body, httpOptions);
    };
    MonteCarloService.prototype.openSaveSimulation = function (userID) {
        return this.httpClient.get(host + 'monte_carlos/users/test21/returnSaveData');
    };
    MonteCarloService.prototype.resetSimulation = function (userID) {
        return this.httpClient.get(host + 'monte_carlos/users/test21/download');
    };
    MonteCarloService.prototype.saveDefaultValue = function (userID) {
        return this.httpClient.get(host + 'monte_carlos/users/test21/download');
    };
    MonteCarloService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], MonteCarloService);
    return MonteCarloService;
}());



/***/ }),

/***/ "./src/app/views/monte-carlo/monte-carlo-routing.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/views/monte-carlo/monte-carlo-routing.module.ts ***!
  \*****************************************************************/
/*! exports provided: MonteCarloRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonteCarloRoutingModule", function() { return MonteCarloRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _monte_carlo_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./monte-carlo.component */ "./src/app/views/monte-carlo/monte-carlo.component.ts");




var routes = [
    {
        path: '',
        component: _monte_carlo_component__WEBPACK_IMPORTED_MODULE_3__["MonteCarloComponent"],
        data: {
            title: 'Monte Carlo'
        }
    }
];
var MonteCarloRoutingModule = /** @class */ (function () {
    function MonteCarloRoutingModule() {
    }
    MonteCarloRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], MonteCarloRoutingModule);
    return MonteCarloRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/monte-carlo/monte-carlo.component.html":
/*!**************************************************************!*\
  !*** ./src/app/views/monte-carlo/monte-carlo.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn d-flex\">\r\n  <div class=\"d-flex w-100 flex-wrap \">\r\n    <!--Parameters-->\r\n    <div class=\"d-flex w-30 p-2 align-self-start flex-min-width\">\r\n      <div class=\"d-flex card card-accent-primary \">\r\n        <div class=\"card-header\">\r\n          SIMULATION AND MODEL DATA\r\n        </div>\r\n        <div class=\"card-body d-flex w-100  \">\r\n          <!--div class=\"d-flex flex-column w-100\"-->\r\n          <form [formGroup]=\"profileForm\" (ngSubmit)=\"onSubmit()\">\r\n\r\n            <div class=\"d-flex flex-column w-100\">\r\n\r\n              <div class=\"d-flex w-100 pt-1 align-content-end\">\r\n                <div class=\"d-flex w-60\"> &nbsp; </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"!profileForm.valid\">SAVE</button>\r\n                </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"!profileForm.valid\">OPEN</button>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"d-flex title-div pb-1 pt-1\">\r\n                Simulation parameters and experimental data\r\n              </div>\r\n              <div class=\"d-flex flex-column w-100 pt-2\">\r\n\r\n                <div class=\"d-flex w-100\">\r\n                  <div class=\"w-30 p-1\">\r\n                    <span style=\"vertical-align: middle\">Total time</span>\r\n                  </div>\r\n                  <div class=\"d-flex w-15 p-1\">\r\n                    <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.s}} formControlName=\"s\">\r\n                  </div>\r\n                  <div class=\"w-25 p-1\">\r\n                    <span style=\"vertical-align: middle\">[s]</span>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex w-100\">\r\n                  <div class=\"w-30 p-1\">\r\n                    <span style=\"vertical-align: middle\">Epsilon</span>\r\n                  </div>\r\n                  <div class=\"w-15 p-1\">\r\n                    <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.epsilon}}\r\n                      formControlName=\"epsilon\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"d-flex w-100\">\r\n                  <div class=\"w-30 p-1\">\r\n                    <span style=\"vertical-align: middle\">Number of iterations</span>\r\n                  </div>\r\n                  <div class=\"w-15 p-1\">\r\n                    <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.number_of_iteration}}\r\n                      formControlName=\"number_of_iteration\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"d-flex w-100\">\r\n                  <div class=\"w-30 p-1\">\r\n                    <span style=\"vertical-align: middle\">Experimental Data File</span>\r\n                  </div>\r\n                  <div class=\"w-40 p-1\">\r\n                    <input class=\"w-100\" type=\"file\">\r\n                  </div>\r\n                </div>\r\n                <div class=\"d-flex w-100 pt-3\">\r\n                    <div class=\"d-flex w-50\">\r\n                      <div class=\"d-flex w-35\"><span>Type of Simulation</span></div>\r\n                      <div class=\"d-flex flex-column w-65\">\r\n                        <div class=\"form-check\">\r\n                          <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                            [(ngModel)]=\"this.inputData.type_of_simulation\" formControlName=\"type_of_simulation\"\r\n                            (ngModelChange)=\"onCheckboxChange('type_of_simulation', 'type_of_simulation_final_myosin')\">\r\n                          <label class=\"form-check-label\">Time Course</label>\r\n                        </div>\r\n                        <div class=\"form-check\">\r\n                          <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                            [(ngModel)]=\"this.inputData.type_of_simulation_final_myosin\" formControlName=\"type_of_simulation_final_myosin\"\r\n                            (ngModelChange)=\"onCheckboxChange('type_of_simulation_final_myosin', 'type_of_simulation')\">\r\n                          <label class=\"form-check-label\">Titration</label>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"d-flex flex-column w-50\">\r\n                      <div class=\"d-flex w-100\">\r\n                        <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Initial)\">\r\n                          <span style=\"vertical-align: middle\">Myosin Initial</span>\r\n                        </div>\r\n                        <div class=\"w-37 p-1\">\r\n                          <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                              formControlName=\"myosin\"-->\r\n                          <input class=\"w-100 text-align-right\" type=\"text\">\r\n                        </div>\r\n                        <div class=\"w-20 p-1\">\r\n                          <span style=\"vertical-align: middle\">[uM]</span>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"d-flex w-100\" *ngIf=\"this.inputData.type_of_simulation_final_myosin\">\r\n                        <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Final)\">\r\n                          <span style=\"vertical-align: middle\">Myosin Final</span>\r\n                        </div>\r\n                        <div class=\"w-37 p-1\">\r\n                          <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                              formControlName=\"myosin\"-->\r\n                          <input class=\"w-100 text-align-right\" type=\"text\">\r\n                        </div>\r\n                        <div class=\"w-20 p-1\">\r\n                          <span style=\"vertical-align: middle\">[uM]</span>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n              </div>\r\n\r\n\r\n\r\n              <div class=\"d-flex title-div pt-2 pb-1\">\r\n                Fixed model parameters\r\n              </div>\r\n              <div class=\"d-flex w-100 pt-2\">\r\n\r\n                <!-- JEDNA KOLONA-->\r\n                <div class=\"d-flex flex-column w-100\">\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Concentration of Actin</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.actin}}\r\n                        formControlName=\"actin\">\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Number of TmTn Units</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.number_of_tm_tn}}\r\n                        formControlName=\"number_of_tm_tn\">\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Number of Filaments</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.number_of_filaments}}\r\n                        formControlName=\"number_of_filaments\">\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n\r\n                  <div class=\"d-flex w-100\">\r\n                    <!--div class=\"d-flex w-10\"></div-->\r\n                    <div class=\"w-30 p-1\">\r\n                      <span style=\"vertical-align: middle\">Myosin</span>\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                        formControlName=\"myosin\">\r\n                    </div>\r\n                    <div class=\"w-15 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n              <div class=\"d-flex flex-column w-100 pt-2\">\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Parameters for estimation\r\n                </div>\r\n                <!--<div class=\"d-flex text-center pt-2 text-bottom\"> -->\r\n                <div class=\"d-flex text-center pt-2 my-font-sm75\">\r\n                  <div class=\"w-40 p-1\"></div>\r\n                  <div class=\"w-10 p-1 align-self-end\">Fitting</div>\r\n                  <div class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1 align-self-end\">Guess 1</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Guess 2</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Lower Bound</div>\r\n                    <div class=\"w-25 p-1 align-self-end\">Upper Bound</div>\r\n                  </div>\r\n                </div>\r\n\r\n                <!-- novo -->\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">Kb</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kb}}\r\n                        formControlName=\"kb\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kb_fiting\"\r\n                      formControlName=\"kb_fiting\" (ngModelChange)=\"onCheckboxChange('kb_fiting', 'kb_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kb_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_guess1}}\r\n                        formControlName=\"kb_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_guess2}}\r\n                        formControlName=\"kb_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_lower}}\r\n                        formControlName=\"kb_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_upper}}\r\n                        formControlName=\"kb_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kb-</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kb_minus}}\r\n                        formControlName=\"kb_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kb_minus_fiting\"\r\n                      formControlName=\"kb_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kb_minus_fiting', 'kb_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kb_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minus_guess1}}\r\n                        formControlName=\"kb_minus_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minus_guess2}}\r\n                        formControlName=\"kb_minus_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minuss_lower}}\r\n                        formControlName=\"kb_minus_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kb_minus_upper}}\r\n                        formControlName=\"kb_minus_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">Kt</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt}}\r\n                        formControlName=\"kt\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_fiting\"\r\n                      formControlName=\"kt_fiting\" (ngModelChange)=\"onCheckboxChange('kt_fiting', 'kt_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_guess1}}\r\n                        formControlName=\"kt_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_guess2}}\r\n                        formControlName=\"kt_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_lower}}\r\n                        formControlName=\"kt_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_upper}}\r\n                        formControlName=\"kt_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">kt-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_minus}}\r\n                        formControlName=\"kt_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.kt_minus_fiting\"\r\n                      formControlName=\"kt_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('kt_minus_fiting', 'kt_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.kt_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_guess1}}\r\n                        formControlName=\"kt_minus_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_guess2}}\r\n                        formControlName=\"kt_minus_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_lower}}\r\n                        formControlName=\"kt_minus_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.kt_minus_upper}}\r\n                        formControlName=\"kt_minus_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k1+</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k1}}\r\n                        formControlName=\"k1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k1_fiting\"\r\n                      formControlName=\"k1_fiting\" (ngModelChange)=\"onCheckboxChange('k1_fiting', 'k1_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k1_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_guess1}}\r\n                        formControlName=\"k1_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_guess2}}\r\n                        formControlName=\"k1_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_lower}}\r\n                        formControlName=\"k1_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_upper}}\r\n                        formControlName=\"k1_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k1-:</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k1_minus}}\r\n                        formControlName=\"k1_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k1_minus_fiting\"\r\n                      formControlName=\"k1_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('k1_minus_fiting', 'k1_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k1_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_guess1}}\r\n                        formControlName=\"k1_minus_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_guess2}}\r\n                        formControlName=\"k1_minus_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_lower}}\r\n                        formControlName=\"k1_minus_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k1_minus_upper}}\r\n                        formControlName=\"k1_minus_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k2+</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k2}}\r\n                        formControlName=\"k2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k2_fiting\"\r\n                      formControlName=\"k2_fiting\" (ngModelChange)=\"onCheckboxChange('k2_fiting', 'k2_minus_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k2_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_guess1}}\r\n                        formControlName=\"k2_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_guess2}}\r\n                        formControlName=\"k2_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_lower}}\r\n                        formControlName=\"k2_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_upper}}\r\n                        formControlName=\"k2_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">k2-</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-50 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k2_minus}}\r\n                        formControlName=\"k2_minus\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[uM]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.k2_minus_fiting\"\r\n                      formControlName=\"k2_minus_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('k2_minus_fiting', 'k2_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.k2_minus_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_guess1}}\r\n                        formControlName=\"k2_minus_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_guess2}}\r\n                        formControlName=\"k2_minus_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_lower}}\r\n                        formControlName=\"k2_minus_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.k2_minus_upper}}\r\n                        formControlName=\"k2_minus_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-50 p-1\">\r\n                      <span style=\"vertical-align: middle\">Cooperativity</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-25 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.factor}}\r\n                        formControlName=\"factor\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\" [(ngModel)]=\"this.inputData.factor_fiting\"\r\n                      formControlName=\"factor_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('factor_fiting', 'negative_factor_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.factor_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.factor_guess1}}\r\n                        formControlName=\"factor_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.factor_guess2}}\r\n                        formControlName=\"factor_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.factor_lower}}\r\n                        formControlName=\"factor_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\" ngModel={{this.inputData.factor_upper}}\r\n                        formControlName=\"factor_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex\">\r\n                  <div class=\"d-flex w-40\">\r\n                    <div class=\"w-50 p-1\">\r\n                      <span style=\"vertical-align: middle\">Negative Coop</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-25 p-1\">\r\n                      <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.negative_factor}}\r\n                        formControlName=\"negative_factor\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <span style=\"vertical-align: middle\">[-]</span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"w-10 p-1 text-center\">\r\n                    <input style=\"vertical-align: middle\" type=\"checkbox\"\r\n                      [(ngModel)]=\"this.inputData.negative_factor_fiting\" formControlName=\"negative_factor_fiting\"\r\n                      (ngModelChange)=\"onCheckboxChange('negative_factor_fiting', 'factor_fiting')\">\r\n                  </div>\r\n                  <div *ngIf=\"this.inputData.negative_factor_fiting\" class=\"d-flex w-50\">\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.negative_factor_guess1}} formControlName=\"negative_factor_guess1\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.negative_factor_guess2}} formControlName=\"negative_factor_guess2\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.negative_factor_lower}} formControlName=\"negative_factor_lower\">\r\n                    </div>\r\n                    <div class=\"w-25 p-1\">\r\n                      <input class=\"w-100 text-align-center\" type=\"text\"\r\n                        ngModel={{this.inputData.negative_factor_upper}} formControlName=\"negative_factor_upper\">\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <!-- end novo-->\r\n\r\n              </div>\r\n\r\n              <div class=\"d-flex w-100 pt-3 \">\r\n                <div class=\"d-flex w-80\"> &nbsp; </div>\r\n                <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                  <button id=\"dugmeRun\" type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                    [disabled]=\"dugmeRun\">RUN </button>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </form>\r\n          <!--/div-->\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"d-flex w-70 p-2\">\r\n      <div class=\"d-flex card card-accent-primary w-100\">\r\n        <div class=\"card-header\">\r\n          RESULTS\r\n          <span class=\"float-right\" tooltip=\"Download graphic data\" placement=\"left\">\r\n            <i (click)=\"download()\" class=\"fa fa-cloud-download\" style=\"font-size:16px\"></i>\r\n          </span>\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex flex-column w-100 mv-400\">\r\n            <div class=\"d-flex w-100 h-5 justify-content-around\">\r\n            </div>\r\n            <div class=\"d-flex flex-column w-100 pt-2\">\r\n              <div class=\"d-flex w-100 flex-wrap justify-content-around\">\r\n\r\n                <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n                <div class=\"w-25 tbl-10\">\r\n                  <!--style=\"height:300px;margin-top:40px;\"-->\r\n                  <div class=\"d-flex pb-10 title-div-table\">\r\n                    Estimated values\r\n                  </div>\r\n                  <!--div class=\"table-responsive\">-->\r\n                  <div class=\"tbl-10\">\r\n                    <table class=\"table table-condensed \" *ngIf=\"isVisible\">\r\n                      <!--table-striped-->\r\n                      <tbody>\r\n                        <tr *ngFor=\"let row of dataSource\">\r\n                          <td *ngFor=\"let col of row\" class=\"td td-1\">{{col}}</td>\r\n                        </tr>\r\n                      </tbody>\r\n                    </table>\r\n                  </div>\r\n                </div>\r\n                <div class=\"w-60 tbl-10 \">\r\n                  <div class=\"d-flex pb-10 title-div-table\">\r\n                    Sensitivity matrix\r\n                  </div>\r\n                  <div class=\"tbl-8kolona\">\r\n                    <table class=\"table table-condensed\" *ngIf=\"isVisible\">\r\n                      <tbody>\r\n                        <tr *ngFor=\"let row of sensitivityMatrix\">\r\n                          <td *ngFor=\"let col of row\" class=\"td td-1\">{{col}}</td>\r\n                        </tr>\r\n                      </tbody>\r\n                    </table>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <!--div class=\"card-body d-flex w-100\"-->\r\n              <div class=\"d-flex w-100 flex-wrap justify-content-around align-content-start\">\r\n                <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n                <div class=\"w-100 p-2 mw-350\">\r\n                  <div class=\"chart-wrapper\">\r\n                    <!--style=\"height:300px;margin-top:40px;\"-->\r\n                    <canvas *ngIf=\"graphs[0]\" baseChart class=\"chart\" [datasets]=\"graphs[0].datasets\"\r\n                      [labels]=\"graphs[0].labels\" [options]=\"graphs[0].options\" [colors]=\"graphs[0].colors\"\r\n                      [legend]=\"graphs[0].legend\" [chartType]=\"graphs[0].chartType\">\r\n                    </canvas>\r\n                    <canvas *ngIf=\"!graphs[0]\" baseChart class=\"chart\"></canvas>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"d-flex title-div-2 w-100 h-20 p-3 justify-content-around\">\r\n              Estimated parameters convergation\r\n            </div>\r\n            <!--div class=\"d-flex flex-column w-100 pt-2\"></div-->\r\n            <div class=\"d-inline-flex p-2 w-100 flex-wrap justify-content-center\">\r\n              <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n              <div class=\"w-100\">\r\n                <div class=\"chart-wrapper\">\r\n                  <!--style=\"height:300px;margin-top:40px;\"-->\r\n                  <canvas *ngIf=\"graphs[1]\" baseChart class=\"chart\" [datasets]=\"graphs[1].datasets\"\r\n                    [labels]=\"graphs[1].labels\" [options]=\"graphs[1].options\" [colors]=\"graphs[1].colors\"\r\n                    [legend]=\"graphs[1].legend\" [chartType]=\"graphs[1].chartType\">\r\n                  </canvas>\r\n                  <canvas *ngIf=\"!graphs[1]\" baseChart class=\"chart\">\r\n                  </canvas>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/monte-carlo/monte-carlo.component.scss":
/*!**************************************************************!*\
  !*** ./src/app/views/monte-carlo/monte-carlo.component.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column; }\n\n.example-container > * {\n  width: 100%; }\n\n.example-container form {\n  margin-bottom: 20px; }\n\n.example-container form > * {\n  margin: 5px 0; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.table_form {\n  min-width: 550px;\n  padding: 0px; }\n\n.table_form p {\n  font-size: 10px;\n  text-align: right; }\n\n.table_form input {\n  height: 25px;\n  text-align: center;\n  max-width: 100px; }\n\n/* Customize the label (the container) */\n\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n/* Hide the browser's default checkbox */\n\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0; }\n\n/* Create a custom checkbox */\n\n.checkmark {\n  position: absolute;\n  margin-top: 5px;\n  top: 0;\n  left: 0;\n  height: 20 px;\n  width: 20px;\n  background-color: #eee; }\n\n/* On mouse-over, add a grey background color */\n\n.container:hover input ~ .checkmark {\n  background-color: #ccc; }\n\n/* When the checkbox is checked, add a blue background */\n\n.container input:checked ~ .checkmark {\n  background-color: #2196F3; }\n\n/* Create the checkmark/indicator (hidden when not checked) */\n\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none; }\n\n/* Show the checkmark when checked */\n\n.container input:checked ~ .checkmark:after {\n  display: block; }\n\n/* Style the checkmark/indicator */\n\n.container .checkmark:after {\n  left: 9px;\n  top: 5px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg); }\n\n.title-div-centar {\n  font-size: 1rem;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\n.title-div {\n  color: cadetblue;\n  font-style: 1rem;\n  border-bottom: 1px dashed lightblue; }\n\n.title-div-table {\n  font-size: 1rem;\n  color: cadetblue;\n  text-align: center !important; }\n\n.title-div-2 {\n  font-size: 1.2rem;\n  color: cadetblue; }\n\ninput {\n  background-color: #c3e4e5;\n  border: 1px solid \t#c3e4e5 !important; }\n\n.text-align-right {\n  text-align: right;\n  padding-right: 2px !important; }\n\n.text-align-center {\n  text-align: center; }\n\n.flex-min-width {\n  min-width: 470px; }\n\n.max-width-70posto {\n  max-width: 70%; }\n\n.over-auto {\n  overflow: visible; }\n\n.chart-wrapper {\n  border: 1px solid lightblue !important; }\n\n.mw-350 {\n  min-width: 280px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-30posto {\n  min-width: 370px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-400 {\n  min-width: 280px; }\n\n.tbl-10 {\n  min-width: 300px !important; }\n\n.tbl-8kolona {\n  min-width: 300px !important; }\n\n.td-1 {\n  padding: 0.4rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvbW9udGUtY2FybG8vRjpcXHRoaW5maWxhbWVudHYzMjBcXG5ld19jbGllbnQvc3JjXFxhcHBcXHZpZXdzXFxtb250ZS1jYXJsb1xcbW9udGUtY2FybG8uY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCLEVBQUE7O0FBR3hCO0VBQ0UsV0FBVyxFQUFBOztBQUdiO0VBQ0UsbUJBQW1CLEVBQUE7O0FBR3JCO0VBQ0UsYUFBYSxFQUFBOztBQUdmO0VBQ0UsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixXQUFXLEVBQUE7O0FBR2I7RUFDRSxXQUFXLEVBQUE7O0FBR2I7RUFDRSxnQkFBZ0I7RUFDaEIsWUFBWSxFQUFBOztBQUdkO0VBQ0UsZUFBZTtFQUNmLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLFlBQVk7RUFDWixrQkFBa0I7RUFDbEIsZ0JBQWdCLEVBQUE7O0FBSWxCLHdDQUFBOztBQUNBO0VBQ0EsY0FBYztFQUNkLGtCQUFrQjtFQUNsQixrQkFBa0I7RUFDbEIsbUJBQW1CO0VBQ25CLGVBQWU7RUFDZixlQUFlO0VBQ2YseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0QixxQkFBcUI7RUFDckIsaUJBQWlCLEVBQUE7O0FBR2pCLHdDQUFBOztBQUNBO0VBQ0Esa0JBQWtCO0VBQ2xCLFVBQVU7RUFDVixlQUFlO0VBQ2YsU0FBUztFQUNULFFBQVEsRUFBQTs7QUFHUiw2QkFBQTs7QUFDQTtFQUNBLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2YsTUFBTTtFQUNOLE9BQU87RUFDUCxhQUFhO0VBQ2IsV0FBVztFQUNYLHNCQUFzQixFQUFBOztBQUd0QiwrQ0FBQTs7QUFDQTtFQUNBLHNCQUFzQixFQUFBOztBQUd0Qix3REFBQTs7QUFDQTtFQUNBLHlCQUF5QixFQUFBOztBQUd6Qiw2REFBQTs7QUFDQTtFQUNBLFdBQVc7RUFDWCxrQkFBa0I7RUFDbEIsYUFBYSxFQUFBOztBQUdiLG9DQUFBOztBQUNBO0VBQ0EsY0FBYyxFQUFBOztBQUdkLGtDQUFBOztBQUNBO0VBQ0EsU0FBUztFQUNULFFBQVE7RUFDUixVQUFVO0VBQ1YsWUFBWTtFQUNaLG1CQUFtQjtFQUNuQix5QkFBeUI7RUFDekIsZ0NBQWdDO0VBRWhDLHdCQUF3QixFQUFBOztBQUd4QjtFQUVBLGVBQWU7RUFDZixpQkFBaUI7RUFDakIsb0JBQW9CLEVBQUE7O0FBSXBCO0VBQ0EsZ0JBQWdCO0VBQ2hCLGdCQUFnQjtFQUNoQixtQ0FBbUMsRUFBQTs7QUFHbkM7RUFDQSxlQUFlO0VBQ2YsZ0JBQWdCO0VBQ2hCLDZCQUE2QixFQUFBOztBQUc3QjtFQUNBLGlCQUFpQjtFQUNqQixnQkFBZ0IsRUFBQTs7QUFJaEI7RUFFQSx5QkFBMEI7RUFDMUIscUNBQXFDLEVBQUE7O0FBR3JDO0VBQ0EsaUJBQWlCO0VBQ2pCLDZCQUE2QixFQUFBOztBQUc3QjtFQUNBLGtCQUFrQixFQUFBOztBQUlsQjtFQUNBLGdCQUFnQixFQUFBOztBQVNoQjtFQUNBLGNBQWMsRUFBQTs7QUFFZDtFQUNBLGlCQUFpQixFQUFBOztBQUdqQjtFQUVBLHNDQUFzQyxFQUFBOztBQUd0QztFQUNBLGdCQUFnQjtFQUNoQiwyQkFBbUI7RUFBbkIsd0JBQW1CO0VBQW5CLG1CQUFtQixFQUFBOztBQUduQjtFQUNBLGdCQUFnQjtFQUNoQiwyQkFBbUI7RUFBbkIsd0JBQW1CO0VBQW5CLG1CQUFtQixFQUFBOztBQUtuQjtFQUNBLGdCQUFnQixFQUFBOztBQUdoQjtFQUVBLDJCQUEyQixFQUFBOztBQUUzQjtFQUVBLDJCQUEyQixFQUFBOztBQUkzQjtFQUNBLDBCQUEwQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvdmlld3MvbW9udGUtY2FybG8vbW9udGUtY2FybG8uY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZXhhbXBsZS1jb250YWluZXIge1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxufVxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyID4gKiB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5leGFtcGxlLWNvbnRhaW5lciBmb3JtIHtcclxuICBtYXJnaW4tYm90dG9tOiAyMHB4O1xyXG59XHJcblxyXG4uZXhhbXBsZS1jb250YWluZXIgZm9ybSA+ICoge1xyXG4gIG1hcmdpbjogNXB4IDA7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZvcm0ge1xyXG4gIG1pbi13aWR0aDogMTUwcHg7XHJcbiAgbWF4LXdpZHRoOiA1MDBweDtcclxuICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmV4YW1wbGUtZnVsbC13aWR0aCB7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi50YWJsZV9mb3JtIHtcclxuICBtaW4td2lkdGg6IDU1MHB4O1xyXG4gIHBhZGRpbmc6IDBweDtcclxufVxyXG5cclxuLnRhYmxlX2Zvcm0gcCB7XHJcbiAgZm9udC1zaXplOiAxMHB4O1xyXG4gIHRleHQtYWxpZ246IHJpZ2h0O1xyXG59XHJcblxyXG4udGFibGVfZm9ybSBpbnB1dCB7XHJcbiAgaGVpZ2h0OiAyNXB4O1xyXG4gIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICBtYXgtd2lkdGg6IDEwMHB4O1xyXG59XHJcblxyXG5cclxuLyogQ3VzdG9taXplIHRoZSBsYWJlbCAodGhlIGNvbnRhaW5lcikgKi9cclxuLmNvbnRhaW5lciB7XHJcbmRpc3BsYXk6IGJsb2NrO1xyXG5wb3NpdGlvbjogcmVsYXRpdmU7XHJcbnBhZGRpbmctbGVmdDogMzVweDtcclxubWFyZ2luLWJvdHRvbTogMTJweDtcclxuY3Vyc29yOiBwb2ludGVyO1xyXG5mb250LXNpemU6IDIycHg7XHJcbi13ZWJraXQtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbi1tb3otdXNlci1zZWxlY3Q6IG5vbmU7XHJcbi1tcy11c2VyLXNlbGVjdDogbm9uZTtcclxudXNlci1zZWxlY3Q6IG5vbmU7XHJcbn1cclxuXHJcbi8qIEhpZGUgdGhlIGJyb3dzZXIncyBkZWZhdWx0IGNoZWNrYm94ICovXHJcbi5jb250YWluZXIgaW5wdXQge1xyXG5wb3NpdGlvbjogYWJzb2x1dGU7XHJcbm9wYWNpdHk6IDA7XHJcbmN1cnNvcjogcG9pbnRlcjtcclxuaGVpZ2h0OiAwO1xyXG53aWR0aDogMDtcclxufVxyXG5cclxuLyogQ3JlYXRlIGEgY3VzdG9tIGNoZWNrYm94ICovXHJcbi5jaGVja21hcmsge1xyXG5wb3NpdGlvbjogYWJzb2x1dGU7XHJcbm1hcmdpbi10b3A6IDVweDtcclxudG9wOiAwO1xyXG5sZWZ0OiAwO1xyXG5oZWlnaHQ6IDIwIHB4O1xyXG53aWR0aDogMjBweDtcclxuYmFja2dyb3VuZC1jb2xvcjogI2VlZTtcclxufVxyXG5cclxuLyogT24gbW91c2Utb3ZlciwgYWRkIGEgZ3JleSBiYWNrZ3JvdW5kIGNvbG9yICovXHJcbi5jb250YWluZXI6aG92ZXIgaW5wdXQgfiAuY2hlY2ttYXJrIHtcclxuYmFja2dyb3VuZC1jb2xvcjogI2NjYztcclxufVxyXG5cclxuLyogV2hlbiB0aGUgY2hlY2tib3ggaXMgY2hlY2tlZCwgYWRkIGEgYmx1ZSBiYWNrZ3JvdW5kICovXHJcbi5jb250YWluZXIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcmsge1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiAjMjE5NkYzO1xyXG59XHJcblxyXG4vKiBDcmVhdGUgdGhlIGNoZWNrbWFyay9pbmRpY2F0b3IgKGhpZGRlbiB3aGVuIG5vdCBjaGVja2VkKSAqL1xyXG4uY2hlY2ttYXJrOmFmdGVyIHtcclxuY29udGVudDogXCJcIjtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG5kaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4vKiBTaG93IHRoZSBjaGVja21hcmsgd2hlbiBjaGVja2VkICovXHJcbi5jb250YWluZXIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcms6YWZ0ZXIge1xyXG5kaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLyogU3R5bGUgdGhlIGNoZWNrbWFyay9pbmRpY2F0b3IgKi9cclxuLmNvbnRhaW5lciAuY2hlY2ttYXJrOmFmdGVyIHtcclxubGVmdDogOXB4O1xyXG50b3A6IDVweDtcclxud2lkdGg6IDVweDtcclxuaGVpZ2h0OiAxMHB4O1xyXG5ib3JkZXI6IHNvbGlkIHdoaXRlO1xyXG5ib3JkZXItd2lkdGg6IDAgM3B4IDNweCAwO1xyXG4td2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxuLW1zLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxudHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG59XHJcblxyXG4udGl0bGUtZGl2LWNlbnRhciB7XHJcbi8vY29sb3I6IGNhZGV0Ymx1ZTtcclxuZm9udC1zaXplOiAxcmVtO1xyXG5wYWRkaW5nLXRvcDogMXJlbTtcclxucGFkZGluZy1ib3R0b206IDFyZW07XHJcbi8vYm9yZGVyLWJvdHRvbTogMXB4IGRhc2hlZCBsaWdodGJsdWU7XHJcbn1cclxuXHJcbi50aXRsZS1kaXYge1xyXG5jb2xvcjogY2FkZXRibHVlO1xyXG5mb250LXN0eWxlOiAxcmVtO1xyXG5ib3JkZXItYm90dG9tOiAxcHggZGFzaGVkIGxpZ2h0Ymx1ZTtcclxufVxyXG5cclxuLnRpdGxlLWRpdi10YWJsZSB7XHJcbmZvbnQtc2l6ZTogMXJlbTsgXHJcbmNvbG9yOiBjYWRldGJsdWU7XHJcbnRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGl0bGUtZGl2LTIge1xyXG5mb250LXNpemU6IDEuMnJlbTsgXHJcbmNvbG9yOiBjYWRldGJsdWU7XHJcbi8vIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pbnB1dCB7XHJcbi8vYm9yZGVyOiBub25lO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiBcdCNjM2U0ZTU7XHJcbmJvcmRlcjogMXB4IHNvbGlkIFx0I2MzZTRlNSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGV4dC1hbGlnbi1yaWdodCB7XHJcbnRleHQtYWxpZ246IHJpZ2h0O1xyXG5wYWRkaW5nLXJpZ2h0OiAycHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtYWxpZ24tY2VudGVyIHtcclxudGV4dC1hbGlnbjogY2VudGVyO1xyXG5cclxufVxyXG5cclxuLmZsZXgtbWluLXdpZHRoIHtcclxubWluLXdpZHRoOiA0NzBweDtcclxuLy8gIG1heC13aWR0aDogNjAwcHg7XHJcbi8vei1pbmRleDogMTAwMDtcclxufVxyXG5cclxuLmZsZXgtbWluLXdpZHRoICoge1xyXG4vL3otaW5kZXg6IDEwMDA7XHJcbn1cclxuXHJcbi5tYXgtd2lkdGgtNzBwb3N0byB7XHJcbm1heC13aWR0aDogNzAlO1xyXG59XHJcbi5vdmVyLWF1dG8ge1xyXG5vdmVyZmxvdzogdmlzaWJsZTtcclxufVxyXG5cclxuLmNoYXJ0LXdyYXBwZXIge1xyXG4vL2JvcmRlci10b3Atc3R5bGU6IGdyb292ZSAhaW1wb3J0YW50O1xyXG5ib3JkZXI6IDFweCBzb2xpZCBsaWdodGJsdWUgIWltcG9ydGFudDtcclxufVxyXG5cclxuLm13LTM1MCB7XHJcbm1pbi13aWR0aDogMjgwcHg7XHJcbmhlaWdodDogZml0LWNvbnRlbnQ7XHJcbn1cclxuXHJcbi5tdy0zMHBvc3RvIHtcclxubWluLXdpZHRoOiAzNzBweDtcclxuaGVpZ2h0OiBmaXQtY29udGVudDtcclxuLy9mb250LXNpemU6IDAuNXJlbSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5cclxuLm13LTQwMCB7XHJcbm1pbi13aWR0aDogMjgwcHg7XHJcbn1cclxuXHJcbi50YmwtMTAge1xyXG4vLyBtYXJnaW46IDEwcHggIWltcG9ydGFudDtcclxubWluLXdpZHRoOiAzMDBweCAhaW1wb3J0YW50O1xyXG59XHJcbi50YmwtOGtvbG9uYSB7XHJcbi8vIG1hcmdpbjogMTBweCAhaW1wb3J0YW50O1xyXG5taW4td2lkdGg6IDMwMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG4udGQtMXtcclxucGFkZGluZzogMC40cmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/views/monte-carlo/monte-carlo.component.ts":
/*!************************************************************!*\
  !*** ./src/app/views/monte-carlo/monte-carlo.component.ts ***!
  \************************************************************/
/*! exports provided: MonteCarloComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonteCarloComponent", function() { return MonteCarloComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @coreui/coreui-plugin-chartjs-custom-tooltips */ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_monte_carlo_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/monte_carlo.service */ "./src/app/service/monte_carlo.service.ts");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");







var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_6__["environment"].server.host;
var MonteCarloComponent = /** @class */ (function () {
    function MonteCarloComponent(fb, MonteCarloService) {
        this.fb = fb;
        this.MonteCarloService = MonteCarloService;
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            s: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            factor: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            factor_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            factor_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            factor_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            factor_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            factor_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            negative_factor: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            negative_factor_fiting: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            negative_factor_guess1: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            negative_factor_guess2: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            negative_factor_lower: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            negative_factor_upper: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            actin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            number_of_tm_tn: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            number_of_filaments: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            epsilon: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            number_of_iteration: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            type_of_simulation_final_myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            type_of_simulation: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.inputData = this.profileForm;
        this.dugmeRun = false;
        this.isVisible = true;
        this.run_button = "RUN";
        this.fileToUpload = null;
        this.numberOfGraphs = 2;
        this.graphs = [];
        this.atModelGraph = [];
        this.errorGraph = [];
    }
    ;
    MonteCarloComponent.prototype.ngOnInit = function () {
    };
    MonteCarloComponent.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.MonteCarloService.getInputData('test21').subscribe(function (data) {
            if (data) {
                _this.inputData = data;
            }
        });
    };
    MonteCarloComponent.prototype.onSubmit = function () {
        var _this = this;
        this.dugmeRun = true;
        this.run_button = "RUNNING";
        this.isVisible = false;
        this.graphs = [];
        var body = this.profileForm.value;
        this.MonteCarloService.upload(this.fileToUpload).subscribe(function (upload) {
            _this.MonteCarloService.getData(body).subscribe(function (data) {
                var a_t = data.a_t;
                var models = data.model;
                var vidljivo = [
                    Boolean(body.kb_fiting),
                    Boolean(body.kb_minus_fiting),
                    Boolean(body.kt_fiting),
                    Boolean(body.kt_minus_fiting),
                    Boolean(body.k1_fiting),
                    Boolean(body.k1_minus_fiting),
                    Boolean(body.k2_fiting),
                    Boolean(body.k2_minus_fiting),
                    Boolean(body.cooperativity_factor_fiting),
                    Boolean(body.negatve_cooperativity_factor_fiting)
                ];
                var podaci = [
                    { data: data.kb, label: 'KB' },
                    { data: data.kb_minus, label: 'KB-' },
                    { data: data.kt, label: 'KT' },
                    { data: data.kt_minus, label: 'KT-' },
                    { data: data.k1, label: 'K1' },
                    { data: data.k1_minus, label: 'K1-' },
                    { data: data.k2, label: 'K2' },
                    { data: data.k2_minus, label: 'K2-' },
                    { data: data.cooperativity_factor, label: 'Cooperativity' },
                    { data: data.negatve_cooperativity_factor, label: 'Negative Cooperativity' }
                ];
                var datasets = [
                    [
                        { data: a_t, label: 'Experiment' },
                        { data: models, label: 'Model' }
                    ],
                    []
                ];
                for (var i = 0; i < 14; i++) {
                    if (vidljivo[i]) {
                        datasets[1].push(podaci[i]);
                    }
                }
                datasets[1].push({ data: data.error, label: 'Error' });
                _this.graphs = [];
                _this.prepareGraphs([datasets[0].length, datasets[1].length]);
                for (var i = 0; i < _this.numberOfGraphs; i++) {
                    _this.graphs[i].datasets = datasets[i];
                    _this.graphs[i].legend = true;
                }
                _this.isVisible = true;
                console.log(_this.graphs);
                for (var i = 1; i < data.estimated_values.length; i++) {
                    data.estimated_values[i][1] = data.estimated_values[i][1].toFixed(6);
                    data.estimated_values[i][2] = data.estimated_values[i][2].toFixed(6);
                    data.estimated_values[i][3] = data.estimated_values[i][3].toFixed(6);
                }
                _this.dataSource = data.estimated_values;
                _this.sensitivityMatrix = data.sensitivity;
                _this.dugmeRun = false;
                _this.run_button = "RUN";
            });
        });
    };
    MonteCarloComponent.prototype.download = function () {
        window.open(host + 'monte_carlos/users/test21/download');
    };
    MonteCarloComponent.prototype.saveSimulation = function () {
        var body = this.profileForm.value;
        this.MonteCarloService.saveSimulation(body).subscribe(function (data) {
            console.log(data);
        });
    };
    MonteCarloComponent.prototype.openSaveSimulation = function () {
        var _this = this;
        this.graphs = [];
        this.MonteCarloService.openSaveSimulation('test21').subscribe(function (data) {
            var inputDatas = data.parameters;
            _this.inputData = data.parameters;
            var vidljivo = [
                Boolean(inputDatas.kb_fiting),
                Boolean(inputDatas.kb_minus_fiting),
                Boolean(inputDatas.kt_fiting),
                Boolean(inputDatas.kt_minus_fiting),
                Boolean(inputDatas.k1_fiting),
                Boolean(inputDatas.k1_minus_fiting),
                Boolean(inputDatas.k2_fiting),
                Boolean(inputDatas.k2_minus_fiting),
                Boolean(inputDatas.cooperativity_factor_fiting),
                Boolean(inputDatas.negatve_cooperativity_factor_fiting)
            ];
            var podaci = [
                { data: data.kb, label: 'KB' },
                { data: data.kb_minus, label: 'KB-' },
                { data: data.kt, label: 'KT' },
                { data: data.kt_minus, label: 'KT-' },
                { data: data.k1, label: 'K1' },
                { data: data.k1_minus, label: 'K1-' },
                { data: data.k2, label: 'K2' },
                { data: data.k2_minus, label: 'K2-' },
                { data: data.cooperativity_factor, label: 'Cooperativity' },
                { data: data.negatve_cooperativity_factor, label: 'Negative Cooperativity' }
            ];
            var params = data.inputData;
            var datasets = [
                [
                    { data: params.a_t, label: 'Experiment' },
                    { data: params.models, label: 'Model' }
                ],
                []
            ];
            for (var i = 0; i < 14; i++) {
                if (vidljivo[i]) {
                    datasets[1].push(podaci[i]);
                }
            }
            datasets[1].push({ data: params.error, label: 'Error' });
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length, datasets[1].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
                _this.graphs[i].legend = true;
            }
            for (var i = 1; i < data.estimated_values.length; i++) {
                data.estimated_values[i][1] = data.estimated_values[i][1].toFixed(6);
                data.estimated_values[i][2] = data.estimated_values[i][2].toFixed(6);
                data.estimated_values[i][3] = data.estimated_values[i][3].toFixed(6);
            }
            _this.dataSource = params.estimated_values;
            _this.sensitivityMatrix = params.sensitivity;
            _this.dugmeRun = false;
            _this.run_button = "RUN";
        });
    };
    //Graph setup
    MonteCarloComponent.prototype.initGraph = function (newTitle, newLabels, newColors, numberOfLines) {
        var basicGraph = {};
        basicGraph.options = {
            title: {
                display: true,
                text: 'Error',
                // fontColor: 'blue',
                fontStyle: 'small-caps',
            },
            tooltips: {
                enabled: false,
                custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                intersect: true,
                mode: 'index',
                position: 'nearest',
                callbacks: {
                    labelColor: function (tooltipItem, chart) {
                        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                    }
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 15,
                    top: 0,
                    bottom: 5
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        type: 'linear',
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            padding: 5,
                            beginAtZero: false,
                            stepSize: numberOfLines === 2 ? 1000 : 1,
                            fontSize: 12,
                            min: 1
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC1 (%)',
                            fontSize: 11
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            beginAtZero: true,
                            padding: 5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC2 (%)',
                            fontSize: 11
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: true,
                position: 'right'
            }
        };
        var newOptions = basicGraph.options;
        newOptions.title.text = newTitle;
        newOptions.scales.xAxes[0].type = newLabels.xAxes.type;
        newOptions.scales.xAxes[0].scaleLabel.labelString = newLabels.xAxes.title;
        newOptions.scales.yAxes[0].type = newLabels.yAxes.type;
        newOptions.scales.yAxes[0].scaleLabel.display = newLabels.yAxes.displayTitle;
        newOptions.scales.yAxes[0].scaleLabel.labelString = newLabels.yAxes.title;
        basicGraph.options = newOptions;
        basicGraph.colors = [];
        basicGraph.datasets = [];
        for (var i = 0; i < numberOfLines; i++) {
            basicGraph.datasets.push({ data: [] });
            basicGraph.colors.push(newColors[i]);
        }
        basicGraph.chartType = 'line';
        basicGraph.labels = [];
        basicGraph.legend = false;
        basicGraph.visibility = true;
        return basicGraph;
    };
    MonteCarloComponent.prototype.prepareGraphs = function (graphLines) {
        var titles = [
            'Experiment VS Model',
            'Error & Parameters convergence'
        ];
        var labels = [
            { xAxes: { type: 'linear', title: 'Time [s]' }, yAxes: { type: 'linear', displayTitle: true, title: 'Fractional Fluorescence' } },
            { xAxes: { type: 'linear', title: 'Iteration' }, yAxes: { type: 'logarithmic', displayTitle: false, title: '' } }
        ];
        var colors = [
            [
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'yellow', backgroundColor: 'transparent' }
            ],
            [
                { borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' },
                { borderColor: 'yellow', backgroundColor: 'transparent' },
                { borderColor: 'oragne', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'black', backgroundColor: 'transparent' },
                { borderColor: 'brown', backgroundColor: 'transparent' },
            ]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i], graphLines[i]));
        }
    };
    MonteCarloComponent.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
    };
    MonteCarloComponent.prototype.onCheckboxChange = function (box1, box2) {
        if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
            if (this.inputData[box1])
                this.inputData[box2] = false;
        }
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_5__["BaseChartDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
    ], MonteCarloComponent.prototype, "charts", void 0);
    MonteCarloComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-monte-carlo',
            template: __webpack_require__(/*! ./monte-carlo.component.html */ "./src/app/views/monte-carlo/monte-carlo.component.html"),
            styles: [__webpack_require__(/*! ./monte-carlo.component.scss */ "./src/app/views/monte-carlo/monte-carlo.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _service_monte_carlo_service__WEBPACK_IMPORTED_MODULE_4__["MonteCarloService"]])
    ], MonteCarloComponent);
    return MonteCarloComponent;
}());



/***/ }),

/***/ "./src/app/views/monte-carlo/monte-carlo.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/views/monte-carlo/monte-carlo.module.ts ***!
  \*********************************************************/
/*! exports provided: MonteCarloModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MonteCarloModule", function() { return MonteCarloModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _monte_carlo_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./monte-carlo.component */ "./src/app/views/monte-carlo/monte-carlo.component.ts");
/* harmony import */ var _monte_carlo_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./monte-carlo-routing.module */ "./src/app/views/monte-carlo/monte-carlo-routing.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");












var MonteCarloModule = /** @class */ (function () {
    function MonteCarloModule() {
    }
    MonteCarloModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _monte_carlo_routing_module__WEBPACK_IMPORTED_MODULE_8__["MonteCarloRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(),
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ],
            declarations: [_monte_carlo_component__WEBPACK_IMPORTED_MODULE_7__["MonteCarloComponent"]],
            exports: [
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ]
        })
    ], MonteCarloModule);
    return MonteCarloModule;
}());



/***/ }),

/***/ "./src/environments/environment.prod.ts":
/*!**********************************************!*\
  !*** ./src/environments/environment.prod.ts ***!
  \**********************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
var environment = {
    production: true,
    server: {
        host: 'http://localhost:3000/api/',
    }
};


/***/ })

}]);
//# sourceMappingURL=views-monte-carlo-monte-carlo-module.js.map