(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["views-probabilistic-probabilistic-module"],{

/***/ "./src/app/service/probabilistic.service.ts":
/*!**************************************************!*\
  !*** ./src/app/service/probabilistic.service.ts ***!
  \**************************************************/
/*! exports provided: ProbabilisticService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProbabilisticService", function() { return ProbabilisticService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../environments/environment.prod */ "./src/environments/environment.prod.ts");




var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_3__["environment"].server.host;
var httpOptions = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Content-Type': 'application/x-www-form-urlencoded'
    })
};
var header = {
    headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
        'Accept': 'application/json'
    })
};
var ProbabilisticService = /** @class */ (function () {
    function ProbabilisticService(httpClient) {
        this.httpClient = httpClient;
    }
    ProbabilisticService.prototype.getConfig = function () {
        // tslint:disable-next-line:max-line-length
        return this.httpClient.get('');
    };
    ProbabilisticService.prototype.getData = function (json) {
        console.log(json);
        return this.httpClient.post(host + 'probabilistics/users/test21/runSimulation', json, header);
    };
    ProbabilisticService.prototype.getInputData = function (userID) {
        return this.httpClient.get(host + 'probabilistics/users/test21/returnInputData');
    };
    ProbabilisticService.prototype.download = function (userID) {
        return this.httpClient.get(host + 'probabilistics/users/test21/download');
    };
    ProbabilisticService.prototype.saveSimulation = function (body) {
        return this.httpClient.post(host + 'probabilistics/users/test21/saveData', body, header);
    };
    ProbabilisticService.prototype.openSaveSimulation = function (userID) {
        return this.httpClient.get(host + 'probabilistics/users/test21/returnSaveData');
    };
    ProbabilisticService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ProbabilisticService);
    return ProbabilisticService;
}());



/***/ }),

/***/ "./src/app/views/probabilistic/probabilistic-routing.module.ts":
/*!*********************************************************************!*\
  !*** ./src/app/views/probabilistic/probabilistic-routing.module.ts ***!
  \*********************************************************************/
/*! exports provided: ProbabilisticRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProbabilisticRoutingModule", function() { return ProbabilisticRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _probabilistic_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./probabilistic.component */ "./src/app/views/probabilistic/probabilistic.component.ts");




var routes = [
    {
        path: '',
        component: _probabilistic_component__WEBPACK_IMPORTED_MODULE_3__["ProbabilisticComponent"],
        data: {
            title: 'Probabilistic'
        }
    }
];
var ProbabilisticRoutingModule = /** @class */ (function () {
    function ProbabilisticRoutingModule() {
    }
    ProbabilisticRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], ProbabilisticRoutingModule);
    return ProbabilisticRoutingModule;
}());



/***/ }),

/***/ "./src/app/views/probabilistic/probabilistic.component.html":
/*!******************************************************************!*\
  !*** ./src/app/views/probabilistic/probabilistic.component.html ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"animated fadeIn d-flex\">\r\n  <div class=\"d-flex w-100 flex-wrap\">\r\n    <!--Parameters-->\r\n    <div class=\"d-flex w-30 p-2 align-self-start flex-min-width\">\r\n      <div class=\"d-flex w-100 card card-accent-primary\">\r\n        <div class=\"card-header\">\r\n          SIMULATION AND MODEL DATA\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex flex-column w-100\">\r\n            <div class=\"d-flex w-100 pt-1 align-content-end\">\r\n              <div class=\"d-flex w-60\"> &nbsp; </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"saveSimulation()\">SAVE</button>\r\n              </div>\r\n              <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                  (click)=\"openSaveSimulation()\">OPEN</button>\r\n              </div>\r\n            </div>\r\n            <form [formGroup]=\"profileForm\" (ngSubmit)=\"onSubmit()\">\r\n              <div class=\"d-flex flex-column w-100\">\r\n                <div class=\"d-flex title-div pb-1\">\r\n                  Simulation paramaters\r\n                </div>\r\n                <div class=\"d-flex w-100 pt-2\">\r\n\r\n                  <div class=\"d-flex flex-column w-45\">\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-35 p-1\">\r\n                        <span style=\"vertical-align: middle\">Total time:</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-45 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.s}}\r\n                          formControlName=\"s\">\r\n                      </div>\r\n                      <div class=\"w-25 p-1\">\r\n                        <span style=\"vertical-align: middle\">[s]</span>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"d-flex w-100 pt-2\">\r\n                      <div class=\"d-flex w-35\"><span>Type of Simulation</span></div>\r\n                      <div class=\"d-flex flex-column w-65\">\r\n                        <div class=\"form-check\">\r\n                          <input name=\"simType\" class=\"form-check-input\" type=\"radio\" [value]=\"1\"\r\n                            [(ngModel)]=\"typeOfSimulation\" [ngModelOptions]=\"{standalone: true}\">\r\n                          <label class=\"form-check-label\" for=\"radio1\">Time Course</label>\r\n                        </div>\r\n                        <div class=\"form-check\">\r\n                          <input name=\"simType\" class=\"form-check-input\" type=\"radio\" [value]=\"2\"\r\n                            [(ngModel)]=\"typeOfSimulation\" [ngModelOptions]=\"{standalone: true}\">\r\n                          <label class=\"form-check-label\" for=\"radio3\">Titration</label>\r\n                        </div>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <div *ngIf=\"typeOfSimulation === 2\" class=\"d-flex flex-column w-55\">\r\n                    <div class=\"w-100 p-1\">\r\n                      <span style=\"vertical-align: middle\">&nbsp;</span>\r\n                    </div>\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Initial)\">\r\n                        <span style=\"vertical-align: middle\">Myosin Initial</span>\r\n                      </div>\r\n                      <div class=\"w-37 p-1\">\r\n                        <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                              formControlName=\"myosin\"-->\r\n                        <input class=\"w-100 text-align-right\" type=\"text\">\r\n                      </div>\r\n                      <div class=\"w-20 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-36 p-1\" tooltip=\"Molar Concentration of Myosin (Final)\">\r\n                        <span style=\"vertical-align: middle\">Myosin Final</span>\r\n                      </div>\r\n                      <div class=\"w-37 p-1\">\r\n                        <!--input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.myosin}}\r\n                              formControlName=\"myosin\"-->\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.final_myosin}}\r\n                          formControlName=\"final_myosin\">\r\n                      </div>\r\n                      <div class=\"w-20 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex title-div pb-1 pt-3\">\r\n                  Fixed model parameters\r\n                </div>\r\n\r\n                <div class=\"d-flex w-100 pt-2\">\r\n                  <!-- LEVA KOLONA-->\r\n                  <div class=\"d-flex flex-column w-50\">\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">Kb:</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-40 p-1 text-align-right\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kb_plus}}\r\n                          formControlName=\"kb_plus\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">Kt: </span>\r\n                      </div>\r\n                      <div class=\"d-flex w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_plus}}\r\n                          formControlName=\"kt_plus\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">K1+:</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k1_plus}}\r\n                          formControlName=\"k1_plus\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[1/(M*s)]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">K2+:</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k2_plus}}\r\n                          formControlName=\"k2_plus\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[1/s]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">Actin:</span>\r\n                      </div>\r\n                      <div class=\"d-flex w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\"\r\n                          ngModel={{this.inputData.molar_concentration_of_actin}}\r\n                          formControlName=\"molar_concentration_of_actin\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                  <!-- DESNA KOLONA-->\r\n                  <div class=\"d-flex flex-column w-50\">\r\n                    <!--div class=\"d-flex w-100\"> &nbsp;</div-->\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">Kb-:</span>\r\n                      </div>\r\n                      <div class=\"w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kb_minus}}\r\n                          formControlName=\"kb_minus\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">Kt-:</span>\r\n                      </div>\r\n                      <div class=\"w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.kt_minus}}\r\n                          formControlName=\"kt_minus\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[-]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">K1-:</span>\r\n                      </div>\r\n                      <div class=\"w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k1_minus}}\r\n                          formControlName=\"k1_minus\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[1/s]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">K2-:</span>\r\n                      </div>\r\n                      <div class=\"w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\" ngModel={{this.inputData.k2_minus}}\r\n                          formControlName=\"k2_minus\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[1/s]</span>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"d-flex w-100\">\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">Myosin:</span>\r\n                      </div>\r\n                      <div class=\"w-40 p-1\">\r\n                        <input class=\"w-100 text-align-right\" type=\"text\"\r\n                          ngModel={{this.inputData.molar_concentration_of_myosin}}\r\n                          formControlName=\"molar_concentration_of_myosin\">\r\n                      </div>\r\n                      <div class=\"w-30 p-1\">\r\n                        <span style=\"vertical-align: middle\">[uM]</span>\r\n                      </div>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n\r\n                <div class=\"d-flex w-100 pt-3 \">\r\n                  <div class=\"d-flex w-80\"> &nbsp; </div>\r\n                  <div class=\"d-flex w-20 pl-1 pr-1\">\r\n                    <button type=\"submit\" class=\"btn btn-sm btn-primary d-flex flex-fill btn-align\"\r\n                      [disabled]=\"!profileForm.valid\">RUN </button>\r\n                  </div>\r\n                </div>\r\n\r\n              </div>\r\n\r\n            </form>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"d-flex w-70 p-2\">\r\n      <div class=\"d-flex card card-accent-primary\">\r\n        <div class=\"card-header\">\r\n          RESULTS\r\n          <span class=\"float-right\" tooltip=\"Download graphic data\" placement=\"left\">\r\n            <i (click)=\"download()\" class=\"fa fa-cloud-download\" style=\"font-size:16px\"></i>\r\n          </span>\r\n        </div>\r\n        <div class=\"card-body d-flex w-100\">\r\n          <div class=\"d-flex w-100 flex-wrap justify-content-around align-content-start\">\r\n            <!--            <div *ngIf=\"!isVisible\" class=\"w-50 p-2 mw-350\" (click)=\"isVisible = !isVisible\"> -->\r\n            <div *ngFor=\"let graph of graphs\" class=\"w-50 p-2 mw-350\">\r\n              <div class=\"chart-wrapper\">\r\n                <!--style=\"height:300px;margin-top:40px;\"-->\r\n                <canvas baseChart class=\"chart\" [datasets]=\"graph.datasets\" [labels]=\"graph.labels\"\r\n                  [options]=\"graph.options\" [colors]=\"graph.colors\" [legend]=\"graph.legend\"\r\n                  [chartType]=\"graph.chartType\">\r\n                </canvas>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <!--div>\r\n      <table>\r\n        <thead>\r\n          <th *ngFor=\"let row of test[0]; let i = index\">\r\n            Col {{ i + 1 }}\r\n          </th>\r\n        </thead>\r\n        <tbody>\r\n          <tr *ngFor=\"let r of test\">\r\n            <td *ngFor=\"let c of r\"> Data {{ c }}</td>\r\n          </tr>\r\n        </tbody>\r\n      </table>\r\n      <button (click)=\"testFja()\">Click me</button>\r\n    </div-->\r\n  </div>\r\n</div>"

/***/ }),

/***/ "./src/app/views/probabilistic/probabilistic.component.scss":
/*!******************************************************************!*\
  !*** ./src/app/views/probabilistic/probabilistic.component.scss ***!
  \******************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".example-container {\n  display: flex;\n  flex-direction: column; }\n\ninput[type=number] {\n  -moz-appearance: textfield !important;\n       appearance: textfield !important;\n  -webkit-appearance: textfield !important;\n  margin-right: 2px !important;\n  text-align: right; }\n\ninput[_ngcontent-c1]::-webkit-outer-spin-button {\n  -webkit-appearance: texfield !important;\n  -moz-appearance: textfield !important;\n  appearance: none;\n  margin: 0; }\n\n.example-container > * {\n  width: 100%; }\n\n.example-container form {\n  margin-bottom: 20px; }\n\n.example-container form > * {\n  margin: 5px 0; }\n\n.example-form {\n  min-width: 150px;\n  max-width: 500px;\n  width: 100%; }\n\n.example-full-width {\n  width: 100%; }\n\n.table_form {\n  min-width: 550px;\n  padding: 0px; }\n\n.table_form p {\n  font-size: 10px;\n  text-align: right; }\n\n.table_form input {\n  height: 25px;\n  text-align: center;\n  max-width: 100px; }\n\n/* Customize the label (the container) */\n\n.container {\n  display: block;\n  position: relative;\n  padding-left: 35px;\n  margin-bottom: 12px;\n  cursor: pointer;\n  font-size: 22px;\n  -webkit-user-select: none;\n  -moz-user-select: none;\n  -ms-user-select: none;\n  user-select: none; }\n\n/* Hide the browser's default checkbox */\n\n.container input {\n  position: absolute;\n  opacity: 0;\n  cursor: pointer;\n  height: 0;\n  width: 0; }\n\n/* Create a custom checkbox */\n\n.checkmark {\n  position: absolute;\n  margin-top: 5px;\n  top: 0;\n  left: 0;\n  height: 20 px;\n  width: 20px;\n  background-color: #eee; }\n\n/* On mouse-over, add a grey background color */\n\n.container:hover input ~ .checkmark {\n  background-color: #ccc; }\n\n/* When the checkbox is checked, add a blue background */\n\n.container input:checked ~ .checkmark {\n  background-color: #2196F3; }\n\n.btn-align {\n  text-align: center !important; }\n\n/* Create the checkmark/indicator (hidden when not checked) */\n\n.checkmark:after {\n  content: \"\";\n  position: absolute;\n  display: none; }\n\n/* Show the checkmark when checked */\n\n.container input:checked ~ .checkmark:after {\n  display: block; }\n\n/* Style the checkmark/indicator */\n\n.container .checkmark:after {\n  left: 9px;\n  top: 5px;\n  width: 5px;\n  height: 10px;\n  border: solid white;\n  border-width: 0 3px 3px 0;\n  -webkit-transform: rotate(45deg);\n  transform: rotate(45deg); }\n\n.title-div-centar {\n  font-size: 1rem;\n  padding-top: 1rem;\n  padding-bottom: 1rem; }\n\n.title-div {\n  color: cadetblue;\n  font-style: 1rem;\n  border-bottom: 1px dashed lightblue; }\n\n.title-div-table {\n  font-size: 1rem;\n  color: cadetblue;\n  text-align: center !important; }\n\n.title-div-2 {\n  font-size: 1.2rem;\n  color: cadetblue; }\n\ninput {\n  background-color: #c3e4e5;\n  border: 1px solid \t#c3e4e5 !important;\n  padding-right: 2px !important; }\n\n.text-align-right {\n  text-align: right !important; }\n\n.text-align-center {\n  text-align: center; }\n\n.flex-min-width {\n  min-width: 470px; }\n\n.max-width-70posto {\n  max-width: 70%; }\n\n.over-auto {\n  overflow: visible; }\n\n.chart-wrapper {\n  border: 1px solid lightblue !important; }\n\n.mw-350 {\n  min-width: 700px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-30posto {\n  min-width: 370px;\n  height: -webkit-fit-content;\n  height: -moz-fit-content;\n  height: fit-content; }\n\n.mw-400 {\n  min-width: 280px; }\n\n.tbl-10 {\n  min-width: 300px !important; }\n\n.tbl-8kolona {\n  min-width: 300px !important; }\n\n.td-1 {\n  padding: 0.4rem !important; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvdmlld3MvcHJvYmFiaWxpc3RpYy9GOlxcdGhpbmZpbGFtZW50djMyMFxcbmV3X2NsaWVudC9zcmNcXGFwcFxcdmlld3NcXHByb2JhYmlsaXN0aWNcXHByb2JhYmlsaXN0aWMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxhQUFhO0VBQ2Isc0JBQXNCLEVBQUE7O0FBSXhCO0VBR0UscUNBQWdDO09BQWhDLGdDQUFnQztFQUNoQyx3Q0FBd0M7RUFDeEMsNEJBQTRCO0VBQzVCLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLHVDQUF1QztFQUN2QyxxQ0FBcUM7RUFDckMsZ0JBQWdCO0VBQ2hCLFNBQVMsRUFBQTs7QUFFWDtFQUNFLFdBQVcsRUFBQTs7QUFHYjtFQUNFLG1CQUFtQixFQUFBOztBQUdyQjtFQUNFLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsV0FBVyxFQUFBOztBQUdiO0VBQ0UsV0FBVyxFQUFBOztBQUdiO0VBQ0UsZ0JBQWdCO0VBQ2hCLFlBQVksRUFBQTs7QUFHZDtFQUNFLGVBQWU7RUFDZixpQkFBaUIsRUFBQTs7QUFHbkI7RUFDRSxZQUFZO0VBQ1osa0JBQWtCO0VBQ2xCLGdCQUFnQixFQUFBOztBQUlsQix3Q0FBQTs7QUFDQTtFQUNBLGNBQWM7RUFDZCxrQkFBa0I7RUFDbEIsa0JBQWtCO0VBQ2xCLG1CQUFtQjtFQUNuQixlQUFlO0VBQ2YsZUFBZTtFQUNmLHlCQUF5QjtFQUN6QixzQkFBc0I7RUFDdEIscUJBQXFCO0VBQ3JCLGlCQUFpQixFQUFBOztBQUdqQix3Q0FBQTs7QUFDQTtFQUNBLGtCQUFrQjtFQUNsQixVQUFVO0VBQ1YsZUFBZTtFQUNmLFNBQVM7RUFDVCxRQUFRLEVBQUE7O0FBR1IsNkJBQUE7O0FBQ0E7RUFDQSxrQkFBa0I7RUFDbEIsZUFBZTtFQUNmLE1BQU07RUFDTixPQUFPO0VBQ1AsYUFBYTtFQUNiLFdBQVc7RUFDWCxzQkFBc0IsRUFBQTs7QUFHdEIsK0NBQUE7O0FBQ0E7RUFDQSxzQkFBc0IsRUFBQTs7QUFHdEIsd0RBQUE7O0FBQ0E7RUFDQSx5QkFBeUIsRUFBQTs7QUFHekI7RUFDRSw2QkFBOEIsRUFBQTs7QUFHaEMsNkRBQUE7O0FBQ0E7RUFDQSxXQUFXO0VBQ1gsa0JBQWtCO0VBQ2xCLGFBQWEsRUFBQTs7QUFHYixvQ0FBQTs7QUFDQTtFQUNBLGNBQWMsRUFBQTs7QUFHZCxrQ0FBQTs7QUFDQTtFQUNBLFNBQVM7RUFDVCxRQUFRO0VBQ1IsVUFBVTtFQUNWLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIseUJBQXlCO0VBQ3pCLGdDQUFnQztFQUVoQyx3QkFBd0IsRUFBQTs7QUFHeEI7RUFFQSxlQUFlO0VBQ2YsaUJBQWlCO0VBQ2pCLG9CQUFvQixFQUFBOztBQUlwQjtFQUNBLGdCQUFnQjtFQUNoQixnQkFBZ0I7RUFDaEIsbUNBQW1DLEVBQUE7O0FBR25DO0VBQ0EsZUFBZTtFQUNmLGdCQUFnQjtFQUNoQiw2QkFBNkIsRUFBQTs7QUFHN0I7RUFDQSxpQkFBaUI7RUFDakIsZ0JBQWdCLEVBQUE7O0FBSWhCO0VBRUEseUJBQTBCO0VBQzFCLHFDQUFxQztFQUNyQyw2QkFBNkIsRUFBQTs7QUFHN0I7RUFDQSw0QkFBNEIsRUFBQTs7QUFJNUI7RUFDQSxrQkFBa0IsRUFBQTs7QUFJbEI7RUFDQSxnQkFBZ0IsRUFBQTs7QUFTaEI7RUFDQSxjQUFjLEVBQUE7O0FBRWQ7RUFDQSxpQkFBaUIsRUFBQTs7QUFHakI7RUFFQSxzQ0FBc0MsRUFBQTs7QUFHdEM7RUFDQSxnQkFBZ0I7RUFDaEIsMkJBQW1CO0VBQW5CLHdCQUFtQjtFQUFuQixtQkFBbUIsRUFBQTs7QUFHbkI7RUFDQSxnQkFBZ0I7RUFDaEIsMkJBQW1CO0VBQW5CLHdCQUFtQjtFQUFuQixtQkFBbUIsRUFBQTs7QUFLbkI7RUFDQSxnQkFBZ0IsRUFBQTs7QUFHaEI7RUFFQSwyQkFBMkIsRUFBQTs7QUFFM0I7RUFFQSwyQkFBMkIsRUFBQTs7QUFJM0I7RUFDQSwwQkFBMEIsRUFBQSIsImZpbGUiOiJzcmMvYXBwL3ZpZXdzL3Byb2JhYmlsaXN0aWMvcHJvYmFiaWxpc3RpYy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5leGFtcGxlLWNvbnRhaW5lciB7XHJcbiAgZGlzcGxheTogZmxleDtcclxuICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG5cclxuaW5wdXRbdHlwZT1udW1iZXJdIHsgXHJcbiAgLy8td2Via2l0LWFwcGVhcmFuY2U6IHRleGZpZWxkICFpbXBvcnRhbnQ7XHJcbiAvLyAtbW96LWFwcGVhcmFuY2U6IHRleHRmaWVsZCAhaW1wb3J0YW50O1xyXG4gIGFwcGVhcmFuY2U6IHRleHRmaWVsZCAhaW1wb3J0YW50O1xyXG4gIC13ZWJraXQtYXBwZWFyYW5jZTogdGV4dGZpZWxkICFpbXBvcnRhbnQ7XHJcbiAgbWFyZ2luLXJpZ2h0OiAycHggIWltcG9ydGFudDsgXHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuXHJcbmlucHV0W19uZ2NvbnRlbnQtYzFdOjotd2Via2l0LW91dGVyLXNwaW4tYnV0dG9uIHsgXHJcbiAgLXdlYmtpdC1hcHBlYXJhbmNlOiB0ZXhmaWVsZCAhaW1wb3J0YW50O1xyXG4gIC1tb3otYXBwZWFyYW5jZTogdGV4dGZpZWxkICFpbXBvcnRhbnQ7XHJcbiAgYXBwZWFyYW5jZTogbm9uZTtcclxuICBtYXJnaW46IDA7IFxyXG59XHJcbi5leGFtcGxlLWNvbnRhaW5lciA+ICoge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZXhhbXBsZS1jb250YWluZXIgZm9ybSB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMjBweDtcclxufVxyXG5cclxuLmV4YW1wbGUtY29udGFpbmVyIGZvcm0gPiAqIHtcclxuICBtYXJnaW46IDVweCAwO1xyXG59XHJcblxyXG4uZXhhbXBsZS1mb3JtIHtcclxuICBtaW4td2lkdGg6IDE1MHB4O1xyXG4gIG1heC13aWR0aDogNTAwcHg7XHJcbiAgd2lkdGg6IDEwMCU7XHJcbn1cclxuXHJcbi5leGFtcGxlLWZ1bGwtd2lkdGgge1xyXG4gIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4udGFibGVfZm9ybSB7XHJcbiAgbWluLXdpZHRoOiA1NTBweDtcclxuICBwYWRkaW5nOiAwcHg7XHJcbn1cclxuXHJcbi50YWJsZV9mb3JtIHAge1xyXG4gIGZvbnQtc2l6ZTogMTBweDtcclxuICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG5cclxuLnRhYmxlX2Zvcm0gaW5wdXQge1xyXG4gIGhlaWdodDogMjVweDtcclxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgbWF4LXdpZHRoOiAxMDBweDtcclxufVxyXG5cclxuXHJcbi8qIEN1c3RvbWl6ZSB0aGUgbGFiZWwgKHRoZSBjb250YWluZXIpICovXHJcbi5jb250YWluZXIge1xyXG5kaXNwbGF5OiBibG9jaztcclxucG9zaXRpb246IHJlbGF0aXZlO1xyXG5wYWRkaW5nLWxlZnQ6IDM1cHg7XHJcbm1hcmdpbi1ib3R0b206IDEycHg7XHJcbmN1cnNvcjogcG9pbnRlcjtcclxuZm9udC1zaXplOiAyMnB4O1xyXG4td2Via2l0LXVzZXItc2VsZWN0OiBub25lO1xyXG4tbW96LXVzZXItc2VsZWN0OiBub25lO1xyXG4tbXMtdXNlci1zZWxlY3Q6IG5vbmU7XHJcbnVzZXItc2VsZWN0OiBub25lO1xyXG59XHJcblxyXG4vKiBIaWRlIHRoZSBicm93c2VyJ3MgZGVmYXVsdCBjaGVja2JveCAqL1xyXG4uY29udGFpbmVyIGlucHV0IHtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG5vcGFjaXR5OiAwO1xyXG5jdXJzb3I6IHBvaW50ZXI7XHJcbmhlaWdodDogMDtcclxud2lkdGg6IDA7XHJcbn1cclxuXHJcbi8qIENyZWF0ZSBhIGN1c3RvbSBjaGVja2JveCAqL1xyXG4uY2hlY2ttYXJrIHtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG5tYXJnaW4tdG9wOiA1cHg7XHJcbnRvcDogMDtcclxubGVmdDogMDtcclxuaGVpZ2h0OiAyMCBweDtcclxud2lkdGg6IDIwcHg7XHJcbmJhY2tncm91bmQtY29sb3I6ICNlZWU7XHJcbn1cclxuXHJcbi8qIE9uIG1vdXNlLW92ZXIsIGFkZCBhIGdyZXkgYmFja2dyb3VuZCBjb2xvciAqL1xyXG4uY29udGFpbmVyOmhvdmVyIGlucHV0IH4gLmNoZWNrbWFyayB7XHJcbmJhY2tncm91bmQtY29sb3I6ICNjY2M7XHJcbn1cclxuXHJcbi8qIFdoZW4gdGhlIGNoZWNrYm94IGlzIGNoZWNrZWQsIGFkZCBhIGJsdWUgYmFja2dyb3VuZCAqL1xyXG4uY29udGFpbmVyIGlucHV0OmNoZWNrZWQgfiAuY2hlY2ttYXJrIHtcclxuYmFja2dyb3VuZC1jb2xvcjogIzIxOTZGMztcclxufVxyXG5cclxuLmJ0bi1hbGlnbiB7XHJcbiAgdGV4dC1hbGlnbjogIGNlbnRlciAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4vKiBDcmVhdGUgdGhlIGNoZWNrbWFyay9pbmRpY2F0b3IgKGhpZGRlbiB3aGVuIG5vdCBjaGVja2VkKSAqL1xyXG4uY2hlY2ttYXJrOmFmdGVyIHtcclxuY29udGVudDogXCJcIjtcclxucG9zaXRpb246IGFic29sdXRlO1xyXG5kaXNwbGF5OiBub25lO1xyXG59XHJcblxyXG4vKiBTaG93IHRoZSBjaGVja21hcmsgd2hlbiBjaGVja2VkICovXHJcbi5jb250YWluZXIgaW5wdXQ6Y2hlY2tlZCB+IC5jaGVja21hcms6YWZ0ZXIge1xyXG5kaXNwbGF5OiBibG9jaztcclxufVxyXG5cclxuLyogU3R5bGUgdGhlIGNoZWNrbWFyay9pbmRpY2F0b3IgKi9cclxuLmNvbnRhaW5lciAuY2hlY2ttYXJrOmFmdGVyIHtcclxubGVmdDogOXB4O1xyXG50b3A6IDVweDtcclxud2lkdGg6IDVweDtcclxuaGVpZ2h0OiAxMHB4O1xyXG5ib3JkZXI6IHNvbGlkIHdoaXRlO1xyXG5ib3JkZXItd2lkdGg6IDAgM3B4IDNweCAwO1xyXG4td2Via2l0LXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxuLW1zLXRyYW5zZm9ybTogcm90YXRlKDQ1ZGVnKTtcclxudHJhbnNmb3JtOiByb3RhdGUoNDVkZWcpO1xyXG59XHJcblxyXG4udGl0bGUtZGl2LWNlbnRhciB7XHJcbi8vY29sb3I6IGNhZGV0Ymx1ZTtcclxuZm9udC1zaXplOiAxcmVtO1xyXG5wYWRkaW5nLXRvcDogMXJlbTtcclxucGFkZGluZy1ib3R0b206IDFyZW07XHJcbi8vYm9yZGVyLWJvdHRvbTogMXB4IGRhc2hlZCBsaWdodGJsdWU7XHJcbn1cclxuXHJcbi50aXRsZS1kaXYge1xyXG5jb2xvcjogY2FkZXRibHVlO1xyXG5mb250LXN0eWxlOiAxcmVtO1xyXG5ib3JkZXItYm90dG9tOiAxcHggZGFzaGVkIGxpZ2h0Ymx1ZTtcclxufVxyXG5cclxuLnRpdGxlLWRpdi10YWJsZSB7XHJcbmZvbnQtc2l6ZTogMXJlbTsgXHJcbmNvbG9yOiBjYWRldGJsdWU7XHJcbnRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGl0bGUtZGl2LTIge1xyXG5mb250LXNpemU6IDEuMnJlbTsgXHJcbmNvbG9yOiBjYWRldGJsdWU7XHJcbi8vIHRleHQtYWxpZ246IGNlbnRlciAhaW1wb3J0YW50O1xyXG59XHJcblxyXG5pbnB1dCB7XHJcbi8vYm9yZGVyOiBub25lO1xyXG5iYWNrZ3JvdW5kLWNvbG9yOiBcdCNjM2U0ZTU7XHJcbmJvcmRlcjogMXB4IHNvbGlkIFx0I2MzZTRlNSAhaW1wb3J0YW50O1xyXG5wYWRkaW5nLXJpZ2h0OiAycHggIWltcG9ydGFudDtcclxufVxyXG5cclxuLnRleHQtYWxpZ24tcmlnaHQge1xyXG50ZXh0LWFsaWduOiByaWdodCAhaW1wb3J0YW50O1xyXG4vL3BhZGRpbmctcmlnaHQ6IDJweCAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4udGV4dC1hbGlnbi1jZW50ZXIge1xyXG50ZXh0LWFsaWduOiBjZW50ZXI7XHJcblxyXG59XHJcblxyXG4uZmxleC1taW4td2lkdGgge1xyXG5taW4td2lkdGg6IDQ3MHB4O1xyXG4vLyAgbWF4LXdpZHRoOiA2MDBweDtcclxuLy96LWluZGV4OiAxMDAwO1xyXG59XHJcblxyXG4uZmxleC1taW4td2lkdGggKiB7XHJcbi8vei1pbmRleDogMTAwMDtcclxufVxyXG5cclxuLm1heC13aWR0aC03MHBvc3RvIHtcclxubWF4LXdpZHRoOiA3MCU7XHJcbn1cclxuLm92ZXItYXV0byB7XHJcbm92ZXJmbG93OiB2aXNpYmxlO1xyXG59XHJcblxyXG4uY2hhcnQtd3JhcHBlciB7XHJcbi8vYm9yZGVyLXRvcC1zdHlsZTogZ3Jvb3ZlICFpbXBvcnRhbnQ7XHJcbmJvcmRlcjogMXB4IHNvbGlkIGxpZ2h0Ymx1ZSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4ubXctMzUwIHtcclxubWluLXdpZHRoOiA3MDBweDtcclxuaGVpZ2h0OiBmaXQtY29udGVudDtcclxufVxyXG5cclxuLm13LTMwcG9zdG8ge1xyXG5taW4td2lkdGg6IDM3MHB4O1xyXG5oZWlnaHQ6IGZpdC1jb250ZW50O1xyXG4vL2ZvbnQtc2l6ZTogMC41cmVtICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcblxyXG4ubXctNDAwIHtcclxubWluLXdpZHRoOiAyODBweDtcclxufVxyXG5cclxuLnRibC0xMCB7XHJcbi8vIG1hcmdpbjogMTBweCAhaW1wb3J0YW50O1xyXG5taW4td2lkdGg6IDMwMHB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuLnRibC04a29sb25hIHtcclxuLy8gbWFyZ2luOiAxMHB4ICFpbXBvcnRhbnQ7XHJcbm1pbi13aWR0aDogMzAwcHggIWltcG9ydGFudDtcclxufVxyXG5cclxuXHJcbi50ZC0xe1xyXG5wYWRkaW5nOiAwLjRyZW0gIWltcG9ydGFudDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./src/app/views/probabilistic/probabilistic.component.ts":
/*!****************************************************************!*\
  !*** ./src/app/views/probabilistic/probabilistic.component.ts ***!
  \****************************************************************/
/*! exports provided: ProbabilisticComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProbabilisticComponent", function() { return ProbabilisticComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @coreui/coreui-plugin-chartjs-custom-tooltips */ "./node_modules/@coreui/coreui-plugin-chartjs-custom-tooltips/dist/umd/custom-tooltips.js");
/* harmony import */ var _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(_coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _service_probabilistic_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../service/probabilistic.service */ "./src/app/service/probabilistic.service.ts");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_6___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_6__);
/* harmony import */ var _environments_environment_prod__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../../environments/environment.prod */ "./src/environments/environment.prod.ts");








var host = _environments_environment_prod__WEBPACK_IMPORTED_MODULE_7__["environment"].server.host;
var ProbabilisticComponent = /** @class */ (function () {
    // displayedColumns: string[] = ['col1', 'col2', 'col3', 'col4', 'col5', 'col6', 'col7', 'col8'];
    function ProbabilisticComponent(fb, probabilisticService) {
        this.fb = fb;
        this.probabilisticService = probabilisticService;
        this.profileForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            s: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_plus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kb_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_plus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            kt_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_plus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k1_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_plus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            k2_minus: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            molar_concentration_of_actin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            molar_concentration_of_myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            final_myosin: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](''),
            type_of_simulation: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"]('')
        });
        this.numberOfGraphs = 1;
        this.graphs = [];
        this.inputData = this.profileForm;
        this.typeOfSimulation = 1;
        this.test = [
            [1, 2, 3, 4],
            [1, 2, 3, 4],
            [1, 6, 3, 4],
            [5, 2, 3, 4],
            [1, 2, 3, 4],
        ];
    }
    ProbabilisticComponent.prototype.testFja = function () {
        this.test = [
            [1, 2, 3, 4, 4, 5],
            [1, 2, 3, 4, 5, 3]
        ];
    };
    ProbabilisticComponent.prototype.ngOnInit = function () {
        var _this = this;
        var titles = [
            'Fractional Fluorescence',
            'Fraction Saturation',
            'Fraction of Unbound Actin',
            'Free Myosin Concentration'
        ];
        var labels = [
            { xAxes: 'Time [s]', yAxes: 'Unit X1' },
            { xAxes: 'Time [s]', yAxes: 'Unit X2' },
            { xAxes: 'Time [s]', yAxes: 'Unit X3' },
            { xAxes: 'Time [s]', yAxes: 'Unit X4' }
        ];
        var colors = [
            [{ borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' }]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i]));
        }
        console.log(this.graphs);
        this.probabilisticService.getInputData('test21').subscribe(function (data) {
            if (data) {
                _this.inputData = data;
            }
        });
    };
    ProbabilisticComponent.prototype.prepareGraphs = function (graphLines) {
        var titles = [
            'Fractional Fluorescence',
            'Fraction Saturation',
            'Fraction of Unbound Actin',
            'Free Myosin Concentration'
        ];
        var labels = [
            { xAxes: 'Time [s]', yAxes: 'Unit X1' },
            { xAxes: 'Time [s]', yAxes: 'Unit X2' },
            { xAxes: 'Time [s]', yAxes: 'Unit X3' },
            { xAxes: 'Time [s]', yAxes: 'Unit X4' }
        ];
        var colors = [
            [{ borderColor: 'blue', backgroundColor: 'transparent' },
                { borderColor: 'red', backgroundColor: 'transparent' },
                { borderColor: 'green', backgroundColor: 'transparent' },
                { borderColor: 'purple', backgroundColor: 'transparent' }]
        ];
        for (var i = 0; i < this.numberOfGraphs; i++) {
            this.graphs.push(this.initGraph(titles[i], labels[i], colors[i]));
        }
    };
    ProbabilisticComponent.prototype.initGraph = function (newTitle, newLabels, newColors) {
        var basicGraph = {};
        basicGraph.options = {
            title: {
                display: true,
                text: 'Error',
                // fontColor: 'blue',
                fontStyle: 'small-caps',
            },
            tooltips: {
                enabled: false,
                custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                intersect: true,
                mode: 'index',
                position: 'nearest',
                callbacks: {
                    labelColor: function (tooltipItem, chart) {
                        return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                    }
                }
            },
            layout: {
                padding: {
                    left: 10,
                    right: 15,
                    top: 0,
                    bottom: 5
                }
            },
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                        type: 'linear',
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            padding: 5,
                            beginAtZero: true,
                            stepSize: 0.5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC1 (%)',
                            fontSize: 11
                        }
                    }],
                yAxes: [{
                        gridLines: {
                            drawOnChartArea: true,
                            color: 'rgba(171,171,171,1)',
                            zeroLineWidth: 0.5,
                            zeroLineColor: 'black',
                            lineWidth: 0.1,
                            tickMarkLength: 5
                        },
                        ticks: {
                            beginAtZero: true,
                            padding: 5,
                            fontSize: 12
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'PC2 (%)',
                            fontSize: 11
                        }
                    }]
            },
            elements: {
                line: {
                    borderWidth: 2
                },
                point: {
                    radius: 0,
                    hitRadius: 10,
                    hoverRadius: 4,
                    hoverBorderWidth: 3,
                }
            },
            legend: {
                display: false
            }
        };
        var newOptions = basicGraph.options;
        newOptions.title.text = newTitle;
        newOptions.scales.xAxes[0].scaleLabel.labelString = newLabels.xAxes;
        newOptions.scales.yAxes[0].scaleLabel.labelString = newLabels.yAxes;
        basicGraph.options = newOptions;
        basicGraph.colors = newColors;
        basicGraph.datasets = [{
                data: []
            }];
        basicGraph.chartType = 'line';
        basicGraph.labels = [];
        return basicGraph;
    };
    ProbabilisticComponent.prototype.onSubmit = function () {
        var _this = this;
        var body = this.profileForm.value;
        body.type_of_simulation = 'titration';
        this.probabilisticService.getData(body).subscribe(function (data) {
            console.log('podaci', data);
            _this.porbabilistic = data;
            _this.histogramData = data.histogram;
            var datasets = [
                [{ data: data.fractional_fluorescence },
                    { data: data.fraction_saturation },
                    { data: data.fraction_of_unbound_actin },
                    { data: data.free_myosin_concentration }]
            ];
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
            }
            _this.graphs[0].options = {
                title: {
                    display: true,
                    text: 'Error',
                    // fontColor: 'blue',
                    fontStyle: 'small-caps',
                },
                tooltips: {
                    enabled: false,
                    custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                    intersect: true,
                    mode: 'index',
                    position: 'nearest',
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                        }
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 15,
                        top: 0,
                        bottom: 5
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                            type: 'linear',
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                padding: 5,
                                beginAtZero: true,
                                stepSize: 0.5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC1 (%)',
                                fontSize: 11
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                beginAtZero: true,
                                padding: 5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC555 (%)',
                                fontSize: 11
                            }
                        }]
                },
                elements: {
                    line: {
                        borderWidth: 2
                    },
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4,
                        hoverBorderWidth: 3,
                    }
                },
                legend: {
                    display: false
                }
            };
            _this.displayedColumns = ['col1', 'col2', 'col3', 'col4', 'col5', 'col6', 'col7', 'col8'];
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](_this.histogramData);
        });
    };
    ProbabilisticComponent.prototype.download = function () {
        window.open(host + 'probabilistics/users/test21/download');
    };
    ProbabilisticComponent.prototype.saveSimulation = function () {
        var body = this.profileForm.value;
        this.probabilisticService.saveSimulation(body).subscribe(function (data) {
            console.log(data);
        });
    };
    ProbabilisticComponent.prototype.openSaveSimulation = function () {
        var _this = this;
        this.probabilisticService.openSaveSimulation('test21').subscribe(function (data) {
            console.log('podaci', data);
            _this.porbabilistic = data;
            _this.histogramData = data.inputData.histogram;
            _this.inputData = data.parameters;
            var datasets = [
                [{ data: data.inputData.fractional_fluorescence },
                    { data: data.inputData.fraction_saturation },
                    { data: data.inputData.fraction_of_unbound_actin },
                    { data: data.inputData.free_myosin_concentration }]
            ];
            _this.graphs = [];
            _this.prepareGraphs([datasets[0].length]);
            for (var i = 0; i < _this.numberOfGraphs; i++) {
                _this.graphs[i].datasets = datasets[i];
            }
            _this.graphs[0].options = {
                title: {
                    display: true,
                    text: 'Error',
                    // fontColor: 'blue',
                    fontStyle: 'small-caps',
                },
                tooltips: {
                    enabled: false,
                    custom: _coreui_coreui_plugin_chartjs_custom_tooltips__WEBPACK_IMPORTED_MODULE_2__["CustomTooltips"],
                    intersect: true,
                    mode: 'index',
                    position: 'nearest',
                    callbacks: {
                        labelColor: function (tooltipItem, chart) {
                            return { backgroundColor: chart.data.datasets[tooltipItem.datasetIndex].borderColor };
                        }
                    }
                },
                layout: {
                    padding: {
                        left: 10,
                        right: 15,
                        top: 0,
                        bottom: 5
                    }
                },
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                            type: 'linear',
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                padding: 5,
                                beginAtZero: true,
                                stepSize: 0.5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC1 (%)',
                                fontSize: 11
                            }
                        }],
                    yAxes: [{
                            gridLines: {
                                drawOnChartArea: true,
                                color: 'rgba(171,171,171,1)',
                                zeroLineWidth: 0.5,
                                zeroLineColor: 'black',
                                lineWidth: 0.1,
                                tickMarkLength: 5
                            },
                            ticks: {
                                beginAtZero: true,
                                padding: 5,
                                fontSize: 12
                            },
                            scaleLabel: {
                                display: true,
                                labelString: 'PC555 (%)',
                                fontSize: 11
                            }
                        }]
                },
                elements: {
                    line: {
                        borderWidth: 2
                    },
                    point: {
                        radius: 0,
                        hitRadius: 10,
                        hoverRadius: 4,
                        hoverBorderWidth: 3,
                    }
                },
                legend: {
                    display: false
                }
            };
            _this.displayedColumns = ['col1', 'col2', 'col3', 'col4', 'col5', 'col6', 'col7', 'col8'];
            _this.dataSource = new _angular_material__WEBPACK_IMPORTED_MODULE_5__["MatTableDataSource"](_this.histogramData);
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["ViewChild"])(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_6__["BaseChartDirective"]),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["QueryList"])
    ], ProbabilisticComponent.prototype, "charts", void 0);
    ProbabilisticComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            template: __webpack_require__(/*! ./probabilistic.component.html */ "./src/app/views/probabilistic/probabilistic.component.html"),
            styles: [__webpack_require__(/*! ./probabilistic.component.scss */ "./src/app/views/probabilistic/probabilistic.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormBuilder"], _service_probabilistic_service__WEBPACK_IMPORTED_MODULE_4__["ProbabilisticService"]])
    ], ProbabilisticComponent);
    return ProbabilisticComponent;
}());



/***/ }),

/***/ "./src/app/views/probabilistic/probabilistic.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/views/probabilistic/probabilistic.module.ts ***!
  \*************************************************************/
/*! exports provided: ProbabilisticModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProbabilisticModule", function() { return ProbabilisticModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ng2-charts/ng2-charts */ "./node_modules/ng2-charts/ng2-charts.js");
/* harmony import */ var ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ngx-bootstrap/dropdown */ "./node_modules/ngx-bootstrap/dropdown/fesm5/ngx-bootstrap-dropdown.js");
/* harmony import */ var ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ngx-bootstrap/buttons */ "./node_modules/ngx-bootstrap/buttons/fesm5/ngx-bootstrap-buttons.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _probabilistic_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./probabilistic.component */ "./src/app/views/probabilistic/probabilistic.component.ts");
/* harmony import */ var _probabilistic_routing_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./probabilistic-routing.module */ "./src/app/views/probabilistic/probabilistic-routing.module.ts");
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/table */ "./node_modules/@angular/material/esm5/table.es5.js");
/* harmony import */ var _angular_material__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material */ "./node_modules/@angular/material/esm5/material.es5.js");
/* harmony import */ var ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ngx-bootstrap/tooltip */ "./node_modules/ngx-bootstrap/tooltip/fesm5/ngx-bootstrap-tooltip.js");













var ProbabilisticModule = /** @class */ (function () {
    function ProbabilisticModule() {
    }
    ProbabilisticModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormsModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_6__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_2__["ReactiveFormsModule"],
                _probabilistic_routing_module__WEBPACK_IMPORTED_MODULE_8__["ProbabilisticRoutingModule"],
                ng2_charts_ng2_charts__WEBPACK_IMPORTED_MODULE_3__["ChartsModule"],
                ngx_bootstrap_dropdown__WEBPACK_IMPORTED_MODULE_4__["BsDropdownModule"],
                ngx_bootstrap_buttons__WEBPACK_IMPORTED_MODULE_5__["ButtonsModule"].forRoot(),
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"],
                ngx_bootstrap_tooltip__WEBPACK_IMPORTED_MODULE_11__["TooltipModule"].forRoot()
            ],
            declarations: [_probabilistic_component__WEBPACK_IMPORTED_MODULE_7__["ProbabilisticComponent"]],
            exports: [
                _angular_material_table__WEBPACK_IMPORTED_MODULE_9__["MatTableModule"],
                _angular_material__WEBPACK_IMPORTED_MODULE_10__["MatPaginatorModule"]
            ]
        })
    ], ProbabilisticModule);
    return ProbabilisticModule;
}());



/***/ })

}]);
//# sourceMappingURL=views-probabilistic-probabilistic-module.js.map