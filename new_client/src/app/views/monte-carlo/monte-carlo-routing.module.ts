import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MonteCarloComponent } from './monte-carlo.component';

const routes: Routes = [
  {
    path: '',
    component: MonteCarloComponent,
    data: {
      title: 'Monte Carlo'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MonteCarloRoutingModule {}
