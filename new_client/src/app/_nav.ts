// interface NavAttributes {
//   [propName: string]: any;
// }
// interface NavWrapper {
//   attributes: NavAttributes;
//   element: string;
// }
// interface NavBadge {
//   text: string;
//   variant: string;
// }
// interface NavLabel {
//   class?: string;
//   variant: string;
// }
//
// export interface NavData {
//   name?: string;
//   url?: string;
//   icon?: string;
//   badge?: NavBadge;
//   title?: boolean;
//   children?: NavData[];
//   variant?: string;
//   attributes?: NavAttributes;
//   divider?: boolean;
//   class?: string;
//   label?: NavLabel;
//   wrapper?: NavWrapper;
// }

export interface NavData {
  name?: string;
  url?: string;
  icon?: string;
  badge?: any;
  title?: boolean;
  children?: any;
  variant?: string;
  attributes?: object;
  divider?: boolean;
  class?: string;
}

export const navItems: NavData[] = [
  {
    name: 'Home',
    url: '/home',
    icon: 'icon-home'
  },
  {
    name: 'Models',
    url: '/models',
    icon: 'icon-graph',
    children: [
      {
        name: 'MG Probabilistic',
        url: '/probabilistic',
      },
      {
        name: 'MG Stochastic',
        url: '/stochastical',
      },
      {
        name: 'Hill Stochastic',
        url: '/hill-stochastical',
      },
      {
        name: 'Hill Probabilistic',
        url: '/hill-probabilistic',
      },
      {
        name: 'MG Probabilistic DLS',
        url: '/mg-probabilistic',
      },
      {
        name: 'MG Stochastic DLS',
        url: '/monte-carlo',
      },
      {
        name: 'Hill Stochastic DLS',
        url: '/hill-monte-carlo',
      },
      {
        name: 'MG Probablistic SIMANN',
        url: '/simulated-annealing',
      },
      {
        name: 'MG Probablistic Quasi-Newton',
        url: '/quasi-newton',
      },
      {
        name: 'David DLS',
        url: '/david',
      },
      {
        name: 'ATPase DLS',
        url: '/widgets',
      }
    ]
  },
  {
    name: 'Archive',
    url: '/archive',
    icon: 'icon-layers'
  },
  {
    name: 'About',
    url: '/about',
    icon: 'icon-info'
  }
];
