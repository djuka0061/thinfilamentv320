import { Component, OnInit } from '@angular/core';
import { ProbabilisticService } from '../service/probabilistic.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private probabilisticService: ProbabilisticService) { }
  public test = "test"
  ngOnInit() {
    this.probabilisticService.getConfig();
  }
  imeFunkcije() {
    debugger
  }
}
