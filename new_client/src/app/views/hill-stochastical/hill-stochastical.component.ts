import { Component, OnInit, QueryList, ViewChild, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl } from '@angular/forms';
import { HillStochasticalService } from '../../service/hill_stochastical.service'
import { environment } from '../../../environments/environment.prod';
import { Chart } from 'chart.js'

const host = environment.server.host;

export interface Graph {
  datasets: Array<any>;
  options: any;
  chartType: string;
  colors: Array<any>;
  legend: boolean;
  labels: Array<any>;
  visibility: Boolean;
}

@Component({
  selector: 'app-hill-stochastical',
  templateUrl: './hill-stochastical.component.html',
  styleUrls: ['./hill-stochastical.component.scss']
})
export class HillStochasticalComponent implements OnInit, AfterViewInit {
  angForm: FormGroup;
  profileForm = new FormGroup({
    s: new FormControl(''),
    alpha_zero: new FormControl(''),
    beta_zero: new FormControl(''),
    constant_y1: new FormControl(''),
    constant_y2: new FormControl(''),
    constant_k1: new FormControl(''),
    constant_k1_prim: new FormControl(''),
    constant_k2: new FormControl(''),
    constant_k2_prim: new FormControl(''),
    parameter_gama: new FormControl(''),
    parameter_delta: new FormControl(''),
    actin: new FormControl(''),
    myosin: new FormControl(''),
    final_const_of_myosin: new FormControl(''),
    number_of_filaments: new FormControl(''),
    number_of_tm_tn_units: new FormControl(''),
    number_of_actin_per_unit: new FormControl(''),
    cooperativity_on: new FormControl('')
  });

  constructor(private fb: FormBuilder, private HillStochasticalService: HillStochasticalService) { }

  public stochastical: any;
  public inputData = this.profileForm;
  public displayedColumns: string[];
  public typeOfSimulation: Number = 1;
  public run_button = "RUN";
  public isVisible = false;
  public dugmeRun: Boolean = false;

  public graphData

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.HillStochasticalService.getInputData('test21').subscribe(data => {
      if (data) {
        this.inputData = data;
      }
    });
    this.graphData = new Chart('graphData', {})
  }

  onSubmit() {
    const body = this.profileForm.value;
    this.isVisible = false;
    this.run_button = "RUNNING";
    this.dugmeRun = true;
    this.graphData.destroy()
    this.HillStochasticalService.getData(body).subscribe(data => {
      this.stochastical = data;

      const datasets = [
        {
          data: data.fractional_fluorescence,
          label: 'Fractional Fluorescence',
          fill: false,
          lineTension: 0.2,
          borderColor: 'blue',
          borderWidth: 1,
        },
        {
          data: data.fraction_saturation,
          label: 'Fraction Saturation',
          fill: false,
          lineTension: 0.2,
          borderColor: 'red',
          borderWidth: 1,
        },
        {
          data: data.fraction_of_unbound_actin,
          label: 'Fraction of Unbound Actin',
          fill: false,
          lineTension: 0.2,
          borderColor: 'green',
          borderWidth: 1,
        },
        {
          data: data.free_myosin_concentration,
          label: 'Free Myosin Concentration',
          fill: false,
          lineTension: 0.2,
          borderColor: 'orange',
          borderWidth: 1,
        }
      ];

      this.parameterForGraph(datasets)

      this.isVisible = true;
      this.dugmeRun = false;
      this.run_button = "RUN";

    });
  }

  download() {
    window.open(host + 'hill_stochasticals/users/test21/download');
  }
  saveSimulation() {
    const body = this.profileForm.value;
    this.HillStochasticalService.saveSimulation(body).subscribe(data => {
      console.log(data);
    })
  }

  openSaveSimulation(file) {
    this.graphData.destroy()
    const fileData = file.target.files
    var reader = new FileReader();
    reader.readAsText(fileData[0]);
    reader.onload = (_event) => {
      const data = JSON.parse(reader.result.toString());
      console.log('podaci', data);
      this.stochastical = data;

      if (data.fractional_fluorescence && data.fraction_saturation && data.fraction_of_unbound_actin && data.free_myosin_concentration) {
        const datasets = [
          {
            data: data.fractional_fluorescence,
            label: 'Fractional Fluorescence',
            fill: false,
            lineTension: 0.2,
            borderColor: 'blue',
            borderWidth: 1,
          },
          {
            data: data.fraction_saturation,
            label: 'Fraction Saturation',
            fill: false,
            lineTension: 0.2,
            borderColor: 'red',
            borderWidth: 1,
          },
          {
            data: data.fraction_of_unbound_actin,
            label: 'Fraction of Unbound Actin',
            fill: false,
            lineTension: 0.2,
            borderColor: 'green',
            borderWidth: 1,
          },
          {
            data: data.free_myosin_concentration,
            label: 'Free Myosin Concentration',
            fill: false,
            lineTension: 0.2,
            borderColor: 'orange',
            borderWidth: 1,
          }
        ];

        this.parameterForGraph(datasets)
      }
    };
  }

  onCheckboxChange(box1: string, box2: string) {
    if (this.inputData[box1] != undefined && this.inputData[box2] != undefined) {
      if (this.inputData[box1])
        this.inputData[box2] = false;
    }
  }

  parameterForGraph(datasets: any) {
    this.graphData = new Chart('graphData', {
      type: 'line',
      data: {
        datasets: datasets
      },
      options: {
        elements: {
          point: {
            radius: 0
          }
        },
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom',
            scaleLabel: {
              display: true,
              labelString: 'Iteration'
            }
          }], yAxes: [{
            ticks: {
              callback: function (value) {
                return value.toExponential(2)
              }
            }
          }]
        }
      }
    })
  }
}
