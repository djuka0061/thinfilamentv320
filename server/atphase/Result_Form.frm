VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form Result_Form 
   AutoRedraw      =   -1  'True
   Caption         =   "Results"
   ClientHeight    =   7740
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9915
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   12
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   ScaleHeight     =   7740
   ScaleWidth      =   9915
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command1 
      Caption         =   "Graph"
      Height          =   495
      Left            =   2760
      TabIndex        =   2
      Top             =   6240
      Width           =   1215
   End
   Begin VB.CommandButton PrintComm 
      Caption         =   "Print"
      Height          =   495
      Left            =   4080
      TabIndex        =   1
      Top             =   6240
      Width           =   1095
   End
   Begin MSComDlg.CommonDialog opencd1 
      Left            =   8520
      Top             =   1200
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.CommandButton CloseB 
      Caption         =   "Close"
      Height          =   495
      Left            =   5400
      TabIndex        =   0
      Top             =   6240
      Width           =   975
   End
End
Attribute VB_Name = "Result_Form"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Base 1

Private Sub Command1_Click()
    QNGraphs_Form.Show
    Result_Form.Hide
End Sub

Private Sub CloseB_Click()
    Result_Form.Hide
    Main_Form.Show
End Sub



Private Sub Form_Activate()
    Call Form_Load
End Sub



Private Sub Form_Load()

    Dim File_name, citem(9) As String
    Dim FileNumber, ip As Integer
    Dim Paras(20) As Double
    Dim position(4), real_vs As Integer
    
    
    citem(1) = "Kb: "
    citem(2) = "kb-: "
    citem(3) = "Kt: "
    citem(4) = "kt-: "
    citem(5) = "k1+(e6):"
    citem(6) = "k1-: "
    citem(7) = "k2+: "
    citem(8) = "k2-: "
    
    
    ChDrive App.path
    ChDir App.path
    
    Cls
    FileNumber = 10
    ip = 1
    
    File_name = "ps.dat"
    Open File_name For Input As #FileNumber
    Input #FileNumber, real_vs
    For ip = 1 To real_vs
            Input #FileNumber, position(ip)
    Next ip
    Close #FileNumber
    
    
    ip = 1
    File_name = QNewton_Form.Text20.Text
    'Print App.path
    Open File_name For Input As #FileNumber
    Do While Not EOF(FileNumber)
       Input #FileNumber, Paras(ip)
       ip = ip + 1
    Loop
    Close #FileNumber
    
    Print "Parameters Estimated: "
    Print
    For i = 1 To real_vs
        Print citem(position(i)), Paras(position(i))
    Next i
    
       
    
    Print
    Print "Error is: ", Paras(5)
    Print
    
    
    
End Sub


Private Sub PrintComm_Click()
    Result_Form.PrintForm
End Sub
