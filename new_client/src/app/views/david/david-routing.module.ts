import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DavidComponent } from './david.component';

const routes: Routes = [
  {
    path: '',
    component: DavidComponent,
    data: {
      title: 'David'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DavidRoutingModule {}
